/*
 * Copyright (c) 2018-2020, The Linux Foundation. All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above
 *       copyright notice, this list of conditions and the following
 *       disclaimer in the documentation and/or other materials provided
 *       with the distribution.
 *     * Neither the name of The Linux Foundation nor the names of its
 *       contributors may be used to endorse or promote products derived
 *       from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS
 * BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
 * BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
 * IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef _MEDIAPLAYERLIB__H
#define _MEDIAPLAYERLIB__H

#include <chrono>
#include <functional>
#include <map>
#include <memory>
#include <string>
#include <tuple>
#include "mediaplayer_properties.h"
/** \mainpage
 *
 * \section intro Introduction
 *
 * The ADK MediaPlayer Library provides a simple interface for the playback of
 * streams on the ADK platform. Mechanisms are provided for playing from a URL
 * (which can be a local file, or a network resource), as well as for
 * application developers to provide buffers with raw or encoded data.
 *
 * The library hides away details of specific codec implementations and
 * underlying devices. The most optimal output for the current system scenario
 * is automatically selected.
 *
 * In addition to this, integration with other system components for features
 * such as metadata display, media controls (next/previous/play/pause) etc. are
 * also automatically taken care of.
 */

namespace adk {
/** \cond */
class MediaPlayerPrivate;
/** \endccond */

class MediaPlayer {
 public:
    /** The return status of a possibly asynchronous operation. */
    enum Result {
        /**< The operation succeeded. */
        Success,
        /**< The operation failed. */
        Failure,
        /**< The operation will complete asynchronously, and the status will be
         * known via the async done callback (see SetOnAsyncDoneCb()) or error
         * callback (seet SetOnErrorCb()). */
        Async
    };

    /** The type of media being played back */
    enum StreamType {
        Music,
        Cue,
        Tone,
        Alarm,
        VoiceUI,
        Call,
        ControlOnly
    };

    /** The current state of the media player */
    enum class State {
        Playing,
        Paused,
        Stopped
    };

    /** The tags used in metadata. */
    enum class Tag {
        Album,
        Artist,
        AudioBitrate,
        CoverArt,
        Duration,
        Genre,
        Title,
        Url,
        Channels,
        SampleFormat,
        SampleRate
    };

    enum class LoopMode {
        None,
        Track,
        Playlist
    };

    /** The error type for error callbacks. Not currently useful, but might be
     * used in the future. */
    enum ErrorType {
        GENERAL,
        STREAM,
        IPC
    };

    /** The flag to select respective sink */
    enum Flags {
        /**< No flags to be set. */
        None = 0x0,
        /**< Enable timestamping of data being played. */
        Timestamp = 0x1,
        /**< Enable buffering of data. Use this if pushing buffers with
         * PushBuffer() and the data source might be a slow or unreliable
         * network. */
        EnableBuffering = 0x2,
        /**< EnableLiveSource will enable dolby and dts decoders in the
         *gstreamer pipeline */
        EnableLiveSource = 0x04,

    };

    using Properties = std::map<std::string, std::string>;

    /** Create a MediaPlayer instance. The given application name must be
     * unique across the system. The stream type is used by system policy to
     * determine behaviour (for example, it may be that only one music stream
     * can be playing at a time. */
    static std::unique_ptr<MediaPlayer> Create(std::string app_name,
        StreamType type = Music, Flags flags = MediaPlayer::Flags::None,
        Properties properties = {});

    /** Media information used when the media source is application provided
     * buffers.
     *
     * \warning The format is currently just the string representation of
     * [GStreamer capabilities](https://gstreamer.freedesktop.org/data/doc/gstreamer/head/gstreamer/html/GstCaps.html)
     * which leaks internal implementation details, and this will be change in
     * the future.
     *
     * */
    struct MediaInfo {
        std::string format;
    };

    ~MediaPlayer();

    /** Sets the source of media to the given URI. This must be a valid URI,
     * such as `file:///path/to/a/file` or `https://some.media/on/the/web`.
     * This function can be called multiple times to change the current URI. */
    bool SetSource(std::string uri);
    /** Sets the source of media to be application provided buffers. Data can
     * be provided using PushBuffer() when the need data callback is called
     * The media can be seeked if the seekDataCb callback is provided and the
     * underlying media support seeking.
     *
     * \note When using this API, the seek callback gets seek an offset in
     * nanoseconds.
     *
     * \note When enough data is called application should stop pushing
     * the buffer until next need data
     *
     * \note While PushBuffer() may be called independently from the need data
     * callback, it _must not_ be called before the first callback occurs.
     *
     * \note A given MediaPlayer instance can not be used to play URIs once it
     * this function is called.
     */
    bool SetSource(MediaInfo info, std::function<void(size_t size)> needDataCb, std::function<void(uint64_t offset)> seekDataCb = {}, std::function<void(void)> enoughDataCb = {});
    /** Sets the source for live sources hdmi, spdif, linein.
     * Note that Play call is not required because SetSource sets pipeline
     * to PLAYING state internally.
     * We need to call DeviceRemoved() between two SetSource call.
    */
    bool SetSource(setup_info_t info);
    /** This version of SetSource() can be used when the media format to be
     * provided is not know before hand. Note that this will not work for
     * uncompressed data, where there is no way to automatically detect the
     * format. The media can be seeked if the seekDataCb callback is provided
     * and the  underlying media support seeking.
     *
     * \note When using this API, the seek callback gets seek an offset in
     * bytes.
     */
    bool SetSource(std::function<void(size_t size)> needDataCb, std::function<void(uint64_t offset)> seekDataCb = {}, std::function<void(void)> enoughDataCb = {});
    /** This has to be called from application if it wants to control
     * the buffering in sink and set the corresponding latency of
     * the playback stream in milliseconds.
     *
     * \note This API has to be called after creation of mediaplayer instance
     *
     * \note If multiple setsource is done on same mediaplayer instance the
     * last set value will be retained
     *
     * \note Default value would be 200ms*/
    void SetPreferredSinkLatencyInMilliSeconds(std::chrono::milliseconds latency_time = std::chrono::milliseconds(200));
    /**Set the live source pipeline to NULL state.
    */
    void DeviceRemoved();
    /** TBD needs to be removed with new PP apis*/
    void SetDolbyGraph(std::string graph);
    /** TBD needs to be removed with new PP apis*/
    void SetVirtualXGraph(std::string graph);
    /** Requests that the next track be played. Note that this will just
     * trigger a callback (see SetNextCb()) as the library does not implement
     * support for playlists. */
    bool Next();
    /** Requests that the previous track be played. Note that this will just
     * trigger a callback (see SetPreviousCb()) as the library does not
     * implement support for playlists. */
    bool Previous();
    /* Retrieves the Loop mode status of player. */
    LoopMode GetLoopMode();
    /* Sets the Loop mode status of Player. */
    void SetLoopMode(LoopMode mode);
    /* Retrieves the current Shuffle mode status of player. */
    bool GetShuffleStatus();
    /* Sets the Shuffle mode status of player. */
    void SetShuffleStatus(bool status);
    /** Starts playback. */
    Result Play();
    /** Pauses playback. */
    Result Pause();
    /** Stops playback. */
    Result Stop();

    /** Set the Rate range to be supported by the player
      * param min_rate : The lowest playback rate supported, default 1.0 cant be 0, should be 1.0 or lesser value.
      * param max_rate : The highest playback rate supported, default 1.0 should be greater than 0. */
    void SetSupportedRateRange(double min = 1.0, double max = 1.0);
    /** Get the Current playback rate of the player */
    double GetPlaybackRate();
    /** Gets the Min playback rate currently set on the player */
    double GetMinPlaybackRate();
    /** Get ths Max playback rate currently set on the player */
    double GetMaxPlaybackRate();
    /** Sets the Playback Rate for the media item on the URI provided using SetSource
     * The rate should be a non-zero value with in the range set.
     * In case of application provided buffer, the method call just updates rate field but doesn't affect the data consumption.*/
    bool SetRate(double rate);
    /** Sets the playback rate for the media buffer provided by the applilcation.
     * The position argument takes the current position of the stream, if not provided it is defaulted to 0.
     * Negative rate is not applicable when application buffer, application needs to process itself in this case and call the SetRate(double) with effective rate.
     * Not Available for ControlOnly player type.*/
    bool SetRate(double rate, std::chrono::seconds position);
    /** Sets the playback rate for the media buffer provided by the applilcation.
     * The offset argument takes the current byte position of the stream, if not provided it is defaulted to 0.
     * Negative rate is not applicable when application buffer, application needs to process itself in this case and call the SetRate(double) with effective rate.
     * Not Available for ControlOnly player type.*/
    bool SetRate(double rate, uint64_t offset);

    /** Pushes a buffer when the source is the application. */
    bool PushBuffer(void *data, size_t size);
    /** Pushes a buffer when the source is the application along with buffer duration and timestamp */
    bool PushBuffer(void *data, size_t size, std::chrono::nanoseconds duration, std::chrono::nanoseconds timestamp);
    /** Notifies the player that the stream has ended. */
    bool PushEndOfStream();
    /** Reports the amount of data currently buffered. */
    size_t GetBufferLevel();

    /** Provides the duration of the current media, in nanoseconds. */
    std::chrono::nanoseconds GetDuration();
    /** Provides the position of the current media, in nanoseconds. */
    std::chrono::nanoseconds GetPosition();
    /** Seeks to the given absolute position in the current media, in
     * nanoseconds. */
    Result Seek(std::chrono::nanoseconds position);

    /** Retrieves the current playback state.It is a blocking call
     * when player executes some ASYNC operation, it will wait till
     * that ASYNC operation gets completed. */
    State GetState();

    /** Retrieves the current playback state and returns a tuple which
     * contains current state and state change result. It is a non-blocking
     * call for ASYNC operations that could be verified via RESULT in return
     * value. Note - This API will be merged with GetState() in future.*/
    std::tuple<State, Result> GetCurrentState();

    /** Check if the pipeline is Seekable */
    bool IsSeekable();

    /** Retrieves metadata for the current media in the form of an std::map,
     * where the key is a ::Tag and value is a string.
     *
     * \note Metadata will not be available immediately after a call to
     * SetSource(). It will be available once the playback state is paused or
     * playing. */
    std::map<Tag, std::string> GetMetadata();

    /** Explicitly provide metadata for the stream. This is useful, for
     * example, when the application is the source of media data.
     *
     * Previously stored metadata will be lost once this API is used.
     *
     * The metadata parameter is an std::map where keys are ::Tag and values
     * are corresponding strings. */
    void SetMetadata(std::map<Tag, std::string> metadata);

    /** Gets Mute status of player */
    bool GetMute();

    /** Sets the Mute status of player */
    bool SetMute(bool value);

    /** Gets Volume status of player */
    double GetVolume();

    /** Sets the desired volume to player between 0 to 1 */
    bool SetVolume(double value);

    /**
     * Gets the pipeline delay
     */
    uint64_t GetPipelineDelay();

    /** Sets the callback to be called when an error occurs. */
    void SetOnErrorCb(std::function<void(ErrorType error_type, std::string error_msg, std::string debug_msg)> onErrorCb);
    /** Sets the callback to be called when the end of the current stream is reached.
     * on end of stream application can choose to set a new source to the player
     * or stop the player */
    void SetOnEndOfStreamCb(std::function<void(void)> onEndOfStreamCb);
    /** Sets the callback to be called when an asynchronous operation is
     * successfully completed. */
    void SetOnAsyncDoneCb(std::function<void(void)> onAsyncDoneCb);
    /** Sets the callback to be called when the playback state changes. */
    void SetOnStateChangedCb(std::function<void(State new_state)> onStateChangedCb);
    /** Sets the callback to be called when stream metadata changes. Current
     * metadata can be retrieved via GetMetadata(). */
    void SetOnMetadataChangedCb(std::function<void(void)> onMetadataChangedCb);
    /** Sets the callback to be called when the next track is requested. This
     * may occur either by a call to Next(), or by some interaction from some
     * other system component (for example, as the result of a button press. */
    void SetNextCb(std::function<void(void)> onNextCb);
    /** Sets the callback to be called when the previous track is requested.
     * This may occur either by a call to Next(), or by some interaction from
     * some other system component (for example, as the result of a button
     * press. */
    void SetPreviousCb(std::function<void(void)> onPreviousCb);
    /** Sets the callback to be called when the Loop Status is enabled.
     * This may occur either by a call to Loop(), or by some interaction from
     * some other system component. */
    void SetLoopModeChangeCb(std::function<void(LoopMode)> onLoopModeChangeCb);
    /** Sets the callback to be called when the Shuffle mode is being set.
     * This may occur either by a call to Shuffle(), or by some interaction from
     * some other system component. */
    void SetShuffleStatusChangeCb(std::function<void(bool)> onShuffleStatusChangeCb);
    /** Sets the callback to be called when the pipeline is buffering.
     * This may occur when the application is streaming content and the
     * pipeline dosent have enough data for smooth playback. When this
     * callback is received, the application should either pause playback
     * (if buffering_percent < 100) or resume playback (if
     * buffering_percent >= 100). */
    void SetBufferingCb(std::function<void(unsigned int buffering_percent)> onBufferingCb);
    /** Sets the callback to be called when the Mute status is updated. */
    void SetOnMuteUpdatedCb(std::function<void(bool mute_state)> onMuteUpdatedCb);
    /** Sets the callback to be called when the Volume status is updated. */
    void SetOnVolumeUpdatedCb(std::function<void(double volume)> onVolumeUpdatedCb);
    /** Sets the callback to be called which gives play control to the application when
     it is registered. This may occur when playback is controlled by either button
     press or any other system component*/
    void SetPlayCb(std::function<void(void)> onPlayCb);
    /** Sets the callback to be called which gives pause control to the application when
     it is registered. This may occur when playback is controlled by either button
     press or any other system component*/
    void SetPauseCb(std::function<void(void)> onPauseCb);
    /** Sets the callback to be called which gives stop control to the application when
     it is registered. This may occur when playback is controlled by either button
     press or any other system component*/
    void SetStopCb(std::function<void(void)> onStopCb);
    /** Sets the callback to be called when the playback rate update request is received.
     * Suitable SetRate method needs to be called by the player application based on the
     * player configuration and capability*/
    void SetOnRateChangeRequestCallback(std::function<void(double)> onRateChangeRequestCb);

    /** Sets the callback to be called which gives total delay induced from source to sink
     * audio path as it is registered. This may occur when source sample specification or sink
     * sink latency changes
     */
    void SetOnPipelineDelayChangeCb(std::function<void(uint64_t)> onPipelineDelayChangeCb);

 private:
    MediaPlayer();

    std::shared_ptr<MediaPlayerPrivate> priv_;
};
}  // namespace adk

#endif
