/*
 * Copyright (c) 2020, The Linux Foundation. All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above
 *       copyright notice, this list of conditions and the following
 *       disclaimer in the documentation and/or other materials provided
 *       with the distribution.
 *     * Neither the name of The Linux Foundation nor the names of its
 *       contributors may be used to endorse or promote products derived
 *       from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS
 * BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
 * BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
 * IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include <vector>

/******** structure for ddp properties for initialization ***********/
typedef struct {
    std::string stream_type;
    int32_t dual_mono;
    int32_t audio_coding_mode;
    int32_t lfe;
    uint32_t custom_channel_map;
    bool force_timeslice_decoding;
    bool drop;
}ddpdec_init_props_t;

/******** structure for mat properties for initialization ***********/
typedef struct {
    int32_t stream_type;
    bool drop;
}matdec_init_props_t;

/******** structure for thd properties for initialization ***********/
typedef struct {
    std::string stream_type;
    uint32_t fbb_ch_summary;
    uint32_t fba_ch;
    uint32_t sync_word;
}thdec_init_props_t;

/******** structure for dtsx properties for initialization ***********/
typedef struct {
    uint32_t spkrout;
    int32_t sett1ccmode;
    bool drop;
}dtsxdec_init_props_t;

/******** structure for the decoder properties for initialization ***********/
typedef struct {
    ddpdec_init_props_t ddp;
    matdec_init_props_t mat;
    thdec_init_props_t thd;
    dtsxdec_init_props_t dtsx;
}decoder_init_props_t;

/*********structure for device sample spec****************/
typedef struct {
    std::string format_;
    uint32_t rate_;
    uint8_t channels_;
} PADeviceSampleSpec_t;

/*********structure for device info****************/
typedef struct {
    std::string name_;
    uint32_t id_;
    PADeviceSampleSpec_t sample_spec_;
    std::vector<std::string> encoding_formats_;
    uint8_t channels_;
    std::vector<std::string> channel_map_;
} PASourceDeviceInfo_t;

/********** structure to send for setting source***********/
typedef struct {
PASourceDeviceInfo_t device_info;
decoder_init_props_t decoder_props;
}setup_info_t;

/************** data type to select decoder **********************************/
typedef enum decoder_list {
    DDP,
    MAT,
    THD,
    DTSX,
    DSD
}decoder_t;

/************** data types for sending the properties requested  *************/
typedef enum property_type
{
    INT32,
    UINT32,
    DOUBLE,
    STRING
}property_type_t;

typedef union property_value
{
    int32_t INT32;
    uint32_t UINT32;
    double DOUBLE;
    std::string STRING;
} property_value_t;

typedef struct get_property
{
    property_type_t type;
    property_value_t value;
}property_t;

