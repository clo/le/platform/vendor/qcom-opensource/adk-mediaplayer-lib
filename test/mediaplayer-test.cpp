/*
 * Copyright (c) 2018, The Linux Foundation. All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above
 *       copyright notice, this list of conditions and the following
 *       disclaimer in the documentation and/or other materials provided
 *       with the distribution.
 *     * Neither the name of The Linux Foundation nor the names of its
 *       contributors may be used to endorse or promote products derived
 *       from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS
 * BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
 * BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
 * IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include "mediaplayer-test.h"

class MediaPlayerTest : public ::testing::Test {
 public:
    bool WaitForObserver() {
        cond.wait(lock);

        return true;
    }

    void OnError(adk::MediaPlayer::ErrorType error_type G_GNUC_UNUSED,
        std::string error_msg G_GNUC_UNUSED, std::string debug_msg G_GNUC_UNUSED) {
        onErrorCalled = true;
        cond.notify_one();
    }

    void OnAsyncDone(void) {
        onAsyncDoneCalled = true;
        cond.notify_one();
    }

    void OnEndOfStream(void) {
        onEndOfStreamCalled = true;
        cond.notify_one();
    }

    void OnStateChanged(adk::MediaPlayer::State new_state) {
        onStateChangedCalled = true;
        current_state = new_state;

        cond.notify_one();
    }

    void OnMetadataChanged(void) {
        onMetadataChangedCalled = true;
    }

    void OnLoopModeChanged(adk::MediaPlayer::LoopMode mode G_GNUC_UNUSED) {
        OnLoopModeChangedCalled = true;
    }

    void OnShuffleStatusChanged(bool status G_GNUC_UNUSED) {
        OnShuffleStatusChangedCalled = true;
    }

    void OnBuffering(unsigned int buffering_percent) {
        onBufferingCalled = true;
    }

    bool onErrorCalled = false, onAsyncDoneCalled = false, OnShuffleStatusChangedCalled = true, OnLoopModeChangedCalled = true, onEndOfStreamCalled = false, onStateChangedCalled = false, onMetadataChangedCalled = false, onBufferingCalled = false;
    adk::MediaPlayer::State current_state;

 protected:
    virtual void SetUp() {
        lock = std::unique_lock<std::mutex>(mutex);
    }

    virtual void TearDown() {
    }

 private:
    std::mutex mutex;
    std::unique_lock<std::mutex> lock;
    std::condition_variable cond;
};

TEST_F(MediaPlayerTest, CreationTest) {
    auto player = adk::MediaPlayer::Create("test-app-create");

    ASSERT_NE(nullptr, player);
}

TEST_F(MediaPlayerTest, SetSource) {
    auto player = adk::MediaPlayer::Create("test-app-set-uri");

    // Valid URI
    EXPECT_TRUE(player->SetSource(LOCAL_MEDIA_FILE_SHORT));

    EXPECT_TRUE(player->SetSource(BAD_LOCAL_MEDIA_FILE));

    // Invalid URI
    EXPECT_FALSE(player->SetSource(""));
}

TEST_F(MediaPlayerTest, Play) {
    auto player = adk::MediaPlayer::Create("test-app-play");
    auto res = adk::MediaPlayer::Failure;

    auto onError = std::bind(std::mem_fn(&MediaPlayerTest::OnError), this, _1, _2, _3);
    auto onAsyncDone = std::bind(std::mem_fn(&MediaPlayerTest::OnAsyncDone), this);

    player->SetOnErrorCb(onError);
    player->SetOnAsyncDoneCb(onAsyncDone);

    // No URI is set
    EXPECT_EQ(adk::MediaPlayer::Failure, player->Play());

    // Local file is set as URI
    EXPECT_TRUE(player->SetSource(LOCAL_MEDIA_FILE_SHORT));
    res = player->Play();
    EXPECT_TRUE(res == adk::MediaPlayer::Success || res == adk::MediaPlayer::Async);
    if (res == adk::MediaPlayer::Async) {
        EXPECT_TRUE(WaitForObserver());
        EXPECT_TRUE(onAsyncDoneCalled);
        onAsyncDoneCalled = false;
    }

    EXPECT_TRUE(player->SetSource(LOCAL_MEDIA_FILE_LONG));
    res = player->Play();
    EXPECT_TRUE(res == adk::MediaPlayer::Success || res == adk::MediaPlayer::Async);
    if (res == adk::MediaPlayer::Async) {
        EXPECT_TRUE(WaitForObserver());
        EXPECT_TRUE(onAsyncDoneCalled);
        onAsyncDoneCalled = false;
    }

    // While playback
    res = player->Play();
    EXPECT_TRUE(res == adk::MediaPlayer::Success || res == adk::MediaPlayer::Async);

    // Bad file as URI
    EXPECT_TRUE(player->SetSource(BAD_LOCAL_MEDIA_FILE));
    res = player->Play();
    EXPECT_TRUE(res == adk::MediaPlayer::Failure || res == adk::MediaPlayer::Async);
    EXPECT_TRUE(WaitForObserver());
    EXPECT_TRUE(onErrorCalled);
    onErrorCalled = false;
}

TEST_F(MediaPlayerTest, PlayWithSetLatency) {
    auto player = adk::MediaPlayer::Create("test-app-play-with-set-latency", adk::MediaPlayer::StreamType::VoiceUI);

    auto onError = std::bind(std::mem_fn(&MediaPlayerTest::OnError), this, _1, _2, _3);
    auto onAsyncDone = std::bind(std::mem_fn(&MediaPlayerTest::OnAsyncDone), this);

    player->SetOnErrorCb(onError);
    player->SetOnAsyncDoneCb(onAsyncDone);

    // While Playback
    EXPECT_TRUE(player->SetSource(LOCAL_MEDIA_FILE_SHORT));
    player->SetPreferredSinkLatencyInMilliSeconds(std::chrono::milliseconds(40));
    auto res = player->Play();
    EXPECT_TRUE(res == adk::MediaPlayer::Success || res == adk::MediaPlayer::Async);
    if (res == adk::MediaPlayer::Async) {
        EXPECT_TRUE(WaitForObserver());
        EXPECT_TRUE(onAsyncDoneCalled);
        onAsyncDoneCalled = false;
    }
}

TEST_F(MediaPlayerTest, Pause) {
    auto player = adk::MediaPlayer::Create("test-app-pause");

    auto onError = std::bind(std::mem_fn(&MediaPlayerTest::OnError), this, _1, _2, _3);
    auto onAsyncDone = std::bind(std::mem_fn(&MediaPlayerTest::OnAsyncDone), this);

    player->SetOnErrorCb(onError);
    player->SetOnAsyncDoneCb(onAsyncDone);

    // No URI is set
    EXPECT_EQ(adk::MediaPlayer::Failure, player->Pause());

    // While Playback
    EXPECT_TRUE(player->SetSource(LOCAL_MEDIA_FILE_SHORT));
    player->Play();
    auto res = player->Pause();
    EXPECT_TRUE(res == adk::MediaPlayer::Success || res == adk::MediaPlayer::Async);
    if (res == adk::MediaPlayer::Async) {
        EXPECT_TRUE(WaitForObserver());
        EXPECT_TRUE(onAsyncDoneCalled);
        onAsyncDoneCalled = false;
    }
}

TEST_F(MediaPlayerTest, Stop) {
    auto player = adk::MediaPlayer::Create("test-app-stop");

    // No URI is set
    EXPECT_EQ(adk::MediaPlayer::Success, player->Stop());

    // While Playback
    EXPECT_TRUE(player->SetSource(LOCAL_MEDIA_FILE_SHORT));
    player->Play();
    EXPECT_EQ(adk::MediaPlayer::Success, player->Stop());
}

TEST_F(MediaPlayerTest, MultiSetSource) {
    auto player = adk::MediaPlayer::Create("test-app-multi-setsource");

    std::cout << " SetSourcePlay " << std::endl;
    auto onError = std::bind(std::mem_fn(&MediaPlayerTest::OnError), this, _1, _2, _3);
    auto onAsyncDone = std::bind(std::mem_fn(&MediaPlayerTest::OnAsyncDone), this);

    player->SetOnErrorCb(onError);
    player->SetOnAsyncDoneCb(onAsyncDone);

    EXPECT_TRUE(player->SetSource(LOCAL_MEDIA_FILE_LONG));
    player->Play();
    auto res = player->Pause();
    EXPECT_TRUE(res == adk::MediaPlayer::Success || res == adk::MediaPlayer::Async);
    if (res == adk::MediaPlayer::Async) {
        EXPECT_TRUE(WaitForObserver());
        EXPECT_TRUE(onAsyncDoneCalled);
        onAsyncDoneCalled = false;
    }
    player->Stop();

    EXPECT_TRUE(player->SetSource(LOCAL_MEDIA_FILE_LONG));
    res = player->Play();
    EXPECT_TRUE(res == adk::MediaPlayer::Success || res == adk::MediaPlayer::Async);
}

TEST_F(MediaPlayerTest, Loop) {
    auto player = adk::MediaPlayer::Create("test-app-loop");

    auto onLoopModeChanged = std::bind(std::mem_fn(&MediaPlayerTest::OnLoopModeChanged), this, _1);
    player->SetLoopModeChangeCb(onLoopModeChanged);

    EXPECT_EQ(adk::MediaPlayer::LoopMode::None, player->GetLoopMode());

    player->SetLoopMode(adk::MediaPlayer::LoopMode::Track);
    EXPECT_TRUE(OnLoopModeChangedCalled);
    EXPECT_EQ(adk::MediaPlayer::LoopMode::Track, player->GetLoopMode());
    OnLoopModeChangedCalled = false;
}

TEST_F(MediaPlayerTest, Shuffle) {
    auto player = adk::MediaPlayer::Create("test-app-shuffle");

    auto onShuffleStatusChanged = std::bind(std::mem_fn(&MediaPlayerTest::OnShuffleStatusChanged), this, _1);
    player->SetShuffleStatusChangeCb(onShuffleStatusChanged);

    EXPECT_FALSE(player->GetShuffleStatus());

    player->SetShuffleStatus(true);
    EXPECT_TRUE(OnShuffleStatusChangedCalled);
    EXPECT_TRUE(player->GetShuffleStatus());
    OnShuffleStatusChangedCalled = false;
}

TEST_F(MediaPlayerTest, GetDuration) {
    auto player = adk::MediaPlayer::Create("test-app-get-duration");

    auto onError = std::bind(std::mem_fn(&MediaPlayerTest::OnError), this, _1, _2, _3);
    auto onAsyncDone = std::bind(std::mem_fn(&MediaPlayerTest::OnAsyncDone), this);

    player->SetOnErrorCb(onError);
    player->SetOnAsyncDoneCb(onAsyncDone);

    // No URI is set
    EXPECT_EQ(0, (player->GetDuration()).count());

    // No Playback
    EXPECT_TRUE(player->SetSource(LOCAL_MEDIA_FILE_SHORT));
    EXPECT_EQ(0, (player->GetDuration()).count());

    // While Playback
    EXPECT_TRUE(player->SetSource(LOCAL_MEDIA_FILE_SHORT));
    auto res = player->Play();
    if (res == adk::MediaPlayer::Async) {
        EXPECT_TRUE(WaitForObserver());
        EXPECT_TRUE(onAsyncDoneCalled);
        onAsyncDoneCalled = false;
    }
    ASSERT_NE(0, (player->GetDuration()).count());
}

TEST_F(MediaPlayerTest, GetPosition) {
    auto player = adk::MediaPlayer::Create("test-app-get-position");

    auto onError = std::bind(std::mem_fn(&MediaPlayerTest::OnError), this, _1, _2, _3);
    auto onAsyncDone = std::bind(std::mem_fn(&MediaPlayerTest::OnAsyncDone), this);

    player->SetOnErrorCb(onError);
    player->SetOnAsyncDoneCb(onAsyncDone);

    // No URI is set
    EXPECT_EQ(0, (player->GetPosition()).count());

    // No Playback
    EXPECT_TRUE(player->SetSource(LOCAL_MEDIA_FILE_SHORT));
    EXPECT_EQ(0, (player->GetPosition()).count());

    // While Playback
    EXPECT_TRUE(player->SetSource(LOCAL_MEDIA_FILE_SHORT));
    auto res = player->Play();
    if (res == adk::MediaPlayer::Async) {
        EXPECT_TRUE(WaitForObserver());
        EXPECT_TRUE(onAsyncDoneCalled);
        onAsyncDoneCalled = false;
    }

    std::this_thread::sleep_for(std::chrono::seconds(1));
    std::chrono::duration<double> position = player->GetPosition();
    EXPECT_TRUE(position.count() > 0.5 && position.count() < 1.5);
}

TEST_F(MediaPlayerTest, Seek) {
    auto player = adk::MediaPlayer::Create("test-app-seek");

    auto onError = std::bind(std::mem_fn(&MediaPlayerTest::OnError), this, _1, _2, _3);
    auto onAsyncDone = std::bind(std::mem_fn(&MediaPlayerTest::OnAsyncDone), this);

    player->SetOnErrorCb(onError);
    player->SetOnAsyncDoneCb(onAsyncDone);

    // Beyond Duration
    EXPECT_TRUE(player->SetSource(LOCAL_MEDIA_FILE_SHORT));
    auto res = player->Play();
    if (res == adk::MediaPlayer::Async) {
        EXPECT_TRUE(WaitForObserver());
        EXPECT_TRUE(onAsyncDoneCalled);
        onAsyncDoneCalled = false;
    }
    EXPECT_TRUE(player->Seek(std::chrono::seconds(3)) == adk::MediaPlayer::Failure);

    // While Playback
    EXPECT_TRUE(player->SetSource(LOCAL_MEDIA_FILE_LONG));
    res = player->Play();
    if (res == adk::MediaPlayer::Async) {
        EXPECT_TRUE(WaitForObserver());
        EXPECT_TRUE(onAsyncDoneCalled);
        onAsyncDoneCalled = false;
    }
    EXPECT_TRUE(player->Seek(std::chrono::seconds(100)) == adk::MediaPlayer::Success);
    EXPECT_TRUE(WaitForObserver());
    EXPECT_TRUE(onAsyncDoneCalled);
    onAsyncDoneCalled = false;
    std::chrono::duration<double> position = player->GetPosition();
    EXPECT_TRUE(position.count() > 99 && position.count() < 101);
}

TEST_F(MediaPlayerTest, SeekUriWithOffset) {
    auto player = adk::MediaPlayer::Create("test-app-seek-uri-with-offset");

    auto onAsyncDone = std::bind(std::mem_fn(&MediaPlayerTest::OnAsyncDone), this);
    player->SetOnAsyncDoneCb(onAsyncDone);

    EXPECT_TRUE(player->SetSource(LOCAL_MEDIA_FILE_LONG));
    EXPECT_TRUE(player->Seek(std::chrono::seconds(10)) == adk::MediaPlayer::Success);

    auto res = player->Play();
    if (res == adk::MediaPlayer::Async) {
        EXPECT_TRUE(WaitForObserver());
        EXPECT_TRUE(onAsyncDoneCalled);
        onAsyncDoneCalled = false;
    }

    std::chrono::duration<double> position = player->GetPosition();
    EXPECT_TRUE(position.count() > 9 && position.count() < 11);
}

TEST_F(MediaPlayerTest, GetState) {
    auto player = adk::MediaPlayer::Create("test-app-state");

    auto onAsyncDone = std::bind(std::mem_fn(&MediaPlayerTest::OnAsyncDone), this);
    player->SetOnAsyncDoneCb(onAsyncDone);

    // No URI is set
    EXPECT_EQ(adk::MediaPlayer::State::Stopped, player->GetState());

    // While Playback with Blocking GetState() method.
    EXPECT_TRUE(player->SetSource(LOCAL_MEDIA_FILE_LONG));
    auto res = player->Play();
    EXPECT_TRUE(res == adk::MediaPlayer::Success || res == adk::MediaPlayer::Async);
    if (res == adk::MediaPlayer::Async) {
        EXPECT_TRUE(WaitForObserver());
        EXPECT_TRUE(onAsyncDoneCalled);
        onAsyncDoneCalled = false;
    }
    EXPECT_EQ(adk::MediaPlayer::State::Playing, player->GetState());

    res = player->Pause();
    EXPECT_TRUE(res == adk::MediaPlayer::Success || res == adk::MediaPlayer::Async);
    if (res == adk::MediaPlayer::Async) {
        EXPECT_TRUE(WaitForObserver());
        EXPECT_TRUE(onAsyncDoneCalled);
        onAsyncDoneCalled = false;
    }
    EXPECT_EQ(adk::MediaPlayer::State::Paused, player->GetState());

    res = player->Stop();
    EXPECT_EQ(adk::MediaPlayer::Success, res);
    EXPECT_EQ(adk::MediaPlayer::State::Stopped, player->GetState());
}

TEST_F(MediaPlayerTest, GetCurrentState) {
    auto player = adk::MediaPlayer::Create("get-current-state");

    auto onAsyncDone = std::bind(std::mem_fn(&MediaPlayerTest::OnAsyncDone), this);
    player->SetOnAsyncDoneCb(onAsyncDone);

    // While Playback.
    EXPECT_TRUE(player->SetSource(LOCAL_MEDIA_FILE_LONG));
    player->Play();

    // Non Blocking GetCurrentState() method.
    auto ret = player->GetCurrentState();

    /** Initially player will be in Stopped State Wait Untill Result
     * become success. */
    while (std::get<1>(ret) != adk::MediaPlayer::Result::Success) {
        ret = player->GetCurrentState();
    }

    EXPECT_EQ(adk::MediaPlayer::State::Playing, std::get<0>(ret));
    EXPECT_EQ(adk::MediaPlayer::Result::Success, std::get<1>(ret));
}

TEST_F(MediaPlayerTest, PlayBuffer) {
    auto player = adk::MediaPlayer::Create("test-app-play-buffer");

    auto endOfStream = std::bind(std::mem_fn(&MediaPlayerTest::OnEndOfStream), this);
    player->SetOnEndOfStreamCb(endOfStream);

    int16_t buffer[SIZE];
    int i;
    int16_t amp = 0.5 * 32767;

    for (i = 0; i < SIZE; i++) {
        buffer[i] = (int16_t)(amp * sin((440.0 * (2 * 3.1415926536) * (double)i) / 44100.0));
    }

    adk::MediaPlayer::MediaInfo info = {
        .format =
            "audio/x-raw, channels=(int)1, rate=(int)44100, "
            "format=(string)S16LE, layout=(string)interleaved"};

    // Run the test 3 times
    auto rep = 3;
    while (rep--) {
        auto index = 0;
        auto end = false;

        auto res = player->SetSource(info, [&](size_t size) {
            int samples = size / 2;

            // We expect to not get the need data callback once we hit end-of-stream
            EXPECT_FALSE(end);

            if (index < SIZE) {
                samples = MIN(samples, SIZE - index);
                EXPECT_TRUE(player->PushBuffer(buffer + index, samples * 2));
                index += samples;
            } else {
                end = true;
                EXPECT_TRUE(player->PushEndOfStream());
            } },
            NULL);

        EXPECT_TRUE(res);

        res = player->Play();

        EXPECT_TRUE(WaitForObserver());
        EXPECT_TRUE(onEndOfStreamCalled);
        onEndOfStreamCalled = false;
    }
}

TEST_F(MediaPlayerTest, PlayBufferWithTimeStamp) {
    auto player = adk::MediaPlayer::Create("test-app-play-buffer-with-timestamp", adk::MediaPlayer::StreamType::Music, adk::MediaPlayer::Flags::Timestamp);

    auto endOfStream = std::bind(std::mem_fn(&MediaPlayerTest::OnEndOfStream), this);
    player->SetOnEndOfStreamCb(endOfStream);

    int16_t buffer[SIZE];
    int i;
    int16_t amp = 0.5 * 32767;
    auto index = 0;
    std::chrono::nanoseconds ts;
    std::chrono::nanoseconds duration;
    double datarate = 44100 * 1 * 2;

    for (i = 0; i < SIZE; i++) {
        buffer[i] = (int16_t)(amp * sin((440.0 * (2 * 3.1415926536) * (double)i) / 44100.0));
    }

    adk::MediaPlayer::MediaInfo info = {
        .format =
            "audio/x-raw, channels=(int)1, rate=(int)44100, "
            "format=(string)S16LE, layout=(string)interleaved"};

    auto res = player->SetSource(info, [&](size_t size) {
        int samples = size / 2;
        if (index < SIZE) {
            samples = MIN(samples, SIZE - index);
            uint64_t dur = (uint64_t)(((double)(samples)/datarate)*1000*1000*1000);
            duration = std::chrono::nanoseconds(dur);

            EXPECT_TRUE(player->PushBuffer(buffer + index, samples * 2, duration, ts));
            ts += std::chrono::nanoseconds(dur);
            index += samples;
        } else {
            EXPECT_TRUE(player->PushEndOfStream());
        } },
        NULL);

    EXPECT_TRUE(res);

    res = player->Play();

    EXPECT_TRUE(WaitForObserver());
    EXPECT_TRUE(onEndOfStreamCalled);
    onEndOfStreamCalled = false;
}

TEST_F(MediaPlayerTest, BufferLevel) {
    auto player = adk::MediaPlayer::Create("test-app-buffer-level");

    auto onAsyncDone = std::bind(std::mem_fn(&MediaPlayerTest::OnAsyncDone), this);
    player->SetOnAsyncDoneCb(onAsyncDone);

    int16_t buffer[SIZE];
    long index;
    int i;
    int16_t amp = 0.5 * 32767;

    for (i = 0; i < SIZE; i++) {
        buffer[i] = (int16_t)(amp * sin((440.0 * (2 * 3.1415926536) * (double)i) / 44100.0));
    }

    adk::MediaPlayer::MediaInfo info = {
        .format =
            "audio/x-raw, channels=(int)1, rate=(int)44100, "
            "format=(string)S16LE, layout=(string)interleaved"};

    auto res = player->SetSource(info, [&](size_t size) {
        int samples = size / 2;

        if (index < SIZE) {
            samples = MIN(samples, SIZE - index);
            player->PushBuffer(buffer + index, samples * 2);
            index += samples;
        } else {
            player->PushEndOfStream();
        } },
        NULL);

    EXPECT_TRUE(res);

    res = player->Pause();

    EXPECT_TRUE(WaitForObserver());
    EXPECT_TRUE(onAsyncDoneCalled);
    onAsyncDoneCalled = false;

    player->PushBuffer(buffer, 10000);
    EXPECT_EQ(player->GetBufferLevel(), 10000);
}

TEST_F(MediaPlayerTest, ReadFilePlay) {
    auto player = adk::MediaPlayer::Create("test-app-read-file-play");

    auto onAsyncDone = std::bind(std::mem_fn(&MediaPlayerTest::OnAsyncDone), this);
    player->SetOnAsyncDoneCb(onAsyncDone);

    auto endOfStream = std::bind(std::mem_fn(&MediaPlayerTest::OnEndOfStream), this);
    player->SetOnEndOfStreamCb(endOfStream);

    std::ifstream file(LOCAL_MEDIA_MP4_FILE, std::ifstream::in);
    file.seekg(0, file.end);
    auto file_size = file.tellg();
    file.seekg(0);

    EXPECT_TRUE(file.good());

    auto res = player->SetSource(
        [&](size_t size) {
            char buf[size];
            auto pos = file.tellg();

            file.read(buf, size);

            size = pos + (int64_t)size > file_size ? file_size - pos : size;
            EXPECT_TRUE(player->PushBuffer(buf, size));

            if (file.eof())
                file.clear();
        },
        [&](uint64_t offset) {
            file.seekg(offset);
        });

    EXPECT_TRUE(res);

    res = player->Play();
    EXPECT_TRUE(WaitForObserver());
    EXPECT_TRUE(onAsyncDoneCalled);
    onAsyncDoneCalled = false;

    EXPECT_TRUE(WaitForObserver());
    EXPECT_TRUE(onEndOfStreamCalled);
    onEndOfStreamCalled = false;

    file.close();
}

TEST_F(MediaPlayerTest, ReadFilePlayEnableBuffering) {
    auto player = adk::MediaPlayer::Create("test-app-read-file-play-enable-buffering",
        adk::MediaPlayer::StreamType::Music, adk::MediaPlayer::Flags::EnableBuffering);

    auto onAsyncDone = std::bind(std::mem_fn(&MediaPlayerTest::OnAsyncDone), this);
    player->SetOnAsyncDoneCb(onAsyncDone);

    auto onBuffering = std::bind(std::mem_fn(&MediaPlayerTest::OnBuffering), this, _1);
    player->SetBufferingCb(onBuffering);

    auto endOfStream = std::bind(std::mem_fn(&MediaPlayerTest::OnEndOfStream), this);
    player->SetOnEndOfStreamCb(endOfStream);

    std::ifstream file(LOCAL_MEDIA_MP4_FILE, std::ifstream::in);
    file.seekg(0, file.end);
    auto file_size = file.tellg();
    file.seekg(0);

    EXPECT_TRUE(file.good());

    auto res = player->SetSource(
        [&](size_t size) {
            char buf[size];
            auto pos = file.tellg();

            file.read(buf, size);

            size = pos + (int64_t)size > file_size ? file_size - pos : size;
            EXPECT_TRUE(player->PushBuffer(buf, size));

            if (file.eof())
                file.clear();
        },
        [&](uint64_t offset) {
            file.seekg(offset);
        });

    EXPECT_TRUE(res);

    res = player->Pause();

    EXPECT_TRUE(WaitForObserver());
    EXPECT_TRUE(onAsyncDoneCalled);
    onAsyncDoneCalled = false;

    EXPECT_TRUE(onBufferingCalled);

    EXPECT_EQ(adk::MediaPlayer::Success, player->Stop());

    file.close();
}

TEST_F(MediaPlayerTest, ReadFileSeek) {
    auto player = adk::MediaPlayer::Create("test-app-read-file-seek");

    auto onAsyncDone = std::bind(std::mem_fn(&MediaPlayerTest::OnAsyncDone), this);
    player->SetOnAsyncDoneCb(onAsyncDone);

    auto endOfStream = std::bind(std::mem_fn(&MediaPlayerTest::OnEndOfStream), this);
    player->SetOnEndOfStreamCb(endOfStream);

    std::ifstream file(LOCAL_MEDIA_MP4_FILE, std::ifstream::in);
    file.seekg(0, file.end);
    auto file_size = file.tellg();
    file.seekg(0);

    EXPECT_TRUE(file.good());

    auto res = player->SetSource(
        [&](size_t size) {
            char buf[size];
            auto pos = file.tellg();

            file.read(buf, size);

            size = pos + (int64_t)size > file_size ? file_size - pos : size;
            EXPECT_TRUE(player->PushBuffer(buf, size));

            if (file.eof())
                file.clear();
        },
        [&](uint64_t offset) {
            file.seekg(offset);
        });

    EXPECT_TRUE(player->Seek(std::chrono::seconds(10)) == adk::MediaPlayer::Success);

    EXPECT_TRUE(res);

    res = player->Play();
    EXPECT_TRUE(WaitForObserver());
    EXPECT_TRUE(onAsyncDoneCalled);
    onAsyncDoneCalled = false;

    std::chrono::duration<double> position = player->GetPosition();
    EXPECT_TRUE(position.count() > 9 && position.count() < 11);

    std::this_thread::sleep_for(std::chrono::seconds(1));
    EXPECT_TRUE(player->Seek(std::chrono::seconds(20)) == adk::MediaPlayer::Success);

    EXPECT_TRUE(WaitForObserver());
    EXPECT_TRUE(onAsyncDoneCalled);
    onAsyncDoneCalled = false;

    position = player->GetPosition();
    EXPECT_TRUE(position.count() > 19 && position.count() < 21);

    EXPECT_TRUE(player->Seek(std::chrono::seconds(138)) == adk::MediaPlayer::Success);

    EXPECT_TRUE(WaitForObserver());
    EXPECT_TRUE(onAsyncDoneCalled);
    onAsyncDoneCalled = false;

    EXPECT_TRUE(WaitForObserver());
    EXPECT_TRUE(onEndOfStreamCalled);
    onEndOfStreamCalled = false;

    file.close();
}

TEST_F(MediaPlayerTest, PlayBufferSeek) {
    auto player = adk::MediaPlayer::Create("test-app-play-buffer-seek");

    auto onAsyncDone = std::bind(std::mem_fn(&MediaPlayerTest::OnAsyncDone), this);
    player->SetOnAsyncDoneCb(onAsyncDone);

    int16_t buffer[SIZE * 8];
    int i;
    int16_t amp = 0.5 * 32767;

    for (i = 0; i < (SIZE * 8); i++) {
        buffer[i] = (int16_t)(amp * sin((440.0 * (2 * 3.1415926536) * (double)i) / 44100.0));
    }

    adk::MediaPlayer::MediaInfo info = {
        .format =
            "audio/x-raw, channels=(int)1, rate=(int)44100, "
            "format=(string)S16LE, layout=(string)interleaved"};

    auto index = 0;

    auto res = player->SetSource(info,
        [&](size_t size) {
            int samples = size / 2;

            if (index < SIZE) {
                samples = MIN(samples, SIZE - index);
                EXPECT_TRUE(player->PushBuffer(buffer + index, samples * 2));
                index += samples;
            } else {
                EXPECT_TRUE(player->PushEndOfStream());
            }
        },
        [&](uint16_t offset) {
            /* convert offset to bytes */
            index = (offset * 1 * 44100) / 1000000000;
        });

    EXPECT_TRUE(res);

    res = player->Play();
    std::this_thread::sleep_for(std::chrono::seconds(1));

    EXPECT_TRUE(player->Seek(std::chrono::seconds(2)) == adk::MediaPlayer::Success);

    EXPECT_TRUE(WaitForObserver());
    EXPECT_TRUE(onAsyncDoneCalled);
    onAsyncDoneCalled = false;

    std::chrono::duration<double> position = player->GetPosition();
    EXPECT_TRUE(position.count() > 1 && position.count() < 3);
}

TEST_F(MediaPlayerTest, PlayBufferEnoughData) {
    auto player = adk::MediaPlayer::Create("test-app-play-buffer-enough-data");

    auto endOfStream = std::bind(std::mem_fn(&MediaPlayerTest::OnEndOfStream), this);
    player->SetOnEndOfStreamCb(endOfStream);

    auto BUF_SIZE = SIZE * 10;
    int16_t buffer[BUF_SIZE];
    int i;
    int16_t amp = 0.5 * 32767;

    std::condition_variable need_data_cond;
    std::mutex need_data_mutex;
    std::unique_lock<std::mutex> need_data_lock;

    need_data_lock = std::unique_lock<std::mutex>(need_data_mutex);

    for (i = 0; i < BUF_SIZE; i++) {
        buffer[i] = (int16_t)(amp * sin((440.0 * (2 * 3.1415926536) * (double)i) / 44100.0));
    }

    adk::MediaPlayer::MediaInfo info = {
        .format =
            "audio/x-raw, channels=(int)1, rate=(int)44100, "
            "format=(string)S16LE, layout=(string)interleaved"};

    auto index = 0;
    auto buf_size = 0;
    std::atomic<bool> enough_data(false);

    auto res = player->SetSource(info, [&](size_t size G_GNUC_UNUSED) {
        if(enough_data.load()) {
            enough_data = false;
            need_data_cond.notify_one();
        } },
        NULL,
        [&]() {
            enough_data = true;
        });

    EXPECT_TRUE(res);

    res = player->Play();

    while (1) {
        if (enough_data.load()) {
            /* Pushing Buffer before wait to appsrc which has enough data will result
             * in another enough data so pushing it after wait of next need data */
            need_data_cond.wait(need_data_lock);
            buf_size = 2000;
        } else {
            buf_size = 20000;
        }

        if (index < BUF_SIZE) {
            /* pushing buf_size or lesser samples */
            auto samples = MIN(buf_size, BUF_SIZE - index);
            EXPECT_TRUE(player->PushBuffer(buffer + index, samples));
            index += samples;
        } else {
            EXPECT_TRUE(player->PushEndOfStream());
            break;
        }
    }

    EXPECT_TRUE(WaitForObserver());
    EXPECT_TRUE(onEndOfStreamCalled);
    onEndOfStreamCalled = false;
}

TEST_F(MediaPlayerTest, GetMetadata) {
    auto player = adk::MediaPlayer::Create("test-app-get-metadata");

    auto onError = std::bind(std::mem_fn(&MediaPlayerTest::OnError), this, _1, _2, _3);
    auto onAsyncDone = std::bind(std::mem_fn(&MediaPlayerTest::OnAsyncDone), this);
    auto onMetadataChanged = std::bind(std::mem_fn(&MediaPlayerTest::OnMetadataChanged), this);

    player->SetOnErrorCb(onError);
    player->SetOnAsyncDoneCb(onAsyncDone);
    player->SetOnMetadataChangedCb(onMetadataChanged);

    EXPECT_TRUE(player->SetSource(LOCAL_MEDIA_FILE_LONG));
    auto res = player->Play();
    if (res == adk::MediaPlayer::Async) {
        EXPECT_TRUE(WaitForObserver());
        EXPECT_TRUE(onAsyncDoneCalled);
        onAsyncDoneCalled = false;
    }

    EXPECT_TRUE(onMetadataChangedCalled);
    onMetadataChangedCalled = false;

    std::map<adk::MediaPlayer::Tag, std::string> metadata = player->GetMetadata();

    EXPECT_EQ(metadata[adk::MediaPlayer::Tag::Album], "Dummy-Album");
    EXPECT_EQ(metadata[adk::MediaPlayer::Tag::Artist], "Dummy-Artist");
    EXPECT_EQ(metadata[adk::MediaPlayer::Tag::AudioBitrate], "80000");
    EXPECT_EQ(metadata[adk::MediaPlayer::Tag::Duration], "139319727891");
    EXPECT_EQ(metadata[adk::MediaPlayer::Tag::Genre], "Rock,Jazz");
    EXPECT_EQ(metadata[adk::MediaPlayer::Tag::Title], "Dummy-Title");
    EXPECT_EQ(metadata[adk::MediaPlayer::Tag::Channels], "1");
    EXPECT_EQ(metadata[adk::MediaPlayer::Tag::SampleFormat], "F32LE");
    EXPECT_EQ(metadata[adk::MediaPlayer::Tag::SampleRate], "44100");

    int16_t buffer[SIZE];
    int i;
    int16_t amp = 0.5 * 32767;

    for (i = 0; i < SIZE; i++) {
        buffer[i] = (int16_t)(amp * sin((440.0 * (2 * 3.1415926536) * (double)i) / 44100.0));
    }

    adk::MediaPlayer::MediaInfo info = {
        .format =
            "audio/x-raw, channels=(int)1, rate=(int)44100, "
            "format=(string)S16LE, layout=(string)interleaved"};

    auto index = 0;
    auto end = false;

    auto ret = player->SetSource(info, [&](size_t size) {
            int samples = size / 2;

            // We expect to not get the need data callback once we hit end-of-stream
            EXPECT_FALSE(end);

            if (index < SIZE) {
                samples = MIN(samples, SIZE - index);
                EXPECT_TRUE(player->PushBuffer(buffer + index, samples * 2));
                index += samples;
            } else {
                end = true;
                EXPECT_TRUE(player->PushEndOfStream());
            } },
        NULL);

    EXPECT_TRUE(ret);

    res = player->Play();

    EXPECT_TRUE(WaitForObserver());
    EXPECT_TRUE(onAsyncDoneCalled);
    onAsyncDoneCalled = false;

    metadata = player->GetMetadata();

    EXPECT_EQ(metadata[adk::MediaPlayer::Tag::Channels], "1");
    EXPECT_EQ(metadata[adk::MediaPlayer::Tag::SampleFormat], "S16LE");
    EXPECT_EQ(metadata[adk::MediaPlayer::Tag::SampleRate], "44100");
}

TEST_F(MediaPlayerTest, MultiSetSourceGetMetadata) {
    auto player = adk::MediaPlayer::Create("test-app-multi-setsource-get-metadata");

    auto onError = std::bind(std::mem_fn(&MediaPlayerTest::OnError), this, _1, _2, _3);
    auto onAsyncDone = std::bind(std::mem_fn(&MediaPlayerTest::OnAsyncDone), this);
    auto onMetadataChanged = std::bind(std::mem_fn(&MediaPlayerTest::OnMetadataChanged), this);

    player->SetOnErrorCb(onError);
    player->SetOnAsyncDoneCb(onAsyncDone);
    player->SetOnMetadataChangedCb(onMetadataChanged);

    auto rep = 2;
    while (rep--) {
        EXPECT_TRUE(player->SetSource(LOCAL_MEDIA_FILE_LONG));

        auto res = player->Play();
        if (res == adk::MediaPlayer::Async) {
            EXPECT_TRUE(WaitForObserver());
            EXPECT_TRUE(onAsyncDoneCalled);
            onAsyncDoneCalled = false;
        }

        EXPECT_TRUE(onMetadataChangedCalled);
        onMetadataChangedCalled = false;

        std::map<adk::MediaPlayer::Tag, std::string> metadata = player->GetMetadata();

        EXPECT_EQ(metadata[adk::MediaPlayer::Tag::Album], "Dummy-Album");
        EXPECT_EQ(metadata[adk::MediaPlayer::Tag::Artist], "Dummy-Artist");
        EXPECT_EQ(metadata[adk::MediaPlayer::Tag::AudioBitrate], "80000");
        EXPECT_EQ(metadata[adk::MediaPlayer::Tag::Duration], "139319727891");
        EXPECT_EQ(metadata[adk::MediaPlayer::Tag::Genre], "Rock,Jazz");
        EXPECT_EQ(metadata[adk::MediaPlayer::Tag::Title], "Dummy-Title");
        EXPECT_EQ(metadata[adk::MediaPlayer::Tag::Channels], "1");
        EXPECT_EQ(metadata[adk::MediaPlayer::Tag::SampleFormat], "F32LE");
        EXPECT_EQ(metadata[adk::MediaPlayer::Tag::SampleRate], "44100");
    }
}

TEST_F(MediaPlayerTest, CoverArt) {
    auto player = adk::MediaPlayer::Create("test-app-album-art");

    auto onError = std::bind(std::mem_fn(&MediaPlayerTest::OnError), this, _1, _2, _3);
    auto onAsyncDone = std::bind(std::mem_fn(&MediaPlayerTest::OnAsyncDone), this);
    auto onMetadataChanged = std::bind(std::mem_fn(&MediaPlayerTest::OnMetadataChanged), this);

    player->SetOnErrorCb(onError);
    player->SetOnAsyncDoneCb(onAsyncDone);
    player->SetOnMetadataChangedCb(onMetadataChanged);

    EXPECT_TRUE(player->SetSource(LOCAL_MEDIA_FILE_LONG));
    auto res = player->Play();
    if (res == adk::MediaPlayer::Async) {
        EXPECT_TRUE(WaitForObserver());
        EXPECT_TRUE(onAsyncDoneCalled);
        onAsyncDoneCalled = false;
    }

    EXPECT_TRUE(onMetadataChangedCalled);
    onMetadataChangedCalled = false;

    std::map<adk::MediaPlayer::Tag, std::string> metadata = player->GetMetadata();

    EXPECT_EQ(metadata[adk::MediaPlayer::Tag::CoverArt], "/tmp/cover_art_test-app-album-art.png");

    std::ifstream file1("/tmp/cover_art_test-app-album-art.png", std::ifstream::in);
    std::ifstream file2(COVER_ART, std::ifstream::in);

    file1.seekg(0, file1.end);
    long size1 = file1.tellg();
    file1.seekg(0);

    file2.seekg(0, file2.end);
    long size2 = file2.tellg();
    file2.seekg(0);

    EXPECT_EQ(size1, size2);

    char *buffer1 = new char[size1];
    char *buffer2 = new char[size2];

    file1.read(buffer1, size1);
    file2.read(buffer2, size2);

    EXPECT_EQ(memcmp(buffer1, buffer2, size1), 0);

    // release dynamically-allocated memory
    delete[] buffer1;
    delete[] buffer2;

    file1.close();
    file2.close();

    std::map<adk::MediaPlayer::Tag, std::string> m;

    m[adk::MediaPlayer::Tag::CoverArt] = COVER_ART;

    player->SetMetadata(m);

    EXPECT_TRUE(onMetadataChangedCalled);
    onMetadataChangedCalled = false;

    metadata = player->GetMetadata();

    EXPECT_EQ(metadata[adk::MediaPlayer::Tag::CoverArt], COVER_ART);
}

TEST_F(MediaPlayerTest, SetMetadata) {
    auto player = adk::MediaPlayer::Create("test-app-set-metadata");

    auto onError = std::bind(std::mem_fn(&MediaPlayerTest::OnError), this, _1, _2, _3);
    auto onAsyncDone = std::bind(std::mem_fn(&MediaPlayerTest::OnAsyncDone), this);
    auto onMetadataChanged = std::bind(std::mem_fn(&MediaPlayerTest::OnMetadataChanged), this);

    player->SetOnErrorCb(onError);
    player->SetOnAsyncDoneCb(onAsyncDone);
    player->SetOnMetadataChangedCb(onMetadataChanged);

    EXPECT_TRUE(player->SetSource(LOCAL_MEDIA_FILE_LONG));
    auto res = player->Play();
    if (res == adk::MediaPlayer::Async) {
        EXPECT_TRUE(WaitForObserver());
        EXPECT_TRUE(onAsyncDoneCalled);
        onAsyncDoneCalled = false;
    }

    std::map<adk::MediaPlayer::Tag, std::string> m;

    m[adk::MediaPlayer::Tag::Album] = "This Isn't Magic";
    m[adk::MediaPlayer::Tag::Artist] = "Murphy";
    m[adk::MediaPlayer::Tag::Genre] = "Jazz";

    player->SetMetadata(m);

    EXPECT_TRUE(onMetadataChangedCalled);
    onMetadataChangedCalled = false;

    std::map<adk::MediaPlayer::Tag, std::string> metadata = player->GetMetadata();

    EXPECT_EQ(metadata[adk::MediaPlayer::Tag::Album], "This Isn't Magic");
    EXPECT_EQ(metadata[adk::MediaPlayer::Tag::Artist], "Murphy");
    EXPECT_EQ(metadata[adk::MediaPlayer::Tag::Genre], "Jazz");

    EXPECT_TRUE(player->SetSource(LOCAL_MEDIA_FILE_LONG));
    res = player->Play();
    if (res == adk::MediaPlayer::Async) {
        EXPECT_TRUE(WaitForObserver());
        EXPECT_TRUE(onAsyncDoneCalled);
        onAsyncDoneCalled = false;
    }

    EXPECT_TRUE(onMetadataChangedCalled);
    onMetadataChangedCalled = false;

    metadata = player->GetMetadata();

    EXPECT_EQ(metadata[adk::MediaPlayer::Tag::Album], "Dummy-Album");
    EXPECT_EQ(metadata[adk::MediaPlayer::Tag::Artist], "Dummy-Artist");
    EXPECT_EQ(metadata[adk::MediaPlayer::Tag::AudioBitrate], "80000");
    EXPECT_EQ(metadata[adk::MediaPlayer::Tag::Duration], "139319727891");
    EXPECT_EQ(metadata[adk::MediaPlayer::Tag::Genre], "Rock,Jazz");
    EXPECT_EQ(metadata[adk::MediaPlayer::Tag::Title], "Dummy-Title");
}
