/*
 * Copyright (c) 2019, The Linux Foundation. All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above
 *       copyright notice, this list of conditions and the following
 *       disclaimer in the documentation and/or other materials provided
 *       with the distribution.
 *     * Neither the name of The Linux Foundation nor the names of its
 *       contributors may be used to endorse or promote products derived
 *       from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS
 * BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
 * BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
 * IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include <gio/gio.h>
#include <glib-unix.h>
#include "mediaplayer-test.h"

class MPRISTest : public ::testing::Test {
 public:
    bool WaitForObserverSc() {
        cond_sc.wait(lock_sc);

        return true;
    }

    bool WaitForObserverAsync() {
        cond_async.wait(lock_async);

        return true;
    }

    void OnStateChanged(adk::MediaPlayer::State new_state) {
        onStateChangedCalled = true;
        current_state = new_state;

        cond_sc.notify_one();
    }

    void OnAsyncDone(void) {
        onAsyncDoneCalled = true;

        cond_async.notify_one();
    }

    void WaitPropertyChanged() {
        cond.wait(lock);
    }

    void OnNext(void) {
        onNextCalled = true;
    }

    void OnPrevious(void) {
        onPreviousCalled = true;
    }

    void OnLoopModeChanged(adk::MediaPlayer::LoopMode mode G_GNUC_UNUSED) {
        OnLoopModeChangedCalled = true;
    }

    void OnShuffleStatusChanged(bool status G_GNUC_UNUSED) {
        OnShuffleStatusChangedCalled = true;
    }

    void OnPlay(void) {
        onPlayCalled = true;
    }

    void OnPause(void) {
        onPauseCalled = true;
    }

    void OnStop(void) {
        onStopCalled = true;
    }

    static void GDBusPropertiesChangedCb(GDBusConnection *connection G_GNUC_UNUSED,
        const gchar *sender_name G_GNUC_UNUSED,
        const gchar *object_path G_GNUC_UNUSED,
        const gchar *interface_name G_GNUC_UNUSED,
        const gchar *signal_name G_GNUC_UNUSED,
        GVariant *parameters,
        gpointer user_data) {
        MPRISTest *mpris_test = (MPRISTest *)user_data;

        GVariantIter iter, array_iter;
        GVariant *value = NULL, *array_value = NULL;
        gsize length;
        const gchar *interface, *type, *key;

        g_variant_iter_init(&iter, parameters);
        value = g_variant_iter_next_value(&iter);
        interface = g_variant_get_string(value, &length);

        if (g_strcmp0(interface, "org.mpris.MediaPlayer2.Player") == 0) {
            value = g_variant_iter_next_value(&iter);
            type = g_variant_get_type_string(value);

            if (g_str_has_prefix(type, "a")) {
                if (g_variant_n_children(value) > 0) {
                    g_variant_iter_init(&array_iter, value);
                    while (g_variant_iter_loop(&array_iter, "{sv}", &key, &array_value)) {
                        if (g_strcmp0(key, mpris_test->watch_prop.c_str()) == 0) {
                            mpris_test->onPropertiesChanged = true;
                            mpris_test->changed_property = std::string(key);
                            mpris_test->property_value = array_value;
                            mpris_test->cond.notify_one();
                        }
                    }
                }
            }
        }
    }

    static void MainloopThread(MPRISTest *priv) {
        g_main_context_push_thread_default(priv->thread_main_context);

        auto *idle_source = g_idle_source_new();
        g_source_set_callback(idle_source, (GSourceFunc)MPRISTest::MainloopIsRunning, priv, NULL);
        g_source_attach(idle_source, priv->thread_main_context);

        g_main_loop_run(priv->main_loop);

        g_source_destroy(idle_source);
        g_main_context_pop_thread_default(priv->thread_main_context);
        g_source_unref(idle_source);
    }

    static gboolean MainloopIsRunning(MPRISTest *priv) {
        // Wake up Create() now that the mainloop is running
        priv->cond.notify_one();

        return FALSE;
    }

    static gboolean GDBusInit(gpointer user_data) {
        auto priv = (MPRISTest *)user_data;
        GError *error = NULL;

        do {
            const gchar *address = g_dbus_address_get_for_bus_sync(G_BUS_TYPE_SESSION, NULL, &error);

            if (error != NULL)
                break;

            const GDBusConnectionFlags flags = (GDBusConnectionFlags)(G_DBUS_CONNECTION_FLAGS_AUTHENTICATION_CLIENT | G_DBUS_CONNECTION_FLAGS_MESSAGE_BUS_CONNECTION);

            priv->connection = g_dbus_connection_new_for_address_sync(address,
                flags,
                NULL,
                NULL,
                &error);

            if (error != NULL)
                break;
        } while (false);

        if (error != NULL) {
            g_error_free(error);
            return TRUE;
        }

        return FALSE;
    }

    GDBusProxy *CreateProxy(std::string service, std::string object_path, std::string interface) {
        GError *error = NULL;

        GDBusProxy *proxy = g_dbus_proxy_new_sync(connection,  //Connection id
            G_DBUS_PROXY_FLAGS_NONE,                           //Flags used when constructing the proxy
            NULL,                                              //GDBusInterfaceInfo
            service.c_str(),                                   //A bus name
            object_path.c_str(),                               //An object path
            interface.c_str(),                                 //A D-Bus interface name
            NULL,                                              //GCancellable
            &error);

        if (error != NULL) {
            g_error_free(error);
            return nullptr;
        }

        return proxy;
    }

    static gboolean Subscribe(gpointer user_data) {
        auto priv = (MPRISTest *)user_data;

        guint res = g_dbus_connection_signal_subscribe(priv->connection,
            NULL,                               //listen to all senders
            "org.freedesktop.DBus.Properties",  //match on all interfaces
            "PropertiesChanged",
            "/org/mpris/MediaPlayer2",  //match on all object paths
            NULL,                       //match on all arguments
            G_DBUS_SIGNAL_FLAGS_NONE,
            MPRISTest::GDBusPropertiesChangedCb,
            priv,  //user_data argument passed to the callback function
            NULL);

        if (res == 0)
            return TRUE;

        // Release lock for property change signals.
        priv->cond.notify_one();

        return FALSE;
    }

    void SubscribeForPropertyChange() {
        subscription_source = g_idle_source_new();
        g_source_set_callback(subscription_source, (GSourceFunc)MPRISTest::Subscribe, this, NULL);
        g_source_attach(subscription_source, thread_main_context);

        // Wait till it actually subscribes for property change signals.
        cond.wait(lock);
    }

    GVariant *CallMethodOnDBus(GDBusProxy *proxy, std::string method, GVariant *arg) {
        GVariant *value = NULL;
        GError *error = NULL;

        if (proxy != NULL) {
            value = g_dbus_proxy_call_sync(proxy,  //A GDBusProxy
                method.c_str(),                    //Name of method to invoke
                arg,
                G_DBUS_CALL_FLAGS_NONE,
                -1,    //The timeout in milliseconds
                NULL,  //A GCancellable
                &error);
        }

        if (error) {
            g_error_free(error);
            return nullptr;
        }

        return value;
    }

    bool onStateChangedCalled = false, OnLoopModeChangedCalled = false, OnShuffleStatusChangedCalled = false, onPropertiesChanged = false,
         onNextCalled = false, onPreviousCalled = false, onAsyncDoneCalled = false, onPlayCalled = false, onPauseCalled = false, onStopCalled = false;
    std::string changed_property, watch_prop;
    adk::MediaPlayer::State current_state;

    GDBusConnection *connection;
    guint dbus_id;
    gsize length;
    GVariant *property_value = NULL;

 protected:
    virtual void SetUp() {
        lock_sc = std::unique_lock<std::mutex>(mutex_sc);
        lock_async = std::unique_lock<std::mutex>(mutex_async);

        thread_main_context = g_main_context_new();
        main_loop = g_main_loop_new(thread_main_context, FALSE);

        lock = std::unique_lock<std::mutex>(mutex);

        // Start up a thread for receiving callbacks from the pipeline's bus
        thread = std::thread(MPRISTest::MainloopThread, this);

        // Wait until the thread is actually created
        cond.wait(lock);

        source = g_idle_source_new();
        g_source_set_callback(source, (GSourceFunc)MPRISTest::GDBusInit, this, NULL);
        g_source_attach(source, thread_main_context);
    }

    virtual void TearDown() {
        if (connection) {
            g_dbus_connection_close_sync(connection, NULL, NULL);
            g_dbus_connection_flush_sync(connection, NULL, NULL);
            g_object_unref(connection);
            connection = NULL;
        }

        if (source) {
            g_source_destroy(source);
            g_source_unref(source);
        }

        if (subscription_source) {
            g_source_destroy(subscription_source);
            g_source_unref(subscription_source);
        }

        if (main_loop)
            g_main_loop_quit(main_loop);

        // Wait until the thread has actually shut down
        if (thread.joinable())
            thread.join();

        if (main_loop)
            g_main_loop_unref(main_loop);

        if (thread_main_context)
            g_main_context_unref(thread_main_context);

        changed_property = std::string();
        watch_prop = std::string();
        onPropertiesChanged = false;
    }

 private:
    std::mutex mutex_sc, mutex_async, mutex;
    std::unique_lock<std::mutex> lock_sc, lock_async, lock;
    std::condition_variable cond_sc, cond_async, cond;
    std::thread thread;
    GMainLoop *main_loop;
    GMainContext *thread_main_context;
    GSource *source = NULL, *subscription_source = NULL;
};

TEST_F(MPRISTest, CreationTest_MPRIS) {
    auto player = adk::MediaPlayer::Create("MPRIS-creation-test");

    GDBusProxy *proxy = CreateProxy("org.mpris.MediaPlayer2.MPRIS-creation-test",
        "/org/mpris/MediaPlayer2",
        "org.freedesktop.DBus.Introspectable");

    if (proxy != NULL) {
        EXPECT_NE(CallMethodOnDBus(proxy, "Introspect", NULL), nullptr);
        g_object_unref(proxy);
        proxy = NULL;
    }
}

TEST_F(MPRISTest, NextPreviousTest_MPRIS) {
    auto player = adk::MediaPlayer::Create("MPRIS-next-previous-test");

    auto onNext = std::bind(std::mem_fn(&MPRISTest::OnNext), this);
    auto onPrevious = std::bind(std::mem_fn(&MPRISTest::OnPrevious), this);

    player->SetNextCb(onNext);
    player->SetPreviousCb(onPrevious);

    GDBusProxy *proxy = CreateProxy("org.mpris.MediaPlayer2.MPRIS-next-previous-test",
        "/org/mpris/MediaPlayer2",
        "org.mpris.MediaPlayer2.Player");

    if (proxy != NULL) {
        EXPECT_NE(CallMethodOnDBus(proxy, "Next", NULL), nullptr);
    }

    EXPECT_TRUE(onNextCalled);
    onNextCalled = false;

    if (proxy != NULL) {
        EXPECT_NE(CallMethodOnDBus(proxy, "Previous", NULL), nullptr);
    }

    EXPECT_TRUE(onPreviousCalled);
    onPreviousCalled = false;
}

TEST_F(MPRISTest, LoopTest_MPRIS) {
    auto player = adk::MediaPlayer::Create("MPRIS-loop-test");

    auto onLoopModeChanged = std::bind(std::mem_fn(&MPRISTest::OnLoopModeChanged), this, _1);
    player->SetLoopModeChangeCb(onLoopModeChanged);

    watch_prop = "LoopStatus";
    SubscribeForPropertyChange();

    player->SetLoopMode(adk::MediaPlayer::LoopMode::Track);

    if (!onPropertiesChanged)
        WaitPropertyChanged();
    EXPECT_TRUE(onPropertiesChanged);
    onPropertiesChanged = false;
    EXPECT_EQ(changed_property, "LoopStatus");

    if (changed_property.compare("LoopStatus") == 0) {
        EXPECT_EQ(g_strcmp0(g_variant_get_string(property_value, &length), "Track"), 0);
    }
}

TEST_F(MPRISTest, ShuffleTest_MPRIS) {
    auto player = adk::MediaPlayer::Create("MPRIS-shuffle-test");

    auto onShuffleStatusChanged = std::bind(std::mem_fn(&MPRISTest::OnShuffleStatusChanged), this, _1);
    player->SetShuffleStatusChangeCb(onShuffleStatusChanged);

    watch_prop = "Shuffle";
    SubscribeForPropertyChange();

    player->SetShuffleStatus(true);

    if (!onPropertiesChanged)
        WaitPropertyChanged();
    EXPECT_TRUE(onPropertiesChanged);
    onPropertiesChanged = false;
    EXPECT_EQ(changed_property, "Shuffle");

    if (changed_property.compare("Shuffle") == 0) {
        EXPECT_TRUE(g_variant_get_boolean(property_value));
    }
}

TEST_F(MPRISTest, PlayTest_MPRIS) {
    auto player = adk::MediaPlayer::Create("MPRIS-play-test");

    watch_prop = "PlaybackStatus";
    SubscribeForPropertyChange();

    EXPECT_TRUE(player->SetSource(LOCAL_MEDIA_FILE_LONG));

    GDBusProxy *proxy = CreateProxy("org.mpris.MediaPlayer2.MPRIS-play-test",
        "/org/mpris/MediaPlayer2",
        "org.mpris.MediaPlayer2.Player");

    EXPECT_NE(CallMethodOnDBus(proxy, "Play", NULL), nullptr);

    if (!onPropertiesChanged)
        WaitPropertyChanged();
    EXPECT_TRUE(onPropertiesChanged);
    onPropertiesChanged = false;
    EXPECT_EQ(changed_property, "PlaybackStatus");

    if (changed_property.compare("PlaybackStatus") == 0) {
        EXPECT_EQ(g_strcmp0(g_variant_get_string(property_value, &length), "Playing"), 0);
    }
}

TEST_F(MPRISTest, PauseTest_MPRIS) {
    auto player = adk::MediaPlayer::Create("MPRIS-pause-test");

    auto onStateChanged = std::bind(std::mem_fn(&MPRISTest::OnStateChanged), this, _1);
    player->SetOnStateChangedCb(onStateChanged);

    EXPECT_TRUE(player->SetSource(LOCAL_MEDIA_FILE_LONG));

    player->Play();

    EXPECT_TRUE(WaitForObserverSc());
    EXPECT_TRUE(onStateChangedCalled);
    onStateChangedCalled = false;

    EXPECT_EQ(current_state, adk::MediaPlayer::State::Playing);

    GDBusProxy *proxy = CreateProxy("org.mpris.MediaPlayer2.MPRIS-pause-test",
        "/org/mpris/MediaPlayer2",
        "org.mpris.MediaPlayer2.Player");

    watch_prop = "PlaybackStatus";
    SubscribeForPropertyChange();

    EXPECT_NE(CallMethodOnDBus(proxy, "Pause", NULL), nullptr);

    if (!onPropertiesChanged)
        WaitPropertyChanged();
    EXPECT_TRUE(onPropertiesChanged);
    onPropertiesChanged = false;
    EXPECT_EQ(changed_property, "PlaybackStatus");

    if (changed_property.compare("PlaybackStatus") == 0) {
        EXPECT_EQ(g_strcmp0(g_variant_get_string(property_value, &length), "Paused"), 0);
    }
}

TEST_F(MPRISTest, StopTest_MPRIS) {
    auto player = adk::MediaPlayer::Create("MPRIS-stop-test");

    auto onStateChanged = std::bind(std::mem_fn(&MPRISTest::OnStateChanged), this, _1);
    player->SetOnStateChangedCb(onStateChanged);

    EXPECT_TRUE(player->SetSource(LOCAL_MEDIA_FILE_LONG));

    player->Play();

    EXPECT_TRUE(WaitForObserverSc());
    EXPECT_TRUE(onStateChangedCalled);
    onStateChangedCalled = false;

    EXPECT_EQ(current_state, adk::MediaPlayer::State::Playing);

    GDBusProxy *proxy = CreateProxy("org.mpris.MediaPlayer2.MPRIS-stop-test",
        "/org/mpris/MediaPlayer2",
        "org.mpris.MediaPlayer2.Player");

    watch_prop = "PlaybackStatus";
    SubscribeForPropertyChange();

    EXPECT_NE(CallMethodOnDBus(proxy, "Stop", NULL), nullptr);

    if (!onPropertiesChanged)
        WaitPropertyChanged();
    EXPECT_TRUE(onPropertiesChanged);
    onPropertiesChanged = false;
    EXPECT_EQ(changed_property, "PlaybackStatus");

    if (changed_property.compare("PlaybackStatus") == 0) {
        EXPECT_EQ(g_strcmp0(g_variant_get_string(property_value, &length), "Stopped"), 0);
    }
}

TEST_F(MPRISTest, PlayPauseTest_MPRIS) {
    auto player = adk::MediaPlayer::Create("MPRIS-playbackstatus-test");

    auto onStateChanged = std::bind(std::mem_fn(&MPRISTest::OnStateChanged), this, _1);
    player->SetOnStateChangedCb(onStateChanged);

    EXPECT_TRUE(player->SetSource(LOCAL_MEDIA_FILE_LONG));

    player->Play();

    EXPECT_TRUE(WaitForObserverSc());
    EXPECT_TRUE(onStateChangedCalled);
    onStateChangedCalled = false;

    EXPECT_EQ(current_state, adk::MediaPlayer::State::Playing);

    GDBusProxy *proxy = CreateProxy("org.mpris.MediaPlayer2.MPRIS-playbackstatus-test",
        "/org/mpris/MediaPlayer2",
        "org.mpris.MediaPlayer2.Player");

    watch_prop = "PlaybackStatus";
    SubscribeForPropertyChange();

    EXPECT_NE(CallMethodOnDBus(proxy, "PlayPause", NULL), nullptr);

    if (!onPropertiesChanged)
        WaitPropertyChanged();
    EXPECT_TRUE(onPropertiesChanged);
    onPropertiesChanged = false;
    EXPECT_EQ(changed_property, "PlaybackStatus");

    if (changed_property.compare("PlaybackStatus") == 0) {
        EXPECT_EQ(g_strcmp0(g_variant_get_string(property_value, &length), "Paused"), 0);
    }

    EXPECT_NE(CallMethodOnDBus(proxy, "PlayPause", NULL), nullptr);

    if (!onPropertiesChanged)
        WaitPropertyChanged();
    EXPECT_TRUE(onPropertiesChanged);
    onPropertiesChanged = false;
    EXPECT_EQ(changed_property, "PlaybackStatus");

    if (changed_property.compare("PlaybackStatus") == 0) {
        EXPECT_EQ(g_strcmp0(g_variant_get_string(property_value, &length), "Playing"), 0);
    }
}

TEST_F(MPRISTest, PlayPauseStopCbTest_MPRIS) {
    auto player = adk::MediaPlayer::Create("MPRIS-play-pause-stop-cb-test");

    auto onPlay = std::bind(std::mem_fn(&MPRISTest::OnPlay), this);
    auto onPause = std::bind(std::mem_fn(&MPRISTest::OnPause), this);
    auto onStop = std::bind(std::mem_fn(&MPRISTest::OnStop), this);
    player->SetPlayCb(onPlay);
    player->SetPauseCb(onPause);
    player->SetStopCb(onStop);

    EXPECT_TRUE(player->SetSource(LOCAL_MEDIA_FILE_LONG));

    GDBusProxy *proxy = CreateProxy("org.mpris.MediaPlayer2.MPRIS-play-pause-stop-cb-test",
        "/org/mpris/MediaPlayer2",
        "org.mpris.MediaPlayer2.Player");

    EXPECT_NE(CallMethodOnDBus(proxy, "Play", NULL), nullptr);

    EXPECT_TRUE(onPlayCalled);
    onPlayCalled = false;

    EXPECT_NE(CallMethodOnDBus(proxy, "Pause", NULL), nullptr);

    EXPECT_TRUE(onPauseCalled);
    onPauseCalled = false;

    EXPECT_NE(CallMethodOnDBus(proxy, "PlayPause", NULL), nullptr);

    EXPECT_TRUE(onPlayCalled);
    onPlayCalled = false;

    EXPECT_NE(CallMethodOnDBus(proxy, "Stop", NULL), nullptr);

    EXPECT_TRUE(onStopCalled);
    onStopCalled = false;
}

TEST_F(MPRISTest, OpenUriTest_MPRIS) {
    auto player = adk::MediaPlayer::Create("MPRIS-openuri-test");

    auto onStateChanged = std::bind(std::mem_fn(&MPRISTest::OnStateChanged), this, _1);
    player->SetOnStateChangedCb(onStateChanged);

    GDBusProxy *proxy = CreateProxy("org.mpris.MediaPlayer2.MPRIS-openuri-test",
        "/org/mpris/MediaPlayer2",
        "org.mpris.MediaPlayer2.Player");

    watch_prop = "PlaybackStatus";
    SubscribeForPropertyChange();

    EXPECT_NE(CallMethodOnDBus(proxy, "OpenUri", g_variant_new("(s)", LOCAL_MEDIA_FILE_LONG)), nullptr);

    if (!onPropertiesChanged)
        WaitPropertyChanged();
    EXPECT_TRUE(onPropertiesChanged);
    onPropertiesChanged = false;
    EXPECT_EQ(changed_property, "PlaybackStatus");

    if (changed_property.compare("PlaybackStatus") == 0) {
        EXPECT_EQ(g_strcmp0(g_variant_get_string(property_value, &length), "Playing"), 0);
    }
}

TEST_F(MPRISTest, SeekTest_MPRIS) {
    auto player = adk::MediaPlayer::Create("MPRIS-seek-test");

    auto onStateChanged = std::bind(std::mem_fn(&MPRISTest::OnStateChanged), this, _1);
    auto onAsyncDone = std::bind(std::mem_fn(&MPRISTest::OnAsyncDone), this);
    auto onNext = std::bind(std::mem_fn(&MPRISTest::OnNext), this);

    player->SetNextCb(onNext);
    player->SetOnAsyncDoneCb(onAsyncDone);
    player->SetOnStateChangedCb(onStateChanged);

    EXPECT_TRUE(player->SetSource(LOCAL_MEDIA_FILE_LONG));

    player->Play();

    EXPECT_TRUE(WaitForObserverSc());
    EXPECT_TRUE(onStateChangedCalled);
    onStateChangedCalled = false;

    EXPECT_EQ(current_state, adk::MediaPlayer::State::Playing);

    std::this_thread::sleep_for(std::chrono::seconds(2));

    GDBusProxy *proxy = CreateProxy("org.mpris.MediaPlayer2.MPRIS-seek-test",
        "/org/mpris/MediaPlayer2",
        "org.mpris.MediaPlayer2.Player");

    // when seeked time is within duration
    EXPECT_NE(CallMethodOnDBus(proxy, "Seek", g_variant_new("(x)", G_GINT64_CONSTANT(10000000))), nullptr);

    EXPECT_TRUE(WaitForObserverAsync());
    EXPECT_TRUE(onAsyncDoneCalled);
    onAsyncDoneCalled = false;

    std::chrono::duration<double> position = player->GetPosition();
    EXPECT_TRUE(position.count() > 11 && position.count() < 13);

    // when seeked time is before duration
    EXPECT_NE(CallMethodOnDBus(proxy, "Seek", g_variant_new("(x)", G_GINT64_CONSTANT(-20000000))), nullptr);

    EXPECT_TRUE(WaitForObserverAsync());
    EXPECT_TRUE(onAsyncDoneCalled);
    onAsyncDoneCalled = false;

    std::this_thread::sleep_for(std::chrono::seconds(2));

    position = player->GetPosition();
    EXPECT_TRUE(position.count() > 1 && position.count() < 3);

    // when seeked time is beyond duration
    EXPECT_NE(CallMethodOnDBus(proxy, "Seek", g_variant_new("(x)", G_GINT64_CONSTANT(2000000000))), nullptr);

    EXPECT_TRUE(onNextCalled);
    onNextCalled = false;
}

TEST_F(MPRISTest, SetPositionTest_MPRIS) {
    auto player = adk::MediaPlayer::Create("MPRIS-set-position-test");

    auto onStateChanged = std::bind(std::mem_fn(&MPRISTest::OnStateChanged), this, _1);
    player->SetOnStateChangedCb(onStateChanged);

    EXPECT_TRUE(player->SetSource(LOCAL_MEDIA_FILE_LONG));

    player->Play();

    EXPECT_TRUE(WaitForObserverSc());
    EXPECT_TRUE(onStateChangedCalled);
    onStateChangedCalled = false;

    EXPECT_EQ(current_state, adk::MediaPlayer::State::Playing);

    GDBusProxy *proxy = CreateProxy("org.mpris.MediaPlayer2.MPRIS-set-position-test",
        "/org/mpris/MediaPlayer2",
        "org.mpris.MediaPlayer2.Player");

    // Valid Track id
    EXPECT_NE(CallMethodOnDBus(proxy, "SetPosition", g_variant_new("(ox)", "/org/mpris/MediaPlayer2/Track/0", G_GINT64_CONSTANT(10000000))), nullptr);

    EXPECT_TRUE(WaitForObserverSc());
    EXPECT_TRUE(onStateChangedCalled);
    onStateChangedCalled = false;

    std::chrono::duration<double> position = player->GetPosition();
    EXPECT_TRUE(position.count() > 9 && position.count() < 11);

    // Invalid Track id
    EXPECT_NE(CallMethodOnDBus(proxy, "SetPosition", g_variant_new("(ox)", "/org/mpris/MediaPlayer2/TrackList/NoTrack", G_GINT64_CONSTANT(10000000))), nullptr);

    position = player->GetPosition();
    EXPECT_FALSE(position.count() > 19 && position.count() < 21);
}

TEST_F(MPRISTest, PlaybackStatusTest_MPRIS) {
    auto player = adk::MediaPlayer::Create("MPRIS-playbackstatus-test");

    auto onStateChanged = std::bind(std::mem_fn(&MPRISTest::OnStateChanged), this, _1);
    player->SetOnStateChangedCb(onStateChanged);

    watch_prop = "PlaybackStatus";
    SubscribeForPropertyChange();

    EXPECT_TRUE(player->SetSource(LOCAL_MEDIA_FILE_LONG));

    player->Play();

    EXPECT_TRUE(WaitForObserverSc());
    EXPECT_TRUE(onStateChangedCalled);
    onStateChangedCalled = false;

    if (!onPropertiesChanged)
        WaitPropertyChanged();
    EXPECT_TRUE(onPropertiesChanged);
    onPropertiesChanged = false;
    EXPECT_EQ(changed_property, "PlaybackStatus");

    if (changed_property.compare("PlaybackStatus") == 0) {
        EXPECT_EQ(g_strcmp0(g_variant_get_string(property_value, &length), "Playing"), 0);
    }
}

TEST_F(MPRISTest, PositionTest_MPRIS) {
    auto player = adk::MediaPlayer::Create("MPRIS-position-test");

    auto onStateChanged = std::bind(std::mem_fn(&MPRISTest::OnStateChanged), this, _1);
    player->SetOnStateChangedCb(onStateChanged);

    EXPECT_TRUE(player->SetSource(LOCAL_MEDIA_FILE_LONG));

    player->Play();

    EXPECT_TRUE(WaitForObserverSc());
    EXPECT_TRUE(onStateChangedCalled);
    onStateChangedCalled = false;

    std::this_thread::sleep_for(std::chrono::seconds(2));

    GDBusProxy *proxy = CreateProxy("org.mpris.MediaPlayer2.MPRIS-position-test",
        "/org/mpris/MediaPlayer2",
        "org.freedesktop.DBus.Properties");

    GVariant *value = CallMethodOnDBus(proxy, "Get", g_variant_new("(ss)", "org.mpris.MediaPlayer2.Player", "Position"));
    EXPECT_NE(value, nullptr);

    GVariant *tmp = NULL;
    g_variant_get(value, "(v)", &tmp);

    gint64 p = g_variant_get_int64(tmp);

    std::chrono::duration<double> position = std::chrono::microseconds(p);
    EXPECT_TRUE(position.count() > 1 && position.count() < 3);

    if (tmp)
        g_variant_unref(tmp);
    if (value)
        g_variant_unref(value);
}

TEST_F(MPRISTest, MetadataTest_MPRIS) {
    auto player = adk::MediaPlayer::Create("MPRIS-metadata-test");

    watch_prop = "Metadata";
    SubscribeForPropertyChange();

    EXPECT_TRUE(player->SetSource(LOCAL_MEDIA_FILE_LONG));

    player->Play();

    if (!onPropertiesChanged)
        WaitPropertyChanged();
    EXPECT_TRUE(onPropertiesChanged);
    onPropertiesChanged = false;
    EXPECT_EQ(changed_property, "Metadata");

    if (changed_property.compare("Metadata") == 0) {
        GVariantIter iter;
        GVariant *value;
        const gchar *key, *prop;

        g_variant_iter_init(&iter, property_value);
        while (g_variant_iter_loop(&iter, "{sv}", &key, &value)) {
            if (g_strcmp0(key, "xesam:album") == 0 || g_strcmp0(key, "xesam:title") == 0 || g_strcmp0(key, "xesam:url") == 0 || g_strcmp0(key, "mpris:artUrl") == 0 || g_strcmp0(key, "adk:SampleFormat") == 0 || g_strcmp0(key, "xesam:audioChannels") == 0) {
                g_variant_get(value, "(s)", &prop);

                if (g_strcmp0(key, "xesam:album") == 0) {
                    EXPECT_EQ(g_strcmp0(prop, "Dummy-Album"), 0);
                } else if (g_strcmp0(key, "xesam:title") == 0) {
                    EXPECT_EQ(g_strcmp0(prop, "Dummy-Title"), 0);
                } else if (g_strcmp0(key, "xesam:url") == 0) {
                    EXPECT_EQ(g_strcmp0(prop, DATA_PATH), 0);
                } else if (g_strcmp0(key, "adk:SampleFormat") == 0) {
                    EXPECT_EQ(g_strcmp0(prop, "F32LE"), 0);
                } else if (g_strcmp0(key, "xesam:audioChannels") == 0) {
                    EXPECT_EQ(g_strcmp0(prop, "1"), 0);
                }
            } else if (g_strcmp0(key, "mpris:length") == 0) {
                gint64 tmp;
                g_variant_get(value, "(x)", &tmp);
                EXPECT_EQ(tmp, (int64_t)139319727);
            } else if (g_strcmp0(key, "xesam:audioBitrate") == 0) {
                gint32 tmp;
                g_variant_get(value, "(i)", &tmp);
                EXPECT_EQ(tmp, (int32_t)80000);
            } else if (g_strcmp0(key, "xesam:audioSampleRate") == 0) {
                gdouble tmp;
                g_variant_get(value, "(d)", &tmp);
                EXPECT_EQ(tmp, 44100);
            } else if (g_strcmp0(key, "xesam:artist") == 0 || g_strcmp0(key, "xesam:genre") == 0) {
                GVariantIter iter_prop;
                std::vector<std::string> list;

                g_variant_iter_init(&iter_prop, value);
                while (g_variant_iter_loop(&iter_prop, "s", &prop)) {
                    list.push_back((std::string)prop);
                }

                if (strcmp(key, "xesam:artist") == 0) {
                    EXPECT_EQ(list[0], "Dummy-Artist");
                } else if (strcmp(key, "xesam:genre") == 0) {
                    EXPECT_EQ(list[0], "Rock");
                    EXPECT_EQ(list[1], "Jazz");
                }
            } else if (strcmp(key, "mpris:trackid") == 0) {
                g_variant_get(value, "(o)", &prop);
                EXPECT_EQ(g_strcmp0(prop, "/org/mpris/MediaPlayer2/Track/0"), 0);
            }
        }

        if (value)
            g_variant_unref(value);
    }
}

/*TEST_F(MPRISTest, VolumeTest_MPRIS) {
    auto player = adk::MediaPlayer::Create("MPRIS-volume-test");

    watch_prop = "Volume";
    SubscribeForPropertyChange();

    GDBusProxy *proxy = CreateProxy("org.mpris.MediaPlayer2.MPRIS-volume-test",
        "/org/mpris/MediaPlayer2",
        "org.freedesktop.DBus.Properties");

    EXPECT_NE(CallMethodOnDBus(proxy, "Set",
                  g_variant_new("(ssv)",
                      "org.mpris.MediaPlayer2.Player",
                      "Volume",
                      g_variant_new("d", 0.65))),
        nullptr);

    if (!onPropertiesChanged)
        WaitPropertyChanged();
    EXPECT_TRUE(onPropertiesChanged);
    onPropertiesChanged = false;
    EXPECT_EQ(changed_property, "Volume");

    if (changed_property.compare("Volume") == 0) {
        EXPECT_EQ(g_variant_get_double(property_value), 0.65);
    }
}*/

TEST_F(MPRISTest, RateTest_MPRIS) {
    auto player = adk::MediaPlayer::Create("MPRIS-rate-test");

    GDBusProxy *proxy = CreateProxy("org.mpris.MediaPlayer2.MPRIS-rate-test",
        "/org/mpris/MediaPlayer2",
        "org.freedesktop.DBus.Properties");

    watch_prop = "Rate";
    SubscribeForPropertyChange();

    GVariant *value = CallMethodOnDBus(proxy, "Get", g_variant_new("(ss)", "org.mpris.MediaPlayer2.Player", "Volume"));
    EXPECT_NE(value, nullptr);

    GVariant *tmp = NULL;
    g_variant_get(value, "(v)", &tmp);

    gdouble rate = g_variant_get_double(tmp);
    EXPECT_EQ(rate, 1.0);

    EXPECT_NE(CallMethodOnDBus(proxy, "Set",
                  g_variant_new("(ssv)",
                      "org.mpris.MediaPlayer2.Player",
                      "Rate",
                      g_variant_new("d", 2.50))),
        nullptr);

    if (!onPropertiesChanged)
        WaitPropertyChanged();
    EXPECT_TRUE(onPropertiesChanged);
    onPropertiesChanged = false;
    EXPECT_EQ(changed_property, "Rate");

    if (changed_property.compare("Rate") == 0) {
        EXPECT_EQ(g_variant_get_double(property_value), 1.0);
    }

    if (tmp)
        g_variant_unref(tmp);
    if (value)
        g_variant_unref(value);
}

TEST_F(MPRISTest, PropertiesTest_MPRIS) {
    auto player = adk::MediaPlayer::Create("MPRIS-properties-test");

    SubscribeForPropertyChange();

    GDBusProxy *proxy = CreateProxy("org.mpris.MediaPlayer2.MPRIS-properties-test",
        "/org/mpris/MediaPlayer2",
        "org.freedesktop.DBus.Properties");

    /// Test case for Minimum Rate Property.
    GVariant *value = CallMethodOnDBus(proxy, "Get", g_variant_new("(ss)", "org.mpris.MediaPlayer2.Player", "MinimumRate"));
    EXPECT_NE(value, nullptr);

    GVariant *tmp = NULL;
    g_variant_get(value, "(v)", &tmp);

    gdouble min_rate = g_variant_get_double(tmp);
    EXPECT_EQ(min_rate, 1.0);

    /// Test case for Maximum Rate Property.
    value = CallMethodOnDBus(proxy, "Get", g_variant_new("(ss)", "org.mpris.MediaPlayer2.Player", "MaximumRate"));
    EXPECT_NE(value, nullptr);

    g_variant_get(value, "(v)", &tmp);

    gdouble max_rate = g_variant_get_double(tmp);
    EXPECT_EQ(max_rate, 1.0);

    /// Test case for CanGoNext Property.
    value = CallMethodOnDBus(proxy, "Get", g_variant_new("(ss)", "org.mpris.MediaPlayer2.Player", "CanGoNext"));
    EXPECT_NE(value, nullptr);

    g_variant_get(value, "(v)", &tmp);
    EXPECT_FALSE(g_variant_get_boolean(tmp));

    auto onNext = std::bind(std::mem_fn(&MPRISTest::OnNext), this);
    player->SetNextCb(onNext);

    value = CallMethodOnDBus(proxy, "Get", g_variant_new("(ss)", "org.mpris.MediaPlayer2.Player", "CanGoNext"));
    EXPECT_NE(value, nullptr);

    g_variant_get(value, "(v)", &tmp);
    EXPECT_TRUE(g_variant_get_boolean(tmp));

    /// Test case for CanGoPrevious Property.
    value = CallMethodOnDBus(proxy, "Get", g_variant_new("(ss)", "org.mpris.MediaPlayer2.Player", "CanGoPrevious"));
    EXPECT_NE(value, nullptr);

    g_variant_get(value, "(v)", &tmp);
    EXPECT_FALSE(g_variant_get_boolean(tmp));

    auto onPrevious = std::bind(std::mem_fn(&MPRISTest::OnPrevious), this);
    player->SetPreviousCb(onPrevious);

    value = CallMethodOnDBus(proxy, "Get", g_variant_new("(ss)", "org.mpris.MediaPlayer2.Player", "CanGoPrevious"));
    EXPECT_NE(value, nullptr);

    g_variant_get(value, "(v)", &tmp);
    EXPECT_TRUE(g_variant_get_boolean(tmp));

    /// Test case for CanPlay Property.
    value = CallMethodOnDBus(proxy, "Get", g_variant_new("(ss)", "org.mpris.MediaPlayer2.Player", "CanPlay"));
    EXPECT_NE(value, nullptr);

    g_variant_get(value, "(v)", &tmp);
    EXPECT_TRUE(g_variant_get_boolean(tmp));

    /// Test case for CanPause Property.
    value = CallMethodOnDBus(proxy, "Get", g_variant_new("(ss)", "org.mpris.MediaPlayer2.Player", "CanPause"));
    EXPECT_NE(value, nullptr);

    g_variant_get(value, "(v)", &tmp);
    EXPECT_TRUE(g_variant_get_boolean(tmp));

    /// Test case for CanSeek Property.
    value = CallMethodOnDBus(proxy, "Get", g_variant_new("(ss)", "org.mpris.MediaPlayer2.Player", "CanSeek"));
    EXPECT_NE(value, nullptr);

    g_variant_get(value, "(v)", &tmp);
    EXPECT_FALSE(g_variant_get_boolean(tmp));

    auto onStateChanged = std::bind(std::mem_fn(&MPRISTest::OnStateChanged), this, _1);
    player->SetOnStateChangedCb(onStateChanged);

    EXPECT_TRUE(player->SetSource("file://" LOCAL_MEDIA_MP4_FILE));

    player->Play();

    EXPECT_TRUE(WaitForObserverSc());
    EXPECT_TRUE(onStateChangedCalled);
    onStateChangedCalled = false;

    EXPECT_EQ(current_state, adk::MediaPlayer::State::Playing);

    value = CallMethodOnDBus(proxy, "Get", g_variant_new("(ss)", "org.mpris.MediaPlayer2.Player", "CanSeek"));
    EXPECT_NE(value, nullptr);

    g_variant_get(value, "(v)", &tmp);
    EXPECT_TRUE(g_variant_get_boolean(tmp));

    /// Test case for CanLoop Property.
    value = CallMethodOnDBus(proxy, "Get", g_variant_new("(ss)", "org.mpris.MediaPlayer2.Player", "CanLoop"));
    EXPECT_NE(value, nullptr);

    g_variant_get(value, "(v)", &tmp);
    EXPECT_FALSE(g_variant_get_boolean(tmp));

    auto onLoopModeChanged = std::bind(std::mem_fn(&MPRISTest::OnLoopModeChanged), this, _1);
    player->SetLoopModeChangeCb(onLoopModeChanged);

    value = CallMethodOnDBus(proxy, "Get", g_variant_new("(ss)", "org.mpris.MediaPlayer2.Player", "CanLoop"));
    EXPECT_NE(value, nullptr);

    g_variant_get(value, "(v)", &tmp);
    EXPECT_TRUE(g_variant_get_boolean(tmp));

    /// Test case for CanShuffle Property.
    value = CallMethodOnDBus(proxy, "Get", g_variant_new("(ss)", "org.mpris.MediaPlayer2.Player", "CanShuffle"));
    EXPECT_NE(value, nullptr);

    g_variant_get(value, "(v)", &tmp);
    EXPECT_FALSE(g_variant_get_boolean(tmp));

    auto onShuffleStatusChanged = std::bind(std::mem_fn(&MPRISTest::OnShuffleStatusChanged), this, _1);
    player->SetShuffleStatusChangeCb(onShuffleStatusChanged);

    value = CallMethodOnDBus(proxy, "Get", g_variant_new("(ss)", "org.mpris.MediaPlayer2.Player", "CanShuffle"));
    EXPECT_NE(value, nullptr);

    g_variant_get(value, "(v)", &tmp);
    EXPECT_TRUE(g_variant_get_boolean(tmp));

    if (tmp)
        g_variant_unref(tmp);
    if (value)
        g_variant_unref(value);
}
