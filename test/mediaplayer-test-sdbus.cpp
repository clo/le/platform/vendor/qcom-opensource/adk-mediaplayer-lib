/*
 * Copyright (c) 2019, The Linux Foundation. All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above
 *       copyright notice, this list of conditions and the following
 *       disclaimer in the documentation and/or other materials provided
 *       with the distribution.
 *     * Neither the name of The Linux Foundation nor the names of its
 *       contributors may be used to endorse or promote products derived
 *       from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS
 * BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
 * BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
 * IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include <systemdq/sd-bus.h>
#include "mediaplayer-test.h"

class MPRISTest : public ::testing::Test {
 public:
    bool WaitForObserverSc() {
        cond_sc.wait(lock_sc);

        return true;
    }

    bool WaitForObserverAsync() {
        cond_async.wait(lock_async);

        return true;
    }

    void OnStateChanged(adk::MediaPlayer::State new_state) {
        onStateChangedCalled = true;
        current_state = new_state;

        cond_sc.notify_one();
    }

    void OnAsyncDone(void) {
        onAsyncDoneCalled = true;

        cond_async.notify_one();
    }

    void OnNext(void) {
        onNextCalled = true;
    }

    void OnPrevious(void) {
        onPreviousCalled = true;
    }

    void OnLoopModeChanged(adk::MediaPlayer::LoopMode mode G_GNUC_UNUSED) {
        OnLoopModeChangedCalled = true;
    }

    void OnShuffleStatusChanged(bool status G_GNUC_UNUSED) {
        OnShuffleStatusChangedCalled = true;
    }

    void OnPlay(void) {
        onPlayCalled = true;
    }

    void OnPause(void) {
        onPauseCalled = true;
    }

    void OnStop(void) {
        onStopCalled = true;
    }

    static int properties_changed_cb(sd_bus_message *m G_GNUC_UNUSED, void *userdata, sd_bus_error *err G_GNUC_UNUSED) {
        MPRISTest *mpris_test = (MPRISTest *)userdata;

        mpris_test->onPropertiesChanged = true;

        return 1;
    }

    bool onStateChangedCalled = false, OnLoopModeChangedCalled = false, OnShuffleStatusChangedCalled = false, onPropertiesChanged = false,
         onNextCalled = false, onPreviousCalled = false, onAsyncDoneCalled = false, onPlayCalled = false, onPauseCalled = false, onStopCalled = false;
    adk::MediaPlayer::State current_state;

    sd_bus_message *m = NULL;
    sd_bus *bus;

    const char *read_msg;

 protected:
    virtual void SetUp() {
        lock_sc = std::unique_lock<std::mutex>(mutex_sc);
        lock_async = std::unique_lock<std::mutex>(mutex_async);

        int r = sd_bus_open_user(&bus);
        if (r < 0) {
            return;
        }
    }

    virtual void TearDown() {
        sd_bus_flush_close_unref(bus);
    }

 private:
    std::mutex mutex_sc, mutex_async;
    std::unique_lock<std::mutex> lock_sc, lock_async;
    std::condition_variable cond_sc, cond_async;
};

TEST_F(MPRISTest, CreationTest_MPRIS) {
    auto player = adk::MediaPlayer::Create("MPRIS-creation-test");
    /* Issue the method call and store the respons message in m */
    int r = sd_bus_call_method(bus,
        "org.mpris.MediaPlayer2.MPRIS-creation-test", /* service to contact */
        "/org/mpris/MediaPlayer2",                    /* object path */
        "org.freedesktop.DBus.Introspectable",        /* interface name */
        "Introspect",                                 /* method name */
        NULL,                                         /* object to return error in */
        &m,                                           /* return message on success */
        NULL);                                        /* input signature */

    EXPECT_GT(r, 0);

    sd_bus_message_unref(m);
}

TEST_F(MPRISTest, NextPreviousTest_MPRIS) {
    auto player = adk::MediaPlayer::Create("MPRIS-next-previous-test");

    auto onNext = std::bind(std::mem_fn(&MPRISTest::OnNext), this);
    auto onPrevious = std::bind(std::mem_fn(&MPRISTest::OnPrevious), this);

    player->SetNextCb(onNext);
    player->SetPreviousCb(onPrevious);

    int r = sd_bus_call_method(bus,
        "org.mpris.MediaPlayer2.MPRIS-next-previous-test",
        "/org/mpris/MediaPlayer2",
        "org.mpris.MediaPlayer2.Player",
        "Next",
        NULL,
        &m,
        NULL);

    EXPECT_GT(r, 0);

    sd_bus_message_unref(m);

    EXPECT_TRUE(onNextCalled);
    onNextCalled = false;

    r = sd_bus_call_method(bus,
        "org.mpris.MediaPlayer2.MPRIS-next-previous-test",
        "/org/mpris/MediaPlayer2",
        "org.mpris.MediaPlayer2.Player",
        "Previous",
        NULL,
        &m,
        NULL);

    EXPECT_GT(r, 0);

    sd_bus_message_unref(m);

    EXPECT_TRUE(onPreviousCalled);
    onPreviousCalled = false;
}

TEST_F(MPRISTest, LoopTest_MPRIS) {
    auto player = adk::MediaPlayer::Create("MPRIS-loop-test");

    auto onLoopModeChanged = std::bind(std::mem_fn(&MPRISTest::OnLoopModeChanged), this, _1);
    player->SetLoopModeChangeCb(onLoopModeChanged);

    std::string rule;
    rule = "type='signal',";
    rule += "sender='org.mpris.MediaPlayer2.MPRIS-loop-test',";
    rule += "interface='org.freedesktop.DBus.Properties',member='PropertiesChanged'";

    int ret = sd_bus_add_match(bus, NULL, rule.c_str(), properties_changed_cb, this);
    EXPECT_GT(ret, 0);

    player->SetLoopMode(adk::MediaPlayer::LoopMode::Track);
    int r;

    for (;;) {
        r = sd_bus_process(bus, NULL);
        if ((r < 0) || (r > 0 && onPropertiesChanged == true))
            break;

        if (r > 0)
            continue;

        r = sd_bus_wait(bus, (uint64_t)-1);
        if (r < 0)
            break;
    }

    EXPECT_TRUE(onPropertiesChanged);
    onPropertiesChanged = false;

    r = sd_bus_call_method(bus,
        "org.mpris.MediaPlayer2.MPRIS-loop-test",
        "/org/mpris/MediaPlayer2",
        "org.freedesktop.DBus.Properties",
        "Get",
        NULL,
        &m,
        "ss",
        "org.mpris.MediaPlayer2.Player",
        "LoopStatus");

    EXPECT_GT(r, 0);

    const char *loop_mode;

    r = sd_bus_message_enter_container(m, 'v', "s");
    EXPECT_GT(r, 0);

    r = sd_bus_message_read_basic(m, 's', &loop_mode);
    EXPECT_GT(r, 0);

    r = sd_bus_message_exit_container(m);
    EXPECT_GT(r, 0);

    EXPECT_EQ(strcmp(loop_mode, "Track"), 0);
}

TEST_F(MPRISTest, ShuffleTest_MPRIS) {
    auto player = adk::MediaPlayer::Create("MPRIS-shuffle-test");

    auto onShuffleStatusChanged = std::bind(std::mem_fn(&MPRISTest::OnShuffleStatusChanged), this, _1);
    player->SetShuffleStatusChangeCb(onShuffleStatusChanged);

    std::string rule;
    rule = "type='signal',";
    rule += "sender='org.mpris.MediaPlayer2.MPRIS-shuffle-test',";
    rule += "interface='org.freedesktop.DBus.Properties',member='PropertiesChanged'";

    int ret = sd_bus_add_match(bus, NULL, rule.c_str(), properties_changed_cb, this);
    EXPECT_GT(ret, 0);

    player->SetShuffleStatus(true);
    int r;

    for (;;) {
        r = sd_bus_process(bus, NULL);
        if ((r < 0) || (r > 0 && onPropertiesChanged == true))
            break;

        if (r > 0)
            continue;

        r = sd_bus_wait(bus, (uint64_t)-1);
        if (r < 0)
            break;
    }

    EXPECT_TRUE(onPropertiesChanged);
    onPropertiesChanged = false;

    r = sd_bus_call_method(bus,
        "org.mpris.MediaPlayer2.MPRIS-shuffle-test",
        "/org/mpris/MediaPlayer2",
        "org.freedesktop.DBus.Properties",
        "Get",
        NULL,
        &m,
        "ss",
        "org.mpris.MediaPlayer2.Player",
        "Shuffle");

    EXPECT_GT(r, 0);

    bool shuffle_status;

    r = sd_bus_message_enter_container(m, 'v', "b");
    EXPECT_GT(r, 0);

    r = sd_bus_message_read_basic(m, 'b', &shuffle_status);
    EXPECT_GT(r, 0);

    r = sd_bus_message_exit_container(m);
    EXPECT_GT(r, 0);

    EXPECT_TRUE(shuffle_status);
}

TEST_F(MPRISTest, PlayTest_MPRIS) {
    auto player = adk::MediaPlayer::Create("MPRIS-play-test");

    auto onStateChanged = std::bind(std::mem_fn(&MPRISTest::OnStateChanged), this, _1);
    player->SetOnStateChangedCb(onStateChanged);

    EXPECT_TRUE(player->SetSource(LOCAL_MEDIA_FILE_LONG));

    int r = sd_bus_call_method(bus,
        "org.mpris.MediaPlayer2.MPRIS-play-test",
        "/org/mpris/MediaPlayer2",
        "org.mpris.MediaPlayer2.Player",
        "Play",
        NULL,
        &m,
        NULL);

    EXPECT_GT(r, 0);

    EXPECT_TRUE(WaitForObserverSc());
    EXPECT_TRUE(onStateChangedCalled);
    onStateChangedCalled = false;

    EXPECT_EQ(current_state, adk::MediaPlayer::State::Playing);

    sd_bus_message_unref(m);
}

TEST_F(MPRISTest, PlayPauseStopCbTest_MPRIS) {
    auto player = adk::MediaPlayer::Create("MPRIS-play-pause-stop-cb-test");

    auto onPlay = std::bind(std::mem_fn(&MPRISTest::OnPlay), this);
    auto onPause = std::bind(std::mem_fn(&MPRISTest::OnPause), this);
    auto onStop = std::bind(std::mem_fn(&MPRISTest::OnStop), this);
    player->SetPlayCb(onPlay);
    player->SetPauseCb(onPause);
    player->SetStopCb(onStop);

    EXPECT_TRUE(player->SetSource(LOCAL_MEDIA_FILE_LONG));

    // For Play() MPRIS method
    int r = sd_bus_call_method(bus,
        "org.mpris.MediaPlayer2.MPRIS-play-pause-stop-cb-test",
        "/org/mpris/MediaPlayer2",
        "org.mpris.MediaPlayer2.Player",
        "Play",
        NULL,
        &m,
        NULL);

    EXPECT_GT(r, 0);

    EXPECT_TRUE(onPlayCalled);
    onPlayCalled = false;

    // For Pause() MPRIS method
    r = sd_bus_call_method(bus,
        "org.mpris.MediaPlayer2.MPRIS-play-pause-stop-cb-test",
        "/org/mpris/MediaPlayer2",
        "org.mpris.MediaPlayer2.Player",
        "Pause",
        NULL,
        &m,
        NULL);

    EXPECT_GT(r, 0);

    EXPECT_TRUE(onPauseCalled);
    onPauseCalled = false;

    // For PlayPause() MPRIS method
    r = sd_bus_call_method(bus,
        "org.mpris.MediaPlayer2.MPRIS-play-pause-stop-cb-test",
        "/org/mpris/MediaPlayer2",
        "org.mpris.MediaPlayer2.Player",
        "PlayPause",
        NULL,
        &m,
        NULL);

    EXPECT_GT(r, 0);

    EXPECT_TRUE(onPlayCalled);
    onPlayCalled = false;

    // For Stop() MPRIS method
    r = sd_bus_call_method(bus,
        "org.mpris.MediaPlayer2.MPRIS-play-pause-stop-cb-test",
        "/org/mpris/MediaPlayer2",
        "org.mpris.MediaPlayer2.Player",
        "Stop",
        NULL,
        &m,
        NULL);

    EXPECT_GT(r, 0);

    EXPECT_TRUE(onStopCalled);
    onStopCalled = false;

    sd_bus_message_unref(m);
}

TEST_F(MPRISTest, PauseTest_MPRIS) {
    auto player = adk::MediaPlayer::Create("MPRIS-pause-test");

    auto onStateChanged = std::bind(std::mem_fn(&MPRISTest::OnStateChanged), this, _1);
    player->SetOnStateChangedCb(onStateChanged);

    EXPECT_TRUE(player->SetSource(LOCAL_MEDIA_FILE_LONG));

    player->Play();

    EXPECT_TRUE(WaitForObserverSc());
    EXPECT_TRUE(onStateChangedCalled);
    onStateChangedCalled = false;

    EXPECT_EQ(current_state, adk::MediaPlayer::State::Playing);

    int r = sd_bus_call_method(bus,
        "org.mpris.MediaPlayer2.MPRIS-pause-test",
        "/org/mpris/MediaPlayer2",
        "org.mpris.MediaPlayer2.Player",
        "Pause",
        NULL,
        &m,
        NULL);

    EXPECT_GT(r, 0);

    sd_bus_message_unref(m);

    if (current_state != adk::MediaPlayer::State::Paused) {
        EXPECT_TRUE(WaitForObserverSc());
        EXPECT_TRUE(onStateChangedCalled);
        onStateChangedCalled = false;
    }

    EXPECT_EQ(current_state, adk::MediaPlayer::State::Paused);
}

TEST_F(MPRISTest, PlayPauseTest_MPRIS) {
    auto player = adk::MediaPlayer::Create("MPRIS-playpause-test");

    auto onStateChanged = std::bind(std::mem_fn(&MPRISTest::OnStateChanged), this, _1);
    player->SetOnStateChangedCb(onStateChanged);

    EXPECT_TRUE(player->SetSource(LOCAL_MEDIA_FILE_LONG));

    player->Play();

    EXPECT_TRUE(WaitForObserverSc());
    EXPECT_TRUE(onStateChangedCalled);
    onStateChangedCalled = false;

    EXPECT_EQ(current_state, adk::MediaPlayer::State::Playing);

    int r = sd_bus_call_method(bus,
        "org.mpris.MediaPlayer2.MPRIS-playpause-test",
        "/org/mpris/MediaPlayer2",
        "org.mpris.MediaPlayer2.Player",
        "PlayPause",
        NULL,
        &m,
        NULL);

    EXPECT_GT(r, 0);

    sd_bus_message_unref(m);

    if (current_state != adk::MediaPlayer::State::Paused) {
        EXPECT_TRUE(WaitForObserverSc());
        EXPECT_TRUE(onStateChangedCalled);
        onStateChangedCalled = false;
    }

    EXPECT_EQ(current_state, adk::MediaPlayer::State::Paused);

    r = sd_bus_call_method(bus,
        "org.mpris.MediaPlayer2.MPRIS-playpause-test",
        "/org/mpris/MediaPlayer2",
        "org.mpris.MediaPlayer2.Player",
        "PlayPause",
        NULL,
        &m,
        NULL);

    EXPECT_GT(r, 0);

    sd_bus_message_unref(m);

    if (current_state != adk::MediaPlayer::State::Playing) {
        EXPECT_TRUE(WaitForObserverSc());
        EXPECT_TRUE(onStateChangedCalled);
        onStateChangedCalled = false;
    }

    EXPECT_EQ(current_state, adk::MediaPlayer::State::Playing);
}

TEST_F(MPRISTest, StopTest_MPRIS) {
    auto player = adk::MediaPlayer::Create("MPRIS-test-stop");

    auto onStateChanged = std::bind(std::mem_fn(&MPRISTest::OnStateChanged), this, _1);
    player->SetOnStateChangedCb(onStateChanged);

    EXPECT_TRUE(player->SetSource(LOCAL_MEDIA_FILE_LONG));

    player->Play();

    EXPECT_TRUE(WaitForObserverSc());
    EXPECT_TRUE(onStateChangedCalled);
    onStateChangedCalled = false;

    EXPECT_EQ(current_state, adk::MediaPlayer::State::Playing);

    int r = sd_bus_call_method(bus,
        "org.mpris.MediaPlayer2.MPRIS-test-stop",
        "/org/mpris/MediaPlayer2",
        "org.mpris.MediaPlayer2.Player",
        "Stop",
        NULL,
        &m,
        NULL);

    EXPECT_GT(r, 0);

    sd_bus_message_unref(m);

    if (current_state != adk::MediaPlayer::State::Stopped) {
        EXPECT_TRUE(WaitForObserverSc());
        EXPECT_TRUE(onStateChangedCalled);
        onStateChangedCalled = false;
    }

    EXPECT_EQ(current_state, adk::MediaPlayer::State::Stopped);
}

TEST_F(MPRISTest, OpenUriTest_MPRIS) {
    auto player = adk::MediaPlayer::Create("MPRIS-openuri-test");

    auto onStateChanged = std::bind(std::mem_fn(&MPRISTest::OnStateChanged), this, _1);
    player->SetOnStateChangedCb(onStateChanged);

    int r = sd_bus_call_method(bus,
        "org.mpris.MediaPlayer2.MPRIS-openuri-test",
        "/org/mpris/MediaPlayer2",
        "org.mpris.MediaPlayer2.Player",
        "OpenUri",
        NULL,
        &m,
        "s",
        LOCAL_MEDIA_FILE_LONG);

    EXPECT_GT(r, 0);

    sd_bus_message_unref(m);

    // OpenUri also triggers playback
    EXPECT_TRUE(WaitForObserverSc());
    EXPECT_TRUE(onStateChangedCalled);
    onStateChangedCalled = false;

    EXPECT_EQ(current_state, adk::MediaPlayer::State::Playing);
}

TEST_F(MPRISTest, SeekTest_MPRIS) {
    auto player = adk::MediaPlayer::Create("MPRIS-seek-test");

    auto onStateChanged = std::bind(std::mem_fn(&MPRISTest::OnStateChanged), this, _1);
    auto onAsyncDone = std::bind(std::mem_fn(&MPRISTest::OnAsyncDone), this);
    auto onNext = std::bind(std::mem_fn(&MPRISTest::OnNext), this);

    player->SetNextCb(onNext);
    player->SetOnAsyncDoneCb(onAsyncDone);
    player->SetOnStateChangedCb(onStateChanged);

    EXPECT_TRUE(player->SetSource(LOCAL_MEDIA_FILE_LONG));

    player->Play();

    EXPECT_TRUE(WaitForObserverSc());
    EXPECT_TRUE(onStateChangedCalled);
    onStateChangedCalled = false;

    EXPECT_EQ(current_state, adk::MediaPlayer::State::Playing);

    std::this_thread::sleep_for(std::chrono::seconds(2));

    // when seeked time is within duration
    int r = sd_bus_call_method(bus,
        "org.mpris.MediaPlayer2.MPRIS-seek-test",
        "/org/mpris/MediaPlayer2",
        "org.mpris.MediaPlayer2.Player",
        "Seek",
        NULL,
        &m,
        "x",
        int64_t(10000000));

    EXPECT_GT(r, 0);

    sd_bus_message_unref(m);

    EXPECT_TRUE(WaitForObserverAsync());
    EXPECT_TRUE(onAsyncDoneCalled);
    onAsyncDoneCalled = false;

    std::chrono::duration<double> position = player->GetPosition();
    EXPECT_TRUE(position.count() > 11 && position.count() < 13);

    // when seeked time is before duration
    r = sd_bus_call_method(bus,
        "org.mpris.MediaPlayer2.MPRIS-seek-test",
        "/org/mpris/MediaPlayer2",
        "org.mpris.MediaPlayer2.Player",
        "Seek",
        NULL,
        &m,
        "x",
        int64_t(-20000000));

    EXPECT_GT(r, 0);

    sd_bus_message_unref(m);

    EXPECT_TRUE(WaitForObserverAsync());
    EXPECT_TRUE(onAsyncDoneCalled);
    onAsyncDoneCalled = false;

    std::this_thread::sleep_for(std::chrono::seconds(2));

    position = player->GetPosition();
    EXPECT_TRUE(position.count() > 1 && position.count() < 3);

    // when seeked time is beyond duration
    r = sd_bus_call_method(bus,
        "org.mpris.MediaPlayer2.MPRIS-seek-test",
        "/org/mpris/MediaPlayer2",
        "org.mpris.MediaPlayer2.Player",
        "Seek",
        NULL,
        &m,
        "x",
        int64_t(2000000000));

    EXPECT_GT(r, 0);

    sd_bus_message_unref(m);

    EXPECT_TRUE(onNextCalled);
    onNextCalled = false;
}

TEST_F(MPRISTest, SetPositionTest_MPRIS) {
    auto player = adk::MediaPlayer::Create("MPRIS-set-position-test");

    auto onStateChanged = std::bind(std::mem_fn(&MPRISTest::OnStateChanged), this, _1);
    player->SetOnStateChangedCb(onStateChanged);

    EXPECT_TRUE(player->SetSource(LOCAL_MEDIA_FILE_LONG));

    player->Play();

    EXPECT_TRUE(WaitForObserverSc());
    EXPECT_TRUE(onStateChangedCalled);
    onStateChangedCalled = false;

    EXPECT_EQ(current_state, adk::MediaPlayer::State::Playing);

    // Valid Track id
    int r = sd_bus_call_method(bus,
        "org.mpris.MediaPlayer2.MPRIS-set-position-test",
        "/org/mpris/MediaPlayer2",
        "org.mpris.MediaPlayer2.Player",
        "SetPosition",
        NULL,
        &m,
        "ox",
        "/org/mpris/MediaPlayer2/Track/0",
        int64_t(10000000));

    EXPECT_GT(r, 0);

    sd_bus_message_unref(m);

    EXPECT_TRUE(WaitForObserverSc());
    EXPECT_TRUE(onStateChangedCalled);
    onStateChangedCalled = false;

    std::chrono::duration<double> position = player->GetPosition();
    EXPECT_TRUE(position.count() > 9 && position.count() < 11);

    // Invalid Track id
    r = sd_bus_call_method(bus,
        "org.mpris.MediaPlayer2.MPRIS-set-position-test",
        "/org/mpris/MediaPlayer2",
        "org.mpris.MediaPlayer2.Player",
        "SetPosition",
        NULL,
        &m,
        "ox",
        "/org/mpris/MediaPlayer2/TrackList/NoTrack",
        int64_t(20000000));

    EXPECT_GT(r, 0);

    sd_bus_message_unref(m);

    position = player->GetPosition();
    EXPECT_FALSE(position.count() > 19 && position.count() < 21);
}

TEST_F(MPRISTest, PlaybackStatusTest_MPRIS) {
    auto player = adk::MediaPlayer::Create("MPRIS-playbackstatus-test");

    auto onStateChanged = std::bind(std::mem_fn(&MPRISTest::OnStateChanged), this, _1);
    player->SetOnStateChangedCb(onStateChanged);

    int r;
    std::string rule;
    rule = "type='signal',";
    rule += "sender='org.mpris.MediaPlayer2.MPRIS-playbackstatus-test',";
    rule += "interface='org.freedesktop.DBus.Properties',member='PropertiesChanged'";

    int ret = sd_bus_add_match(bus, NULL, rule.c_str(), properties_changed_cb, this);
    EXPECT_GT(ret, 0);

    EXPECT_TRUE(player->SetSource(LOCAL_MEDIA_FILE_LONG));

    player->Play();

    EXPECT_TRUE(WaitForObserverSc());
    EXPECT_TRUE(onStateChangedCalled);
    onStateChangedCalled = false;

    EXPECT_EQ(current_state, adk::MediaPlayer::State::Playing);

    for (;;) {
        r = sd_bus_process(bus, NULL);
        if ((r < 0) || (r > 0 && onPropertiesChanged == true))
            break;

        if (r > 0)
            continue;

        r = sd_bus_wait(bus, (uint64_t)-1);
        if (r < 0)
            break;
    }

    EXPECT_TRUE(onPropertiesChanged);
    onPropertiesChanged = false;

    r = sd_bus_call_method(bus,
        "org.mpris.MediaPlayer2.MPRIS-playbackstatus-test",
        "/org/mpris/MediaPlayer2",
        "org.freedesktop.DBus.Properties",
        "Get",
        NULL,
        &m,
        "ss",
        "org.mpris.MediaPlayer2.Player",
        "PlaybackStatus");

    EXPECT_GT(r, 0);

    const char *playback_status;

    r = sd_bus_message_enter_container(m, 'v', "s");
    EXPECT_GT(r, 0);

    r = sd_bus_message_read_basic(m, 's', &playback_status);
    EXPECT_GT(r, 0);

    r = sd_bus_message_exit_container(m);
    EXPECT_GT(r, 0);

    EXPECT_EQ(strcmp(playback_status, "Playing"), 0);
}

TEST_F(MPRISTest, PositionTest_MPRIS) {
    auto player = adk::MediaPlayer::Create("MPRIS-position-test");

    auto onStateChanged = std::bind(std::mem_fn(&MPRISTest::OnStateChanged), this, _1);
    player->SetOnStateChangedCb(onStateChanged);

    EXPECT_TRUE(player->SetSource(LOCAL_MEDIA_FILE_LONG));

    player->Play();

    EXPECT_TRUE(WaitForObserverSc());
    EXPECT_TRUE(onStateChangedCalled);
    onStateChangedCalled = false;

    EXPECT_EQ(current_state, adk::MediaPlayer::State::Playing);

    std::this_thread::sleep_for(std::chrono::seconds(2));

    int r = sd_bus_call_method(bus,
        "org.mpris.MediaPlayer2.MPRIS-position-test",
        "/org/mpris/MediaPlayer2",
        "org.freedesktop.DBus.Properties",
        "Get",
        NULL,
        &m,
        "ss",
        "org.mpris.MediaPlayer2.Player",
        "Position");

    EXPECT_GT(r, 0);

    int64_t p;

    r = sd_bus_message_enter_container(m, 'v', "x");
    EXPECT_GT(r, 0);

    r = sd_bus_message_read_basic(m, 'x', &p);
    EXPECT_GT(r, 0);

    r = sd_bus_message_exit_container(m);
    EXPECT_GT(r, 0);

    std::chrono::duration<double> position = std::chrono::microseconds(p);

    EXPECT_TRUE(position.count() > 1 && position.count() < 3);
}

TEST_F(MPRISTest, MetadataTest_MPRIS) {
    auto player = adk::MediaPlayer::Create("MPRIS-metadata-test");

    auto onStateChanged = std::bind(std::mem_fn(&MPRISTest::OnStateChanged), this, _1);
    player->SetOnStateChangedCb(onStateChanged);

    int r;
    std::string rule;
    rule = "type='signal',";
    rule += "sender='org.mpris.MediaPlayer2.MPRIS-metadata-test',";
    rule += "interface='org.freedesktop.DBus.Properties',member='PropertiesChanged'";

    int ret = sd_bus_add_match(bus, NULL, rule.c_str(), properties_changed_cb, this);
    EXPECT_GT(ret, 0);

    EXPECT_TRUE(player->SetSource(LOCAL_MEDIA_FILE_LONG));

    player->Play();

    EXPECT_TRUE(WaitForObserverSc());
    EXPECT_TRUE(onStateChangedCalled);
    onStateChangedCalled = false;

    EXPECT_EQ(current_state, adk::MediaPlayer::State::Playing);

    for (;;) {
        r = sd_bus_process(bus, NULL);
        if ((r < 0) || (r > 0 && onPropertiesChanged == true))
            break;

        if (r > 0)
            continue;

        r = sd_bus_wait(bus, (uint64_t)-1);
        if (r < 0)
            break;
    }

    EXPECT_TRUE(onPropertiesChanged);
    onPropertiesChanged = false;

    r = sd_bus_call_method(bus,
        "org.mpris.MediaPlayer2.MPRIS-metadata-test",
        "/org/mpris/MediaPlayer2",
        "org.freedesktop.DBus.Properties",
        "Get",
        NULL,
        &m,
        "ss",
        "org.mpris.MediaPlayer2.Player",
        "Metadata");

    EXPECT_GT(r, 0);

    r = sd_bus_message_enter_container(m, SD_BUS_TYPE_VARIANT, "a{sv}");
    EXPECT_GT(r, 0);

    r = sd_bus_message_enter_container(m, SD_BUS_TYPE_ARRAY, "{sv}");
    EXPECT_GT(r, 0);

    int n = 0;
    const char *tag, *value;

    for (;;) {
        r = sd_bus_message_skip(m, "{sv}");

        if (r == 0)
            break;
        n++;
    }

    r = sd_bus_message_rewind(m, 0);
    EXPECT_GT(r, 0);

    while (n > 0) {
        r = sd_bus_message_enter_container(m, SD_BUS_TYPE_DICT_ENTRY, "sv");
        EXPECT_GT(r, 0);

        r = sd_bus_message_read_basic(m, 's', &tag);
        EXPECT_GT(r, 0);

        if (strcmp(tag, "xesam:album") == 0 || strcmp(tag, "xesam:title") == 0 || strcmp(tag, "xesam:url") == 0 || strcmp(tag, "adk:SampleFormat") == 0 || strcmp(tag, "xesam:audioChannels") == 0) {
            r = sd_bus_message_enter_container(m, SD_BUS_TYPE_VARIANT, "s");
            EXPECT_GT(r, 0);

            r = sd_bus_message_read_basic(m, 's', &value);
            EXPECT_GT(r, 0);

            r = sd_bus_message_exit_container(m);
            EXPECT_GT(r, 0);

            if (strcmp(tag, "xesam:album") == 0) {
                EXPECT_EQ(strcmp(value, "Dummy-Album"), 0);
            } else if (strcmp(tag, "xesam:title") == 0) {
                EXPECT_EQ(strcmp(value, "Dummy-Title"), 0);
            } else if (strcmp(tag, "xesam:url") == 0) {
                EXPECT_EQ(strcmp(value, DATA_PATH), 0);
            } else if (strcmp(tag, "adk:SampleFormat") == 0) {
                EXPECT_EQ(strcmp(value, "F32LE"), 0);
            } else if (strcmp(tag, "xesam:audioChannels") == 0) {
                EXPECT_EQ(strcmp(value, "1"), 0);
            }
        } else if (strcmp(tag, "mpris:length") == 0) {
            int64_t tmp;

            r = sd_bus_message_enter_container(m, SD_BUS_TYPE_VARIANT, "x");
            EXPECT_GT(r, 0);

            r = sd_bus_message_read(m, "x", &tmp);
            EXPECT_GT(r, 0);

            r = sd_bus_message_exit_container(m);
            EXPECT_GT(r, 0);

            EXPECT_EQ(tmp, (int64_t)139319727);
        } else if (strcmp(tag, "xesam:audioBitrate") == 0) {
            int32_t tmp;

            r = sd_bus_message_enter_container(m, SD_BUS_TYPE_VARIANT, "i");
            EXPECT_GT(r, 0);

            r = sd_bus_message_read(m, "i", &tmp);
            EXPECT_GT(r, 0);

            r = sd_bus_message_exit_container(m);
            EXPECT_GT(r, 0);

            EXPECT_EQ(tmp, 80000);
        } else if (strcmp(tag, "xesam:audioSampleRate") == 0) {
            double tmp;

            r = sd_bus_message_enter_container(m, SD_BUS_TYPE_VARIANT, "d");
            EXPECT_GT(r, 0);

            r = sd_bus_message_read(m, "d", &tmp);
            EXPECT_GT(r, 0);

            r = sd_bus_message_exit_container(m);
            EXPECT_GT(r, 0);

            EXPECT_EQ(tmp, 44100);
        } else if (strcmp(tag, "xesam:artist") == 0 || strcmp(tag, "xesam:genre") == 0) {
            int size = 0;
            std::vector<std::string> list;

            r = sd_bus_message_enter_container(m, SD_BUS_TYPE_VARIANT, "as");
            EXPECT_GT(r, 0);

            r = sd_bus_message_enter_container(m, SD_BUS_TYPE_ARRAY, "s");
            EXPECT_GT(r, 0);

            for (;;) {
                r = sd_bus_message_skip(m, "s");

                if (r == 0)
                    break;
                size++;
            }

            r = sd_bus_message_rewind(m, 0);
            EXPECT_GT(r, 0);

            while (size > 0) {
                r = sd_bus_message_read_basic(m, 's', &value);
                EXPECT_GT(r, 0);

                list.push_back(value);

                size--;
            }

            r = sd_bus_message_exit_container(m);
            EXPECT_GT(r, 0);

            r = sd_bus_message_exit_container(m);
            EXPECT_GT(r, 0);

            if (strcmp(tag, "xesam:artist") == 0) {
                EXPECT_EQ(list[0], "Dummy-Artist");
            } else if (strcmp(tag, "xesam:genre") == 0) {
                EXPECT_EQ(list[0], "Rock");
                EXPECT_EQ(list[1], "Jazz");
            }
        } else if (strcmp(tag, "mpris:artUrl") == 0) {
            r = sd_bus_message_enter_container(m, SD_BUS_TYPE_VARIANT, "s");
            EXPECT_GT(r, 0);

            r = sd_bus_message_read_basic(m, 's', &value);
            EXPECT_GT(r, 0);

            r = sd_bus_message_exit_container(m);
            EXPECT_GT(r, 0);

            EXPECT_EQ(strcmp(value, "/tmp/cover_art_MPRIS-metadata-test.png"), 0);
        } else if (strcmp(tag, "mpris:trackid") == 0) {
            r = sd_bus_message_enter_container(m, SD_BUS_TYPE_VARIANT, "o");
            EXPECT_GT(r, 0);

            r = sd_bus_message_read_basic(m, 'o', &value);
            EXPECT_GT(r, 0);

            r = sd_bus_message_exit_container(m);
            EXPECT_GT(r, 0);

            EXPECT_EQ(strcmp(value, "/org/mpris/MediaPlayer2/Track/0"), 0);
        } else {
            r = sd_bus_message_skip(m, "v");
            EXPECT_GT(r, 0);
        }

        r = sd_bus_message_exit_container(m);
        EXPECT_GT(r, 0);

        n--;
    }
}

TEST_F(MPRISTest, ControlOnlyPlayerTest_MPRIS) {
    auto player = adk::MediaPlayer::Create("MPRIS-control-only-player-test", adk::MediaPlayer::StreamType::ControlOnly);

    auto onStateChanged = std::bind(std::mem_fn(&MPRISTest::OnStateChanged), this, _1);
    player->SetOnStateChangedCb(onStateChanged);

    int r;
    std::string rule;
    rule = "type='signal',";
    rule += "sender='org.mpris.MediaPlayer2.MPRIS-control-only-player-test',";
    rule += "interface='org.freedesktop.DBus.Properties',member='PropertiesChanged'";

    int ret = sd_bus_add_match(bus, NULL, rule.c_str(), properties_changed_cb, this);
    EXPECT_GT(ret, 0);

    // MPRIS play is being called
    r = sd_bus_call_method(bus,
        "org.mpris.MediaPlayer2.MPRIS-control-only-player-test",
        "/org/mpris/MediaPlayer2",
        "org.mpris.MediaPlayer2.Player",
        "Play",
        NULL,
        &m,
        NULL);

    EXPECT_GT(r, 0);

    EXPECT_TRUE(onStateChangedCalled);
    onStateChangedCalled = false;

    //MediaPlayer API Play called
    auto res = player->Play();
    EXPECT_TRUE(res == adk::MediaPlayer::Success);

    for (;;) {
        r = sd_bus_process(bus, NULL);
        if ((r < 0) || (r > 0 && onPropertiesChanged == true))
            break;

        if (r > 0)
            continue;

        r = sd_bus_wait(bus, (uint64_t)-1);
        if (r < 0)
            break;
    }

    EXPECT_TRUE(onPropertiesChanged);
    onPropertiesChanged = false;

    r = sd_bus_call_method(bus,
        "org.mpris.MediaPlayer2.MPRIS-control-only-player-test",
        "/org/mpris/MediaPlayer2",
        "org.freedesktop.DBus.Properties",
        "Get",
        NULL,
        &m,
        "ss",
        "org.mpris.MediaPlayer2.Player",
        "PlaybackStatus");

    EXPECT_GT(r, 0);

    const char *playback_status;

    r = sd_bus_message_enter_container(m, 'v', "s");
    EXPECT_GT(r, 0);

    r = sd_bus_message_read_basic(m, 's', &playback_status);
    EXPECT_GT(r, 0);

    r = sd_bus_message_exit_container(m);
    EXPECT_GT(r, 0);

    EXPECT_EQ(strcmp(playback_status, "Playing"), 0);
}

TEST_F(MPRISTest, VolumeTest_MPRIS) {
    auto player = adk::MediaPlayer::Create("MPRIS-volume-test");

    int r = sd_bus_call_method(bus,
        "org.mpris.MediaPlayer2.MPRIS-volume-test",
        "/org/mpris/MediaPlayer2",
        "org.freedesktop.DBus.Properties",
        "Set",
        NULL,
        &m,
        "ssv",
        "org.mpris.MediaPlayer2.Player",
        "Volume",
        "d",
        0.65);

    EXPECT_GT(r, 0);

    r = sd_bus_call_method(bus,
        "org.mpris.MediaPlayer2.MPRIS-volume-test",
        "/org/mpris/MediaPlayer2",
        "org.freedesktop.DBus.Properties",
        "Get",
        NULL,
        &m,
        "ss",
        "org.mpris.MediaPlayer2.Player",
        "Volume");

    EXPECT_GT(r, 0);

    double volume;

    r = sd_bus_message_enter_container(m, 'v', "d");
    EXPECT_GT(r, 0);

    r = sd_bus_message_read_basic(m, 'd', &volume);
    EXPECT_GT(r, 0);

    r = sd_bus_message_exit_container(m);
    EXPECT_GT(r, 0);

    EXPECT_EQ(std::round(volume * 100), 65);
}

TEST_F(MPRISTest, RateTest_MPRIS) {
    auto player = adk::MediaPlayer::Create("MPRIS-rate-test");

    int r = sd_bus_call_method(bus,
        "org.mpris.MediaPlayer2.MPRIS-rate-test",
        "/org/mpris/MediaPlayer2",
        "org.freedesktop.DBus.Properties",
        "Get",
        NULL,
        &m,
        "ss",
        "org.mpris.MediaPlayer2.Player",
        "Rate");

    EXPECT_GT(r, 0);

    double rate;

    r = sd_bus_message_enter_container(m, 'v', "d");
    EXPECT_GT(r, 0);

    r = sd_bus_message_read_basic(m, 'd', &rate);
    EXPECT_GT(r, 0);

    r = sd_bus_message_exit_container(m);
    EXPECT_GT(r, 0);

    EXPECT_EQ(rate, 1.0);

    r = sd_bus_call_method(bus,
        "org.mpris.MediaPlayer2.MPRIS-rate-test",
        "/org/mpris/MediaPlayer2",
        "org.freedesktop.DBus.Properties",
        "Set",
        NULL,
        &m,
        "ssv",
        "org.mpris.MediaPlayer2.Player",
        "Rate",
        "d",
        2.3);

    EXPECT_GT(r, 0);

    r = sd_bus_call_method(bus,
        "org.mpris.MediaPlayer2.MPRIS-rate-test",
        "/org/mpris/MediaPlayer2",
        "org.freedesktop.DBus.Properties",
        "Get",
        NULL,
        &m,
        "ss",
        "org.mpris.MediaPlayer2.Player",
        "Rate");

    EXPECT_GT(r, 0);

    r = sd_bus_message_enter_container(m, 'v', "d");
    EXPECT_GT(r, 0);

    r = sd_bus_message_read_basic(m, 'd', &rate);
    EXPECT_GT(r, 0);

    r = sd_bus_message_exit_container(m);
    EXPECT_GT(r, 0);

    EXPECT_EQ(rate, 1.0);
}

TEST_F(MPRISTest, PropertiesTest_MPRIS) {
    auto player = adk::MediaPlayer::Create("MPRIS-properties-test");

    /// Test case for Minimum Rate Property.
    int r = sd_bus_call_method(bus,
        "org.mpris.MediaPlayer2.MPRIS-properties-test",
        "/org/mpris/MediaPlayer2",
        "org.freedesktop.DBus.Properties",
        "Get",
        NULL,
        &m,
        "ss",
        "org.mpris.MediaPlayer2.Player",
        "MinimumRate");

    EXPECT_GT(r, 0);

    double min_rate;

    r = sd_bus_message_enter_container(m, 'v', "d");
    EXPECT_GT(r, 0);

    r = sd_bus_message_read_basic(m, 'd', &min_rate);
    EXPECT_GT(r, 0);

    r = sd_bus_message_exit_container(m);
    EXPECT_GT(r, 0);

    EXPECT_EQ(min_rate, 1.0);

    /// Test case for Maximum Rate Property.
    r = sd_bus_call_method(bus,
        "org.mpris.MediaPlayer2.MPRIS-properties-test",
        "/org/mpris/MediaPlayer2",
        "org.freedesktop.DBus.Properties",
        "Get",
        NULL,
        &m,
        "ss",
        "org.mpris.MediaPlayer2.Player",
        "MaximumRate");

    EXPECT_GT(r, 0);

    double max_rate;

    r = sd_bus_message_enter_container(m, 'v', "d");
    EXPECT_GT(r, 0);

    r = sd_bus_message_read_basic(m, 'd', &max_rate);
    EXPECT_GT(r, 0);

    r = sd_bus_message_exit_container(m);
    EXPECT_GT(r, 0);

    EXPECT_EQ(max_rate, 1.0);

    /// Test case for CanGoNext Property.
    r = sd_bus_call_method(bus,
        "org.mpris.MediaPlayer2.MPRIS-properties-test",
        "/org/mpris/MediaPlayer2",
        "org.freedesktop.DBus.Properties",
        "Get",
        NULL,
        &m,
        "ss",
        "org.mpris.MediaPlayer2.Player",
        "CanGoNext");

    EXPECT_GT(r, 0);

    int can_go_next;

    r = sd_bus_message_enter_container(m, 'v', "b");
    EXPECT_GT(r, 0);

    r = sd_bus_message_read_basic(m, 'b', &can_go_next);
    EXPECT_GT(r, 0);

    r = sd_bus_message_exit_container(m);
    EXPECT_GT(r, 0);

    EXPECT_EQ(can_go_next, 0);

    auto onNext = std::bind(std::mem_fn(&MPRISTest::OnNext), this);
    player->SetNextCb(onNext);

    r = sd_bus_call_method(bus,
        "org.mpris.MediaPlayer2.MPRIS-properties-test",
        "/org/mpris/MediaPlayer2",
        "org.freedesktop.DBus.Properties",
        "Get",
        NULL,
        &m,
        "ss",
        "org.mpris.MediaPlayer2.Player",
        "CanGoNext");

    EXPECT_GT(r, 0);

    r = sd_bus_message_enter_container(m, 'v', "b");
    EXPECT_GT(r, 0);

    r = sd_bus_message_read_basic(m, 'b', &can_go_next);
    EXPECT_GT(r, 0);

    r = sd_bus_message_exit_container(m);
    EXPECT_GT(r, 0);

    EXPECT_EQ(can_go_next, 1);

    /// Test case for CanGoPrevious Property.
    r = sd_bus_call_method(bus,
        "org.mpris.MediaPlayer2.MPRIS-properties-test",
        "/org/mpris/MediaPlayer2",
        "org.freedesktop.DBus.Properties",
        "Get",
        NULL,
        &m,
        "ss",
        "org.mpris.MediaPlayer2.Player",
        "CanGoPrevious");

    EXPECT_GT(r, 0);

    int can_go_previous;

    r = sd_bus_message_enter_container(m, 'v', "b");
    EXPECT_GT(r, 0);

    r = sd_bus_message_read_basic(m, 'b', &can_go_previous);
    EXPECT_GT(r, 0);

    r = sd_bus_message_exit_container(m);
    EXPECT_GT(r, 0);

    EXPECT_EQ(can_go_previous, 0);

    auto onPrevious = std::bind(std::mem_fn(&MPRISTest::OnPrevious), this);
    player->SetPreviousCb(onPrevious);

    r = sd_bus_call_method(bus,
        "org.mpris.MediaPlayer2.MPRIS-properties-test",
        "/org/mpris/MediaPlayer2",
        "org.freedesktop.DBus.Properties",
        "Get",
        NULL,
        &m,
        "ss",
        "org.mpris.MediaPlayer2.Player",
        "CanGoPrevious");

    EXPECT_GT(r, 0);

    r = sd_bus_message_enter_container(m, 'v', "b");
    EXPECT_GT(r, 0);

    r = sd_bus_message_read_basic(m, 'b', &can_go_previous);
    EXPECT_GT(r, 0);

    r = sd_bus_message_exit_container(m);
    EXPECT_GT(r, 0);

    EXPECT_EQ(can_go_previous, 1);

    /// Test case for CanPlay Property.
    r = sd_bus_call_method(bus,
        "org.mpris.MediaPlayer2.MPRIS-properties-test",
        "/org/mpris/MediaPlayer2",
        "org.freedesktop.DBus.Properties",
        "Get",
        NULL,
        &m,
        "ss",
        "org.mpris.MediaPlayer2.Player",
        "CanPlay");

    EXPECT_GT(r, 0);

    int can_play;

    r = sd_bus_message_enter_container(m, 'v', "b");
    EXPECT_GT(r, 0);

    r = sd_bus_message_read_basic(m, 'b', &can_play);
    EXPECT_GT(r, 0);

    r = sd_bus_message_exit_container(m);
    EXPECT_GT(r, 0);

    EXPECT_EQ(can_play, 1);

    /// Test case for CanPause Property.
    r = sd_bus_call_method(bus,
        "org.mpris.MediaPlayer2.MPRIS-properties-test",
        "/org/mpris/MediaPlayer2",
        "org.freedesktop.DBus.Properties",
        "Get",
        NULL,
        &m,
        "ss",
        "org.mpris.MediaPlayer2.Player",
        "CanPause");

    EXPECT_GT(r, 0);

    int can_pause;

    r = sd_bus_message_enter_container(m, 'v', "b");
    EXPECT_GT(r, 0);

    r = sd_bus_message_read_basic(m, 'b', &can_pause);
    EXPECT_GT(r, 0);

    r = sd_bus_message_exit_container(m);
    EXPECT_GT(r, 0);

    EXPECT_EQ(can_pause, 1);

    /// Test case for CanSeek Property.
    r = sd_bus_call_method(bus,
        "org.mpris.MediaPlayer2.MPRIS-properties-test",
        "/org/mpris/MediaPlayer2",
        "org.freedesktop.DBus.Properties",
        "Get",
        NULL,
        &m,
        "ss",
        "org.mpris.MediaPlayer2.Player",
        "CanSeek");

    EXPECT_GT(r, 0);

    int can_seek;

    r = sd_bus_message_enter_container(m, 'v', "b");
    EXPECT_GT(r, 0);

    r = sd_bus_message_read_basic(m, 'b', &can_seek);
    EXPECT_GT(r, 0);

    r = sd_bus_message_exit_container(m);
    EXPECT_GT(r, 0);

    EXPECT_EQ(can_seek, 0);

    auto onStateChanged = std::bind(std::mem_fn(&MPRISTest::OnStateChanged), this, _1);
    player->SetOnStateChangedCb(onStateChanged);

    EXPECT_TRUE(player->SetSource("file://" LOCAL_MEDIA_MP4_FILE));

    player->Play();

    EXPECT_TRUE(WaitForObserverSc());
    EXPECT_TRUE(onStateChangedCalled);
    onStateChangedCalled = false;

    EXPECT_EQ(current_state, adk::MediaPlayer::State::Playing);

    r = sd_bus_call_method(bus,
        "org.mpris.MediaPlayer2.MPRIS-properties-test",
        "/org/mpris/MediaPlayer2",
        "org.freedesktop.DBus.Properties",
        "Get",
        NULL,
        &m,
        "ss",
        "org.mpris.MediaPlayer2.Player",
        "CanSeek");

    EXPECT_GT(r, 0);

    r = sd_bus_message_enter_container(m, 'v', "b");
    EXPECT_GT(r, 0);

    r = sd_bus_message_read_basic(m, 'b', &can_seek);
    EXPECT_GT(r, 0);

    r = sd_bus_message_exit_container(m);
    EXPECT_GT(r, 0);

    EXPECT_EQ(can_seek, 1);

    player->Stop();

    /// Test case for CanControl Property.
    r = sd_bus_call_method(bus,
        "org.mpris.MediaPlayer2.MPRIS-properties-test",
        "/org/mpris/MediaPlayer2",
        "org.freedesktop.DBus.Properties",
        "Get",
        NULL,
        &m,
        "ss",
        "org.mpris.MediaPlayer2.Player",
        "CanControl");

    EXPECT_GT(r, 0);

    int can_control;

    r = sd_bus_message_enter_container(m, 'v', "b");
    EXPECT_GT(r, 0);

    r = sd_bus_message_read_basic(m, 'b', &can_control);
    EXPECT_GT(r, 0);

    r = sd_bus_message_exit_container(m);
    EXPECT_GT(r, 0);

    EXPECT_EQ(can_control, 1);

    /// Test case for CanLoop Property.
    r = sd_bus_call_method(bus,
        "org.mpris.MediaPlayer2.MPRIS-properties-test",
        "/org/mpris/MediaPlayer2",
        "org.freedesktop.DBus.Properties",
        "Get",
        NULL,
        &m,
        "ss",
        "org.mpris.MediaPlayer2.Player",
        "CanLoop");

    EXPECT_GT(r, 0);

    int can_loop;

    r = sd_bus_message_enter_container(m, 'v', "b");
    EXPECT_GT(r, 0);

    r = sd_bus_message_read_basic(m, 'b', &can_loop);
    EXPECT_GT(r, 0);

    r = sd_bus_message_exit_container(m);
    EXPECT_GT(r, 0);

    EXPECT_EQ(can_loop, 0);

    auto onLoopModeChanged = std::bind(std::mem_fn(&MPRISTest::OnLoopModeChanged), this, _1);
    player->SetLoopModeChangeCb(onLoopModeChanged);

    r = sd_bus_call_method(bus,
        "org.mpris.MediaPlayer2.MPRIS-properties-test",
        "/org/mpris/MediaPlayer2",
        "org.freedesktop.DBus.Properties",
        "Get",
        NULL,
        &m,
        "ss",
        "org.mpris.MediaPlayer2.Player",
        "CanLoop");

    EXPECT_GT(r, 0);

    r = sd_bus_message_enter_container(m, 'v', "b");
    EXPECT_GT(r, 0);

    r = sd_bus_message_read_basic(m, 'b', &can_loop);
    EXPECT_GT(r, 0);

    r = sd_bus_message_exit_container(m);
    EXPECT_GT(r, 0);

    EXPECT_EQ(can_loop, 1);

    /// Test case for CanShuffle Property.
    r = sd_bus_call_method(bus,
        "org.mpris.MediaPlayer2.MPRIS-properties-test",
        "/org/mpris/MediaPlayer2",
        "org.freedesktop.DBus.Properties",
        "Get",
        NULL,
        &m,
        "ss",
        "org.mpris.MediaPlayer2.Player",
        "CanShuffle");

    EXPECT_GT(r, 0);

    int can_shuffle;

    r = sd_bus_message_enter_container(m, 'v', "b");
    EXPECT_GT(r, 0);

    r = sd_bus_message_read_basic(m, 'b', &can_shuffle);
    EXPECT_GT(r, 0);

    r = sd_bus_message_exit_container(m);
    EXPECT_GT(r, 0);

    EXPECT_EQ(can_shuffle, 0);

    auto onShuffleStatusChanged = std::bind(std::mem_fn(&MPRISTest::OnShuffleStatusChanged), this, _1);
    player->SetShuffleStatusChangeCb(onShuffleStatusChanged);

    r = sd_bus_call_method(bus,
        "org.mpris.MediaPlayer2.MPRIS-properties-test",
        "/org/mpris/MediaPlayer2",
        "org.freedesktop.DBus.Properties",
        "Get",
        NULL,
        &m,
        "ss",
        "org.mpris.MediaPlayer2.Player",
        "CanShuffle");

    EXPECT_GT(r, 0);

    r = sd_bus_message_enter_container(m, 'v', "b");
    EXPECT_GT(r, 0);

    r = sd_bus_message_read_basic(m, 'b', &can_shuffle);
    EXPECT_GT(r, 0);

    r = sd_bus_message_exit_container(m);
    EXPECT_GT(r, 0);

    EXPECT_EQ(can_shuffle, 1);
}
