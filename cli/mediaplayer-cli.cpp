/*
 * Copyright (c) 2018, The Linux Foundation. All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above
 *       copyright notice, this list of conditions and the following
 *       disclaimer in the documentation and/or other materials provided
 *       with the distribution.
 *     * Neither the name of The Linux Foundation nor the names of its
 *       contributors may be used to endorse or promote products derived
 *       from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS
 * BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
 * BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
 * IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include "mediaplayer-cli.h"
#include <chrono>
#include <cstring>
#include <iostream>

#include <glib.h>

using namespace std::placeholders;

const char *commands[] = {"play", "pause", "stop", "getduration",
    "getposition", "seek", "setrate", "getmute", "getvolume", "setmute", "setvolume", "quit", "help", NULL};

const char *commands_help[] = {"play <URI | ABSPATH>", NULL, NULL,
    NULL, NULL, "seek <hh:mm:ss>", NULL, NULL, NULL};

void FrontEnd::OnMuteUpdated(bool mute_status) {
    std::cout << "Mute Status Updated : " << mute_status << std::endl;
}

void FrontEnd::OnVolumeUpdated(double volume) {
    std::cout << "Volume Updated : " << volume << std::endl;
}

void FrontEnd::OnRateUpdateRequest(double rate) {
    std::cout << "Change Rate Request to " << rate << std::endl;
    player_->SetRate(rate);
}
FrontEnd::FrontEnd(adk::MediaPlayer::StreamType stream_type)
    : player_(adk::MediaPlayer::Create("mediaplayer-cli", stream_type)) {
    if (player_ == nullptr) {
        std::cout << "Player Can't be Created" << std::endl;
    }

    auto onMuteUpdated = std::bind(std::mem_fn(&FrontEnd::OnMuteUpdated), this, _1);
    auto onVolumeUpdated = std::bind(std::mem_fn(&FrontEnd::OnVolumeUpdated), this, _1);
    auto onRateRequestUpdate = std::bind(std::mem_fn(&FrontEnd::OnRateUpdateRequest), this, _1);

    player_->SetOnMuteUpdatedCb(onMuteUpdated);
    player_->SetOnVolumeUpdatedCb(onVolumeUpdated);
    player_->SetOnRateChangeRequestCallback(onRateRequestUpdate);
    player_->SetSupportedRateRange(-4.0, 5.0);
}

FrontEnd::~FrontEnd() {
}

void FrontEnd::Usage() {
    for (int i = 0; commands[i]; i++)
        // Print command help if we have it, else just the command name
        std::cout << (commands_help[i] ? commands_help[i] : commands[i]) << std::endl;
}

static std::string TimeFormat(std::chrono::nanoseconds time) {
    std::chrono::hours hh = std::chrono::duration_cast<std::chrono::hours>(time);
    std::chrono::minutes mm =
        std::chrono::duration_cast<std::chrono::minutes>(time % std::chrono::hours(1));
    std::chrono::seconds ss =
        std::chrono::duration_cast<std::chrono::seconds>(time % std::chrono::minutes(1));

    std::string str = std::to_string(hh.count()) + ':' + std::to_string(mm.count()) + ':' + std::to_string(ss.count());

    return str;
}

static gint64 ToNanoSec(std::string arg) {
    unsigned int hour, minute, second;

    if (sscanf(arg.c_str(), "%u:%u:%u", &hour, &minute, &second) == 3) {
        std::chrono::hours hh(hour);
        std::chrono::minutes mm(minute);
        std::chrono::seconds ss(second);

        gint64 start = std::chrono::nanoseconds(hh).count() + std::chrono::nanoseconds(mm).count() + std::chrono::nanoseconds(ss).count();

        return start;
    }
    return -1;
}

bool FrontEnd::HandleInput(std::string cmd, std::string arg) {
    if (cmd == "play") {
        if (arg != "\0") {
            player_->SetSource(arg);
        }

        player_->Play();
    } else if (cmd == "pause") {
        player_->Pause();
    } else if (cmd == "stop") {
        player_->Stop();
    } else if (cmd == "getduration") {
        std::cout << "Duration = " << TimeFormat(player_->GetDuration()) << std::endl;
    } else if (cmd == "getposition") {
        std::cout << "Position = " << TimeFormat(player_->GetPosition()) << std::endl;
    } else if (cmd == "seek") {
        gint64 start = ToNanoSec(arg);
        if (start != -1) {
            player_->Seek(std::chrono::nanoseconds(start));
        } else {
            std::cout << "Enter time in correct format" << std::endl;
        }
    } else if (cmd == "setrate") {
        double rate = std::stod(arg);
        player_->SetRate(rate);
    } else if (cmd == "getmute") {
        std::cout << "Mute = " << player_->GetMute() << std::endl;
    } else if (cmd == "getvolume") {
        std::cout << "Volume = " << player_->GetVolume() << std::endl;
    } else if (cmd == "setmute") {
        bool value = false;

        if (arg == "true")
            value = true;
        else if (arg == "false")
            value = false;

        if (player_->SetMute(value))
            std::cout << "SetMute Successful" << std::endl;
        else
            std::cout << "SetMute Failed" << std::endl;
    } else if (cmd == "setvolume") {
        if (player_->SetVolume(std::stod(arg)))
            std::cout << "SetVolume Successful" << std::endl;
        else
            std::cout << "SetVolume Failed" << std::endl;
    } else if (cmd == "help") {
        Usage();
    } else if (cmd == "quit") {
        std::cout << "Exiting ..." << std::endl;
        return false;
    } else if (cmd == "") {
    } else {
        std::cout << "Wrong Input" << std::endl;
    }
    return true;
}

bool ParseInput(FrontEnd &app, const char *line) {
    int divider = 0;

    std::string in = line, cmd, arg;

    for (uint i = 0; i < in.length(); i++) {
        if (in[i] != ' ') {
            if (divider == 0) {
                cmd.push_back(in[i]);
                if (in[i + 1] == ' ')
                    divider = 1;
            } else {
                arg.push_back(in[i]);
            }
        }
    }

    return app.HandleInput(cmd, arg);
}

void FrontEnd::Run() {
    std::string line;
    bool again = true;

    while (again) {
        std::cout << "> ";
        std::getline(std::cin, line);
        again = ParseInput(*this, line.c_str());
    }
}

static adk::MediaPlayer::StreamType DetectStreamType(char *stream_type) {
    if (strcmp("music", stream_type) == 0) {
        return adk::MediaPlayer::StreamType::Music;
    } else if (strcmp("cue", stream_type) == 0) {
        return adk::MediaPlayer::StreamType::Cue;
    } else if (strcmp("tone", stream_type) == 0) {
        return adk::MediaPlayer::StreamType::Tone;
    } else if (strcmp("alarm", stream_type) == 0) {
        return adk::MediaPlayer::StreamType::Alarm;
    } else if (strcmp("voiceui", stream_type) == 0) {
        return adk::MediaPlayer::StreamType::VoiceUI;
    } else if (strcmp("call", stream_type) == 0) {
        return adk::MediaPlayer::StreamType::Call;
    } else {
        std::cout << "No valid stream type choosing Music as defualt" << std::endl;
        return adk::MediaPlayer::StreamType::Music;
    }
}

int main(int argc G_GNUC_UNUSED, char *argv[]) {
    adk::MediaPlayer::StreamType stream_type = adk::MediaPlayer::StreamType::Music;
    if (argv[1] != NULL) {
        stream_type = DetectStreamType(argv[1]);
    } else {
        std::cout << "No stream type given setting Music as defualt" << std::endl;
    }

    FrontEnd app(stream_type);

    app.Run();

    return 0;
}
