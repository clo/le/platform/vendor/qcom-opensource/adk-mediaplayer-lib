﻿# adk-mediaplayer-lib

The ADK MediaPlayer Library provides an API for applications to easily play and
control media on QTI ADK platforms.

## Compilation

The library uses GStreamer as the underlying multimedia framework, so you need
GStreamer development packages for your distribution.

We use CMake for our build system. To compile tests, you need a local copy of
[Google Test](https://github.com/google/googletest), and you can set
`GTEST_DIR` to point CMake to it.

```
$ mkdir build && cd build
$ cmake .. -DGTEST_DIR=<googletestdir>
$ make
```

## Unit Tests

Unit tests are in the `test/` directory, and if they're built, you can run them
with.

```
$ <builddir>/test/mediaplayer-test
```

## Command Line

There is a small command-line test application that lets you exercise various functionality.
```
$ <builddir>/cli/mediaplayer-cli
))) play file:///path/to/some/media.mp3
))) help
...
```
