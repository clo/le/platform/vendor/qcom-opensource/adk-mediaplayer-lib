/*
 * Copyright (c) 2018-2020, The Linux Foundation. All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above
 *       copyright notice, this list of conditions and the following
 *       disclaimer in the documentation and/or other materials provided
 *       with the distribution.
 *     * Neither the name of The Linux Foundation nor the names of its
 *       contributors may be used to endorse or promote products derived
 *       from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS
 * BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
 * BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
 * IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef _MEDIAPLAYERPRIVATE__H
#define _MEDIAPLAYERPRIVATE__H

#include <gst/app/gstappsrc.h>
#include <gst/audio/audio-channels.h>
#include <gst/gst.h>

#include <glib-unix.h>
#include <stdint.h>
#include <dolby_dap.h>

#ifdef BASEMACHINE_qcs40x
#include <systemdq/sd-bus.h>
#endif

#ifdef BASEMACHINE_IPQx
#include <gio/gio.h>
#include <glib-unix.h>
#include <glib.h>
#endif

#include <condition_variable>
#include <cstdio>
#include <cstring>
#include <fstream>
#include <iterator>
#include <mutex>
#include <sstream>
#include <thread>
#include <unordered_map>
#include <functional>

#include <adk/config/config.h>
#include <adk/mediaplayer.h>

#include "mediaplayer-strings.h"

#define PA_MAX_CHANNEL_POSITION 25

typedef enum {
    GST_GRAPH_TYPE_DOLBY_DEC_OAR_DAP,
    GST_GRAPH_TYPE_DOLBY_DEC_DAP,
    GST_GRAPH_TYPE_DOLBY_DEC_ONLY,
} gst_dolby_graph_type_t;

typedef enum {
    GST_GRAPH_TYPE_DTSX_ONLY,
    GST_GRAPH_TYPE_DTSX_VIRTUALX
} gst_dts_graph_type_t;

typedef enum {
    GST_CODEC_TYPE_DDP,
    GST_CODEC_TYPE_DD,
    GST_CODEC_TYPE_MAT,
    GST_CODEC_TYPE_THD,
    GST_CODEC_TYPE_DTSX,
    GST_CODEC_TYPE_DTS_CD,
    GST_CODEC_TYPE_MP3,
    GST_CODEC_TYPE_AAC,
    GST_CODEC_TYPE_WMA,
    GST_CODEC_TYPE_ALS
} gst_codec_type_t;

typedef enum {
    GST_STREAM_TYPE_PCM,
    GST_STREAM_TYPE_PCM_ZERO,
    GST_STREAM_TYPE_ENCODED,
    GST_STREAM_TYPE_DTS_CD,
    GST_STREAM_TYPE_DSD,
    GST_STREAM_TYPE_ERROR
} gst_stream_type_t;

typedef enum {
    GST_SOURCE_FORMAT_PCM,
    GST_SOURCE_FORMAT_IEC61937,
    GST_SOURCE_FORMAT_DSD,
    GST_SOURCE_FORMAT_UNKNOWN
} gst_source_format_t;

typedef enum {
    GST_DEVICE_INACTIVE,
    GST_DEVICE_ACTIVE,
} gst_device_status_t;

typedef enum {
    PCM_NO_POSTPROC,
    PCM_DOLBY_DAP_POSTPROC,
    PCM_DTS_VIRTUALX_POSTPROC,
} gst_pcm_postproc_path;

typedef enum {
    DSD_PATH_NATIVE,
    DSD_PATH_NON_NATIVE,
} gst_dsd_path;

typedef enum { LATENCY_UNSET,
    LATENCY_NORMAL,
    LATENCY_LOW } latency_val;

static const struct {
    gchar *pa_pos_name;
    GstAudioChannelPosition gst_pos;
} pa_gst_ch_pos_table[] = {
    {"mono", GST_AUDIO_CHANNEL_POSITION_MONO},
    {"front-left", GST_AUDIO_CHANNEL_POSITION_FRONT_LEFT},
    {"front-right", GST_AUDIO_CHANNEL_POSITION_FRONT_RIGHT},
    {"front-center", GST_AUDIO_CHANNEL_POSITION_FRONT_CENTER},
    {"lfe", GST_AUDIO_CHANNEL_POSITION_LFE1},
    {"rear-left", GST_AUDIO_CHANNEL_POSITION_REAR_LEFT},
    {"rear-right", GST_AUDIO_CHANNEL_POSITION_REAR_RIGHT},
    {"front-left-of-center", GST_AUDIO_CHANNEL_POSITION_FRONT_LEFT_OF_CENTER},
    {"front-right-of-center", GST_AUDIO_CHANNEL_POSITION_FRONT_RIGHT_OF_CENTER},
    {"rear-center", GST_AUDIO_CHANNEL_POSITION_REAR_CENTER},
    {"side-left", GST_AUDIO_CHANNEL_POSITION_SIDE_LEFT},
    {"side-right", GST_AUDIO_CHANNEL_POSITION_SIDE_RIGHT},
    {"top-front-left", GST_AUDIO_CHANNEL_POSITION_TOP_FRONT_LEFT},
    {"top-front-right", GST_AUDIO_CHANNEL_POSITION_TOP_FRONT_RIGHT},
    {"top-front-center", GST_AUDIO_CHANNEL_POSITION_TOP_FRONT_CENTER},
    {"top-center", GST_AUDIO_CHANNEL_POSITION_TOP_CENTER},
    {"top-rear-left", GST_AUDIO_CHANNEL_POSITION_TOP_REAR_LEFT},
    {"top-rear-right", GST_AUDIO_CHANNEL_POSITION_TOP_REAR_RIGHT},
    {"top-side-left", GST_AUDIO_CHANNEL_POSITION_TOP_SIDE_LEFT},
    {"top-side-right", GST_AUDIO_CHANNEL_POSITION_TOP_SIDE_RIGHT},
    {"top-rear-center", GST_AUDIO_CHANNEL_POSITION_TOP_REAR_CENTER},
    {"front-left-wide", GST_AUDIO_CHANNEL_POSITION_WIDE_LEFT},
    {"front-right-wide", GST_AUDIO_CHANNEL_POSITION_WIDE_RIGHT},
    {"surround-left", GST_AUDIO_CHANNEL_POSITION_SURROUND_LEFT},
    {"surround-right", GST_AUDIO_CHANNEL_POSITION_SURROUND_RIGHT}};

#define MAXRATE 384000
#define GST_DEVICE_NAMELEN (2048)

/* Default values for static params */
#define GST_DOLBY_SPKR_MASK 1039
#define GST_DOLBY_VIRT_FRONT_ANGLE 15
#define GST_DOLBY_VIRT_SURR_ANGLE 30
#define GST_DOLBY_VIRT_HIEGHT_ANGLE 15
#define GST_DTS_SPKR_MASK 47
#define GST_DTS_SETT1CCMODE 1
#define GST_DTS_EXTPP_BASSLEVEL 0.334
#define GST_DTS_EXTPP_DYNAMICS 0.3
#define GST_DTS_EXTPP_SPEAKERSIZE 3
#define GST_DTS_TBHDX_SMALL_SPKR_MASK 7
#define GST_DOLBY_VIRT_REAR_SURR_ANGLE 0

#define AROUND_PAR_EFFECT_STRENGTH_SPATIAL_RECOMMENDED 50

namespace adk {

class MediaPlayerPrivate {
 private:
    /* Live and non-live source common elements */
    enum class GstElemPropType {
        kDirect,
        kPointer
    };

    enum class Types {
        kUnknown,
        kUint8,
        kInt32,
        kUint32,
        kInt64,
        kUint64,
        kBool,
        kStr,
        kDouble,
        kVoidptr,
    };

    // structs to create hash functions for delay tables
    struct DecodeBinDelayParams {
        uint32_t sampling_rate;
        uint8_t channels;
        std::string bit_depth;
    };

    struct DecodeBinDelayHash {
        std::size_t operator() (DecodeBinDelayParams const& params) const noexcept {
            std::size_t h1 = std::hash<uint32_t>{}(params.sampling_rate);
            std::size_t h2 = std::hash<uint8_t>{}(params.channels);
            std::size_t h3 = std::hash<std::string>{}(params.bit_depth);
            return ((h1 ^ (h2 << 1)) >> 1) ^ (h3 << 1);
        }
    };

    struct DecodeBinDelayParamsEqual {
        bool operator()(const DecodeBinDelayParams& lhs, const DecodeBinDelayParams& rhs) const {
            // TODO: Match bitdepth as well when the right values are available
            return (lhs.sampling_rate == rhs.sampling_rate && lhs.channels == rhs.channels);
        }
    };

    struct PPBinDelayParams {
        uint32_t sampling_rate;
        uint8_t channels;
    };

    struct PPBinDelayHash {
        std::size_t operator() (PPBinDelayParams const& params) const noexcept {
            std::size_t h1 = std::hash<uint32_t>{}(params.sampling_rate);
            std::size_t h2 = std::hash<uint8_t>{}(params.channels);
            return (h1 ^ (h2 << 1));
        }
    };

    struct PPBinDelayParamsEqual {
        bool operator()(const PPBinDelayParams& lhs, const PPBinDelayParams& rhs) const {
            return (lhs.sampling_rate == rhs.sampling_rate && lhs.channels == rhs.channels);
        }
    };

    struct DelayFields {
        uint64_t src_delay = 10;
        uint64_t gst_pipeline_delay = src_delay;
        uint64_t pp_bin_delay = 0;
        uint64_t decode_bin_delay = 0;
        uint64_t sink_delay = 10;
        uint64_t pa_delay = 0;
        // TODO: Add different defaults for different types of elements
        const uint64_t kDecDelayDefault = 20;
        const uint64_t kPluginDelayDefault = 5;
    };

    // map {DbusPropName: Property_type(Basic or Structure), ElementPropertyName, ElementPropertyType, FunctionPointer(in case of struct prop)}
    typedef std::unordered_map<std::string, std::tuple<GstElemPropType, std::string, Types, std::function<void(void*)>>> PPTable;
    std::unordered_map<std::string, PPTable> pp_prop_match;

    // Delay Table with custom hash
    typedef std::unordered_map<DecodeBinDelayParams, uint64_t, DecodeBinDelayHash, DecodeBinDelayParamsEqual> DecoderDelayTable;
    typedef std::unordered_map<PPBinDelayParams, uint64_t, PPBinDelayHash, PPBinDelayParamsEqual> PPDelayTable;

    // Delay Variables
    DelayFields delay_fields;
    std::unordered_map<std::string, DecoderDelayTable> decode_bin_delay_map;
    std::unordered_map<std::string, PPDelayTable> pp_bin_delay_map;

    GstPipeline *pipeline = NULL;
    GstElement *bin = NULL;
    GstCaps *caps_ = NULL;
    DecodeBinDelayParams params_;
    GstElement *pulsesink = NULL;
    GstElement *pulsedirectsrc = NULL;
    GstElement *pulsedirectsink = NULL;
    GstElement *pulsesrc = NULL;
    GstElement *capsfilter = NULL;

    /* Live source decoder elements */
    GstElement *avdec_dsd = NULL;
    GstElement *ddpdec = NULL;
    GstElement *matdec = NULL;
    GstElement *thdec = NULL;
    GstElement *dtsxdec = NULL;
    GstElement *oar = NULL;
    ;
    GstElement *decbin = NULL;

    /* Live source decoder bin element */
    GstElement *decoder_bin = NULL;

    /* Live source parser elements */
    GstElement *stream_detect = NULL;
    GstElement *pcm_stream_detect = NULL;

    /* Live source PP elements */
    GstElement *dolby_postproc_bin = NULL;
    GstElement *datdownmix = NULL;
    GstElement *dap = NULL;
    GstElement *audioresampler = NULL;
    GstElement *virtualx_postproc_bin = NULL;
    GstElement *soundx = NULL;
    GstElement *mcdyn = NULL;
    GstElement *identity_sink = NULL;  // need to remove unused
    GstElement *identity_src = NULL;   // need to remove unused
    GstElement *wavenc = NULL;
#ifdef BASEMACHINE_qcs40x
    GstPad *melod_bin_src_pad = NULL;
    GstPad *melod_bin_sink_pad = NULL;
    GstElement *melod_postproc_bin = NULL;
    GstElement *melod_current = NULL; // last linked element in melod bin
    GstElement *melod_bassmanagement = NULL;
    GstElement *melod_matrix = NULL;
    GstElement *melod_around = NULL;
    GstElement *melod_drc = NULL;
    GstElement *melod_audioconvert_drc = NULL;
    GstElement *melod_audioconvert_matrix = NULL;
    GstElement *melod_audioconvert_around = NULL;
    GstElement *melod_audioconvert_bm = NULL;
#endif

    bool disable_ddp_force_timeslice_decoding = false;
    gst_codec_type_t cur_codec_type;
    gst_codec_type_t new_codec_type;
    // gint source = GST_DEVICE_SOURCE_HDMI;
    gint format = GST_SOURCE_FORMAT_UNKNOWN;
    gchar *sink_name = "splitter_music";
    gboolean print_available_sink_devices_and_exit = 0;  // need to remove
                                                         // unused
    gint dts_spkr_mask = GST_DTS_SPKR_MASK;
    gint sett1ccmode = GST_DTS_SETT1CCMODE;
    gint tbhdx_small_spkr_mask = GST_DTS_TBHDX_SMALL_SPKR_MASK;
    gdouble extpp_basslevel = GST_DTS_EXTPP_BASSLEVEL;
    gdouble extpp_dynamics = GST_DTS_EXTPP_DYNAMICS;
    gint extpp_speakersize = GST_DTS_EXTPP_SPEAKERSIZE;
    gst_dolby_graph_type_t dolby_graph = GST_GRAPH_TYPE_DOLBY_DEC_OAR_DAP;
    gst_dts_graph_type_t dts_graph = GST_GRAPH_TYPE_DTSX_ONLY;
    gst_pcm_postproc_path pcm_postproc_path = PCM_NO_POSTPROC;
    gboolean pause_frames = FALSE;
    gboolean stream_type_unknown = FALSE;
    gchar *cur_device_name = NULL;
    latency_val latency = LATENCY_UNSET;
    gboolean dsd_path = DSD_PATH_NATIVE;
    gboolean chosen_dsd_path = DSD_PATH_NATIVE;
    gst_stream_type_t cur_stream_type;
    gst_stream_type_t new_stream_type;
    gst_stream_type_t pcm_decoded_stream_type;
    gst_device_status_t device_status;

    GstElement *uridecodebin = NULL;
    GstAppSrc *appsrc = NULL;
    bool set_source_buffer_ = false;
    bool is_raw_format_ = false;
    bool is_wake_lock_ = false;
    bool is_live_src_ = false;

    // Gst Elements
    std::thread thread;
    std::mutex mutex;
    std::condition_variable cond;
    volatile bool thread_running = false;
    std::unique_lock<std::mutex> lock;

    // mutex to hold dbus process
    std::mutex dbus_mutex;

    GMainLoop *main_loop;
    GMainContext *thread_main_context;
    GSource *source = NULL;

#ifdef BASEMACHINE_qcs40x
    sd_bus_slot *slot = NULL, *player_slot = NULL, *vm_match_slot = NULL;
    sd_bus *d_bus = NULL;
    // map to match the effects uuid with element names <uuid, element_name>;
    std::unordered_map<std::string, std::string> effects_uuid_map;
    using GetAllFunction = int (*)(sd_bus_message*, void*, sd_bus_error*);
    std::unordered_map<std::string, std::tuple<std::string, std::string, GetAllFunction>> pp_elem_dbus_params_map;
#endif

#ifdef BASEMACHINE_IPQx
    GDBusConnection *connection = NULL;
    GDBusProxy *proxy = NULL;
    guint dbus_id = 0;
    guint registration_id = 0;
#endif

    std::map<MediaPlayer::Tag, std::string> metadata;
    MediaPlayer::StreamType stream_type;
    std::string app_name;
    MediaPlayer::State control_only_mediaplayer_state;
    bool mute_state = false;
    double volume = 1.0;
    bool local_cover_art = false;
    std::chrono::nanoseconds seek_offset = std::chrono::nanoseconds(0);
    bool skip_async = false;
    MediaPlayer::LoopMode loop_mode_ = MediaPlayer::LoopMode::None;
    bool shuffle_status_ = false;
    bool compressed_offload_flag = false;
    std::unique_ptr<adk::config::Config> config_;
    bool higher_sample_rate_flag = false;

    double rate_ = 1.0;
    double min_rate_ = 1.0;
    double max_rate_ = 1.0;
    int sample_rate_ = 0;
    int channel_count_ = 1;
    guint64 channel_mask_ = 0;

#ifdef BASEMACHINE_qcs40x
    int hw_channel_count_ = 0;
    int hw_height_channels_ = 0;
    int melod_flag = 0;
    std::unique_ptr<adk::config::Config> audiomanager_config_;
    // max channels supported by bass management is 12
    std::vector<uint16_t> channel_position_;
#endif

    std::function<void(MediaPlayer::ErrorType error_type, std::string error_msg,
        std::string debug_msg)>
        onError;
    std::function<void(void)> onAsyncDone;
    std::function<void(void)> onEndOfStream;
    std::function<void(MediaPlayer::State new_state)> onStateChanged;
    std::function<void(void)> onMetadataChanged;
    std::function<void(size_t size)> needData;
    std::function<void(void)> enoughData;
    std::function<void(uint64_t offset)> seekData;
    std::function<void(void)> onNext;
    std::function<void(void)> onPrevious;
    std::function<void(MediaPlayer::LoopMode)> onLoopModeChange;
    std::function<void(bool)> onShuffleStatusChange;
    std::function<void(unsigned int buffering_percent)> onBuffering;
    std::function<void(bool mute_state)> onMuteUpdated;
    std::function<void(double volume)> onVolumeUpdated;
    std::function<void(void)> onPlay;
    std::function<void(void)> onPause;
    std::function<void(void)> onStop;
    std::function<void(double)> onRateChangeRequest;
    std::function<void(uint64_t)> onTotalPipelineDelayChange;

    void WakeLockToggle(bool lock, std::string lock_name, int delay_ms);
#ifdef BASEMACHINE_qcs40x
    void CreateMelodPostprocBin();
#endif
    void CreateVirtualxPostprocBin();
    void CreateDolbyPostprocBin();
    void CreateDecoderBin();
    void SwapDecodersInDecoderBin();
    GstCaps *set_dsd_non_native_caps(GstCaps *caps);
    GstCaps *GetGstCaps(PASourceDeviceInfo_t info);
    static void RaisePauseBurstSignal();
    static gint SetPsrcOriginalBuffer(gpointer user_data);
    static gint SetPsrcBufferMat(gpointer user_data);
    static gboolean IsNonDolbyCodec(gst_codec_type_t codec_type);
    void StreamTypeChanged(gst_stream_type_t new_stream_type,
        gst_codec_type_t codec_type, MediaPlayerPrivate *priv);
    char * StreamTypeToString(gst_stream_type_t stream_type, gst_codec_type_t codec_type);
    guint64 GetChannelMaskFromChannelMap(guint8 channels, std::vector<std::string> channel_map);
    gint GetChannelPositionFromString(gchar *str);

 public:
    MediaPlayerPrivate();
    ~MediaPlayerPrivate();
    bool Init(std::string app_name, MediaPlayer::StreamType type,
        MediaPlayer::Flags flags, MediaPlayer::Properties props);
    bool SetSource(setup_info_t info);
    bool SetSource(std::string uri);
    bool SetSource(MediaPlayer::MediaInfo info,
        std::function<void(size_t size)> needDataCb,
        std::function<void(uint64_t offset)> seekDataCb,
        std::function<void(void)> enoughDataCb);
    bool SetSource(std::function<void(size_t size)> needDataCb,
        std::function<void(uint64_t size)> seekDataCb,
        std::function<void(void)> enoughDataCb);
    void SetPreferredSinkLatencyInMilliSeconds(std::chrono::milliseconds latency);
    void DeviceRemoved();
    void SetDolbyGraph(std::string graph);
    void SetVirtualXGraph(std::string graph);
    bool PushBuffer(void *data, size_t size);
    bool PushBuffer(void *data, size_t size, std::chrono::nanoseconds duration,
        std::chrono::nanoseconds timestamp);
    bool PushEndOfStream();
    size_t GetBufferLevel();
    bool Next();
    bool Previous();
    MediaPlayer::LoopMode GetLoopMode();
    void SetLoopMode(MediaPlayer::LoopMode mode);
    bool GetShuffleStatus();
    void SetShuffleStatus(bool status);
    MediaPlayer::Result Play();
    MediaPlayer::Result Pause();
    MediaPlayer::Result Stop();
    void SetSupportedRateRange(double min, double max);
    void SetRateRequest(double rate);
    double GetPlaybackRate();
    double GetMinPlaybackRate();
    double GetMaxPlaybackRate();
    bool SetRate(double rate);
    bool SetRate(double rate, std::chrono::seconds position);
    bool SetRate(double rate, uint64_t offset);
    std::chrono::nanoseconds GetDuration();
    std::chrono::nanoseconds GetPosition();
    MediaPlayer::Result Seek(std::chrono::nanoseconds position);
    MediaPlayer::State GetState();
    std::tuple<MediaPlayer::State, MediaPlayer::Result> GetCurrentState();
    bool IsSeekable();
    std::map<MediaPlayer::Tag, std::string> GetMetadata();
    void SetMetadata(std::map<MediaPlayer::Tag, std::string> metadata);
    bool GetMute();
    bool SetMute(bool value);
    double GetVolume();
    bool SetVolume(double value);
    void SetOnErrorCb(std::function<void(MediaPlayer::ErrorType error_type,
            std::string error_msg, std::string debug_msg)>
            onErrorCb);
    void SetOnEndOfStreamCb(std::function<void(void)> onEndOfStreamCb);
    void SetOnAsyncDoneCb(std::function<void(void)> onAsyncDoneCb);
    void SetOnStateChangedCb(
        std::function<void(MediaPlayer::State new_state)> onStateChangedCb);
    void SetOnMetadataChangedCb(std::function<void(void)> onMetadataChangedCb);
    void SetNextCb(std::function<void(void)> onNextCb);
    void SetPreviousCb(std::function<void(void)> onPreviousCb);
    void SetLoopModeChangeCb(
        std::function<void(MediaPlayer::LoopMode)> onLoopModeChangeCb);
    void SetShuffleStatusChangeCb(
        std::function<void(bool)> onShuffleStatusChangeCb);
    void SetBufferingCb(
        std::function<void(unsigned int buffering_percent)> onBufferingCb);
    void SetOnMuteUpdatedCb(
        std::function<void(bool mute_state)> onMuteUpdatedCb);
    void SetOnVolumeUpdatedCb(
        std::function<void(double volume)> onVolumeUpdatedCb);
    void SetPlayCb(std::function<void(void)> onPlayCb);
    void SetPauseCb(std::function<void(void)> onPauseCb);
    void SetStopCb(std::function<void(void)> onStopCb);
    void SetOnRateChangeRequestCallback(std::function<void(double)> onRateChangeRequestCb);
    void SetOnTotalPipelineDelayChangeCb(std::function<void(uint64_t)> onPipelineDelayChangeCb);

    void SystemWakeLockToggle(bool lock);
    MediaPlayer::State StateToEnum(GstState state);
    void MetadataChanged();
    void StateChanged(MediaPlayer::State state);
    void PlaybackStatusChanged(MediaPlayer::State state);
    void ControlOnlyStateChanged(MediaPlayer::State state);
    static gboolean BusCallback(GstBus *bus, GstMessage *msg, gpointer data);
    static gboolean MainloopIsRunning(MediaPlayerPrivate *priv);
    static void PadAdded(GstElement *dec, GstPad *pad, gpointer data);
    int DBusInit();
    void MPRISInit();
    void EmitSeekedSignal(gint64 value);
    void EmitPropertyChangeSignal(std::string property);
    static void MainloopThread(MediaPlayerPrivate *priv);
    const char *EnumToString(MediaPlayer::StreamType type);
    MediaPlayer::Result GstToMediaPlayerResult(GstStateChangeReturn ret);
    bool GstFlowReturnToBool(GstFlowReturn ret);
    static void FillMetadata(
        const GstTagList *list, const gchar *tag, gpointer user_data);
    void ClearMetadata();
    void InitGlobalSetting();
    void InitialiseMuteStatus();
    void InitialiseVolumeStatus();
    static void NeedDataCb(GstAppSrc *src, guint size, gpointer user_data);
    static void EnoughDataCb(GstAppSrc *src, gpointer user_data);
    static gboolean SeekDataCb(
        GstAppSrc *src, guint64 offset, gpointer user_data);
    static void SourceSetup(
        GstElement *pipeline, GstElement *source, gpointer user_data);
    static GstPadProbeReturn sd_pad_probe_event_cb_caps(
        GstPad *pad, GstPadProbeInfo *info, gpointer user_data);
    static GstPadProbeReturn sd_pad_probe_event_cb_pcm_detect(
        GstPad *pad, GstPadProbeInfo *info, gpointer user_data);
    static void ElementSetupCb(GstElement *pipeline, GstElement *element, gpointer user_data);
    static void DecodebinPadAddedCb(GstElement *element, GstPad *pad, gpointer data);
    void DBusDeinit();
    bool CanLoop();
    bool CanShuffle();
    bool CanGoNext();
    bool CanGoPrevious();
    MediaPlayer::StreamType GetStreamType();
    bool PlayCb();
    bool PauseCb();
    bool StopCb();
    void SetHigherSampleRate(bool flag);

    void InitPPPropMatchTables();
    PPTable GetPPPropMatchTable(std::string elem_name);
    void UpdatePPElementStatus(std::string elem_name, bool status) {
        g_print("PP Element update: %s %d\n", elem_name.c_str(), (status ? 1 : 0));
    }

    template<typename T>
    void UpdateGstElemPropertyInt(std::string elem_name, std::string dbus_prop_name, T value) {
        GstElement* elem = gst_bin_get_by_name(GST_BIN(pipeline), elem_name.c_str());
        PPTable pp_table = GetPPPropMatchTable(elem_name);
        // TODO: Check if the element is active in the pipeline
        if (!elem) {
            g_printerr("element is null, returning\n");
            return;
        }
        if (pp_table.find(dbus_prop_name) != pp_table.end()) {
            auto val = pp_table[dbus_prop_name];
            switch (std::get<0>(val)) {
                case GstElemPropType::kDirect:
                    SetElemPropertyInt(elem, std::get<1>(val), std::get<2>(val), value);
                    break;
                case GstElemPropType::kPointer:
                    std::get<3>(val)(&value);
                    break;
            }
        }
    }

    template<typename T>
    void SetElemPropertyInt(GstElement* elem, std::string prop_name, Types prop_type, T value) {
        switch (prop_type) {
            case Types::kInt32:
                g_object_set(G_OBJECT(elem), prop_name.c_str(), static_cast<int32_t>(value), NULL);
                break;
            case Types::kUint32:
                g_object_set(G_OBJECT(elem), prop_name.c_str(), static_cast<uint32_t>(value), NULL);
                break;
            case Types::kInt64:
                g_object_set(G_OBJECT(elem), prop_name.c_str(), static_cast<int64_t>(value), NULL);
                break;
            case Types::kUint64:
                g_object_set(G_OBJECT(elem), prop_name.c_str(), static_cast<uint64_t>(value), NULL);
                break;
            case Types::kBool:
                g_object_set(G_OBJECT(elem), prop_name.c_str(), static_cast<bool>(value), NULL);
                break;
            case Types::kDouble:
                g_object_set(G_OBJECT(elem), prop_name.c_str(), static_cast<double>(value), NULL);
                break;
            case Types::kUint8:
                g_object_set(G_OBJECT(elem), prop_name.c_str(), static_cast<uint8_t>(value), NULL);
            default:
                break;
        }
    }

    void UpdateGstElemPropertyChar(std::string elem_name, std::string dbus_prop_name, char *value);
    void UpdateGstElemPropertyStr(std::string elem_name, std::string dbus_prop_name, std::string value);

    // Callback functions for handling Dap pointer properties
    void UpdateDapEnableStatus(void* value);
    void VirtualizerFrontSpeakerAngle(void* value);
    void VirtualizerSurroundSpeakerAngle(void* value);
    void VirtualizerRearSurroundSpeakerAngle(void* value);
    void VirtualizerHeightSpeakerAngle(void* value);
    void VirtualizerRearHeightSpeakerAngle(void* value);
    void IntelligentEqEnable(void* value);
    void IntelligentEqAmount(void* value);
    void VolumeLevelerAmount(void* value);
    void VolumeLevelerInTarget(void* value);
    void VolumeLevelerOutTarget(void* value);
    void DialogEnhancerEnable(void* value);
    void DialogEnhancerAmount(void* value);
    void DialogEnhancerDuckingAmt(void* value);
    void MiDialogEnhancerEn(void* value);
    void MiVolLevelerEn(void* value);
    void MiIeqEn(void* value);
    void MiSurroundBoostEn(void* value);
    void BassEnhancerEnable(void* value);
    void BassEnhancerBoostSet(void* value);
    void BassEnhancerCutoffFreq(void* value);
    void BassEnhancerWidthSet(void* value);
    void RegulatorEnable(void* value);
    void RegulatorSpeakerDistEnable(void* value);
    void RegulatorRelaxationAmount(void* value);
    void RegulatorTimbrePreservation(void* value);

    // Delay Calculation functions
    void InitDelayTables();
    void UpdateDelayOnDecoderBinChange(std::vector<std::string> decode_bin_formats, DecodeBinDelayParams params);
    void UpdateDelayOnPPBinChange(std::vector<std::string> pp_bin_elements, PPBinDelayParams params);

    // Returns only the Gstreamer pipeline delay
    uint64_t GetGSTPipelineDelay();
    // Returns the total delay of gst pipeline and pulse sink
    uint64_t GetTotalDelay();

    // SD Bus method handlers.
#ifdef BASEMACHINE_qcs40x
    static gboolean ProcessDBusRequest(gint fd, GIOCondition cond, gpointer user_data);
    static int InitMuteCb(sd_bus_message *m, void *userdata, sd_bus_error *ret_error);
    static int InitVolumeCb(sd_bus_message *m, void *userdata, sd_bus_error *ret_error);
    static int PropertiesChangedCb(sd_bus_message *m G_GNUC_UNUSED, void *userdata, sd_bus_error *err);
    static int MethodHandlerCb(sd_bus_message *m, void *userdata, sd_bus_error *ret_error);
    int HandlePropertyUpdateOnGstElement(std::string elem_name, sd_bus_message *m);
    int HandleEffectsPropertyUpdate(sd_bus_message *m);
    std::unordered_map<std::string, bool> ParseSupportedEffects(sd_bus_message *m);
    std::vector<std::string> GetEnabledPPElementsList();
    void InitPPElementWithSystemValue(std::string elem_name);
    static int DapGetAllCb(sd_bus_message *m, void *userdata, sd_bus_error *ret_error);
    static int SoundxGetAllCb(sd_bus_message *m, void *userdata, sd_bus_error *ret_error);
    static int McdynamicsGetAllCb(sd_bus_message *m, void *userdata, sd_bus_error *ret_error);

    static int MelodGetAllCb(sd_bus_message *m, void *userdata, sd_bus_error *ret_error);
    void EmptyMelodBin();
    void MelodInit();
    void MelodLink();
    uint16_t StrToCHID(std::string channel_str);
    uint16_t StrToCHIDAround(std::string channel_str);
    uint16_t StrToCHIDMatrix(std::string channel_str);
    GstAudioChannelPosition StrToGSTPosition(std::string channel_str);
    uint16_t PositionToCHIDMatrix(GstAudioChannelPosition position);
    uint16_t PositionToCHIDAround(GstAudioChannelPosition position);
    void BMHelper(std::string key, void* value_p);
    void DRCHelper(std::string key, void* value_p);
    void MatrixHelper(std::string key, void* value_p);
    void AroundHelper(std::string key, void* value_p);

#endif

    // GDBUS method handlers.
#ifdef BASEMACHINE_IPQx
    static gboolean GDBusInit(gpointer user_data);
    static void on_name_acquired(
        GDBusConnection *connection, const gchar *name, gpointer user_data);
    static void on_bus_acquired(
        GDBusConnection *connection, const gchar *name, gpointer user_data);
    static void on_name_lost(
        GDBusConnection *connection, const gchar *name, gpointer user_data);
    static GDBusProxy *CreateProxy(GDBusConnection *connection,
        std::string service, std::string object_path, std::string interface);
    static void GDBusPropertiesChangedCb(GDBusConnection *connection,
        const gchar *sender_name, const gchar *object_path,
        const gchar *interface_name, const gchar *signal_name,
        GVariant *parameters, gpointer user_data);
    void UpdateProperty(std::string name);
#endif
};
}  // namespace adk

#endif
