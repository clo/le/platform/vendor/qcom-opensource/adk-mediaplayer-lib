/*
 * Copyright (c) 2020, The Linux Foundation. All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above
 *       copyright notice, this list of conditions and the following
 *       disclaimer in the documentation and/or other materials provided
 *       with the distribution.
 *     * Neither the name of The Linux Foundation nor the names of its
 *       contributors may be used to endorse or promote products derived
 *       from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS
 * BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
 * BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
 * IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef MEDIAPLAYER_STRINGS_H_
#define MEDIAPLAYER_STRINGS_H_
namespace adk {
namespace mediaplayerlib {
// src and sink strings
const char kPulseSrc[] = "pulsesrc";
const char kPulseDirectSrc[] = "pulsedirectsrc";
const char kPulseSink[] = "pulsesink";
const char kPulseDirectSink[] = "pulsedirectsink";

// Miscellaneous plugins
const char kIecParser[] = "iec61937parser";
const char kPcmDetector[] = "pcmdetector";
const char kIdentitySrc[] = "identity_src";
const char kIdentitySink[] = "identity_sink";

// Decoder bin element strings
const char kAvdecDsd[] = "avdec_dsd";
const char kDdpDecoder[] = "ddp_decoder";
const char kMatDecoder[] = "mat_decoder";
const char kTrueHdDecoder[] = "truehd_decoder";
const char kDtsxDecoder[] = "dtsx_decoder";
const char kOar[] = "oar";
const char kAacDecoder[] = "aac_decoder";
const char kMp3Decoder[] = "mp3_decoder";

// Codec format strings
const char kDd[] = "DD";
const char kDdp[] = "DDP";
const char kMat[] = "MAT";
const char kTrueHd[] = "TrueHD";
const char kDdAtmos[] = "DD ATMOS";
const char kDdCh[] = "DD CH";
const char kDdpAtmos[] = "DDP ATMOS";
const char kDdpCh[] = "DDP CH";
const char kDts[] = "DTS";
const char kDtsCd[] = "DTS CD";
const char kMp3[] = "MP3";
const char kAac[] = "AAC";
const char kWma[] = "WMA";
const char kPcm[] = "PCM";
const char kPcm0[] = "PCM Zero";
const char kDsd[] = "DSD";
const char kUnknown[] = "UNKNOWN";

// Decoder Delay use case strings
const char kLiveSourceBin[] = "live_source_bin";
const char kAppSrc[] = "appsrc";
const char kUridecodeBin[] = "uridecode-bin";

// Post processing bin element strings
const char kDap[] = "dap";
const char kDatDownmix[] = "datdownmix";
const char kAudioResampler[] = "audioresampler";
const char kDatDownmixPassthrough[] = "datdownmix-passthrough";
const char kAudioResamplerPassthrough[] = "audioresampler-passthrough";
const char kSoundx[] = "soundx";
const char kMcdynamics[] = "mcdynamics";
const char kMelod[] = "melod";
const char kMelodBin[] = "melod_postproc_bin";

// AudioManager Dbus strings
const char kAMGlobalControllerIface[] = "com.qualcomm.qti.adk.audiomanager.global_controller";
const char kAMGlobalControllerObjPath[] = "/com/qualcomm/qti/adk/audiomanager/global_controller";
const char kAMDeviceControllerObjPath[] = "/com/qualcomm/qti/adk/audiomanager/device_controller";
const char kAMDeviceControllerIface[] = "com.qualcomm.qti.adk.audiomanager.device_controller";
const char kAMDapIface[] = "com.qualcomm.qti.adk.audiomanager.dap";
const char kAMSoundxIface[] = "com.qualcomm.qti.adk.audiomanager.virtualx.soundx";
const char kAMMcdynamicsIface[] = "com.qualcomm.qti.adk.audiomanager.virtualx.mcdynamics";
const char kAMServiceName[] = "com.qualcomm.qti.adk.audiomanager";
const char kDbusPropIface[] = "org.freedesktop.DBus.Properties";
const char kAMEffectsControllerObjPath[] = "/com/qualcomm/qti/adk/audiomanager/effects";
const char kAMEffectsControllerIface[] = "com.qualcomm.qti.adk.audiomanager.effects";
const char kAMMelodIface[] = "com.qualcomm.qti.adk.audiomanager.melod";

const char kDapUuid[] = "109ddea4d91c4969a2346fbe2ad0ef3a";
const char kSoundxUuid[] = "ba53503374c641d0b2b02545f17a3a3c";
const char kMcdynamicsUuid[] = "a380398a4f4e448d8a412b8f204a5605";
const char kMelodUuid[] = "3bb071bf73994f2ebc5e4037ee75a5ff";

}  // namespace mediaplayerlib
}  // namespace adk
#endif  //MEDIAPLAYER_STRINGS_H_
