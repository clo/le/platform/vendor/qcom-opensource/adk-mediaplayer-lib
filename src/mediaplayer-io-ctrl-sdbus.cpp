/*
 * Copyright (c) 2018-2020, The Linux Foundation. All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above
 *       copyright notice, this list of conditions and the following
 *       disclaimer in the documentation and/or other materials provided
 *       with the distribution.
 *     * Neither the name of The Linux Foundation nor the names of its
 *       contributors may be used to endorse or promote products derived
 *       from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS
 * BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
 * BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
 * IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include <adk/mediaplayer.h>
#include "mediaplayer-private.h"

#include <melod/bass_management_calibration_api.h>
#include <melod/meloD_matrix_calibration_api.h>
#include <melod/meloD_multiband_drc_calibration_api.h>
#include <melod/melod_around_calibration_api.h>

namespace adk {
static int method_next(sd_bus_message *m, void *userdata, sd_bus_error *ret_error G_GNUC_UNUSED) {
    MediaPlayerPrivate *priv = static_cast<MediaPlayerPrivate *>(userdata);

    priv->Next();

    return sd_bus_reply_method_return(m, NULL);
}

static int method_previous(sd_bus_message *m, void *userdata G_GNUC_UNUSED, sd_bus_error *ret_error G_GNUC_UNUSED) {
    MediaPlayerPrivate *priv = static_cast<MediaPlayerPrivate *>(userdata);

    priv->Previous();

    return sd_bus_reply_method_return(m, NULL);
}

static int method_play(sd_bus_message *m G_GNUC_UNUSED, void *userdata, sd_bus_error *ret_error G_GNUC_UNUSED) {
    MediaPlayerPrivate *priv = static_cast<MediaPlayerPrivate *>(userdata);

    if (!priv->PlayCb()) {
        MediaPlayer::Result res = priv->Play();

        if (res == MediaPlayer::Result::Failure) {
            return -1;
        }
    }

    return sd_bus_reply_method_return(m, NULL);
}

static int method_pause(sd_bus_message *m G_GNUC_UNUSED, void *userdata, sd_bus_error *ret_error G_GNUC_UNUSED) {
    MediaPlayerPrivate *priv = static_cast<MediaPlayerPrivate *>(userdata);

    if (!priv->PauseCb()) {
        MediaPlayer::Result res = priv->Pause();

        if (res == MediaPlayer::Result::Failure) {
            return -1;
        }
    }

    return sd_bus_reply_method_return(m, NULL);
}

static int method_play_pause(sd_bus_message *m G_GNUC_UNUSED, void *userdata, sd_bus_error *ret_error G_GNUC_UNUSED) {
    MediaPlayerPrivate *priv = static_cast<MediaPlayerPrivate *>(userdata);

    if (priv->GetState() == MediaPlayer::State::Playing) {
        if (!priv->PauseCb()) {
            MediaPlayer::Result res = priv->Pause();

            if (res == MediaPlayer::Result::Failure)
                return -1;
        }
    } else {
        if (!priv->PlayCb()) {
            MediaPlayer::Result res = priv->Play();

            if (res == MediaPlayer::Result::Failure)
                return -1;
        }
    }

    return sd_bus_reply_method_return(m, NULL);
}

static int method_stop(sd_bus_message *m, void *userdata, sd_bus_error *ret_error G_GNUC_UNUSED) {
    MediaPlayerPrivate *priv = static_cast<MediaPlayerPrivate *>(userdata);

    if (!priv->StopCb()) {
        MediaPlayer::Result res = priv->Stop();

        if (res == MediaPlayer::Result::Failure) {
            return -1;
        }
    }

    return sd_bus_reply_method_return(m, NULL);
}

static int method_open_uri(sd_bus_message *m G_GNUC_UNUSED, void *userdata, sd_bus_error *ret_error G_GNUC_UNUSED) {
    MediaPlayerPrivate *priv = static_cast<MediaPlayerPrivate *>(userdata);
    const char *uri;

    int r = sd_bus_message_read(m, "s", &uri);
    if (r < 0) {
        return r;
    }

    if (priv->SetSource(uri) == false) {
        return -1;
    }

    if (priv->Play() == MediaPlayer::Result::Failure) {
        return -1;
    }

    return sd_bus_reply_method_return(m, NULL);
}

static int method_seek(sd_bus_message *m G_GNUC_UNUSED, void *userdata, sd_bus_error *ret_error G_GNUC_UNUSED) {
    MediaPlayerPrivate *priv = static_cast<MediaPlayerPrivate *>(userdata);
    int64_t t;
    std::chrono::nanoseconds time;

    int r = sd_bus_message_read(m, "x", &t);
    if (r < 0) {
        return r;
    }

    time = priv->GetPosition() + std::chrono::microseconds(t);

    // when seeked time is within duration
    if (time <= priv->GetDuration() && time >= std::chrono::nanoseconds(0)) {
        if (priv->Seek(time) == MediaPlayer::Result::Failure) {
            return -1;
        }
    }

    // when seeked time is beyond duration
    if (time > priv->GetDuration()) {
        priv->Next();
    }

    // when seeked time is before duration
    if (time < std::chrono::nanoseconds(0)) {
        if (priv->Seek(std::chrono::nanoseconds(0)) == MediaPlayer::Result::Failure) {
            return -1;
        }
    }

    return sd_bus_reply_method_return(m, NULL);
}

static int method_set_position(sd_bus_message *m G_GNUC_UNUSED, void *userdata, sd_bus_error *ret_error G_GNUC_UNUSED) {
    MediaPlayerPrivate *priv = static_cast<MediaPlayerPrivate *>(userdata);
    int64_t t;
    std::chrono::nanoseconds time;
    char *track_id;

    int r = sd_bus_message_read(m, "ox", &track_id, &t);
    if (r < 0) {
        return r;
    }

    time = std::chrono::microseconds(t);

    if (time <= priv->GetDuration() && time >= std::chrono::nanoseconds(0) && strcmp(track_id, "/org/mpris/MediaPlayer2/Track/0") == 0) {
        if (priv->Seek(time) == MediaPlayer::Result::Failure) {
            return -1;
        }
    }

    return sd_bus_reply_method_return(m, NULL);
}

static int property_get_playback_status(
    sd_bus *bus G_GNUC_UNUSED,
    const char *path G_GNUC_UNUSED,
    const char *interface G_GNUC_UNUSED,
    const char *property G_GNUC_UNUSED,
    sd_bus_message *reply,
    void *userdata G_GNUC_UNUSED,
    sd_bus_error *error G_GNUC_UNUSED) {
    MediaPlayerPrivate *priv = static_cast<MediaPlayerPrivate *>(userdata);

    const char *state;

    switch (priv->GetState()) {
        case MediaPlayer::State::Playing:
            state = "Playing";
            break;

        case MediaPlayer::State::Paused:
            state = "Paused";
            break;

        case MediaPlayer::State::Stopped:
            state = "Stopped";
            break;
    }

    return sd_bus_message_append(reply, "s", state);
}

static int property_get_loop_status(
    sd_bus *bus G_GNUC_UNUSED,
    const char *path G_GNUC_UNUSED,
    const char *interface G_GNUC_UNUSED,
    const char *property G_GNUC_UNUSED,
    sd_bus_message *reply,
    void *userdata,
    sd_bus_error *error G_GNUC_UNUSED) {
    MediaPlayerPrivate *priv = static_cast<MediaPlayerPrivate *>(userdata);

    const char *loop_status;

    switch (priv->GetLoopMode()) {
        case MediaPlayer::LoopMode::None:
            loop_status = "None";
            break;

        case MediaPlayer::LoopMode::Track:
            loop_status = "Track";
            break;

        case MediaPlayer::LoopMode::Playlist:
            loop_status = "Playlist";
            break;
    }

    return sd_bus_message_append(reply, "s", loop_status);
}

static int property_set_loop_status(
    sd_bus *bus G_GNUC_UNUSED,
    const char *path G_GNUC_UNUSED,
    const char *interface G_GNUC_UNUSED,
    const char *property G_GNUC_UNUSED,
    sd_bus_message *reply,
    void *userdata,
    sd_bus_error *error G_GNUC_UNUSED) {
    MediaPlayerPrivate *priv = static_cast<MediaPlayerPrivate *>(userdata);

    const char *loop_status;

    int r = sd_bus_message_read(reply, "s", &loop_status);
    if (r < 0) {
        return r;
    }

    MediaPlayer::LoopMode loop;

    if (strcmp(loop_status, "None") == 0)
        loop = MediaPlayer::LoopMode::None;
    else if (strcmp(loop_status, "Track") == 0)
        loop = MediaPlayer::LoopMode::Track;
    else if (strcmp(loop_status, "Playlist") == 0)
        loop = MediaPlayer::LoopMode::Playlist;

    priv->SetLoopMode(loop);

    return r;
}

static int property_get_rate(
    sd_bus *bus G_GNUC_UNUSED,
    const char *path G_GNUC_UNUSED,
    const char *interface G_GNUC_UNUSED,
    const char *property G_GNUC_UNUSED,
    sd_bus_message *reply,
    void *userdata G_GNUC_UNUSED,
    sd_bus_error *error G_GNUC_UNUSED) {
    MediaPlayerPrivate *priv = static_cast<MediaPlayerPrivate *>(userdata);
    auto rate = priv->GetPlaybackRate();
    return sd_bus_message_append(reply, "d", rate);
}

static int property_set_rate(
    sd_bus *bus G_GNUC_UNUSED,
    const char *path G_GNUC_UNUSED,
    const char *interface G_GNUC_UNUSED,
    const char *property G_GNUC_UNUSED,
    sd_bus_message *reply,
    void *userdata G_GNUC_UNUSED,
    sd_bus_error *error G_GNUC_UNUSED) {
    double rate;
    MediaPlayerPrivate *priv = static_cast<MediaPlayerPrivate *>(userdata);

    int r = sd_bus_message_read(reply, "d", &rate);
    if (r < 0) {
        return r;
    }

    priv->SetRateRequest(rate);
    return r;
}

static int property_get_shuffle(
    sd_bus *bus G_GNUC_UNUSED,
    const char *path G_GNUC_UNUSED,
    const char *interface G_GNUC_UNUSED,
    const char *property G_GNUC_UNUSED,
    sd_bus_message *reply,
    void *userdata,
    sd_bus_error *error G_GNUC_UNUSED) {
    MediaPlayerPrivate *priv = static_cast<MediaPlayerPrivate *>(userdata);

    return sd_bus_message_append(reply, "b", priv->GetShuffleStatus());
}

static int property_set_shuffle(
    sd_bus *bus G_GNUC_UNUSED,
    const char *path G_GNUC_UNUSED,
    const char *interface G_GNUC_UNUSED,
    const char *property G_GNUC_UNUSED,
    sd_bus_message *reply,
    void *userdata,
    sd_bus_error *error G_GNUC_UNUSED) {
    MediaPlayerPrivate *priv = static_cast<MediaPlayerPrivate *>(userdata);

    int shuffle;

    int r = sd_bus_message_read(reply, "b", &shuffle);
    if (r < 0) {
        return r;
    }

    bool shuffle_status = shuffle ? true : false;
    priv->SetShuffleStatus(shuffle_status);

    return r;
}

static int property_get_metadata(
    sd_bus *bus G_GNUC_UNUSED,
    const char *path G_GNUC_UNUSED,
    const char *interface G_GNUC_UNUSED,
    const char *property G_GNUC_UNUSED,
    sd_bus_message *reply,
    void *userdata,
    sd_bus_error *error G_GNUC_UNUSED) {
    MediaPlayerPrivate *priv = static_cast<MediaPlayerPrivate *>(userdata);
    const char *tag, *value;

    std::map<MediaPlayer::Tag, std::string> m = priv->GetMetadata();

    int r = sd_bus_message_open_container(reply, SD_BUS_TYPE_ARRAY, "{sv}");
    if (r < 0)
        return r;

    for (auto it : m) {
        switch (it.first) {
            case MediaPlayer::Tag::CoverArt:
                tag = "mpris:artUrl";
                break;

            case MediaPlayer::Tag::Duration:
                tag = "mpris:length";
                break;

            case MediaPlayer::Tag::Album:
                tag = "xesam:album";
                break;

            case MediaPlayer::Tag::Artist:
                tag = "xesam:artist";
                break;

            case MediaPlayer::Tag::AudioBitrate:
                tag = "xesam:audioBitrate";
                break;

            case MediaPlayer::Tag::Genre:
                tag = "xesam:genre";
                break;

            case MediaPlayer::Tag::Title:
                tag = "xesam:title";
                break;

            case MediaPlayer::Tag::Url:
                tag = "xesam:url";
                break;

            case MediaPlayer::Tag::Channels:
                tag = "xesam:audioChannels";
                break;

            case MediaPlayer::Tag::SampleFormat:
                tag = "adk:SampleFormat";
                break;

            case MediaPlayer::Tag::SampleRate:
                tag = "xesam:audioSampleRate";
                break;

            default:
                /* Unknown tag, we just skip it */
                continue;
        }

        r = sd_bus_message_open_container(reply, SD_BUS_TYPE_DICT_ENTRY, "sv");
        if (r < 0)
            return r;

        r = sd_bus_message_append_basic(reply, 's', tag);
        if (r < 0)
            return r;

        value = it.second.c_str();

        if (strcmp(tag, "xesam:album") == 0 || strcmp(tag, "xesam:title") == 0 || strcmp(tag, "xesam:url") == 0 || strcmp(tag, "mpris:artUrl") == 0 || strcmp(tag, "adk:SampleFormat") == 0 || strcmp(tag, "xesam:audioChannels") == 0) {
            r = sd_bus_message_open_container(reply, SD_BUS_TYPE_VARIANT, "s");
            if (r < 0)
                return r;

            r = sd_bus_message_append_basic(reply, 's', value);
            if (r < 0)
                return r;

            r = sd_bus_message_close_container(reply);
            if (r < 0)
                return r;
        } else if (strcmp(tag, "mpris:length") == 0) {
            int64_t tmp;

            sscanf(value, "%ld", &tmp);

            tmp = std::chrono::duration_cast<std::chrono::microseconds>(std::chrono::nanoseconds(tmp)).count();

            r = sd_bus_message_open_container(reply, SD_BUS_TYPE_VARIANT, "x");
            if (r < 0)
                return r;

            r = sd_bus_message_append(reply, "x", tmp);
            if (r < 0)
                return r;

            r = sd_bus_message_close_container(reply);
            if (r < 0)
                return r;
        } else if (strcmp(tag, "xesam:audioBitrate") == 0) {
            int32_t tmp;

            sscanf(value, "%d", &tmp);

            r = sd_bus_message_open_container(reply, SD_BUS_TYPE_VARIANT, "i");
            if (r < 0)
                return r;

            r = sd_bus_message_append(reply, "i", tmp);
            if (r < 0)
                return r;

            r = sd_bus_message_close_container(reply);
            if (r < 0)
                return r;
        } else if (strcmp(tag, "xesam:audioSampleRate") == 0) {
            double tmp;

            sscanf(value, "%lf", &tmp);

            r = sd_bus_message_open_container(reply, SD_BUS_TYPE_VARIANT, "d");
            if (r < 0)
                return r;

            r = sd_bus_message_append(reply, "d", tmp);
            if (r < 0)
                return r;

            r = sd_bus_message_close_container(reply);
            if (r < 0)
                return r;
        } else if (strcmp(tag, "xesam:artist") == 0 || strcmp(tag, "xesam:genre") == 0) {
            std::istringstream ss(value);
            std::string tmp;

            r = sd_bus_message_open_container(reply, SD_BUS_TYPE_VARIANT, "as");
            if (r < 0)
                return r;

            r = sd_bus_message_open_container(reply, SD_BUS_TYPE_ARRAY, "s");
            if (r < 0)
                return r;

            while (std::getline(ss, tmp, ',')) {
                r = sd_bus_message_append_basic(reply, 's', tmp.c_str());
                if (r < 0)
                    return r;
            }

            r = sd_bus_message_close_container(reply);
            if (r < 0)
                return r;

            r = sd_bus_message_close_container(reply);
            if (r < 0)
                return r;
        }

        r = sd_bus_message_close_container(reply);
        if (r < 0)
            return r;
    }

    r = sd_bus_message_open_container(reply, SD_BUS_TYPE_DICT_ENTRY, "sv");
    if (r < 0)
        return r;

    r = sd_bus_message_append_basic(reply, 's', "mpris:trackid");
    if (r < 0)
        return r;

    r = sd_bus_message_open_container(reply, SD_BUS_TYPE_VARIANT, "o");
    if (r < 0)
        return r;

    r = sd_bus_message_append_basic(reply, 'o', "/org/mpris/MediaPlayer2/Track/0");
    if (r < 0)
        return r;

    r = sd_bus_message_close_container(reply);
    if (r < 0)
        return r;

    r = sd_bus_message_close_container(reply);
    if (r < 0)
        return r;

    return sd_bus_message_close_container(reply);
}

static int property_get_volume(
    sd_bus *bus G_GNUC_UNUSED,
    const char *path G_GNUC_UNUSED,
    const char *interface G_GNUC_UNUSED,
    const char *property G_GNUC_UNUSED,
    sd_bus_message *reply,
    void *userdata,
    sd_bus_error *error G_GNUC_UNUSED) {
    MediaPlayerPrivate *priv = static_cast<MediaPlayerPrivate *>(userdata);

    auto volume = priv->GetVolume();

    return sd_bus_message_append(reply, "d", volume);
}

static int property_set_volume(
    sd_bus *bus G_GNUC_UNUSED,
    const char *path G_GNUC_UNUSED,
    const char *interface G_GNUC_UNUSED,
    const char *property G_GNUC_UNUSED,
    sd_bus_message *reply,
    void *userdata,
    sd_bus_error *error G_GNUC_UNUSED) {
    MediaPlayerPrivate *priv = static_cast<MediaPlayerPrivate *>(userdata);

    double volume;

    int r = sd_bus_message_read(reply, "d", &volume);
    if (r < 0) {
        return r;
    }

    if (!priv->SetVolume(volume))
        return -1;

    return r;
}

static int property_get_position(
    sd_bus *bus G_GNUC_UNUSED,
    const char *path G_GNUC_UNUSED,
    const char *interface G_GNUC_UNUSED,
    const char *property G_GNUC_UNUSED,
    sd_bus_message *reply,
    void *userdata,
    sd_bus_error *error G_GNUC_UNUSED) {
    MediaPlayerPrivate *priv = static_cast<MediaPlayerPrivate *>(userdata);

    auto position = std::chrono::duration_cast<std::chrono::microseconds>(priv->GetPosition());

    return sd_bus_message_append(reply, "x", position.count());
}

static int property_get_minimum_rate(
    sd_bus *bus G_GNUC_UNUSED,
    const char *path G_GNUC_UNUSED,
    const char *interface G_GNUC_UNUSED,
    const char *property G_GNUC_UNUSED,
    sd_bus_message *reply,
    void *userdata G_GNUC_UNUSED,
    sd_bus_error *error G_GNUC_UNUSED) {
    MediaPlayerPrivate *priv = static_cast<MediaPlayerPrivate *>(userdata);
    auto rate = priv->GetMinPlaybackRate();

    return sd_bus_message_append(reply, "d", rate);
}

static int property_get_maximum_rate(
    sd_bus *bus G_GNUC_UNUSED,
    const char *path G_GNUC_UNUSED,
    const char *interface G_GNUC_UNUSED,
    const char *property G_GNUC_UNUSED,
    sd_bus_message *reply,
    void *userdata G_GNUC_UNUSED,
    sd_bus_error *error G_GNUC_UNUSED) {
    MediaPlayerPrivate *priv = static_cast<MediaPlayerPrivate *>(userdata);
    auto rate = priv->GetMaxPlaybackRate();
    return sd_bus_message_append(reply, "d", rate);
}

static int property_get_can_go_next(
    sd_bus *bus G_GNUC_UNUSED,
    const char *path G_GNUC_UNUSED,
    const char *interface G_GNUC_UNUSED,
    const char *property G_GNUC_UNUSED,
    sd_bus_message *reply,
    void *userdata,
    sd_bus_error *error G_GNUC_UNUSED) {
    MediaPlayerPrivate *priv = static_cast<MediaPlayerPrivate *>(userdata);

    return sd_bus_message_append(reply, "b", priv->CanGoNext());
}

static int property_get_can_go_previous(
    sd_bus *bus G_GNUC_UNUSED,
    const char *path G_GNUC_UNUSED,
    const char *interface G_GNUC_UNUSED,
    const char *property G_GNUC_UNUSED,
    sd_bus_message *reply,
    void *userdata,
    sd_bus_error *error G_GNUC_UNUSED) {
    MediaPlayerPrivate *priv = static_cast<MediaPlayerPrivate *>(userdata);

    return sd_bus_message_append(reply, "b", priv->CanGoPrevious());
}

static int property_get_can_play(
    sd_bus *bus G_GNUC_UNUSED,
    const char *path G_GNUC_UNUSED,
    const char *interface G_GNUC_UNUSED,
    const char *property G_GNUC_UNUSED,
    sd_bus_message *reply,
    void *userdata G_GNUC_UNUSED,
    sd_bus_error *error G_GNUC_UNUSED) {
    return sd_bus_message_append(reply, "b", true);
}

static int property_get_can_pause(
    sd_bus *bus G_GNUC_UNUSED,
    const char *path G_GNUC_UNUSED,
    const char *interface G_GNUC_UNUSED,
    const char *property G_GNUC_UNUSED,
    sd_bus_message *reply,
    void *userdata G_GNUC_UNUSED,
    sd_bus_error *error G_GNUC_UNUSED) {
    return sd_bus_message_append(reply, "b", true);
}

static int property_get_can_seek(
    sd_bus *bus G_GNUC_UNUSED,
    const char *path G_GNUC_UNUSED,
    const char *interface G_GNUC_UNUSED,
    const char *property G_GNUC_UNUSED,
    sd_bus_message *reply,
    void *userdata,
    sd_bus_error *error G_GNUC_UNUSED) {
    MediaPlayerPrivate *priv = static_cast<MediaPlayerPrivate *>(userdata);
    bool value;

    if (priv->GetStreamType() == MediaPlayer::StreamType::ControlOnly)
        value = false;
    else
        value = priv->IsSeekable();

    return sd_bus_message_append(reply, "b", value);
}

static int property_get_can_control(
    sd_bus *bus G_GNUC_UNUSED,
    const char *path G_GNUC_UNUSED,
    const char *interface G_GNUC_UNUSED,
    const char *property G_GNUC_UNUSED,
    sd_bus_message *reply,
    void *userdata G_GNUC_UNUSED,
    sd_bus_error *error G_GNUC_UNUSED) {
    return sd_bus_message_append(reply, "b", true);
}

static int property_get_can_loop(
    sd_bus *bus G_GNUC_UNUSED,
    const char *path G_GNUC_UNUSED,
    const char *interface G_GNUC_UNUSED,
    const char *property G_GNUC_UNUSED,
    sd_bus_message *reply,
    void *userdata,
    sd_bus_error *error G_GNUC_UNUSED) {
    MediaPlayerPrivate *priv = static_cast<MediaPlayerPrivate *>(userdata);

    return sd_bus_message_append(reply, "b", priv->CanLoop());
}

static int property_get_can_shuffle(
    sd_bus *bus G_GNUC_UNUSED,
    const char *path G_GNUC_UNUSED,
    const char *interface G_GNUC_UNUSED,
    const char *property G_GNUC_UNUSED,
    sd_bus_message *reply,
    void *userdata,
    sd_bus_error *error G_GNUC_UNUSED) {
    MediaPlayerPrivate *priv = static_cast<MediaPlayerPrivate *>(userdata);

    return sd_bus_message_append(reply, "b", priv->CanShuffle());
}

static int property_get_gst_pipeline_delay(
    sd_bus *bus G_GNUC_UNUSED,
    const char *path G_GNUC_UNUSED,
    const char *interface G_GNUC_UNUSED,
    const char *property G_GNUC_UNUSED,
    sd_bus_message *reply,
    void *userdata,
    sd_bus_error *error G_GNUC_UNUSED) {
    MediaPlayerPrivate *priv = static_cast<MediaPlayerPrivate *>(userdata);

    return sd_bus_message_append(reply, "t", priv->GetGSTPipelineDelay());
}

// The vtable of our little object, implements the org.mpris.MediaPlayer2 interface
static const sd_bus_vtable root_mediaplayer_vtable[] = {
    SD_BUS_VTABLE_START(0),
    SD_BUS_VTABLE_END};

// The vtable of our little object, implements the org.mpris.MediaPlayer2.Player interface
static const sd_bus_vtable mediaplayer_vtable[] = {
    SD_BUS_VTABLE_START(0),
    SD_BUS_METHOD("Next", NULL, NULL, method_next, 0),
    SD_BUS_METHOD("Previous", NULL, NULL, method_previous, 0),
    SD_BUS_METHOD("Play", NULL, NULL, method_play, 0),
    SD_BUS_METHOD("Pause", NULL, NULL, method_pause, 0),
    SD_BUS_METHOD("PlayPause", NULL, NULL, method_play_pause, 0),
    SD_BUS_METHOD("Stop", NULL, NULL, method_stop, 0),
    SD_BUS_METHOD("OpenUri", "s", NULL, method_open_uri, 0),
    SD_BUS_METHOD("Seek", "x", NULL, method_seek, 0),
    SD_BUS_METHOD("SetPosition", "ox", NULL, method_set_position, 0),
    SD_BUS_SIGNAL("Seeked", "x", 0),
    SD_BUS_PROPERTY("PlaybackStatus", "s", property_get_playback_status, 0, SD_BUS_VTABLE_PROPERTY_EMITS_CHANGE),
    SD_BUS_WRITABLE_PROPERTY("LoopStatus", "s", property_get_loop_status, property_set_loop_status, 0, SD_BUS_VTABLE_PROPERTY_EMITS_CHANGE),
    SD_BUS_WRITABLE_PROPERTY("Rate", "d", property_get_rate, property_set_rate, 0, SD_BUS_VTABLE_PROPERTY_EMITS_CHANGE),
    SD_BUS_WRITABLE_PROPERTY("Shuffle", "b", property_get_shuffle, property_set_shuffle, 0, SD_BUS_VTABLE_PROPERTY_EMITS_CHANGE),
    SD_BUS_PROPERTY("Metadata", "a{sv}", property_get_metadata, 0, SD_BUS_VTABLE_PROPERTY_EMITS_CHANGE),
    SD_BUS_WRITABLE_PROPERTY("Volume", "d", property_get_volume, property_set_volume, 0, SD_BUS_VTABLE_PROPERTY_EMITS_CHANGE),
    SD_BUS_PROPERTY("Position", "x", property_get_position, 0, SD_BUS_VTABLE_PROPERTY_EMITS_CHANGE),
    SD_BUS_PROPERTY("MinimumRate", "d", property_get_minimum_rate, 0, SD_BUS_VTABLE_PROPERTY_EMITS_CHANGE),
    SD_BUS_PROPERTY("MaximumRate", "d", property_get_maximum_rate, 0, SD_BUS_VTABLE_PROPERTY_EMITS_CHANGE),
    SD_BUS_PROPERTY("CanGoNext", "b", property_get_can_go_next, 0, SD_BUS_VTABLE_PROPERTY_EMITS_CHANGE),
    SD_BUS_PROPERTY("CanGoPrevious", "b", property_get_can_go_previous, 0, SD_BUS_VTABLE_PROPERTY_EMITS_CHANGE),
    SD_BUS_PROPERTY("CanPlay", "b", property_get_can_play, 0, SD_BUS_VTABLE_PROPERTY_EMITS_CHANGE),
    SD_BUS_PROPERTY("CanPause", "b", property_get_can_pause, 0, SD_BUS_VTABLE_PROPERTY_EMITS_CHANGE),
    SD_BUS_PROPERTY("CanSeek", "b", property_get_can_seek, 0, SD_BUS_VTABLE_PROPERTY_EMITS_CHANGE),
    SD_BUS_PROPERTY("CanControl", "b", property_get_can_control, 0, SD_BUS_VTABLE_PROPERTY_EMITS_CHANGE),
    SD_BUS_PROPERTY("CanLoop", "b", property_get_can_loop, 0, SD_BUS_VTABLE_PROPERTY_EMITS_CHANGE),
    SD_BUS_PROPERTY("CanShuffle", "b", property_get_can_shuffle, 0, SD_BUS_VTABLE_PROPERTY_EMITS_CHANGE),
    SD_BUS_PROPERTY("GSTPipelineDelay", "t", property_get_gst_pipeline_delay, 0, SD_BUS_VTABLE_PROPERTY_EMITS_CHANGE),
    SD_BUS_VTABLE_END};

static int control_only_method_next(sd_bus_message *m, void *userdata, sd_bus_error *ret_error G_GNUC_UNUSED) {
    MediaPlayerPrivate *priv = static_cast<MediaPlayerPrivate *>(userdata);

    priv->Next();

    return sd_bus_reply_method_return(m, NULL);
}

static int control_only_method_previous(sd_bus_message *m, void *userdata G_GNUC_UNUSED, sd_bus_error *ret_error G_GNUC_UNUSED) {
    MediaPlayerPrivate *priv = static_cast<MediaPlayerPrivate *>(userdata);

    priv->Previous();

    return sd_bus_reply_method_return(m, NULL);
}

static int control_only_method_play(sd_bus_message *m G_GNUC_UNUSED, void *userdata, sd_bus_error *ret_error G_GNUC_UNUSED) {
    MediaPlayerPrivate *priv = static_cast<MediaPlayerPrivate *>(userdata);

    priv->ControlOnlyStateChanged(MediaPlayer::State::Playing);

    return sd_bus_reply_method_return(m, NULL);
}

static int control_only_method_pause(sd_bus_message *m G_GNUC_UNUSED, void *userdata, sd_bus_error *ret_error G_GNUC_UNUSED) {
    MediaPlayerPrivate *priv = static_cast<MediaPlayerPrivate *>(userdata);

    priv->ControlOnlyStateChanged(MediaPlayer::State::Paused);

    return sd_bus_reply_method_return(m, NULL);
}

static int control_only_method_play_pause(sd_bus_message *m G_GNUC_UNUSED, void *userdata, sd_bus_error *ret_error G_GNUC_UNUSED) {
    MediaPlayerPrivate *priv = static_cast<MediaPlayerPrivate *>(userdata);

    if (priv->GetState() == MediaPlayer::State::Playing) {
        priv->ControlOnlyStateChanged(MediaPlayer::State::Paused);
    } else {
        priv->ControlOnlyStateChanged(MediaPlayer::State::Playing);
    }

    return sd_bus_reply_method_return(m, NULL);
}

static int control_only_method_stop(sd_bus_message *m, void *userdata, sd_bus_error *ret_error G_GNUC_UNUSED) {
    MediaPlayerPrivate *priv = static_cast<MediaPlayerPrivate *>(userdata);

    priv->ControlOnlyStateChanged(MediaPlayer::State::Stopped);

    return sd_bus_reply_method_return(m, NULL);
}

// The vtable of our little object, implements the org.mpris.MediaPlayer2 interface
static const sd_bus_vtable control_only_root_mediaplayer_vtable[] = {
    SD_BUS_VTABLE_START(0),
    SD_BUS_VTABLE_END};

// The vtable of our little object, implements the org.mpris.MediaPlayer2.Player interface
static const sd_bus_vtable control_only_mediaplayer_vtable[] = {
    SD_BUS_VTABLE_START(0),
    SD_BUS_METHOD("Next", NULL, NULL, control_only_method_next, 0),
    SD_BUS_METHOD("Previous", NULL, NULL, control_only_method_previous, 0),
    SD_BUS_METHOD("Play", NULL, NULL, control_only_method_play, 0),
    SD_BUS_METHOD("Pause", NULL, NULL, control_only_method_pause, 0),
    SD_BUS_METHOD("PlayPause", NULL, NULL, control_only_method_play_pause, 0),
    SD_BUS_METHOD("Stop", NULL, NULL, control_only_method_stop, 0),
    SD_BUS_SIGNAL("Seeked", "x", 0),
    SD_BUS_PROPERTY("PlaybackStatus", "s", property_get_playback_status, 0, SD_BUS_VTABLE_PROPERTY_EMITS_CHANGE),
    SD_BUS_WRITABLE_PROPERTY("LoopStatus", "s", property_get_loop_status, property_set_loop_status, 0, SD_BUS_VTABLE_PROPERTY_EMITS_CHANGE),
    SD_BUS_WRITABLE_PROPERTY("Rate", "d", property_get_rate, property_set_rate, 0, SD_BUS_VTABLE_PROPERTY_EMITS_CHANGE),
    SD_BUS_WRITABLE_PROPERTY("Shuffle", "b", property_get_shuffle, property_set_shuffle, 0, SD_BUS_VTABLE_PROPERTY_EMITS_CHANGE),
    SD_BUS_PROPERTY("Metadata", "a{sv}", property_get_metadata, 0, SD_BUS_VTABLE_PROPERTY_EMITS_CHANGE),
    SD_BUS_WRITABLE_PROPERTY("Volume", "d", property_get_volume, property_set_volume, 0, SD_BUS_VTABLE_PROPERTY_EMITS_CHANGE),
    SD_BUS_PROPERTY("Position", "x", property_get_position, 0, SD_BUS_VTABLE_PROPERTY_EMITS_CHANGE),
    SD_BUS_PROPERTY("MinimumRate", "d", property_get_minimum_rate, 0, SD_BUS_VTABLE_PROPERTY_EMITS_CHANGE),
    SD_BUS_PROPERTY("MaximumRate", "d", property_get_maximum_rate, 0, SD_BUS_VTABLE_PROPERTY_EMITS_CHANGE),
    SD_BUS_PROPERTY("CanGoNext", "b", property_get_can_go_next, 0, SD_BUS_VTABLE_PROPERTY_EMITS_CHANGE),
    SD_BUS_PROPERTY("CanGoPrevious", "b", property_get_can_go_previous, 0, SD_BUS_VTABLE_PROPERTY_EMITS_CHANGE),
    SD_BUS_PROPERTY("CanPlay", "b", property_get_can_play, 0, SD_BUS_VTABLE_PROPERTY_EMITS_CHANGE),
    SD_BUS_PROPERTY("CanPause", "b", property_get_can_pause, 0, SD_BUS_VTABLE_PROPERTY_EMITS_CHANGE),
    SD_BUS_PROPERTY("CanSeek", "b", property_get_can_seek, 0, SD_BUS_VTABLE_PROPERTY_EMITS_CHANGE),
    SD_BUS_PROPERTY("CanControl", "b", property_get_can_control, 0, SD_BUS_VTABLE_PROPERTY_EMITS_CHANGE),
    SD_BUS_PROPERTY("CanLoop", "b", property_get_can_loop, 0, SD_BUS_VTABLE_PROPERTY_EMITS_CHANGE),
    SD_BUS_PROPERTY("CanShuffle", "b", property_get_can_shuffle, 0, SD_BUS_VTABLE_PROPERTY_EMITS_CHANGE),
    SD_BUS_PROPERTY("GSTPipelineDelay", "t", property_get_gst_pipeline_delay, 0, SD_BUS_VTABLE_PROPERTY_EMITS_CHANGE),
    SD_BUS_VTABLE_END};

void MediaPlayerPrivate::MPRISInit() {
    int res;

    do {
        if (d_bus) {
            res = sd_bus_add_object_vtable(d_bus,
                &slot,
                "/org/mpris/MediaPlayer2",  // object path
                "org.mpris.MediaPlayer2",   // interface name
                (stream_type == MediaPlayer::StreamType::ControlOnly) ? control_only_root_mediaplayer_vtable : root_mediaplayer_vtable,
                NULL);

            if (res < 0) {
                break;
            }

            res = sd_bus_add_object_vtable(d_bus,
                &player_slot,
                "/org/mpris/MediaPlayer2",        // object path
                "org.mpris.MediaPlayer2.Player",  // interface name
                (stream_type == MediaPlayer::StreamType::ControlOnly) ? control_only_mediaplayer_vtable : mediaplayer_vtable,
                this);

            if (res < 0) {
                break;
            }
        }
    } while (false);

    if (res < 0) {
        if (slot) {
            sd_bus_slot_unref(slot);
            slot = NULL;
        }

        if (player_slot) {
            sd_bus_slot_unref(player_slot);
            player_slot = NULL;
        }
    }

    return;
}

int MediaPlayerPrivate::PropertiesChangedCb(sd_bus_message *m G_GNUC_UNUSED, void *userdata, sd_bus_error *err G_GNUC_UNUSED) {
    auto priv = (MediaPlayerPrivate *)userdata;
    const char *iface_name;
    int r = sd_bus_message_read_basic(m, 's', &iface_name);

    if (std::string(iface_name) == std::string(adk::mediaplayerlib::kAMGlobalControllerIface)) {
        const char *contents = NULL;
        char type;
        r = sd_bus_message_peek_type(m, &type, &contents);

        if (SD_BUS_TYPE_ARRAY == type) {
            r = sd_bus_message_enter_container(m, type, contents);
            if (r < 0) {
                return r;
            }
            for (;;) {
                r = sd_bus_message_peek_type(m, &type, &contents);
                if (r < 0)
                    break;
                r = sd_bus_message_enter_container(m, type, contents);
                if (r <= 0)
                    break;
                {
                    r = sd_bus_message_read_basic(m, 's', &contents);
                    if (r == 0 || r < 0)
                        break;

                    std::string name(contents);

                    if (name.compare("Mute") == 0) {
                        int value;
                        r = sd_bus_message_peek_type(
                            m, &type, &contents);
                        if (r < 0)
                            break;
                        r = sd_bus_message_enter_container(m, type, contents);
                        if (r < 0)
                            break;
                        r = sd_bus_message_read_basic(m, 'b', &value);
                        if (r < 0)
                            break;

                        priv->mute_state = value ? true : false;

                        if (priv->onMuteUpdated)
                            priv->onMuteUpdated(priv->mute_state);

                        r = sd_bus_message_exit_container(m);
                    }

                    if (name.compare("Volume") == 0) {
                        double value;
                        r = sd_bus_message_peek_type(
                            m, &type, &contents);
                        if (r < 0)
                            break;
                        r = sd_bus_message_enter_container(m, type, contents);
                        if (r < 0)
                            break;
                        r = sd_bus_message_read_basic(m, 'd', &value);
                        if (r < 0)
                            break;

                        priv->volume = value;

                        if (priv->onVolumeUpdated)
                            priv->onVolumeUpdated(priv->volume);

                        r = sd_bus_message_exit_container(m);
                    }

                    /// Skip Processing of the rest of the containers data of variant type
                    r = sd_bus_message_skip(m, "v");
                }
                r = sd_bus_message_exit_container(m);
            }
            r = sd_bus_message_exit_container(m);
            if (r < 0)
                return r;
        }

    } else if (std::string(iface_name) == std::string(adk::mediaplayerlib::kAMDapIface)) {
        return priv->HandlePropertyUpdateOnGstElement(adk::mediaplayerlib::kDap, m);
    } else if (std::string(iface_name) == std::string(adk::mediaplayerlib::kAMSoundxIface)) {
        return priv->HandlePropertyUpdateOnGstElement(adk::mediaplayerlib::kSoundx, m);
    } else if (std::string(iface_name) == std::string(adk::mediaplayerlib::kAMMcdynamicsIface)) {
        return priv->HandlePropertyUpdateOnGstElement(adk::mediaplayerlib::kMcdynamics, m);
    } else if (std::string(iface_name) == std::string(adk::mediaplayerlib::kAMEffectsControllerIface)) {
        return priv->HandleEffectsPropertyUpdate(m);
    } else if (std::string(iface_name) == std::string(adk::mediaplayerlib::kAMDeviceControllerIface)) {
        int r = sd_bus_message_enter_container(m, SD_BUS_TYPE_ARRAY, "{sv}");
        if (r < 0) {
            return r;
        }
        for (;;) {
            r = sd_bus_message_enter_container(m, SD_BUS_TYPE_DICT_ENTRY, "sv");
            if (r <= 0) {
                break;
            }
            char *key;
            sd_bus_message_read_basic(m, SD_BUS_TYPE_STRING, &key);
            if (std::string(key) == "PADelay") {
                uint64_t value;
                r = sd_bus_message_enter_container(m, SD_BUS_TYPE_VARIANT, "t");
                if (r < 0) {
                    // exit dict container
                    sd_bus_message_exit_container(m);
                    break;
                }
                r = sd_bus_message_read_basic(m, SD_BUS_TYPE_UINT64, &value);
                priv->delay_fields.pa_delay = value;
                if (priv->onTotalPipelineDelayChange) {
                    priv->onTotalPipelineDelayChange(priv->GetTotalDelay());
                }
                // exit variant container
                sd_bus_message_exit_container(m);
            }
            // exit dict container
            sd_bus_message_exit_container(m);
        }
        // exit array container
        sd_bus_message_exit_container(m);
        if (r < 0) {
            return r;
        }
    } else if (std::string(iface_name) == std::string(adk::mediaplayerlib::kAMMelodIface)) {
        return priv->HandlePropertyUpdateOnGstElement(adk::mediaplayerlib::kMelodBin, m);
    }
    return 0;
}

void MediaPlayerPrivate::BMHelper(std::string key, void *value_p) {
    if (!melod_bassmanagement || key.empty() || !value_p) {
        g_printerr(
            "%s :: Line %d, melod_bassmanagement is null \n"
            "or key is null \n or value is null\n",
            __func__, __LINE__);
        return;
    }
    GST_CAT_DEBUG(GST_CAT_DEFAULT,"%s :: Line %d, key %s ", __func__, __LINE__, key.c_str());
    if (key.compare("enable") == 0) {
        bmgt_par_enable_t payload;
        //payload.enable = *(uint16_t *)value_p;
        payload.enable = *static_cast<uint16_t *>(value_p);
        GST_CAT_DEBUG(GST_CAT_DEFAULT,"value %d \n", payload.enable);
        g_object_set(
            G_OBJECT(melod_bassmanagement), "enable", &payload, NULL);
    } else if (key.compare("sub_woofer_out") == 0) {
        bmgt_par_auto_setup_t *payload_ptr = NULL;
        g_object_get(
            G_OBJECT(melod_bassmanagement), "auto-setup", &payload_ptr, NULL);
        if (payload_ptr != NULL) {
            if (*static_cast<bool *>(value_p)) {
                payload_ptr->bass_lfe_output =
                    BMGT_BASS_AND_LFE_TO_LARGE_AND_SUB;
            } else {
                payload_ptr->bass_lfe_output = BMGT_BASS_AND_LFE_TO_LARGE;
            }
            GST_CAT_DEBUG(GST_CAT_DEFAULT,"value %d \n", payload_ptr->bass_lfe_output);
            g_object_set(G_OBJECT(melod_bassmanagement), "auto-setup",
                payload_ptr, NULL);
            g_free(payload_ptr);
        }
    } else if (key.find("surr_") != std::string::npos) {
        bmgt_par_ch_spkr_map_t *spkr_map = NULL;
        bmgt_config_t *config = NULL;
        g_object_get(
            G_OBJECT(melod_bassmanagement), "bmgt-config", &config, NULL);
        g_object_get(
            G_OBJECT(melod_bassmanagement), "ch-spkr-map", &spkr_map, NULL);
        if (!config || !spkr_map) {
            g_printerr("Line: %d :: %s, config or spkr_map is null \n", __LINE__, __func__);
            return;
        }
        if (key.compare("surr_spk_left") == 0) {
            //char *value = (char *)value_p;
            char *value = static_cast<char *>(value_p);
            GST_CAT_DEBUG(GST_CAT_DEFAULT,"value %s \n", value);
            for (int i = 0; (i < config->channel_count) && (i < channel_position_.size()); i++) {
                if (channel_position_[i] == BMGT_CHID_LEFT_BACK) {
                    if (strcmp(value, "large") == 0) {
                        spkr_map->ch_spkr[i].id = BMGT_CHID_LEFT_BACK;
                        spkr_map->ch_spkr[i].size = BMGT_SIZE_LARGE;
                    } else if (strcmp(value, "small") == 0) {
                        spkr_map->ch_spkr[i].id = BMGT_CHID_LEFT_BACK;
                        spkr_map->ch_spkr[i].size = BMGT_SIZE_SMALL;
                    } else if (strcmp(value, "no") == 0) {
                        spkr_map->ch_spkr[i].id = BMGT_CHID_NONE;
                        spkr_map->ch_spkr[i].size = BMGT_SIZE_DEFAULT;
                    } else {
                        g_printerr("wrong input\n");
                        break;
                    }
                    g_object_set(G_OBJECT(melod_bassmanagement), "ch-spkr-map", spkr_map, NULL);
                }
            }
        } else if (key.compare("surr_spk_right") == 0) {
            //char *value = (char *)value_p;
            char *value = static_cast<char *>(value_p);
            GST_CAT_DEBUG(GST_CAT_DEFAULT,"value %s \n", value);
            for (int i = 0; (i < config->channel_count) && (i < channel_position_.size()); i++) {
                if (channel_position_[i] == BMGT_CHID_RIGHT_BACK) {
                    if (strcmp(value, "large") == 0) {
                        spkr_map->ch_spkr[i].id = BMGT_CHID_RIGHT_BACK;
                        spkr_map->ch_spkr[i].size = BMGT_SIZE_LARGE;
                    } else if (strcmp(value, "small") == 0) {
                        spkr_map->ch_spkr[i].id = BMGT_CHID_RIGHT_BACK;
                        spkr_map->ch_spkr[i].size = BMGT_SIZE_SMALL;
                    } else if (strcmp(value, "no") == 0) {
                        spkr_map->ch_spkr[i].id = BMGT_CHID_NONE;
                        spkr_map->ch_spkr[i].size = BMGT_SIZE_DEFAULT;
                    } else {
                        g_printerr("wrong input\n");
                        break;
                    }
                    g_object_set(G_OBJECT(melod_bassmanagement), "ch-spkr-map", spkr_map, NULL);
                }
            }
        } else if (key.compare("surr_h_left") == 0) {
            GST_CAT_DEBUG(GST_CAT_DEFAULT,"value %d \n", *static_cast<bool *>(value_p));
            for (int i = 0; (i < config->channel_count) && (i < channel_position_.size()); i++) {
                if (channel_position_[i] == BMGT_CHID_LEFT_BACK_HEIGHT) {
                    if (*(bool *)value_p) {
                        spkr_map->ch_spkr[i].id = BMGT_CHID_LEFT_BACK_HEIGHT;
                        spkr_map->ch_spkr[i].size = BMGT_SIZE_LARGE;
                    } else {
                        spkr_map->ch_spkr[i].id = BMGT_CHID_NONE;
                        spkr_map->ch_spkr[i].size = BMGT_SIZE_DEFAULT;
                    }
                    g_object_set(G_OBJECT(melod_bassmanagement), "ch-spkr-map", spkr_map, NULL);
                }
            }
        } else if (key.compare("surr_h_right") == 0) {
            bool value = *static_cast<bool *>(value_p);
            GST_CAT_DEBUG(GST_CAT_DEFAULT,"value %d \n", value);
            for (int i = 0; (i < config->channel_count) && (i < channel_position_.size()); i++) {
                if (channel_position_[i] == BMGT_CHID_RIGHT_BACK_HEIGHT) {
                    if (value) {
                        spkr_map->ch_spkr[i].id = BMGT_CHID_RIGHT_BACK_HEIGHT;
                        spkr_map->ch_spkr[i].size = BMGT_SIZE_LARGE;
                    } else {
                        spkr_map->ch_spkr[i].id = BMGT_CHID_NONE;
                        spkr_map->ch_spkr[i].size = BMGT_SIZE_DEFAULT;
                    }
                    g_object_set(G_OBJECT(melod_bassmanagement), "ch-spkr-map", spkr_map, NULL);
                }
            }
        }
        g_free(spkr_map);
        g_free(config);
    } else if (key.compare("freq") == 0) {
        bmgt_par_auto_setup_t *payload_ptr = NULL;
        g_object_get(
            G_OBJECT(melod_bassmanagement), "auto-setup", &payload_ptr, NULL);
        if (payload_ptr) {
            payload_ptr->stage1_freq = *static_cast<uint16_t *>(value_p);
            GST_CAT_DEBUG(GST_CAT_DEFAULT,"value %d \n", payload_ptr->stage1_freq);
            g_object_set(
                G_OBJECT(melod_bassmanagement), "auto-setup", payload_ptr, NULL);
            g_free(payload_ptr);
        }
    } else {
        g_printerr("key %s not supported", key);
    }
}

void MediaPlayerPrivate::DRCHelper(std::string key, void *value_p) {
    if (!melod_drc || key.empty() || !value_p) {
        g_printerr(
            "%s :: Line %d, melod_drc is null \n"
            "or key is null \n or value is null\n",
            __func__, __LINE__);
        return;
    }
    GST_CAT_DEBUG(GST_CAT_DEFAULT,"%s :: Line %d, started function\n key %s ", __func__, __LINE__, key.c_str());
    if (key.compare("enable") == 0) {
        mbdrc_par_drc_state_t payload;
        payload.enable = *static_cast<uint16_t *>(value_p);
        GST_CAT_DEBUG(GST_CAT_DEFAULT,"value %d \n", payload.enable);
        g_object_set(
            G_OBJECT(melod_drc), "enable", &payload, NULL);
    } else if (key.compare("mode") == 0) {
        mbdrc_par_mode_t *payload_ptr = NULL;
        g_object_get(
            G_OBJECT(melod_drc), "mode", &payload_ptr, NULL);
        if (payload_ptr != NULL) {
            char *value = static_cast<char *>(value_p);
            if (strcmp(value, "off") == 0) {
                payload_ptr->drc_mode = MBDRC_MODE_OFF;
            } else if (strcmp(value, "night_plus") == 0) {
                payload_ptr->drc_mode = MBDRC_MODE_NIGHT;
            } else if (strcmp(value, "volume_plus") == 0) {
                payload_ptr->drc_mode = MBDRC_MODE_VOLUME;
            } else {
                g_printerr("wrong input %s\n", value);
                g_free(payload_ptr);
                return;
            }

            GST_CAT_DEBUG(GST_CAT_DEFAULT,"value_str %s value %d \n", value, payload_ptr->drc_mode);
            g_object_set(G_OBJECT(melod_drc), "mode", payload_ptr, NULL);
            g_free(payload_ptr);
        }
    } else if (key.compare("ref_level") == 0) {
        mbdrc_par_mode_t *payload_ptr = NULL;
        g_object_get(
            G_OBJECT(melod_drc), "mode", &payload_ptr, NULL);
        if (payload_ptr != NULL) {
            payload_ptr->refLevel = *static_cast<int16_t *>(value_p);
            GST_CAT_DEBUG(GST_CAT_DEFAULT,"value %d \n", payload_ptr->refLevel);
            g_object_set(G_OBJECT(melod_drc), "mode", payload_ptr, NULL);
            g_free(payload_ptr);
        }
    } else {
        g_printerr("key %s not supported", key);
    }
}

void MediaPlayerPrivate::MatrixHelper(std::string key, void *value_p) {
    if (!melod_matrix || key.empty() || !value_p) {
        g_printerr(
            "%s :: Line %d, melod_matrix is null \n"
            "or key is null \n or value is null\n",
            __func__, __LINE__);
        return;
    }
    GST_CAT_DEBUG(GST_CAT_DEFAULT,"%s :: Line %d, started function\n key %s ", __func__, __LINE__, key.c_str());
    if (key.compare("enable") == 0) {
        matrix_par_state_t payload;
        payload.enable = *static_cast<uint16_t *>(value_p);
        GST_CAT_DEBUG(GST_CAT_DEFAULT,"value %d \n", payload.enable);
        g_object_set(
            G_OBJECT(melod_matrix), "enable", &payload, NULL);
    } else if (key.compare("mode") == 0) {
        matrix_par_ctrl_config_t *payload_ptr = NULL;
        g_object_get(
            G_OBJECT(melod_matrix), "ctrl-config", &payload_ptr, NULL);
        if (payload_ptr != NULL) {
            char *value = static_cast<char *>(value_p);
            GST_CAT_DEBUG(GST_CAT_DEFAULT,"value = %s\n", value);
            if (strcmp(value, "movie") == 0) {
                payload_ptr->decoder_mode = MATRIX_PAR_CTRL_CONFIG_DECODER_MOVIE;
            } else if (strcmp(value, "music") == 0) {
                payload_ptr->decoder_mode = MATRIX_PAR_CTRL_CONFIG_DECODER_MUSIC;
            } else {
                g_printerr("wrong input, value = %s\n", value);
                g_free(payload_ptr);
                return;
            }
            GST_CAT_DEBUG(GST_CAT_DEFAULT,"value %d \n", payload_ptr->decoder_mode);
            g_object_set(G_OBJECT(melod_matrix), "ctrl-config", payload_ptr, NULL);
            g_free(payload_ptr);
        }
    } else {
        g_printerr("key %s not supported", key);
    }
}

void MediaPlayerPrivate::AroundHelper(std::string key, void *value_p) {
    if (!melod_around || key.empty() || !value_p) {
        g_printerr(
            "%s :: Line %d, melod_around is null \n"
            "or key is null \n or value is null\n",
            __func__, __LINE__);
        return;
    }
    GST_CAT_DEBUG(GST_CAT_DEFAULT,"%s :: Line %d, started function\n key %s ", __func__, __LINE__, key.c_str());
    if (key.compare("enable") == 0) {
        around_par_state_t payload;
        payload.enable = *static_cast<uint16_t *>(value_p);
        GST_CAT_DEBUG(GST_CAT_DEFAULT,"value %d \n", payload.enable);
        g_object_set(G_OBJECT(melod_around), "enable", &payload, NULL);
    } else if (key.compare("effect") == 0) {
        around_par_effect_t *payload_ptr = NULL;
        g_object_get(
            G_OBJECT(melod_around), "effect-strength", &payload_ptr, NULL);
        if (payload_ptr != NULL) {
            uint16_t *value = static_cast<uint16_t *>(value_p);
            payload_ptr->strength_surround = *value;
            payload_ptr->strength_spatial = AROUND_PAR_EFFECT_STRENGTH_SPATIAL_RECOMMENDED;
            GST_CAT_DEBUG(GST_CAT_DEFAULT,"value %d \n", payload_ptr->strength_surround);
            g_object_set(G_OBJECT(melod_around), "effect-strength", payload_ptr, NULL);
            g_free(payload_ptr);
        }
    } else if (key.compare("mode") == 0) {
        around_par_ctrl_config_t *payload_ptr = NULL;
        g_object_get(G_OBJECT(melod_around), "ctrl-config", &payload_ptr, NULL);
        if (payload_ptr != NULL) {
            char *value = static_cast<char *>(value_p);
            if (strcmp(value, "movie") == 0) {
                payload_ptr->mode_center = AROUND_PAR_CTRL_CONFIG_CENTER_MODE_MOVIE;
            } else if (strcmp(value, "music") == 0) {
                payload_ptr->mode_center = AROUND_PAR_CTRL_CONFIG_CENTER_MODE_MUSIC;
            } else {
                g_printerr("wrong input, value = %s\n", value);
                g_free(payload_ptr);
                return;
            }
            GST_CAT_DEBUG(GST_CAT_DEFAULT,"value %d \n", payload_ptr->mode_center);
            g_object_set(G_OBJECT(melod_around), "ctrl-config", payload_ptr, NULL);
            g_free(payload_ptr);
        }
    } else {
        g_printerr("key %s not supported", key);
    }
}

int MediaPlayerPrivate::MelodGetAllCb(sd_bus_message *m, void *userdata, sd_bus_error *ret_error) {
    g_printerr("%s :: Line %d, started function", __func__, __LINE__);
    auto priv = (MediaPlayerPrivate *)userdata;
    const sd_bus_error *err;
    err = sd_bus_message_get_error(m);
    if (err) {
        int r = sd_bus_error_get_errno(err);
        g_printerr("error code r = %d %s", r, err->message);
        return r;
    }
    return priv->HandlePropertyUpdateOnGstElement(adk::mediaplayerlib::kMelod, m);
}

uint16_t MediaPlayerPrivate::StrToCHID(std::string channel_str) {
    uint16_t chid;
    if (channel_str.compare("front-left") == 0) {
        chid = BMGT_CHID_LEFT_FRONT;
    } else if (channel_str.compare("front-right") == 0) {
        chid = BMGT_CHID_RIGHT_FRONT;
    } else if (channel_str.compare("front-center") == 0) {
        chid = BMGT_CHID_CENTER_FRONT;
    } else if (channel_str.compare("lfe") == 0) {
        chid = BMGT_CHID_LFE;
    } else if (channel_str.compare("rear-left") == 0) {
        chid = BMGT_CHID_LEFT_BACK;
    } else if (channel_str.compare("rear-right") == 0) {
        chid = BMGT_CHID_RIGHT_BACK;
    } else if (channel_str.compare("side-left") == 0) {
        chid = BMGT_CHID_LEFT_SURROUND;
    } else if (channel_str.compare("side-right") == 0) {
        chid = BMGT_CHID_RIGHT_SURROUND;
    } else if (channel_str.compare("top-front-left") == 0) {
        chid = BMGT_CHID_LEFT_FRONT_HEIGHT;
    } else if (channel_str.compare("top-front-right") == 0) {
        chid = BMGT_CHID_RIGHT_FRONT_HEIGHT;
    } else if (channel_str.compare("top-rear-left") == 0) {
        chid = BMGT_CHID_LEFT_BACK_HEIGHT;
    } else if (channel_str.compare("top-rear-right") == 0) {
        chid = BMGT_CHID_RIGHT_BACK_HEIGHT;
    } else {
        chid = BMGT_CHID_NONE;
    }
    return chid;
}

uint16_t MediaPlayerPrivate::StrToCHIDAround(std::string channel_str) {
    uint16_t chid = AROUND_PAR_CHMAP_ID_NONE;
    if (channel_str == "front-left") {
        chid = AROUND_PAR_CHMAP_ID_LEFT_FRONT;
    } else if (channel_str == "front-right") {
        chid = AROUND_PAR_CHMAP_ID_RIGHT_FRONT;
    } else if (channel_str == "front-center") {
        chid = AROUND_PAR_CHMAP_ID_CENTER_FRONT;
    } else if (channel_str == "lfe") {
        chid = AROUND_PAR_CHMAP_ID_LFE;
    } else if (channel_str == "rear-left") {
        chid = AROUND_PAR_CHMAP_ID_LEFT_BACK;
    } else if (channel_str == "rear-right") {
        chid = AROUND_PAR_CHMAP_ID_RIGHT_BACK;
    } else if (channel_str == "side-left") {
        chid = AROUND_PAR_CHMAP_ID_LEFT_SURROUND;
    } else if (channel_str == "side-right") {
        chid = AROUND_PAR_CHMAP_ID_RIGHT_SURROUND;
    }
    return chid;
}

uint16_t MediaPlayerPrivate::StrToCHIDMatrix(std::string channel_str) {
    uint16_t chid = MATRIX_PAR_CHMAP_ID_NONE;
    if (channel_str == "front-left") {
        chid = MATRIX_PAR_CHMAP_ID_LEFT_FRONT;
    } else if (channel_str == "front-right") {
        chid = MATRIX_PAR_CHMAP_ID_RIGHT_FRONT;
    } else if (channel_str == "front-center") {
        chid = MATRIX_PAR_CHMAP_ID_CENTER_FRONT;
    } else if (channel_str == "lfe") {
        chid = MATRIX_PAR_CHMAP_ID_LFE;
    } else if (channel_str == "rear-left") {
        chid = MATRIX_PAR_CHMAP_ID_LEFT_BACK;
    } else if (channel_str == "rear-right") {
        chid = MATRIX_PAR_CHMAP_ID_RIGHT_BACK;
    } else if (channel_str == "side-left") {
        chid = MATRIX_PAR_CHMAP_ID_LEFT_SURROUND;
    } else if (channel_str == "side-right") {
        chid = MATRIX_PAR_CHMAP_ID_RIGHT_SURROUND;
    }
    return chid;
}
GstAudioChannelPosition MediaPlayerPrivate::StrToGSTPosition(std::string channel_str) {
    GstAudioChannelPosition chid = GST_AUDIO_CHANNEL_POSITION_NONE;
    if (channel_str == "front-left") {
        chid = GST_AUDIO_CHANNEL_POSITION_FRONT_LEFT;
    } else if (channel_str == "front-right") {
        chid = GST_AUDIO_CHANNEL_POSITION_FRONT_RIGHT;
    } else if (channel_str == "front-center") {
        chid = GST_AUDIO_CHANNEL_POSITION_FRONT_CENTER;
    } else if (channel_str == "lfe") {
        chid = GST_AUDIO_CHANNEL_POSITION_LFE1;
    } else if (channel_str == "rear-left") {
        chid = GST_AUDIO_CHANNEL_POSITION_REAR_LEFT;
    } else if (channel_str == "rear-right") {
        chid = GST_AUDIO_CHANNEL_POSITION_REAR_RIGHT;
    } else if (channel_str == "side-left") {
        chid = GST_AUDIO_CHANNEL_POSITION_SURROUND_LEFT;
    } else if (channel_str == "side-right") {
        chid = GST_AUDIO_CHANNEL_POSITION_SURROUND_RIGHT;
    } if (channel_str == "top-front-left") {
        chid = GST_AUDIO_CHANNEL_POSITION_TOP_FRONT_LEFT;
    } else if (channel_str == "top-front-right") {
        chid = GST_AUDIO_CHANNEL_POSITION_TOP_FRONT_RIGHT;
    } else if (channel_str == "top-rear-left") {
        chid = GST_AUDIO_CHANNEL_POSITION_TOP_REAR_LEFT;
    } else if (channel_str == "top-rear-right") {
        chid = GST_AUDIO_CHANNEL_POSITION_TOP_REAR_RIGHT;
    }
    return chid;
}
uint16_t MediaPlayerPrivate::PositionToCHIDMatrix(GstAudioChannelPosition position) {
    uint16_t chid;
    switch (position) {
        case GST_AUDIO_CHANNEL_POSITION_MONO:
        case GST_AUDIO_CHANNEL_POSITION_FRONT_LEFT:
            chid = MATRIX_PAR_CHMAP_ID_LEFT_FRONT;
            break;
        case GST_AUDIO_CHANNEL_POSITION_FRONT_RIGHT:
            chid = MATRIX_PAR_CHMAP_ID_RIGHT_FRONT;
            break;
        case GST_AUDIO_CHANNEL_POSITION_FRONT_CENTER:
            chid = MATRIX_PAR_CHMAP_ID_CENTER_FRONT;
            break;
        case GST_AUDIO_CHANNEL_POSITION_LFE1:
            chid = MATRIX_PAR_CHMAP_ID_LFE;
            break;
        case GST_AUDIO_CHANNEL_POSITION_REAR_LEFT:
            chid = MATRIX_PAR_CHMAP_ID_LEFT_BACK;
            break;
        case GST_AUDIO_CHANNEL_POSITION_REAR_RIGHT:
            chid = MATRIX_PAR_CHMAP_ID_RIGHT_BACK;
            break;
        case GST_AUDIO_CHANNEL_POSITION_SURROUND_LEFT:
            chid = MATRIX_PAR_CHMAP_ID_LEFT_SURROUND;
            break;
        case GST_AUDIO_CHANNEL_POSITION_SURROUND_RIGHT:
            chid = MATRIX_PAR_CHMAP_ID_RIGHT_SURROUND;
            break;
        case GST_AUDIO_CHANNEL_POSITION_TOP_FRONT_LEFT:
        case GST_AUDIO_CHANNEL_POSITION_TOP_FRONT_RIGHT:
        case GST_AUDIO_CHANNEL_POSITION_TOP_REAR_LEFT:
        case GST_AUDIO_CHANNEL_POSITION_TOP_REAR_RIGHT:
        default:
            chid = MATRIX_PAR_CHMAP_ID_NONE;
            break;
    }
    return chid;
}

uint16_t MediaPlayerPrivate::PositionToCHIDAround(GstAudioChannelPosition position) {
    uint16_t chid;
    switch (position) {
        case GST_AUDIO_CHANNEL_POSITION_MONO:
        case GST_AUDIO_CHANNEL_POSITION_FRONT_LEFT:
            chid = AROUND_PAR_CHMAP_ID_LEFT_FRONT;
            break;
        case GST_AUDIO_CHANNEL_POSITION_FRONT_RIGHT:
            chid = AROUND_PAR_CHMAP_ID_RIGHT_FRONT;
            break;
        case GST_AUDIO_CHANNEL_POSITION_FRONT_CENTER:
            chid = AROUND_PAR_CHMAP_ID_CENTER_FRONT;
            break;
        case GST_AUDIO_CHANNEL_POSITION_LFE1:
            chid = AROUND_PAR_CHMAP_ID_LFE;
            break;
        case GST_AUDIO_CHANNEL_POSITION_REAR_LEFT:
            chid = AROUND_PAR_CHMAP_ID_LEFT_BACK;
            break;
        case GST_AUDIO_CHANNEL_POSITION_REAR_RIGHT:
            chid = AROUND_PAR_CHMAP_ID_RIGHT_BACK;
            break;
        case GST_AUDIO_CHANNEL_POSITION_SURROUND_LEFT:
            chid = AROUND_PAR_CHMAP_ID_LEFT_SURROUND;
            break;
        case GST_AUDIO_CHANNEL_POSITION_SURROUND_RIGHT:
            chid = AROUND_PAR_CHMAP_ID_RIGHT_SURROUND;
            break;
        case GST_AUDIO_CHANNEL_POSITION_TOP_FRONT_LEFT:
        case GST_AUDIO_CHANNEL_POSITION_TOP_FRONT_RIGHT:
        case GST_AUDIO_CHANNEL_POSITION_TOP_REAR_LEFT:
        case GST_AUDIO_CHANNEL_POSITION_TOP_REAR_RIGHT:
        default:
            chid = AROUND_PAR_CHMAP_ID_NONE;
            break;
    }
    return chid;
}

void MediaPlayerPrivate::MelodInit() {
    g_print("%s :: Line %d, started function\n", __func__, __LINE__);

    GstAudioChannelPosition *position = (GstAudioChannelPosition *)malloc((sizeof(GstAudioChannelPosition) * (channel_count_)));
    if (gst_audio_channel_positions_from_mask(channel_count_, channel_mask_, position) < 0) {
        g_printerr("%s :: Line %d, failed to get position from channel mask", __func__, __LINE__);
    }

    if (gst_bin_get_by_name(GST_BIN(melod_postproc_bin), "melodmultibanddrc")) {
        g_print("initializing melod_drc\n");
        int value;
        std::string value_str;
        audiomanager_config_->Read(&value, "audio.manager.md_drcenable");
        DRCHelper("enable", &value);

        audiomanager_config_->Read(&value_str, "audio.manager.md_drcmode");
        char *value_char = const_cast<char *>(value_str.c_str());
        DRCHelper("mode", value_char);

        audiomanager_config_->Read(&value, "audio.manager.md_drcreflevel");
        DRCHelper("ref_level", &value);
    }

    if (gst_bin_get_by_name(GST_BIN(melod_postproc_bin), "melodmatrix")) {
        g_print("Initializing melod_matrix\n");
        // disable melod_matrix if input channel is more then 8
        // 8 is max channel matrix upmix to.
        // set input channel_count
        matrix_config_t *payload_ptr = NULL;
        g_object_get(G_OBJECT(melod_matrix), "matrix-config", &payload_ptr, NULL);
        if (payload_ptr != NULL) {
            if(!gst_bin_get_by_name(GST_BIN(melod_postproc_bin), "melodaround"))
                payload_ptr->channel_count = (hw_channel_count_ - hw_height_channels_);
            else
                payload_ptr->channel_count = 6;
            g_object_set(G_OBJECT(melod_matrix), "matrix-config", payload_ptr, NULL);
        }
        // set input channel map
        matrix_par_chmap_in_t *chmap_in = NULL;
        g_object_get(G_OBJECT(melod_matrix), "chmap-in", &chmap_in, NULL);
        for (int i = 0; i < channel_count_; i++) {
            chmap_in->chmap_in_ch[i].ch_id = PositionToCHIDMatrix(position[i]);
        }
        g_object_set(G_OBJECT(melod_matrix), "chmap-in", chmap_in, NULL);
        // set output channel map
        matrix_par_chmap_out_t *chmap_out = NULL;
        g_object_get(G_OBJECT(melod_matrix), "chmap-out", &chmap_out, NULL);
        std::string key;
        std::string channel_str;
        for (int i = 0; i < payload_ptr->channel_count; i++) {
            key = "audio.manager.zone1.profile_1.ch_list." + std::to_string(i) + ".ch_label";
            audiomanager_config_->Read(&channel_str, key);
            chmap_out->chmap_out_ch[i].ch_id = StrToCHIDMatrix(channel_str);
        }
        g_object_set(G_OBJECT(melod_matrix), "chmap-out", chmap_out, NULL);
        g_free(chmap_in);
        g_free(chmap_out);
        g_free(payload_ptr);
        int value;
        std::string value_str;
        audiomanager_config_->Read(&value, "audio.manager.md_matrixenable");
        MatrixHelper("enable", &value);

        audiomanager_config_->Read(&value_str, "audio.manager.md_matrixmode");
        char *value_char = const_cast<char *>(value_str.c_str());
        MatrixHelper("mode", value_char);
    }

    if (gst_bin_get_by_name(GST_BIN(melod_postproc_bin), "melodaround")) {
        g_printerr("initializing melod_around\n");
        // disable melod_matrix if input channel is more then 8
        // 8 is max channel matrix upmix to.

        // set input channel_count
        around_config_t *payload_ptr = NULL;
        g_object_get(G_OBJECT(melod_around), "around-config", &payload_ptr, NULL);
        if (payload_ptr != NULL) {
            if(!gst_bin_get_by_name(GST_BIN(melod_postproc_bin), "melodmatrix"))
                payload_ptr->channel_count = channel_count_;
            else
                payload_ptr->channel_count = 6;
            g_object_set(G_OBJECT(melod_around), "around-config", payload_ptr, NULL);
        }

        // set input channel map
        around_par_chmap_in_t *chmap_in = NULL;
        g_object_get(G_OBJECT(melod_around), "chmap-in", &chmap_in, NULL);
        for (int i = 0; i < payload_ptr->channel_count; i++) {
            chmap_in->chmap_in_ch[i].ch_id = PositionToCHIDAround(position[i]);
        }

        g_object_set(G_OBJECT(melod_around), "chmap-in", chmap_in, NULL);
        g_free(chmap_in);

        around_par_chmap_out_t *chmap_out = NULL;
        std::string key, channel_str;
        g_object_get(G_OBJECT(melod_around), "chmap-out", &chmap_out, NULL);
        for (int i = 0; i < (hw_channel_count_ - hw_height_channels_); i++) {
            key = "audio.manager.zone1.profile_1.ch_list." + std::to_string(i) + ".ch_label";
            audiomanager_config_->Read(&channel_str, key);
            chmap_out->chmap_out_ch[i].ch_id = StrToCHIDAround(channel_str);
        }
        g_object_set(G_OBJECT(melod_around), "chmap-out", chmap_out, NULL);
        g_free(chmap_out);
        g_free(payload_ptr);

        int value;
        char *value_char;
        std::string value_str;
        audiomanager_config_->Read(&value, "audio.manager.md_aroundproenable");
        AroundHelper("enable", &value);

        audiomanager_config_->Read(&value_str, "audio.manager.md_aroundpromode");
        value_char = const_cast<char *>(value_str.c_str());
        AroundHelper("mode", value_char);

        audiomanager_config_->Read(&value_str, "audio.manager.md_aroundproeffect");
        value_char = const_cast<char *>(value_str.c_str());
        AroundHelper("effect", value_char);
    }

    if (gst_bin_get_by_name(GST_BIN(melod_postproc_bin), "melodbassmanagement")) {
        g_printerr("initializing melod_bassmanagement\n");

        int max_channel = hw_channel_count_;
        // BassManagement supports max channel configuration 7.1.4,
        // so max number of hw_channel_count_ supported is 12
        if (max_channel > 12) {
            max_channel = 12;
        }
        bmgt_par_ch_spkr_map_t *spkr_map = (bmgt_par_ch_spkr_map_t *)malloc((sizeof(bmgt_par_ch_spkr_t) * (max_channel)));
        g_assert(spkr_map);
        channel_position_.resize(max_channel);
        std::string key;
        std::string channel_str;
        for (int i = 0; i < max_channel; i++) {
            key = "audio.manager.zone1.profile_1.ch_list." + std::to_string(i) + ".ch_label";
            audiomanager_config_->Read(&channel_str, key);
            // g_printerr("key = %s value = %s\n", key.c_str(), channel_str.c_str());
            spkr_map->ch_spkr[i].id = StrToCHID(channel_str);
            channel_position_[i] = spkr_map->ch_spkr[i].id;
            if (spkr_map->ch_spkr[i].id != BMGT_CHID_LFE)
                spkr_map->ch_spkr[i].size = BMGT_SIZE_SMALL;
            else
                spkr_map->ch_spkr[i].size = BMGT_SIZE_LARGE;
        }

        // Setting BMGT CONFIG
        // Get default values of BMGT CONFIG from plugin
        bmgt_config_t *config = NULL;
        g_object_get(G_OBJECT(melod_bassmanagement), "bmgt-config", &config, NULL);
        if (config) {
            // modify properties
            config->channel_count = hw_channel_count_;           // total hw_channel_count_ containing height
            config->height_channel_count = hw_height_channels_;  // get from channel map
            // set bmgt config
            g_object_set(G_OBJECT(melod_bassmanagement), "bmgt-config", config, NULL);

            // set speaker channel map
            g_object_set(G_OBJECT(melod_bassmanagement), "ch-spkr-map", spkr_map, NULL);
        } else {
            g_printerr("bmgt-config failed to update, melod_bassmanagement is faild to initialize");
        }
        g_free(config);
        g_free(spkr_map);

        int value;
        void *value_p;
        char *value_char;
        std::string value_str;
        audiomanager_config_->Read(&value, "audio.manager.md_bassmanagementenable");
        BMHelper("enable", &value);

        audiomanager_config_->Read(&value, "audio.manager.md_bassmanagementsubwooferout");
        BMHelper("sub_woofer_out", &value);

        audiomanager_config_->Read(&value_str, "audio.manager.md_bassmanagementsurroundspeakerleft");
        value_char = const_cast<char *>(value_str.c_str());
        BMHelper("surr_spk_left", value_char);

        audiomanager_config_->Read(&value_str, "audio.manager.md_bassmanagementsurroundspeakerright");
        value_char = const_cast<char *>(value_str.c_str());
        BMHelper("surr_spk_right", value_char);

        audiomanager_config_->Read(&value, "audio.manager.md_bassmanagementspksurrheightleft");
        BMHelper("surr_h_left", &value);

        audiomanager_config_->Read(&value, "audio.manager.md_bassmanagementspksurrheightright");
        BMHelper("surr_h_right", &value);

        audiomanager_config_->Read(&value, "audio.manager.md_bassmanagementcornerfreqbass");
        BMHelper("freq", &value);
    }
}

int MediaPlayerPrivate::HandleEffectsPropertyUpdate(sd_bus_message *m) {
    int r = sd_bus_message_enter_container(m, SD_BUS_TYPE_ARRAY, "{sv}");
    if (r < 0) {
        return r;
    }
    for (;;) {
        r = sd_bus_message_enter_container(m, SD_BUS_TYPE_DICT_ENTRY, "sv");
        if (r <= 0) {
            break;
        }
        char* key;
        sd_bus_message_read_basic(m, 's', &key);
        if (std::string(key) == "SupportedEffects") {
            r = sd_bus_message_enter_container(m, SD_BUS_TYPE_VARIANT, "a{s(sb)}");
            if (r < 0) {
                // exit dict container and exit the loop
                sd_bus_message_exit_container(m);
                break;
            }
            std::unordered_map<std::string, bool> effects = ParseSupportedEffects(m);
            for (auto &it : effects) {
                UpdatePPElementStatus(it.first, it.second);
            }
            // exit variant container
            sd_bus_message_exit_container(m);
        }
        // exit dict container
        sd_bus_message_exit_container(m);
    }
    // exit array container
    sd_bus_message_exit_container(m);
    return r;
}

std::unordered_map<std::string, bool> MediaPlayerPrivate::ParseSupportedEffects(sd_bus_message *m) {
    int r;
    std::unordered_map<std::string, bool> effects_status;
    r = sd_bus_message_enter_container(m, SD_BUS_TYPE_ARRAY, "{s(sb)}");
    if (r < 0) {
        return effects_status;
    }
    for (;;) {
        r = sd_bus_message_enter_container(m, SD_BUS_TYPE_DICT_ENTRY, "s(sb)");
        if (r <= 0) {
            break;
        }
        char *uuid;
        sd_bus_message_read_basic(m, 's', &uuid);
        r = sd_bus_message_enter_container(m, SD_BUS_TYPE_STRUCT, "sb");
        if (r < 0) {
            // exit dict container
            sd_bus_message_exit_container(m);
            break;
        }
        char *name;
        uint32_t status;
        sd_bus_message_read(m, "sb", &name, &status);
        if (effects_uuid_map.find(std::string(uuid)) != effects_uuid_map.end()) {
            effects_status.emplace(effects_uuid_map[uuid], static_cast<bool>(status));
        }
        // exit struct container
        sd_bus_message_exit_container(m);
        // exit dict container
        sd_bus_message_exit_container(m);
    }
    // exit array container
    sd_bus_message_exit_container(m);
    return effects_status;
}

int MediaPlayerPrivate::HandlePropertyUpdateOnGstElement(std::string elem_name, sd_bus_message *m) {
    int r = sd_bus_message_enter_container(m, SD_BUS_TYPE_ARRAY, "{sv}");
    if (r < 0) {
        return r;
    }
    for (;;) {
        r = sd_bus_message_enter_container(m, SD_BUS_TYPE_DICT_ENTRY, "sv");
        if (r <= 0) {
            break;
        }
        char *key;
        r = sd_bus_message_read_basic(m, 's', &key);

        const char *contents = NULL;
        char type;
        r = sd_bus_message_peek_type(m, &type, &contents);
        if (r < 0) {
            sd_bus_message_exit_container(m);
            break;
        }
        r = sd_bus_message_enter_container(m, type, contents);
        if (r < 0) {
            sd_bus_message_exit_container(m);
            break;
        }
        r = sd_bus_message_peek_type(m, &type, &contents);
        switch (type) {
            case SD_BUS_TYPE_INT16:
                int16_t val_int16;
                r = sd_bus_message_read_basic(m, 'n', &val_int16);
                UpdateGstElemPropertyInt(elem_name, std::string(key), val_int16);
                break;
            case SD_BUS_TYPE_INT32:
                int32_t val_int32;
                r = sd_bus_message_read_basic(m, 'i', &val_int32);
                UpdateGstElemPropertyInt(elem_name, std::string(key), val_int32);
                break;
            case SD_BUS_TYPE_UINT16:
                uint16_t val_uint16;
                r = sd_bus_message_read_basic(m, 'q', &val_uint16);
                UpdateGstElemPropertyInt(elem_name, std::string(key), val_uint16);
                break;
            case SD_BUS_TYPE_UINT32:
                uint32_t val_uint32;
                r = sd_bus_message_read_basic(m, 'u', &val_uint32);
                UpdateGstElemPropertyInt(elem_name, std::string(key), val_uint32);
                break;
            case SD_BUS_TYPE_INT64:
                int64_t val_int64;
                r = sd_bus_message_read_basic(m, 'x', &val_int64);
                UpdateGstElemPropertyInt(elem_name, std::string(key), val_int64);
                break;
            case SD_BUS_TYPE_UINT64:
                uint64_t val_uint64;
                r = sd_bus_message_read_basic(m, 't', &val_uint64);
                UpdateGstElemPropertyInt(elem_name, std::string(key), val_uint64);
                break;
            case SD_BUS_TYPE_STRING:
                char* val_str;
                r = sd_bus_message_read_basic(m, 's', &val_str);
                if(elem_name.compare(adk::mediaplayerlib::kMelodBin) == 0)
                    UpdateGstElemPropertyChar(elem_name, std::string(key), val_str);
                else
                    UpdateGstElemPropertyStr(elem_name, std::string(key), val_str);
                break;
            case SD_BUS_TYPE_DOUBLE:
                double val_double;
                r = sd_bus_message_read_basic(m, 'd', &val_double);
                UpdateGstElemPropertyInt(elem_name, std::string(key), val_double);
                break;
            case SD_BUS_TYPE_BOOLEAN:
                uint32_t val_bool;
                r = sd_bus_message_read_basic(m, 'b', &val_bool);
                UpdateGstElemPropertyInt(elem_name, std::string(key), val_bool);
                break;
            case SD_BUS_TYPE_BYTE:
                uint8_t val_uint8;
                r = sd_bus_message_read_basic(m, 'y', &val_uint8);
                UpdateGstElemPropertyInt(elem_name, std::string(key), val_uint8);
                break;
            default:
                // skip all messages of range type
                r = sd_bus_message_skip(m, NULL);
                break;
        }
        // exit variant
        sd_bus_message_exit_container(m);
        // exit dict entry
        sd_bus_message_exit_container(m);
    }
    // exit array
    sd_bus_message_exit_container(m);
    return r;
}

std::vector<std::string> MediaPlayerPrivate::GetEnabledPPElementsList() {
    std::vector<std::string> enabled_effects;
    if (d_bus) {
        int r;
        sd_bus_message *reply;
        sd_bus_error *err = nullptr;
        {
            std::lock_guard<std::mutex> bus_lock(dbus_mutex);
            r = sd_bus_call_method(d_bus,
                adk::mediaplayerlib::kAMServiceName,
                adk::mediaplayerlib::kAMEffectsControllerObjPath,
                adk::mediaplayerlib::kDbusPropIface,
                "Get",
                err,
                &reply,
                "ss",
                adk::mediaplayerlib::kAMEffectsControllerIface,
                "SupportedEffects");
            if (!err) {
                std::string signature = std::string(sd_bus_message_get_signature(reply, true));
                if (!signature.empty()) {
                    r = sd_bus_message_enter_container(reply, SD_BUS_TYPE_VARIANT, "a{s(sb)}");
                    if (r < 0) {
                        return enabled_effects;
                    }
                    std::unordered_map<std::string, bool> effects = ParseSupportedEffects(reply);
                    for (auto &it : effects) {
                        if (it.second) {
                            enabled_effects.emplace_back(it.first);
                        }
                    }
                    // exit variant container
                    sd_bus_message_exit_container(reply);
                }
            }
        }
    }
    return enabled_effects;
}

void MediaPlayerPrivate::InitPPElementWithSystemValue(std::string elem_name) {
    if (d_bus) {
        if (pp_elem_dbus_params_map.find(elem_name) != pp_elem_dbus_params_map.end()) {
            auto params = pp_elem_dbus_params_map[elem_name];
            std::string obj_path = std::string(adk::mediaplayerlib::kAMEffectsControllerObjPath)+"/";
            obj_path += std::get<0>(params);
            sd_bus_call_method_async(d_bus,
                nullptr,
                adk::mediaplayerlib::kAMServiceName,
                obj_path.c_str(),
                adk::mediaplayerlib::kDbusPropIface,
                "GetAll",
                std::get<2>(params),
                this,
                "s",
                std::get<1>(params).c_str());
        }
    }
}

int MediaPlayerPrivate::DapGetAllCb(sd_bus_message *m, void *userdata, sd_bus_error *ret_error) {
    auto priv = (MediaPlayerPrivate *)userdata;
    const sd_bus_error *err;
    err = sd_bus_message_get_error(m);
    if (err) {
        int r = sd_bus_error_get_errno(err);
        return r;
    }
    return priv->HandlePropertyUpdateOnGstElement(adk::mediaplayerlib::kDap, m);
}

int MediaPlayerPrivate::SoundxGetAllCb(sd_bus_message *m, void *userdata, sd_bus_error *ret_error) {
    auto priv = (MediaPlayerPrivate *)userdata;
    const sd_bus_error *err;
    err = sd_bus_message_get_error(m);
    if (err) {
        int r = sd_bus_error_get_errno(err);
        return r;
    }
    return priv->HandlePropertyUpdateOnGstElement(adk::mediaplayerlib::kSoundx, m);
}

int MediaPlayerPrivate::McdynamicsGetAllCb(sd_bus_message *m, void *userdata, sd_bus_error *ret_error) {
    auto priv = (MediaPlayerPrivate *)userdata;
    const sd_bus_error *err;
    err = sd_bus_message_get_error(m);
    if (err) {
        int r = sd_bus_error_get_errno(err);
        return r;
    }
    return priv->HandlePropertyUpdateOnGstElement(adk::mediaplayerlib::kMcdynamics, m);
}

int MediaPlayerPrivate::DBusInit() {
    int res;
    std::string rule("type='signal',sender='com.qualcomm.qti.adk.audiomanager',interface='org.freedesktop.DBus.Properties',member='PropertiesChanged'");

    effects_uuid_map = {
        {adk::mediaplayerlib::kMelodUuid, adk::mediaplayerlib::kMelod},
        {adk::mediaplayerlib::kDapUuid, adk::mediaplayerlib::kDap},
        {adk::mediaplayerlib::kSoundxUuid, adk::mediaplayerlib::kSoundx},
        {adk::mediaplayerlib::kMcdynamicsUuid, adk::mediaplayerlib::kMcdynamics}
    };

    pp_elem_dbus_params_map = {
        {adk::mediaplayerlib::kMelod,
            std::make_tuple(adk::mediaplayerlib::kMelodUuid, adk::mediaplayerlib::kAMMelodIface, &MediaPlayerPrivate::MelodGetAllCb)},
        {adk::mediaplayerlib::kDap,
            std::make_tuple(adk::mediaplayerlib::kDapUuid, adk::mediaplayerlib::kAMDapIface, &MediaPlayerPrivate::DapGetAllCb)},
        {adk::mediaplayerlib::kSoundx,
            std::make_tuple(adk::mediaplayerlib::kSoundxUuid, adk::mediaplayerlib::kAMSoundxIface, &MediaPlayerPrivate::SoundxGetAllCb)},
        {adk::mediaplayerlib::kMcdynamics,
            std::make_tuple(adk::mediaplayerlib::kMcdynamicsUuid, adk::mediaplayerlib::kAMMcdynamicsIface, &MediaPlayerPrivate::McdynamicsGetAllCb)}
    };

    do {
        // Connect to the user bus this time
        res = sd_bus_open_user(&d_bus);

        if (res < 0) {
            break;
        }

        /// Take a well-known service name so that clients can find us
        if ((stream_type == MediaPlayer::StreamType::Music) || (stream_type == MediaPlayer::StreamType::ControlOnly)) {
            res = sd_bus_request_name(d_bus, ("org.mpris.MediaPlayer2." + app_name).c_str(), 0);
            if (res < 0) {
                break;
            }
        }

        /// Add a match for properties changed for Volume and Mute property.
        res = sd_bus_add_match(d_bus, &vm_match_slot, rule.c_str(), MediaPlayerPrivate::PropertiesChangedCb, this);

        if (res < 0) {
            break;
        }

        source = g_unix_fd_source_new(sd_bus_get_fd(d_bus), G_IO_IN);
        g_source_set_callback(source, (GSourceFunc)MediaPlayerPrivate::ProcessDBusRequest, this, NULL);
        g_source_attach(source, thread_main_context);
    } while (false);

    if (res < 0) {
        if (d_bus) {
            sd_bus_unref(d_bus);
            d_bus = NULL;
        }

        if (vm_match_slot) {
            sd_bus_slot_unref(vm_match_slot);
            vm_match_slot = NULL;
        }
    }

    if ((stream_type == MediaPlayer::StreamType::Music) || (stream_type == MediaPlayer::StreamType::ControlOnly)) {
        MPRISInit();
    }

    return res;
}

void MediaPlayerPrivate::DBusDeinit() {
    if (slot) {
        sd_bus_slot_unref(slot);
        slot = NULL;
    }

    if (player_slot) {
        sd_bus_slot_unref(player_slot);
        player_slot = NULL;
    }

    if (vm_match_slot) {
        sd_bus_slot_unref(vm_match_slot);
        vm_match_slot = NULL;
    }

    if (d_bus) {
        sd_bus_flush_close_unref(d_bus);
        d_bus = NULL;
    }
}

gboolean MediaPlayerPrivate::ProcessDBusRequest(gint fd G_GNUC_UNUSED, GIOCondition cond G_GNUC_UNUSED, gpointer user_data) {
    auto priv = (MediaPlayerPrivate *)user_data;
    int r;
    {
        std::lock_guard<std::mutex> bus_lock(priv->dbus_mutex);
        // Process requests
        r = sd_bus_process(priv->d_bus, NULL);
    }

    if (r < 0 && priv->onError) {
        priv->onError(MediaPlayer::ErrorType::GENERAL, strerror(-r), {});
    }

    return TRUE;
}

void MediaPlayerPrivate::EmitSeekedSignal(int64_t value) {
    if (d_bus && player_slot) {
        sd_bus_emit_signal(d_bus, "/org/mpris/MediaPlayer2", "org.mpris.MediaPlayer2.Player",
            "Seeked", "x", value);
    }
}

void MediaPlayerPrivate::EmitPropertyChangeSignal(std::string property) {
    if (d_bus && player_slot) {
        sd_bus_emit_properties_changed(d_bus, "/org/mpris/MediaPlayer2",
            "org.mpris.MediaPlayer2.Player", property.c_str(), NULL);
    }
}

int MediaPlayerPrivate::InitMuteCb(sd_bus_message *m, void *userdata, sd_bus_error *ret_error G_GNUC_UNUSED) {
    auto priv = (MediaPlayerPrivate *)userdata;
    const sd_bus_error *e;
    int r;

    e = sd_bus_message_get_error(m);
    if (e) {
        if (priv->onError)
            priv->onError(MediaPlayer::ErrorType::IPC, e->message, {});

        return -1;
    }

    do {
        r = sd_bus_message_enter_container(m, 'v', "b");
        if (r < 0) {
            break;
        }

        r = sd_bus_message_read_basic(m, 'b', &priv->mute_state);
        if (r < 0) {
            break;
        }

        r = sd_bus_message_exit_container(m);
        if (r < 0) {
            break;
        }
    } while (false);

    return 1;
}

bool MediaPlayerPrivate::GetMute() {
    return mute_state;
}

bool MediaPlayerPrivate::SetMute(bool value) {
    int ret = 0;

    if (d_bus) {
        ret = sd_bus_call_method_async(d_bus,
            NULL,
            "com.qualcomm.qti.adk.audiomanager",
            "/com/qualcomm/qti/adk/audiomanager/global_controller",
            "org.freedesktop.DBus.Properties",
            "Set",
            MediaPlayerPrivate::MethodHandlerCb,
            this,
            "ssv",
            "com.qualcomm.qti.adk.audiomanager.global_controller",
            "Mute",
            "b",
            value);
    }

    if (ret < 0) {
        return false;
    }

    return true;
}

double MediaPlayerPrivate::GetVolume() {
    return volume;
}

bool MediaPlayerPrivate::SetVolume(double value) {
    /** Checks if value is within boundaries i.e. (0-1). */
    value = (value < 0.0) ? 0.0 : (value > 1.0) ? 1.0 : value;

    int ret = 0;

    if (d_bus) {
        ret = sd_bus_call_method_async(d_bus, NULL,
            "com.qualcomm.qti.adk.audiomanager",
            "/com/qualcomm/qti/adk/audiomanager/global_controller",
            "org.freedesktop.DBus.Properties",
            "Set",
            MediaPlayerPrivate::MethodHandlerCb,
            this,
            "ssv",
            "com.qualcomm.qti.adk.audiomanager.global_controller",
            "Volume",
            "d",
            value);
    }

    if (ret < 0) {
        return false;
    }

    return true;
}

int MediaPlayerPrivate::InitVolumeCb(sd_bus_message *m, void *userdata, sd_bus_error *ret_error G_GNUC_UNUSED) {
    auto priv = (MediaPlayerPrivate *)userdata;
    const sd_bus_error *e;
    double volume = 1.0;
    int r;

    e = sd_bus_message_get_error(m);
    if (e) {
        if (priv->onError)
            priv->onError(MediaPlayer::ErrorType::IPC, e->message, {});

        return -1;
    }

    do {
        r = sd_bus_message_enter_container(m, 'v', "d");
        if (r < 0) {
            break;
        }

        r = sd_bus_message_read_basic(m, 'd', &volume);
        if (r < 0) {
            break;
        }

        r = sd_bus_message_exit_container(m);
        if (r < 0) {
            break;
        }
    } while (false);

    /** Checks if value returned by dbus method call is
     * within boundaries i.e. (0-1). */
    priv->volume = (volume < 0.0) ? 0.0 : (volume > 1.0) ? 1.0 : volume;

    return 1;
}

void MediaPlayerPrivate::InitialiseVolumeStatus() {
    int r;
    if (d_bus) {
        r = sd_bus_call_method_async(d_bus, NULL,
            "com.qualcomm.qti.adk.audiomanager",
            "/com/qualcomm/qti/adk/audiomanager/global_controller",
            "org.freedesktop.DBus.Properties",
            "Get",
            MediaPlayerPrivate::InitVolumeCb,
            this,
            "ss",
            "com.qualcomm.qti.adk.audiomanager.global_controller",
            "Volume");

        if (r < 0) {
            return;
        }
    }

    return;
}

void MediaPlayerPrivate::InitialiseMuteStatus() {
    int r;
    if (d_bus) {
        r = sd_bus_call_method_async(d_bus, NULL,
            "com.qualcomm.qti.adk.audiomanager",
            "/com/qualcomm/qti/adk/audiomanager/global_controller",
            "org.freedesktop.DBus.Properties",
            "Get",
            MediaPlayerPrivate::InitMuteCb,
            this,
            "ss",
            "com.qualcomm.qti.adk.audiomanager.global_controller",
            "Mute");

        if (r < 0) {
            return;
        }
    }

    return;
}

int MediaPlayerPrivate::MethodHandlerCb(sd_bus_message *m, void *userdata, sd_bus_error *ret_error G_GNUC_UNUSED) {
    const sd_bus_error *e;
    auto priv = (MediaPlayerPrivate *)userdata;

    e = sd_bus_message_get_error(m);
    if (e) {
        if (priv->onError)
            priv->onError(MediaPlayer::ErrorType::IPC, e->message, {});

        return -1;
    }
    return 1;
}

void MediaPlayerPrivate::EmptyMelodBin() {
    GST_CAT_DEBUG(GST_CAT_DEFAULT,"%s :: Line %d, started function\n", __func__, __LINE__);
    if (gst_bin_get_by_name(GST_BIN(melod_postproc_bin), "melodaudioconvertdrc")) {
        gst_object_ref(melod_audioconvert_drc);
        if (!gst_bin_remove(GST_BIN(melod_postproc_bin), melod_audioconvert_drc))
            g_printerr("failed to remove melod_audioconvert_drc");
    }
    if (gst_bin_get_by_name(GST_BIN(melod_postproc_bin), "melodmultibanddrc")) {
        gst_object_ref(melod_drc);
        if (!gst_bin_remove(GST_BIN(melod_postproc_bin), melod_drc))
            g_printerr("failed to remove melod_drc");
    }
    if (gst_bin_get_by_name(GST_BIN(melod_postproc_bin), "melodaudioconvertmatrix")) {
        gst_object_ref(melod_audioconvert_matrix);
        if (!gst_bin_remove(GST_BIN(melod_postproc_bin), melod_audioconvert_matrix))
            g_printerr("failed to remove melod_audioconvert_matrix");
    }
    if (gst_bin_get_by_name(GST_BIN(melod_postproc_bin), "melodmatrix")) {
        gst_object_ref(melod_matrix);
        if (!gst_bin_remove(GST_BIN(melod_postproc_bin), melod_matrix))
            g_printerr("failed to remove melod_matrix");
    }
    if (gst_bin_get_by_name(GST_BIN(melod_postproc_bin), "melodaudioconvertaround")) {
        gst_object_ref(melod_audioconvert_around);
        if (!gst_bin_remove(GST_BIN(melod_postproc_bin), melod_audioconvert_around))
            g_printerr("failed to remove melod_audioconvert_around");
    }
    if (gst_bin_get_by_name(GST_BIN(melod_postproc_bin), "melodaround")) {
        gst_object_ref(melod_around);
        if (!gst_bin_remove(GST_BIN(melod_postproc_bin), melod_around))
            g_printerr("failed to remove melod_around");
    }
    if (gst_bin_get_by_name(GST_BIN(melod_postproc_bin), "melodaudioconvertbm")) {
        gst_object_ref(melod_audioconvert_bm);
        if (!gst_bin_remove(GST_BIN(melod_postproc_bin), melod_audioconvert_bm))
            g_printerr("failed to remove melod_audioconvert_bm");
    }
    if (gst_bin_get_by_name(GST_BIN(melod_postproc_bin), "melodbassmanagement")) {
        gst_object_ref(melod_bassmanagement);
        if (!gst_bin_remove(GST_BIN(melod_postproc_bin), melod_bassmanagement))
            g_printerr("failed to remove melod_bassmanagement");
    }
}

void MediaPlayerPrivate::MelodLink() {
    GST_CAT_DEBUG(GST_CAT_DEFAULT,"%s :: Line %d, started function\n", __func__, __LINE__);
    if (!melod_postproc_bin) {
        g_printerr("melod_postproc_bin is null, returning\n");
        return;
    }
    GstPad *pad = NULL;
    GstCaps *i_caps_matrix, *i_caps_around, *i_caps_bm;
    int input_height_channels = 0;
    // empty the bin
    EmptyMelodBin();
    std::vector<GstElement *> elements_order;
    GstAudioChannelPosition *position = (GstAudioChannelPosition *)malloc((sizeof(GstAudioChannelPosition) * (channel_count_)));
    if (gst_audio_channel_positions_from_mask(channel_count_, channel_mask_, position) < 0) {
        GST_CAT_DEBUG(GST_CAT_DEFAULT,"%s :: Line %d, failed to get position from channel mask", __func__, __LINE__);
    }

    for (int i = 0; i < channel_count_; i++) {
        switch (position[i]) {
            case GST_AUDIO_CHANNEL_POSITION_TOP_FRONT_LEFT:
            case GST_AUDIO_CHANNEL_POSITION_TOP_FRONT_RIGHT:
            case GST_AUDIO_CHANNEL_POSITION_TOP_REAR_LEFT:
            case GST_AUDIO_CHANNEL_POSITION_TOP_REAR_RIGHT:
                input_height_channels++;
                break;
            default:
                break;
        }
    }

    // purpuse of melod_audioconvert_drc is to negociate format to "S32LE"
    gst_bin_add(GST_BIN(melod_postproc_bin), melod_audioconvert_drc);
    elements_order.push_back(melod_audioconvert_drc);

    if (melod_drc) {
        gst_bin_add(GST_BIN(melod_postproc_bin), melod_drc);
        elements_order.push_back(melod_drc);
    }

    if (melod_matrix && melod_audioconvert_matrix && (channel_count_ == 2) && (input_height_channels == 0) && (channel_count_ < (hw_channel_count_ - hw_height_channels_))) {
        i_caps_matrix = gst_caps_new_simple("audio/x-raw",
            "rate", G_TYPE_INT, sample_rate_,
            "channels", G_TYPE_INT, (hw_channel_count_ - hw_height_channels_),
            //"channel-mask", G_TYPE_INT,0x1,
            "format", G_TYPE_STRING, "S32LE",
            "layout", G_TYPE_STRING, "interleaved",
            NULL);
        g_print("Line %d channels %d", __LINE__, (hw_channel_count_ - hw_height_channels_));
        GValue v = G_VALUE_INIT;
        g_value_init(&v, GST_TYPE_ARRAY);  // empty matrix gives (truncated) identity matrix for "mix-matrix"
        g_object_set_property(G_OBJECT(melod_audioconvert_matrix), "mix-matrix", &v);
        g_value_unset(&v);

        gst_bin_add(GST_BIN(melod_postproc_bin), melod_audioconvert_matrix);
        elements_order.push_back(melod_audioconvert_matrix);
        gst_bin_add(GST_BIN(melod_postproc_bin), melod_matrix);
        elements_order.push_back(melod_matrix);
    } else if (melod_around && melod_audioconvert_around && (channel_count_ <= 8) && (input_height_channels == 0) && (channel_count_ > (hw_channel_count_ - hw_height_channels_))) {
        i_caps_around = gst_caps_new_simple("audio/x-raw",
            "rate", G_TYPE_INT, sample_rate_,
            "channels", G_TYPE_INT, (hw_channel_count_ - hw_height_channels_),
            //"channel-mask", G_TYPE_INT,0x1,
            "format", G_TYPE_STRING, "S32LE",
            "layout", G_TYPE_STRING, "interleaved",
            NULL);
        g_print("Line %d channels %d", __LINE__, (hw_channel_count_ - hw_height_channels_));
        GValue v = G_VALUE_INIT;
        g_value_init(&v, GST_TYPE_ARRAY);  // empty matrix gives (truncated) identity matrix for "mix-matrix"
        g_object_set_property(G_OBJECT(melod_audioconvert_around), "mix-matrix", &v);
        g_value_unset(&v);

        gst_bin_add(GST_BIN(melod_postproc_bin), melod_around);
        elements_order.push_back(melod_around);
        gst_bin_add(GST_BIN(melod_postproc_bin), melod_audioconvert_around);
        elements_order.push_back(melod_audioconvert_around);
    } else if (melod_matrix && melod_audioconvert_matrix && melod_around && melod_audioconvert_around && (channel_count_ == 2) && (input_height_channels == 0) && (channel_count_ == (hw_channel_count_ - hw_height_channels_))) {
        i_caps_matrix = gst_caps_new_simple("audio/x-raw",
            "rate", G_TYPE_INT, sample_rate_,
            "channels", G_TYPE_INT, 6,
            //"channel-mask", G_TYPE_INT,0x1,
            "format", G_TYPE_STRING, "S32LE",
            "layout", G_TYPE_STRING, "interleaved",
            NULL);
        GValue v = G_VALUE_INIT;
        g_value_init(&v, GST_TYPE_ARRAY);  // empty matrix gives (truncated) identity matrix for "mix-matrix"
        g_object_set_property(G_OBJECT(melod_audioconvert_matrix), "mix-matrix", &v);
        g_value_unset(&v);

        gst_bin_add(GST_BIN(melod_postproc_bin), melod_audioconvert_matrix);
        elements_order.push_back(melod_audioconvert_matrix);
        gst_bin_add(GST_BIN(melod_postproc_bin), melod_matrix);
        elements_order.push_back(melod_matrix);

        i_caps_around = gst_caps_new_simple("audio/x-raw",
            "rate", G_TYPE_INT, sample_rate_,
            "channels", G_TYPE_INT, (hw_channel_count_ - hw_height_channels_),
            //"channel-mask", G_TYPE_INT,0x1,
            "format", G_TYPE_STRING, "S32LE",
            "layout", G_TYPE_STRING, "interleaved",
            NULL);
        v = G_VALUE_INIT;
        g_value_init(&v, GST_TYPE_ARRAY);  // empty matrix gives (truncated) identity matrix for "mix-matrix"
        g_object_set_property(G_OBJECT(melod_audioconvert_around), "mix-matrix", &v);
        g_value_unset(&v);

        gst_bin_add(GST_BIN(melod_postproc_bin), melod_around);
        elements_order.push_back(melod_around);
        gst_bin_add(GST_BIN(melod_postproc_bin), melod_audioconvert_around);
        elements_order.push_back(melod_audioconvert_around);
    }
    GstAudioChannelPosition gst_channel_positions[hw_channel_count_];
    guint64 channel_mask = 0;
    std::string channel_str, key;

    for (int i = 0; (i < hw_channel_count_); i++) {
        key = "audio.manager.zone1.profile_1.ch_list." + std::to_string(i) + ".ch_label";
        audiomanager_config_->Read(&channel_str, key);
        gst_channel_positions[i] = StrToGSTPosition(channel_str);
    }

    gboolean r = gst_audio_channel_positions_to_mask(gst_channel_positions, hw_channel_count_, false, &channel_mask);
    g_print("channel mask before returning= %d\n",channel_mask);

    if (melod_bassmanagement && melod_audioconvert_bm && r) {
        i_caps_bm = gst_caps_new_simple("audio/x-raw",
            "rate", G_TYPE_INT, sample_rate_,
            "channels", G_TYPE_INT, hw_channel_count_,
            "channel-mask", GST_TYPE_BITMASK, channel_mask,
            "format", G_TYPE_STRING, "S32LE",
            "layout", G_TYPE_STRING, "interleaved",
            NULL);
        GValue v = G_VALUE_INIT;
        g_value_init(&v, GST_TYPE_ARRAY);  // empty matrix gives (truncated) identity matrix for "mix-matrix"
        g_object_set_property(G_OBJECT(melod_audioconvert_bm), "mix-matrix", &v);
        g_value_unset(&v);

        gst_bin_add(GST_BIN(melod_postproc_bin), melod_audioconvert_bm);
        elements_order.push_back(melod_audioconvert_bm);
        gst_bin_add(GST_BIN(melod_postproc_bin), melod_bassmanagement);
        elements_order.push_back(melod_bassmanagement);
    }

    // melod link elements
    GstElement *melod_current;
    if (elements_order.size() == 0) {
        g_printerr("melod post proc bin is empty\n");
        return;
    }
    GST_CAT_DEBUG(GST_CAT_DEFAULT,"melod post proc bin has %d elements\n", elements_order.size());

    for (int i = 0; i < elements_order.size(); i++) {
        if (i == 0) {
            /* set melod_postproc_bin ghost sink pads to actual element pad */
            pad = gst_element_get_static_pad(elements_order[i], "sink");
            gst_ghost_pad_set_target(GST_GHOST_PAD_CAST(melod_bin_sink_pad), pad);
            gst_pad_set_active(melod_bin_sink_pad, TRUE);
            gst_object_unref(pad);
            melod_current = elements_order[i];
            continue;
        }
        GST_CAT_DEBUG(GST_CAT_DEFAULT,"%s-->%s linking\n",
            gst_element_get_name(melod_current), gst_element_get_name(elements_order[i]));
        if (elements_order[i] == melod_matrix) {
            if (!gst_element_link_pads_filtered(melod_current, "src", melod_matrix, "sink", i_caps_matrix)) {
                g_printerr("%s failed in linking\n", gst_element_get_name(elements_order[i]));
                return;
            }
            melod_current = melod_matrix;
            gst_caps_unref(i_caps_matrix);
        } else if ((melod_current == melod_audioconvert_around) && (i != (elements_order.size() - 1))) {
            if (!gst_element_link_pads_filtered(melod_current, "src", elements_order[i], "sink", i_caps_around)) {
                g_printerr("%s-->%s failed in linking\n",gst_element_get_name(melod_current), gst_element_get_name(elements_order[i]));
                return;
            }
            melod_current = elements_order[i];
            gst_caps_unref(i_caps_around);
        } else if (elements_order[i] == melod_bassmanagement) {
            if (!gst_element_link_pads_filtered(melod_current, "src", melod_bassmanagement, "sink", i_caps_bm)) {
                g_printerr("%s failed in linking\n", gst_element_get_name(elements_order[i]));
                return;
            }
            melod_current = melod_bassmanagement;
            gst_caps_unref(i_caps_bm);
        } else {
            if (!gst_element_link_pads(melod_current, "src", elements_order[i], "sink")) {
                g_printerr("%s failed in linking\n", gst_element_get_name(elements_order[i]));
                return;
            }
            melod_current = elements_order[i];
        }
    }
    /* set melod_postproc_bin ghost sink pads to actual element pad */
    pad = gst_element_get_static_pad(elements_order.back(), "src");
    gst_ghost_pad_set_target(GST_GHOST_PAD_CAST(melod_bin_src_pad), pad);
    gst_pad_set_active(melod_bin_src_pad, TRUE);
    gst_object_unref(pad);

    // initialize melod plugins
    MelodInit();
    gst_element_sync_state_with_parent(melod_postproc_bin);
}

void MediaPlayerPrivate::CreateMelodPostprocBin() {
    GST_CAT_DEBUG(GST_CAT_DEFAULT,"%s :: Line %d, Entered the function\n", __func__, __LINE__);

    audiomanager_config_ = adk::config::Config::Create("/data/adk.audiomanager.db");
    audiomanager_config_->Read(&hw_channel_count_, "audio.manager.zone1.profile_1.ch_cnt");

    melod_postproc_bin = gst_bin_new("melod_postproc_bin");
    if (!melod_postproc_bin) {
        g_printerr("Failed to create melod_postproc_bin\n");
        return;
    }

    g_object_ref_sink(melod_postproc_bin);

    melod_drc = gst_element_factory_make("melodmultibanddrc", "melodmultibanddrc");
    melod_matrix = gst_element_factory_make("melodmatrix", "melodmatrix");
    melod_around = gst_element_factory_make("melodaround", "melodaround");
    melod_bassmanagement = gst_element_factory_make("melodbassmanagement", "melodbassmanagement");

    melod_audioconvert_drc = gst_element_factory_make("audioconvert", "melodaudioconvertdrc");
    melod_audioconvert_matrix = gst_element_factory_make("audioconvert", "melodaudioconvertmatrix");
    melod_audioconvert_around = gst_element_factory_make("audioconvert", "melodaudioconvertaround");
    melod_audioconvert_bm = gst_element_factory_make("audioconvert", "melodaudioconvertbm");

    if ((melod_drc && melod_audioconvert_drc) || (melod_matrix && melod_audioconvert_matrix) || (melod_around && melod_audioconvert_around) || (melod_bassmanagement && melod_audioconvert_bm)) {
        melod_bin_sink_pad = gst_ghost_pad_new_no_target("sink", GST_PAD_SINK);
        if (!gst_element_add_pad(melod_postproc_bin, melod_bin_sink_pad))
            g_printerr("adding melod_bin_sink_pad failed\n");

        melod_bin_src_pad = gst_ghost_pad_new_no_target("src", GST_PAD_SRC);
        if (!gst_element_add_pad(melod_postproc_bin, melod_bin_src_pad))
            g_printerr("adding melod_bin_ssrc_pad failed\n");

        // elements are linked after PadAdded() callback calls MelodLink()
        // MelodInit() is called from MelodLink()
        std::string key, channel_str;
        channel_position_.resize(hw_channel_count_);
        hw_height_channels_ = 0;
        for (int i = 0; (i < hw_channel_count_) && (i < 12); i++) {
            key = "audio.manager.zone1.profile_1.ch_list." + std::to_string(i) + ".ch_label";
            audiomanager_config_->Read(&channel_str, key);
            channel_position_[i] = StrToCHID(channel_str);
            if (channel_str.find("top-") != std::string::npos) {
                hw_height_channels_++;
            }
        }
        return;
    }
    GST_CAT_DEBUG(GST_CAT_DEFAULT,"melod plugins not found! melod post processing is disabled\n");
    g_object_unref(melod_postproc_bin);
    gst_object_unref(melod_postproc_bin);
    if (!melod_postproc_bin)
        g_printerr("melod_postproc_bin is NULL\n");
    return;
}

}  // namespace adk
