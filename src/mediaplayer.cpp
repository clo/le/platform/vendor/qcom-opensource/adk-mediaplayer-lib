/*
 * Copyright (c) 2018-2020, The Linux Foundation. All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above
 *       copyright notice, this list of conditions and the following
 *       disclaimer in the documentation and/or other materials provided
 *       with the distribution.
 *     * Neither the name of The Linux Foundation nor the names of its
 *       contributors may be used to endorse or promote products derived
 *       from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS
 * BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
 * BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
 * IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include <adk/mediaplayer.h>
#include "mediaplayer-private.h"

namespace adk {
MediaPlayer::MediaPlayer()
    : priv_(new MediaPlayerPrivate()) {
}

MediaPlayer::~MediaPlayer() {
}

std::unique_ptr<MediaPlayer> MediaPlayer::Create(std::string app_name, StreamType type, Flags flags, Properties properties) {
    std::unique_ptr<MediaPlayer> player(new MediaPlayer());

    if (player->priv_->Init(app_name, type, flags, properties)) {
        return player;
    } else {
        return nullptr;
    }
}

bool MediaPlayer::SetSource(std::string uri) {
    return priv_->SetSource(uri);
}

bool MediaPlayer::SetSource(MediaInfo info, std::function<void(size_t size)> needDataCb, std::function<void(uint64_t offset)> seekDataCb, std::function<void(void)> enoughDataCb) {
    return priv_->SetSource(info, needDataCb, seekDataCb, enoughDataCb);
}

bool MediaPlayer::SetSource(setup_info_t info) {
    return priv_->SetSource(info);
}

void MediaPlayer::SetPreferredSinkLatencyInMilliSeconds(std::chrono::milliseconds latency_time) {
    priv_->SetPreferredSinkLatencyInMilliSeconds(latency_time);
}

void MediaPlayer::DeviceRemoved() {
    priv_->DeviceRemoved();
}

bool MediaPlayer::SetSource(std::function<void(size_t size)> needDataCb, std::function<void(uint64_t size)> seekDataCb, std::function<void(void)> enoughDataCb) {
    return priv_->SetSource(needDataCb, seekDataCb, enoughDataCb);
}

void MediaPlayer::SetDolbyGraph(std::string graph) {
    priv_->SetDolbyGraph(graph);
}

void MediaPlayer::SetVirtualXGraph(std::string graph) {
    priv_->SetVirtualXGraph(graph);
}

bool MediaPlayer::PushBuffer(void *data, size_t size) {
    return priv_->PushBuffer(data, size);
}

bool MediaPlayer::PushBuffer(void *data, size_t size, std::chrono::nanoseconds duration, std::chrono::nanoseconds timestamp) {
    return priv_->PushBuffer(data, size, duration, timestamp);
}

bool MediaPlayer::PushEndOfStream() {
    return priv_->PushEndOfStream();
}

size_t MediaPlayer::GetBufferLevel() {
    return priv_->GetBufferLevel();
}

bool MediaPlayer::Next() {
    return priv_->Next();
}

bool MediaPlayer::Previous() {
    return priv_->Previous();
}

MediaPlayer::LoopMode MediaPlayer::GetLoopMode() {
    return priv_->GetLoopMode();
}

void MediaPlayer::SetLoopMode(MediaPlayer::LoopMode mode) {
    return priv_->SetLoopMode(mode);
}

bool MediaPlayer::GetShuffleStatus() {
    return priv_->GetShuffleStatus();
}

void MediaPlayer::SetShuffleStatus(bool status) {
    return priv_->SetShuffleStatus(status);
}

MediaPlayer::Result MediaPlayer::Play() {
    return priv_->Play();
}

MediaPlayer::Result MediaPlayer::Pause() {
    return priv_->Pause();
}

MediaPlayer::Result MediaPlayer::Stop() {
    return priv_->Stop();
}

void MediaPlayer::SetSupportedRateRange(double min, double max) {
    return priv_->SetSupportedRateRange(min, max);
}

double MediaPlayer::GetPlaybackRate() {
    return priv_->GetPlaybackRate();
}

double MediaPlayer::GetMinPlaybackRate() {
    return priv_->GetMinPlaybackRate();
}

double MediaPlayer::GetMaxPlaybackRate() {
    return priv_->GetMaxPlaybackRate();
}

bool MediaPlayer::SetRate(double rate) {
    return priv_->SetRate(rate);
}

bool MediaPlayer::SetRate(double rate, std::chrono::seconds position) {
    return priv_->SetRate(rate, position);
}

bool MediaPlayer::SetRate(double rate, uint64_t offset) {
    return priv_->SetRate(rate, offset);
}
std::chrono::nanoseconds MediaPlayer::GetDuration() {
    return priv_->GetDuration();
}

std::chrono::nanoseconds MediaPlayer::GetPosition() {
    return priv_->GetPosition();
}

bool MediaPlayer::IsSeekable() {
    return priv_->IsSeekable();
}

MediaPlayer::Result MediaPlayer::Seek(std::chrono::nanoseconds position) {
    return priv_->Seek(position);
}

MediaPlayer::State MediaPlayer::GetState() {
    return priv_->GetState();
}

std::tuple<MediaPlayer::State, MediaPlayer::Result> MediaPlayer::GetCurrentState() {
    return priv_->GetCurrentState();
}

std::map<MediaPlayer::Tag, std::string> MediaPlayer::GetMetadata() {
    return priv_->GetMetadata();
}

void MediaPlayer::SetMetadata(std::map<Tag, std::string> m) {
    return priv_->SetMetadata(m);
}

bool MediaPlayer::GetMute() {
    return priv_->GetMute();
}

bool MediaPlayer::SetMute(bool value) {
    return priv_->SetMute(value);
}

double MediaPlayer::GetVolume() {
    return priv_->GetVolume();
}

bool MediaPlayer::SetVolume(double value) {
    return priv_->SetVolume(value);
}

uint64_t MediaPlayer::GetPipelineDelay() {
    return priv_->GetTotalDelay();
}

void MediaPlayer::SetOnErrorCb(std::function<void(ErrorType error_type, std::string error_msg, std::string debug_msg)> onErrorCb) {
    priv_->SetOnErrorCb(onErrorCb);
}

void MediaPlayer::SetOnAsyncDoneCb(std::function<void(void)> onAsyncDoneCb) {
    priv_->SetOnAsyncDoneCb(onAsyncDoneCb);
}

void MediaPlayer::SetOnEndOfStreamCb(std::function<void(void)> onEndOfStreamCb) {
    priv_->SetOnEndOfStreamCb(onEndOfStreamCb);
}

void MediaPlayer::SetOnStateChangedCb(std::function<void(State new_state)> onStateChangedCb) {
    priv_->SetOnStateChangedCb(onStateChangedCb);
}

void MediaPlayer::SetOnMetadataChangedCb(std::function<void(void)> onMetadataChangedCb) {
    priv_->SetOnMetadataChangedCb(onMetadataChangedCb);
}

void MediaPlayer::SetNextCb(std::function<void(void)> onNextCb) {
    priv_->SetNextCb(onNextCb);
}

void MediaPlayer::SetPreviousCb(std::function<void(void)> onPreviousCb) {
    priv_->SetPreviousCb(onPreviousCb);
}

void MediaPlayer::SetLoopModeChangeCb(std::function<void(LoopMode status)> onLoopModeChangeCb) {
    priv_->SetLoopModeChangeCb(onLoopModeChangeCb);
}

void MediaPlayer::SetShuffleStatusChangeCb(std::function<void(bool)> onShuffleStatusChangeCb) {
    priv_->SetShuffleStatusChangeCb(onShuffleStatusChangeCb);
}

void MediaPlayer::SetBufferingCb(std::function<void(unsigned int buffering_percent)> onBufferingCb) {
    priv_->SetBufferingCb(onBufferingCb);
}

void MediaPlayer::SetOnMuteUpdatedCb(std::function<void(bool mute_state)> onMuteUpdatedCb) {
    priv_->SetOnMuteUpdatedCb(onMuteUpdatedCb);
}

void MediaPlayer::SetOnVolumeUpdatedCb(std::function<void(double volume)> onVolumeUpdatedCb) {
    priv_->SetOnVolumeUpdatedCb(onVolumeUpdatedCb);
}

void MediaPlayer::SetPlayCb(std::function<void(void)> onPlayCb) {
    priv_->SetPlayCb(onPlayCb);
}

void MediaPlayer::SetPauseCb(std::function<void(void)> onPauseCb) {
    priv_->SetPauseCb(onPauseCb);
}

void MediaPlayer::SetStopCb(std::function<void(void)> onStopCb) {
    priv_->SetStopCb(onStopCb);
}
void MediaPlayer::SetOnRateChangeRequestCallback(std::function<void(double)> onRateChangeRequestCb) {
    priv_->SetOnRateChangeRequestCallback(onRateChangeRequestCb);
}

void MediaPlayer::SetOnPipelineDelayChangeCb(std::function<void(uint64_t)> onPipelineDelayChangeCb) {
    priv_->SetOnTotalPipelineDelayChangeCb(onPipelineDelayChangeCb);
}
}  // namespace adk
