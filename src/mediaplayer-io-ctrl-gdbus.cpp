/*
 * Copyright (c) 2019, The Linux Foundation. All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above
 *       copyright notice, this list of conditions and the following
 *       disclaimer in the documentation and/or other materials provided
 *       with the distribution.
 *     * Neither the name of The Linux Foundation nor the names of its
 *       contributors may be used to endorse or promote products derived
 *       from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS
 * BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
 * BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
 * IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include <adk/mediaplayer.h>
#include "mediaplayer-private.h"

namespace adk {

/* Introspection data for the service we are exporting */
static const gchar introspection_xml[] =
    "<node>"
    "    <interface name='org.mpris.MediaPlayer2.Player'>"
    "<!-- Methods of interface org.mpris.MediaPlayer2.Player-->"
    "        <method name='Next'> </method>"
    "        <method name='Previous'> </method>"
    "        <method name='Pause'> </method>"
    "        <method name='PlayPause'> </method>"
    "        <method name='Stop'> </method>"
    "        <method name='Play'> </method>"
    "        <method name='SetPosition'>"
    "            <arg name='trackid' direction='in' type='o' />"
    "            <arg name='offset' direction='in' type='x' />"
    "        </method>"
    "        <method name='Seek'>"
    "            <arg name='position' direction='in' type='x' />"
    "        </method>"
    "        <method name='OpenUri'>"
    "            <arg name='uri' direction='in' type='s' />"
    "        </method>"
    "<!-- Signals of interface org.mpris.MediaPlayer2.Player-->"
    "        <signal name='Seeked'>"
    "            <arg type='x' name='position' />"
    "        </signal>"
    "<!-- Properties of interface org.mpris.MediaPlayer2.Player-->"
    "        <property name='PlaybackStatus' type='s' access='read' />"
    "        <property name='LoopStatus' type='s' access='readwrite' />"
    "        <property name='Rate' type='d' access='readwrite' />"
    "        <property name='Shuffle' type='b' access='readwrite' />"
    "        <property name='Metadata' type='a{sv}' access='read' />"
    "        <property name='Volume' type='d' access='readwrite' />"
    "        <property name='Position' type='x' access='read' />"
    "        <property name='MinimumRate' type='d' access='read' />"
    "        <property name='MaximumRate' type='d' access='read' />"
    "        <property name='CanGoNext' type='b' access='read' />"
    "        <property name='CanGoPrevious' type='b' access='read' />"
    "        <property name='CanPlay' type='b' access='read' />"
    "        <property name='CanPause' type='b' access='read' />"
    "        <property name='CanSeek' type='b' access='read' />"
    "        <property name='CanControl' type='b' access='read' />"
    "        <property name='CanLoop' type='b' access='read' />"
    "        <property name='CanShuffle' type='b' access='read' />"
    "    </interface>"
    "</node>";

static void handle_next(GVariant *parameters G_GNUC_UNUSED,
    GDBusMethodInvocation *invocation,
    gpointer user_data) {
    auto priv = (MediaPlayerPrivate *)user_data;

    priv->Next();

    g_dbus_method_invocation_return_value(invocation, NULL);
}

static void handle_previous(GVariant *parameters G_GNUC_UNUSED,
    GDBusMethodInvocation *invocation,
    gpointer user_data) {
    auto priv = (MediaPlayerPrivate *)user_data;

    priv->Previous();

    g_dbus_method_invocation_return_value(invocation, NULL);
}

static void handle_pause(GVariant *parameters G_GNUC_UNUSED,
    GDBusMethodInvocation *invocation,
    gpointer user_data) {
    auto priv = (MediaPlayerPrivate *)user_data;

    if (!priv->PauseCb()) {
        MediaPlayer::Result res = priv->Pause();

        if (res == MediaPlayer::Result::Failure) {
            g_dbus_method_invocation_return_dbus_error(invocation,
                "GDBus : method call failure",
                "GDBus : Pause method failed");
        }
    }

    g_dbus_method_invocation_return_value(invocation, NULL);
}

static void handle_play_pause(GVariant *parameters G_GNUC_UNUSED,
    GDBusMethodInvocation *invocation,
    gpointer user_data) {
    auto priv = (MediaPlayerPrivate *)user_data;

    if (priv->GetState() == MediaPlayer::State::Playing) {
        if (!priv->PauseCb()) {
            MediaPlayer::Result res = priv->Pause();

            if (res == MediaPlayer::Result::Failure) {
                g_dbus_method_invocation_return_dbus_error(invocation,
                    "GDBus : method call failure",
                    "GDBus : PlayPause method failed");
            }
        }
    } else {
        if (!priv->PlayCb()) {
            MediaPlayer::Result res = priv->Play();

            if (res == MediaPlayer::Result::Failure) {
                g_dbus_method_invocation_return_dbus_error(invocation,
                    "GDBus : method call failure",
                    "GDBus : PlayPause method failed");
            }
        }
    }

    g_dbus_method_invocation_return_value(invocation, NULL);
}

static void handle_stop(GVariant *parameters G_GNUC_UNUSED,
    GDBusMethodInvocation *invocation,
    gpointer user_data) {
    auto priv = (MediaPlayerPrivate *)user_data;

    if (!priv->StopCb()) {
        MediaPlayer::Result res = priv->Stop();

        if (res == MediaPlayer::Result::Failure) {
            g_dbus_method_invocation_return_dbus_error(invocation,
                "GDBus : method call failure",
                "GDBus : Stop method failed");
        }
    }

    g_dbus_method_invocation_return_value(invocation, NULL);
}

static void handle_play(GVariant *parameters G_GNUC_UNUSED,
    GDBusMethodInvocation *invocation,
    gpointer user_data) {
    auto priv = (MediaPlayerPrivate *)user_data;

    if (!priv->PlayCb()) {
        MediaPlayer::Result res = priv->Play();

        if (res == MediaPlayer::Result::Failure) {
            g_dbus_method_invocation_return_dbus_error(invocation,
                "GDBus : method call failure",
                "GDBus : Play method failed");
        }
    }

    g_dbus_method_invocation_return_value(invocation, NULL);
}

static void handle_set_position(GVariant *parameters,
    GDBusMethodInvocation *invocation,
    gpointer user_data) {
    auto priv = (MediaPlayerPrivate *)user_data;
    const gchar *trackid;
    gint64 position;

    g_variant_get(parameters, "(ox)", &trackid, &position);

    std::chrono::nanoseconds time = std::chrono::microseconds(position);

    if (time <= priv->GetDuration() && time >= std::chrono::nanoseconds(0) && g_strcmp0(trackid, "/org/mpris/MediaPlayer2/Track/0") == 0) {
        if (priv->Seek(time) == MediaPlayer::Result::Failure) {
            g_dbus_method_invocation_return_dbus_error(invocation,
                "GDBus : method call failure",
                "GDBus : SetPosition method failed");
        }
    }

    g_dbus_method_invocation_return_value(invocation, NULL);
}

static void handle_seek(GVariant *parameters,
    GDBusMethodInvocation *invocation,
    gpointer user_data) {
    auto priv = (MediaPlayerPrivate *)user_data;
    gint64 offset;

    g_variant_get(parameters, "(x)", &offset);

    std::chrono::nanoseconds time = priv->GetPosition() + std::chrono::microseconds(offset);

    // when seeked time is within duration
    if (time <= priv->GetDuration() && time >= std::chrono::nanoseconds(0)) {
        if (priv->Seek(time) == MediaPlayer::Result::Failure) {
            g_dbus_method_invocation_return_dbus_error(invocation,
                "GDBus : method call failure",
                "GDBus : Seek method failed");
        }
    }

    // when seeked time is beyond duration
    if (time > priv->GetDuration()) {
        priv->Next();
    }

    // when seeked time is before duration
    if (time < std::chrono::nanoseconds(0)) {
        if (priv->Seek(std::chrono::nanoseconds(0)) == MediaPlayer::Result::Failure) {
            g_dbus_method_invocation_return_dbus_error(invocation,
                "GDBus : method call failure",
                "GDBus : Seek method failed");
        }
    }

    g_dbus_method_invocation_return_value(invocation, NULL);
}

static void handle_open_uri(GVariant *parameters,
    GDBusMethodInvocation *invocation,
    gpointer user_data) {
    auto priv = (MediaPlayerPrivate *)user_data;
    const gchar *uri;

    g_variant_get(parameters, "(s)", &uri);

    if (priv->SetSource(uri) == false) {
        g_dbus_method_invocation_return_dbus_error(invocation,
            "GDBus : method call failure",
            "GDBus : OpenUri method failed");
    }

    if (priv->Play() == MediaPlayer::Result::Failure) {
        g_dbus_method_invocation_return_dbus_error(invocation,
            "GDBus : method call failure",
            "GDBus : OpenUri method failed");
    }

    g_dbus_method_invocation_return_value(invocation, NULL);
}

static GVariant *get_playback_status(gpointer user_data) {
    auto priv = (MediaPlayerPrivate *)user_data;
    GVariant *ret = NULL;
    const char *state = NULL;

    switch (priv->GetState()) {
        case MediaPlayer::State::Playing:
            state = "Playing";
            break;

        case MediaPlayer::State::Paused:
            state = "Paused";
            break;

        case MediaPlayer::State::Stopped:
            state = "Stopped";
            break;
    }

    if (state)
        ret = g_variant_new_string(state);

    return ret;
}

static GVariant *get_loop_status(gpointer user_data) {
    auto priv = (MediaPlayerPrivate *)user_data;
    GVariant *ret = NULL;
    const char *loop_status = NULL;

    switch (priv->GetLoopMode()) {
        case MediaPlayer::LoopMode::None:
            loop_status = "None";
            break;

        case MediaPlayer::LoopMode::Track:
            loop_status = "Track";
            break;

        case MediaPlayer::LoopMode::Playlist:
            loop_status = "Playlist";
            break;
    }

    if (loop_status)
        ret = g_variant_new_string(loop_status);

    return ret;
}

static GVariant *get_rate(gpointer user_data G_GNUC_UNUSED) {
    GVariant *ret = NULL;

    ret = g_variant_new_double((gdouble)1.0);

    return ret;
}

static GVariant *get_shuffle(gpointer user_data) {
    auto priv = (MediaPlayerPrivate *)user_data;
    GVariant *ret = NULL;

    ret = g_variant_new_boolean(priv->GetShuffleStatus());

    return ret;
}

static GVariant *get_metadata(gpointer user_data) {
    auto priv = (MediaPlayerPrivate *)user_data;
    const char *tag, *value;
    GVariantBuilder builder, inner_builder;

    std::map<MediaPlayer::Tag, std::string> m = priv->GetMetadata();

    g_variant_builder_init(&builder, G_VARIANT_TYPE_ARRAY);

    for (auto it : m) {
        switch (it.first) {
            case MediaPlayer::Tag::CoverArt:
                tag = "mpris:artUrl";
                break;

            case MediaPlayer::Tag::Duration:
                tag = "mpris:length";
                break;

            case MediaPlayer::Tag::Album:
                tag = "xesam:album";
                break;

            case MediaPlayer::Tag::Artist:
                tag = "xesam:artist";
                break;

            case MediaPlayer::Tag::AudioBitrate:
                tag = "xesam:audioBitrate";
                break;

            case MediaPlayer::Tag::Genre:
                tag = "xesam:genre";
                break;

            case MediaPlayer::Tag::Title:
                tag = "xesam:title";
                break;

            case MediaPlayer::Tag::Url:
                tag = "xesam:url";
                break;

            case MediaPlayer::Tag::Channels:
                tag = "xesam:audioChannels";
                break;

            case MediaPlayer::Tag::SampleFormat:
                tag = "adk:SampleFormat";
                break;

            case MediaPlayer::Tag::SampleRate:
                tag = "xesam:audioSampleRate";
                break;

            default:
                /* Unknown tag, we just skip it */
                continue;
        }

        value = it.second.c_str();

        if (strcmp(tag, "xesam:album") == 0 || strcmp(tag, "xesam:title") == 0 || strcmp(tag, "xesam:url") == 0 || strcmp(tag, "mpris:artUrl") == 0 || strcmp(tag, "adk:SampleFormat") == 0 || strcmp(tag, "xesam:audioChannels") == 0) {
            g_variant_builder_add(&builder, "{sv}", tag, g_variant_new("(s)", value));
        } else if (strcmp(tag, "mpris:length") == 0) {
            int64_t tmp;

            sscanf(value, "%ld", &tmp);

            tmp = std::chrono::duration_cast<std::chrono::microseconds>(std::chrono::nanoseconds(tmp)).count();

            g_variant_builder_add(&builder, "{sv}", tag, g_variant_new("(x)", tmp));
        } else if (strcmp(tag, "xesam:audioBitrate") == 0) {
            int32_t tmp;

            sscanf(value, "%d", &tmp);

            g_variant_builder_add(&builder, "{sv}", tag, g_variant_new("(i)", tmp));
        } else if (strcmp(tag, "xesam:audioSampleRate") == 0) {
            double tmp;

            sscanf(value, "%lf", &tmp);

            g_variant_builder_add(&builder, "{sv}", tag, g_variant_new("(d)", tmp));
        } else if (strcmp(tag, "xesam:artist") == 0 || strcmp(tag, "xesam:genre") == 0) {
            std::istringstream ss(value);
            std::string tmp;

            g_variant_builder_init(&inner_builder, G_VARIANT_TYPE_ARRAY);

            while (std::getline(ss, tmp, ',')) {
                g_variant_builder_add(&inner_builder, "s", tmp.c_str());
            }

            g_variant_builder_add(&builder, "{sv}", tag, g_variant_builder_end(&inner_builder));
        }
    }

    g_variant_builder_add(&builder, "{sv}", "mpris:trackid", g_variant_new("(o)", "/org/mpris/MediaPlayer2/Track/0"));

    return g_variant_builder_end(&builder);
}

static GVariant *get_volume(gpointer user_data) {
    auto priv = (MediaPlayerPrivate *)user_data;
    GVariant *ret = NULL;

    auto volume = priv->GetVolume();

    if (volume)
        ret = g_variant_new_double(volume);

    return ret;
}

static GVariant *get_position(gpointer user_data) {
    auto priv = (MediaPlayerPrivate *)user_data;
    GVariant *ret = NULL;

    auto position = std::chrono::duration_cast<std::chrono::microseconds>(priv->GetPosition());

    if (position.count())
        ret = g_variant_new_int64(position.count());

    return ret;
}

static GVariant *get_minimum_rate(gpointer user_data G_GNUC_UNUSED) {
    GVariant *ret = NULL;

    ret = g_variant_new_double((gdouble)1.0);

    return ret;
}

static GVariant *get_maximum_rate(gpointer user_data G_GNUC_UNUSED) {
    GVariant *ret = NULL;

    ret = g_variant_new_double((gdouble)1.0);

    return ret;
}

static GVariant *get_can_go_next(gpointer user_data) {
    auto priv = (MediaPlayerPrivate *)user_data;
    GVariant *ret = NULL;

    ret = g_variant_new_boolean(priv->CanGoNext());

    return ret;
}

static GVariant *get_can_go_previous(gpointer user_data) {
    auto priv = (MediaPlayerPrivate *)user_data;
    GVariant *ret = NULL;

    ret = g_variant_new_boolean(priv->CanGoPrevious());

    return ret;
}

static GVariant *get_can_play(gpointer user_data G_GNUC_UNUSED) {
    GVariant *ret = NULL;

    ret = g_variant_new_boolean(TRUE);

    return ret;
}

static GVariant *get_can_pause(gpointer user_data G_GNUC_UNUSED) {
    GVariant *ret = NULL;

    ret = g_variant_new_boolean(TRUE);

    return ret;
}

static GVariant *get_can_seek(gpointer user_data) {
    auto priv = (MediaPlayerPrivate *)user_data;
    GVariant *ret = NULL;

    ret = g_variant_new_boolean(priv->IsSeekable());

    return ret;
}

static GVariant *get_can_control(gpointer user_data G_GNUC_UNUSED) {
    GVariant *ret = NULL;

    ret = g_variant_new_boolean(TRUE);

    return ret;
}

static GVariant *get_can_loop(gpointer user_data) {
    auto priv = (MediaPlayerPrivate *)user_data;
    GVariant *ret = NULL;

    ret = g_variant_new_boolean(priv->CanLoop());

    return ret;
}

static GVariant *get_can_shuffle(gpointer user_data) {
    auto priv = (MediaPlayerPrivate *)user_data;
    GVariant *ret = NULL;

    ret = g_variant_new_boolean(priv->CanShuffle());

    return ret;
}

static gboolean set_loop_status(GVariant *value, gpointer user_data) {
    auto priv = (MediaPlayerPrivate *)user_data;
    gsize length;

    const gchar *loop_status = g_variant_get_string(value, &length);
    MediaPlayer::LoopMode loop;

    if (strcmp(loop_status, "None") == 0)
        loop = MediaPlayer::LoopMode::None;
    else if (strcmp(loop_status, "Track") == 0)
        loop = MediaPlayer::LoopMode::Track;
    else if (strcmp(loop_status, "Playlist") == 0)
        loop = MediaPlayer::LoopMode::Playlist;
    else
        return FALSE;

    priv->SetLoopMode(loop);

    return TRUE;
}

static gboolean set_rate(GVariant *value, gpointer user_data) {
    auto priv = (MediaPlayerPrivate *)user_data;

    double rate = g_variant_get_double(value);

    priv->SetRate(rate);

    return TRUE;
}

static gboolean set_shuffle(GVariant *value, gpointer user_data) {
    auto priv = (MediaPlayerPrivate *)user_data;

    gboolean shuffle = g_variant_get_boolean(value);

    priv->SetShuffleStatus(shuffle);

    return TRUE;
}

static gboolean set_volume(GVariant *value, gpointer user_data) {
    auto priv = (MediaPlayerPrivate *)user_data;

    gdouble volume = g_variant_get_double(value);

    if (!priv->SetVolume(volume))
        return FALSE;

    return TRUE;
}

static GVariant *GetPropertyValue(std::string property_name, MediaPlayerPrivate *user_data) {
    if (property_name.compare("PlaybackStatus") == 0) {
        return get_playback_status(user_data);
    } else if (property_name.compare("LoopStatus") == 0) {
        return get_loop_status(user_data);
    } else if (property_name.compare("Rate") == 0) {
        return get_rate(user_data);
    } else if (property_name.compare("Shuffle") == 0) {
        return get_shuffle(user_data);
    } else if (property_name.compare("Metadata") == 0) {
        return get_metadata(user_data);
    } else if (property_name.compare("Volume") == 0) {
        return get_volume(user_data);
    } else if (property_name.compare("Position") == 0) {
        return get_position(user_data);
    } else if (property_name.compare("MinimumRate") == 0) {
        return get_maximum_rate(user_data);
    } else if (property_name.compare("MaximumRate") == 0) {
        return get_minimum_rate(user_data);
    } else if (property_name.compare("CanGoNext") == 0) {
        return get_can_go_next(user_data);
    } else if (property_name.compare("CanGoPrevious") == 0) {
        return get_can_go_previous(user_data);
    } else if (property_name.compare("CanPlay") == 0) {
        return get_can_play(user_data);
    } else if (property_name.compare("CanPause") == 0) {
        return get_can_pause(user_data);
    } else if (property_name.compare("CanSeek") == 0) {
        return get_can_seek(user_data);
    } else if (property_name.compare("CanControl") == 0) {
        return get_can_control(user_data);
    } else if (property_name.compare("CanLoop") == 0) {
        return get_can_loop(user_data);
    } else if (property_name.compare("CanShuffle") == 0) {
        return get_can_shuffle(user_data);
    }

    return NULL;
}

static void handle_method_call(GDBusConnection *connection G_GNUC_UNUSED,
    const gchar *sender G_GNUC_UNUSED,
    const gchar *object_path G_GNUC_UNUSED,
    const gchar *interface_name G_GNUC_UNUSED,
    const gchar *method_name,
    GVariant *parameters,
    GDBusMethodInvocation *invocation,
    gpointer user_data) {
    if (g_strcmp0(method_name, "Next") == 0) {
        handle_next(parameters, invocation, user_data);
    } else if (g_strcmp0(method_name, "Previous") == 0) {
        handle_previous(parameters, invocation, user_data);
    } else if (g_strcmp0(method_name, "Pause") == 0) {
        handle_pause(parameters, invocation, user_data);
    } else if (g_strcmp0(method_name, "PlayPause") == 0) {
        handle_play_pause(parameters, invocation, user_data);
    } else if (g_strcmp0(method_name, "Stop") == 0) {
        handle_stop(parameters, invocation, user_data);
    } else if (g_strcmp0(method_name, "Play") == 0) {
        handle_play(parameters, invocation, user_data);
    } else if (g_strcmp0(method_name, "SetPosition") == 0) {
        handle_set_position(parameters, invocation, user_data);
    } else if (g_strcmp0(method_name, "Seek") == 0) {
        handle_seek(parameters, invocation, user_data);
    } else if (g_strcmp0(method_name, "OpenUri") == 0) {
        handle_open_uri(parameters, invocation, user_data);
    }
}

static GVariant *handle_get_property(GDBusConnection *connection G_GNUC_UNUSED,
    const gchar *sender G_GNUC_UNUSED,
    const gchar *object_path G_GNUC_UNUSED,
    const gchar *interface_name G_GNUC_UNUSED,
    const gchar *property_name,
    GError **error G_GNUC_UNUSED,
    gpointer user_data) {
    auto priv = (MediaPlayerPrivate *)user_data;

    return GetPropertyValue(property_name, priv);
}

static gboolean handle_set_property(GDBusConnection *connection G_GNUC_UNUSED,
    const gchar *sender G_GNUC_UNUSED,
    const gchar *object_path G_GNUC_UNUSED,
    const gchar *interface_name G_GNUC_UNUSED,
    const gchar *property_name,
    GVariant *value,
    GError **error G_GNUC_UNUSED,
    gpointer user_data) {
    if (g_strcmp0(property_name, "LoopStatus") == 0) {
        return set_loop_status(value, user_data);
    } else if (g_strcmp0(property_name, "Rate") == 0) {
        return set_rate(value, user_data);
    } else if (g_strcmp0(property_name, "Shuffle") == 0) {
        return set_shuffle(value, user_data);
    } else if (g_strcmp0(property_name, "Volume") == 0) {
        return set_volume(value, user_data);
    }

    return FALSE;
}

static const GDBusInterfaceVTable interface_vtable =
    {
        handle_method_call,
        handle_get_property,
        handle_set_property};

void MediaPlayerPrivate::MPRISInit() {
    GError *error = NULL;

    GDBusNodeInfo *introspection_data = g_dbus_node_info_new_for_xml(introspection_xml, &error);

    if (error != NULL) {
        if (onError)
            onError(MediaPlayer::ErrorType::IPC, "GDBusMPRIS Introspection Failed", error->message);

        return;
    }

    registration_id = g_dbus_connection_register_object(connection,
        "/org/mpris/MediaPlayer2",
        introspection_data->interfaces[0],
        &interface_vtable,
        this,    /* user_data */
        NULL,    /* user_data_free_func */
        &error); /* GError** */

    if (error != NULL) {
        if (onError)
            onError(MediaPlayer::ErrorType::IPC, "GDBusMPRIS Init Failed", error->message);

        return;
    }
}

void MediaPlayerPrivate::GDBusPropertiesChangedCb(GDBusConnection *connection G_GNUC_UNUSED,
    const gchar *sender_name G_GNUC_UNUSED,
    const gchar *object_path G_GNUC_UNUSED,
    const gchar *interface_name G_GNUC_UNUSED,
    const gchar *signal_name G_GNUC_UNUSED,
    GVariant *parameters,
    gpointer user_data) {
    auto priv = (MediaPlayerPrivate *)user_data;

    GVariantIter iter, array_iter;
    GVariant *value = NULL, *array_value = NULL;
    gsize length;
    const gchar *interface, *type, *key;

    g_variant_iter_init(&iter, parameters);
    value = g_variant_iter_next_value(&iter);
    interface = g_variant_get_string(value, &length);

    if (g_strcmp0(interface, "com.qualcomm.qti.adk.audiomanager.global_controller") == 0) {
        value = g_variant_iter_next_value(&iter);
        type = g_variant_get_type_string(value);

        if (g_str_has_prefix(type, "a")) {
            if (g_variant_n_children(value) > 0) {
                g_variant_iter_init(&array_iter, value);
                while (g_variant_iter_loop(&array_iter, "{sv}", &key, &array_value)) {
                    if (g_strcmp0(key, "Volume") == 0) {
                        gdouble volume = g_variant_get_double(array_value);

                        priv->volume = volume;

                        if (priv->onVolumeUpdated)
                            priv->onVolumeUpdated(priv->volume);
                    }

                    if (g_strcmp0(key, "Mute") == 0) {
                        gboolean mute = g_variant_get_boolean(array_value);

                        priv->mute_state = mute;

                        if (priv->onMuteUpdated)
                            priv->onMuteUpdated(priv->mute_state);
                    }
                }
            }
        }
    }

    if (value)
        g_variant_unref(value);
    if (array_value)
        g_variant_unref(array_value);
}

void MediaPlayerPrivate::on_name_acquired(GDBusConnection *connection, const gchar *name G_GNUC_UNUSED, gpointer user_data) {
    g_print("Aquired name : %s on the session bus\n", name);
    auto priv = (MediaPlayerPrivate *)user_data;

    if ((priv->stream_type == MediaPlayer::StreamType::Music) || (priv->stream_type == MediaPlayer::StreamType::ControlOnly)) {
        priv->MPRISInit();
    }

    // TO notify that bus has been created and also name has been aquired.
    priv->cond.notify_one();

    guint res = g_dbus_connection_signal_subscribe(connection,
        NULL,                               //listen to all senders
        "org.freedesktop.DBus.Properties",  //match on all interfaces
        "PropertiesChanged",
        "/com/qualcomm/qti/adk/audiomanager/global_controller",  //match on all object paths
        NULL,                                                    //match on all arguments
        G_DBUS_SIGNAL_FLAGS_NONE,
        MediaPlayerPrivate::GDBusPropertiesChangedCb,
        user_data,  //user_data argument passed to the callback function
        NULL);

    if (res == 0) {
        if (priv->onError)
            priv->onError(MediaPlayer::ErrorType::IPC, "Unable to subscribe for Volume and Mute property change", {});
    }
}

void MediaPlayerPrivate::on_name_lost(GDBusConnection *connection G_GNUC_UNUSED, const gchar *name, gpointer user_data G_GNUC_UNUSED) {
    g_print("Lost the name %s on the session bus\n", name);
}

int MediaPlayerPrivate::DBusInit() {
    source = g_idle_source_new();
    g_source_set_callback(source, (GSourceFunc)MediaPlayerPrivate::GDBusInit, this, NULL);
    guint res = g_source_attach(source, thread_main_context);

    // Wait until the dbus session is actually created
    cond.wait(lock);

    if (res == 0)
        res = -1;

    return res;
}

gboolean MediaPlayerPrivate::GDBusInit(gpointer user_data) {
    auto priv = (MediaPlayerPrivate *)user_data;
    GError *error = NULL;

    do {
        const gchar *address = g_dbus_address_get_for_bus_sync(G_BUS_TYPE_SESSION, NULL, &error);

        if (error != NULL)
            break;

        const GDBusConnectionFlags flags = (GDBusConnectionFlags)(G_DBUS_CONNECTION_FLAGS_AUTHENTICATION_CLIENT | G_DBUS_CONNECTION_FLAGS_MESSAGE_BUS_CONNECTION);

        priv->connection = g_dbus_connection_new_for_address_sync(address,
            flags,
            NULL,
            NULL,
            &error);

        if (error != NULL)
            break;

        priv->dbus_id = g_bus_own_name_on_connection(priv->connection,  //the type of bus to own a name on
            ("org.mpris.MediaPlayer2." + priv->app_name).c_str(),       //the well-known name to own
            G_BUS_NAME_OWNER_FLAGS_NONE,                                //a set of flags from the GBusNameOwnerFlags enumeration                                 //handler to invoke when connected to the bus of type bus_type
            priv->on_name_acquired,                                     //handler to invoke when name is acquired
            priv->on_name_lost,                                         //handler to invoke when name is lost
            user_data,                                                  //user data to pass to handlers
            NULL);                                                      //function for freeing user_data

        if (priv->dbus_id == 0) {
            return TRUE;
        }
    } while (false);

    if (error != NULL) {
        g_error_free(error);
        return TRUE;
    }

    return FALSE;
}

void MediaPlayerPrivate::DBusDeinit() {
    if (proxy) {
        g_object_unref(proxy);
        proxy = NULL;
    }

    if (registration_id) {
        g_dbus_connection_unregister_object(connection, registration_id);
        registration_id = 0;
    }

    if (connection) {
        g_dbus_connection_close_sync(connection, NULL, NULL);
        g_dbus_connection_flush_sync(connection, NULL, NULL);
        gst_object_unref(connection);
        connection = NULL;
    }

    if (dbus_id) {
        g_bus_unown_name(dbus_id);
        dbus_id = 0;
    }
}

void MediaPlayerPrivate::EmitPropertyChangeSignal(std::string property) {
    if (connection && dbus_id) {
        GError *error = NULL;

        GVariantBuilder builder;
        g_variant_builder_init(&builder, G_VARIANT_TYPE_ARRAY);

        g_variant_builder_add(&builder, "{sv}", property.c_str(), GetPropertyValue(property, this));

        g_dbus_connection_emit_signal(connection,
            NULL,
            "/org/mpris/MediaPlayer2",
            "org.freedesktop.DBus.Properties",
            "PropertiesChanged",
            g_variant_new("(sa{sv}as)",
                "org.mpris.MediaPlayer2.Player",
                &builder,
                NULL),
            &error);

        if (error) {
            if (onError)
                onError(MediaPlayer::ErrorType::IPC, "Unable to emit property changed signal", error->message);
        }
    }
}

void MediaPlayerPrivate::EmitSeekedSignal(gint64 value) {
    if (connection && dbus_id) {
        GError *error = NULL;

        g_dbus_connection_emit_signal(connection,
            NULL,
            "/org/mpris/MediaPlayer2",
            "org.mpris.MediaPlayer2.Player",
            "Seeked",
            g_variant_new("(x)", value),
            &error);

        if (error) {
            if (onError)
                onError(MediaPlayer::ErrorType::IPC, "Unable to emit seeked signal", error->message);
        }
    }
}

GDBusProxy *MediaPlayerPrivate::CreateProxy(GDBusConnection *connection, std::string service, std::string object_path, std::string interface) {
    GError *error = NULL;
    GDBusProxy *proxy = NULL;

    if (connection) {
        proxy = g_dbus_proxy_new_sync(connection,  //Connection id
            G_DBUS_PROXY_FLAGS_NONE,               //Flags used when constructing the proxy
            NULL,                                  //GDBusInterfaceInfo
            service.c_str(),                       //A bus name
            object_path.c_str(),                   //An object path
            interface.c_str(),                     //A D-Bus interface name
            NULL,                                  //GCancellable
            &error);

        if (error != NULL) {
            g_error_free(error);
            if (proxy != NULL)
                g_object_unref(proxy);

            return nullptr;
        }
    }

    return proxy;
}

bool MediaPlayerPrivate::GetMute() {
    return mute_state;
}

bool MediaPlayerPrivate::SetMute(bool value) {
    GError *error = NULL;
    GVariant *var = NULL;
    bool res = true;

    if (proxy) {
        var = g_dbus_proxy_call_sync(proxy,  //A GDBusProxy
            "Set",                           //Name of method to invoke
            g_variant_new("(ssv)", "com.qualcomm.qti.adk.audiomanager.global_controller", "Mute", g_variant_new("b", value)),
            G_DBUS_CALL_FLAGS_NONE,
            -1,    //The timeout in milliseconds
            NULL,  //A GCancellable
            &error);
    } else {
        res = false;
    }

    if (var)
        g_variant_unref(var);
    if (error) {
        if (onError)
            onError(MediaPlayer::ErrorType::IPC, "g_dbus_proxy_call_sync failed for set_mute() API", {});

        g_error_free(error);

        res = false;
    }

    return res;
}

double MediaPlayerPrivate::GetVolume() {
    return volume;
}

bool MediaPlayerPrivate::SetVolume(double value) {
    /** Checks if value is within boundaries i.e. (0-1). */
    value = (value < 0.0) ? 0.0 : (value > 1.0) ? 1.0 : value;

    GError *error = NULL;
    GVariant *var = NULL;
    bool res = true;

    if (proxy) {
        var = g_dbus_proxy_call_sync(proxy,  //A GDBusProxy
            "Set",                           //Name of method to invoke
            g_variant_new("(ssv)", "com.qualcomm.qti.adk.audiomanager.global_controller", "Volume", g_variant_new("d", value)),
            G_DBUS_CALL_FLAGS_NONE,
            -1,    //The timeout in milliseconds
            NULL,  //A GCancellable
            &error);
    } else {
        res = false;
    }

    if (var)
        g_variant_unref(var);
    if (error) {
        if (onError)
            onError(MediaPlayer::ErrorType::IPC, "g_dbus_proxy_call_sync failed for set_volume() API", {});

        g_error_free(error);

        res = false;
    }

    return res;
}

// Get initial value of volume status of system.
void MediaPlayerPrivate::InitialiseVolumeStatus() {
    GError *error = NULL;
    GVariant *value = NULL;

    if (proxy != NULL) {
        value = g_dbus_proxy_call_sync(proxy,  //A GDBusProxy
            "Get",                             //Name of method to invoke
            g_variant_new("(ss)", "com.qualcomm.qti.adk.audiomanager.global_controller", "Volume"),
            G_DBUS_CALL_FLAGS_NONE,
            -1,    //The timeout in milliseconds
            NULL,  //A GCancellable
            &error);
    }

    if (value) {
        gdouble local_volume;
        g_variant_get(value, "((d))", &local_volume);
        /** Checks if value returned by dbus method call is
         * within boundaries i.e. (0-1). */
        volume = (local_volume < 0.0) ? 0.0 : (local_volume > 1.0) ? 1.0 : local_volume;
    }

    if (value)
        g_variant_unref(value);
    if (error) {
        if (onError)
            onError(MediaPlayer::ErrorType::IPC, "Unable to Initialise Initial Volume value", {});
        g_error_free(error);
    }

    return;
}

// Get initial value of mute status of system.
void MediaPlayerPrivate::InitialiseMuteStatus() {
    GError *error = NULL;
    GVariant *value = NULL;
    proxy = CreateProxy(connection, "com.qualcomm.qti.adk.audiomanager",
        "/com/qualcomm/qti/adk/audiomanager/global_controller",
        "org.freedesktop.DBus.Properties");
    if (proxy != NULL) {
        value = g_dbus_proxy_call_sync(proxy,  //A GDBusProxy
            "Get",                             //Name of method to invoke
            g_variant_new("(ss)", "com.qualcomm.qti.adk.audiomanager.global_controller", "Mute"),
            G_DBUS_CALL_FLAGS_NONE,
            -1,    //The timeout in milliseconds
            NULL,  //A GCancellable
            &error);
    }

    if (value) {
        gboolean mute;
        g_variant_get(value, "((b))", &mute);

        mute_state = mute;
    }

    if (value)
        g_variant_unref(value);
    if (error) {
        if (onError)
            onError(MediaPlayer::ErrorType::IPC, "Unable to Initialise Initial Mute value", {});
        g_error_free(error);
    }

    return;
}

}  // namespace adk
