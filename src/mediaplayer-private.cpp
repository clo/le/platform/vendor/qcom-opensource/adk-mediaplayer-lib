/*
 * Copyright (c) 2018-2020, The Linux Foundation. All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above
 *       copyright notice, this list of conditions and the following
 *       disclaimer in the documentation and/or other materials provided
 *       with the distribution.
 *     * Neither the name of The Linux Foundation nor the names of its
 *       contributors may be used to endorse or promote products derived
 *       from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS
 * BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
 * BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
 * IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include "mediaplayer-private.h"

#include <adk/mediaplayer.h>
#include <cmath>
#include <iostream>

#define TMP_PATH "/tmp"

#include <syslog.h>

#include <fstream>

/* WakeLock macros*/
#define MAX_WAKE_LOCK_COMMAND_STRING 100
#define WAKELOCK_NO_TIMEOUT 0
#define WAKELOCK_RELEASE_AFTER_3_SEC 3000000000

GST_DEBUG_CATEGORY(mediaplayerlib_debug);
#define GST_CAT_DEFAULT mediaplayerlib_debug

namespace adk {
MediaPlayerPrivate::MediaPlayerPrivate() {
    config_ = adk::config::Config::Create(CONFIG_FILENAME);
    InitPPPropMatchTables();
    InitDelayTables();
}

MediaPlayerPrivate::~MediaPlayerPrivate() {
    if (pipeline)
        gst_element_set_state(GST_ELEMENT(pipeline), GST_STATE_NULL);

    if (main_loop)
        g_main_loop_quit(main_loop);

    // Wait until the thread has actually shut down
    if (thread.joinable())
        thread.join();

    if (main_loop)
        g_main_loop_unref(main_loop);

    if (thread_main_context)
        g_main_context_unref(thread_main_context);

    DBusDeinit();

    if (source) {
        g_source_destroy(source);
        g_source_unref(source);
    }

    if (appsrc)
        gst_object_unref(appsrc);

    if (pipeline)
        gst_object_unref(GST_OBJECT(pipeline));

    ClearMetadata();
}

void MediaPlayerPrivate::WakeLockToggle(
    bool lock, std::string lock_name, int delay_ms) {
    if (lock_name.empty())
        return;

    syslog(LOG_INFO, "Wakelock: %s, status:%s, timeout:%ums \n",
        lock_name.c_str(), lock ? "lock" : "unlock", delay_ms);

    long long delay = delay_ms * 1000000ll;
    std::ofstream wakelock_file;
    wakelock_file.open("/sys/power/wake_lock");

    if (lock) {
        if (delay != WAKELOCK_NO_TIMEOUT) {
            wakelock_file << lock_name << " " << delay << std::endl;
        } else {
            wakelock_file << lock_name << std::endl;
        }
    } else {
        // acquire lock permanently
        wakelock_file << lock_name << std::endl;
        // acquire lock using 3 sec timeout in order to release it.
        wakelock_file << lock_name << " " << WAKELOCK_RELEASE_AFTER_3_SEC
                      << std::endl;
    }

    if (wakelock_file.bad())
        syslog(LOG_INFO, "Failed to acquire %s %s \n", lock_name.c_str(),
            lock ? "lock" : "unlock");
    else
        syslog(LOG_INFO, "Success to acquire %s %s\n", lock_name.c_str(),
            lock ? "lock" : "unlock");

    wakelock_file.close();
}

void MediaPlayerPrivate::SystemWakeLockToggle(bool lock) {
    std::string lock_name = "mediaplayer_" + app_name;

    if (lock) {
        WakeLockToggle(true, lock_name, 0);
    } else {
        WakeLockToggle(false, lock_name, 0);
    }
}

MediaPlayer::State MediaPlayerPrivate::StateToEnum(GstState state) {
    switch (state) {
        case GST_STATE_PLAYING:
            return MediaPlayer::State::Playing;

        case GST_STATE_PAUSED:
            return MediaPlayer::State::Paused;

        case GST_STATE_NULL:
            return MediaPlayer::State::Stopped;

        default:
            return MediaPlayer::State::Stopped;
    }
}

void MediaPlayerPrivate::MetadataChanged() {
    if (onMetadataChanged) {
        onMetadataChanged();
    }

    EmitPropertyChangeSignal("Metadata");
}

void MediaPlayerPrivate::StateChanged(MediaPlayer::State state) {
    if (onStateChanged) {
        onStateChanged(state);
    }

    EmitPropertyChangeSignal("PlaybackStatus");
}

void MediaPlayerPrivate::PlaybackStatusChanged(MediaPlayer::State state) {
    control_only_mediaplayer_state = state;

    EmitPropertyChangeSignal("PlaybackStatus");
}

void MediaPlayerPrivate::ControlOnlyStateChanged(MediaPlayer::State state) {
    if (onStateChanged) {
        onStateChanged(state);
    }
}

gboolean MediaPlayerPrivate::BusCallback(
    GstBus *bus G_GNUC_UNUSED, GstMessage *msg, gpointer data) {
    auto priv = (MediaPlayerPrivate *)data;

    switch (GST_MESSAGE_TYPE(msg)) {
        case GST_MESSAGE_ERROR: {
            gchar *debug;
            GError *error;
            gst_message_parse_error(msg, &error, &debug);
            if (priv->is_live_src_) {
                gboolean format_changed = FALSE;
                const char *property = "sync";
                if (!g_strcmp0(error->message,
                        "Disconnected: Connection terminated")) {
                    GST_CAT_DEBUG(
                        GST_CAT_DEFAULT, "Disconnected: Connection terminated");
                    format_changed = TRUE;
                } else if (!g_strcmp0(
                               error->message, "Disconnected: Entity killed")) {
                    GST_CAT_DEBUG(
                        GST_CAT_DEFAULT, "Disconnected: Entity killed");
                    format_changed = TRUE;
                } else if (!g_strcmp0(error->message,
                               "Failed to connect stream: Timeout")) {
                    GST_CAT_DEBUG(
                        GST_CAT_DEFAULT, "Failed to connect stream: Timeout");
                    /* Try to reopen the device if exists */
                    format_changed = TRUE;
                } else if (g_error_matches(error, GST_LIBRARY_ERROR,
                               GST_LIBRARY_ERROR_INIT)) {
                    /* This case is specifically for DTS & DD 2ch/48kHz clips if
                     * license validation fails. Format change can happen
                     * between DTX DVD & Dolby Digital without stream restart.
                     * Pipeline goes to NULL state on error if module init
                     * fails. Audio will be mute until we switch to a file where
                     * stream restart happens ie., DDP, THD, MAT (transmission
                     * rate in 192kHz). But if switch between AC3 & DTS, we are
                     * in a dead lock and audio will be mute.
                     */
                    GST_CAT_ERROR(GST_CAT_DEFAULT, "%s", error->message);
                    if (priv->device_status == GST_DEVICE_ACTIVE
                        && GST_STREAM_TYPE_ENCODED == priv->cur_stream_type) {
                        GST_DEBUG_OBJECT(priv->pipeline,
                            "removing %" GST_PTR_FORMAT, priv->decoder_bin);
                        gst_bin_remove(
                            GST_BIN(priv->pipeline), priv->decoder_bin);
                    }
                    GST_CAT_DEBUG(GST_CAT_DEFAULT, "Restarting Pipeline..");
                    gst_element_set_state(
                        GST_ELEMENT(priv->pipeline), GST_STATE_NULL);
                    gst_element_set_state(
                        GST_ELEMENT(priv->pipeline), GST_STATE_PLAYING);
                    return TRUE;
                } else if (g_error_matches(error, GST_STREAM_ERROR,
                               GST_STREAM_ERROR_DECODE)) {
                    GST_CAT_ERROR(GST_CAT_DEFAULT, "%s", error->message);

                    if (priv->cur_stream_type == GST_STREAM_TYPE_ENCODED) {
                        if (GST_ELEMENT_CAST(msg->src) == priv->ddpdec) {
                            if ((priv->cur_codec_type == GST_CODEC_TYPE_DD)
                                || (priv->cur_codec_type
                                       == GST_CODEC_TYPE_DDP)) {
                                GST_CAT_ERROR(GST_CAT_DEFAULT,
                                    "Error from DD/DDP decoder. "
                                    "[ENCODED->UNKNOWN]");

                                /* Check for encoded audio in dolby stream
                                 * detector */
                                g_object_set(G_OBJECT(priv->stream_detect),
                                    property, TRUE, NULL);
                            }

                        } else if (GST_ELEMENT_CAST(msg->src)
                            == priv->dtsxdec) {
                            if (priv->cur_codec_type == GST_CODEC_TYPE_DTSX) {
                                GST_CAT_ERROR(GST_CAT_DEFAULT,
                                    "Error from DTSX decoder. "
                                    "[ENCODED->UNKNOWN]");

                                /* Check for encoded audio in dolby stream
                                 * detector */
                                g_object_set(G_OBJECT(priv->stream_detect),
                                    property, TRUE, NULL);

                                /* Raise Unknown stream type signal */
                            }
                        } else {
                            /* TODO: for MAT/THD, we are seeing unexpected
                             * errors which we are analyzing */
                            GST_CAT_ERROR(GST_CAT_DEFAULT,
                                "Current codec and decoder error doesn't "
                                "match");

                            /* Raise Unknown stream type signal */
                            // stream_type_changed(GST_STREAM_TYPE_ERROR,
                            // comp_holder->new_codec_type, priv);
                        }
                    } else if (priv->cur_stream_type == GST_STREAM_TYPE_PCM) {
                        /* PCM detector is linked in pipeline */
                        if (priv->pcm_decoded_stream_type
                            == GST_STREAM_TYPE_DTS_CD) {
                            if (GST_ELEMENT_CAST(msg->src) == priv->dtsxdec) {
                                if (priv->cur_codec_type
                                    == GST_CODEC_TYPE_DTSX) {
                                    GST_CAT_ERROR(GST_CAT_DEFAULT,
                                        "Error from DTSX decoder. "
                                        "[PCM->UNKNOWN]");

                                    /* Check for valid DTS audio in pcm stream
                                     * detector */
                                    g_object_set(
                                        G_OBJECT(priv->pcm_stream_detect),
                                        property, TRUE, NULL);

                                    /* Raise Unknown stream type signal */
                                }
                            } else {
                                GST_CAT_ERROR(GST_CAT_DEFAULT,
                                    "Current codec and decoder error doesn't "
                                    "match");

                                /* Raise Unknown stream type signal */
                            }
                        }
                    }

                    GST_CAT_DEBUG(GST_CAT_DEFAULT, "Restarting Pipeline..");
                    gst_element_set_state(
                        GST_ELEMENT(priv->pipeline), GST_STATE_NULL);
                    gst_element_set_state(
                        GST_ELEMENT(priv->pipeline), GST_STATE_PLAYING);

                    return TRUE;

                } else {
                    GST_CAT_ERROR(GST_CAT_DEFAULT, "Pipeline set to NULL");
                    GST_CAT_ERROR(GST_CAT_DEFAULT, "error %s from %s",
                        error->message, GST_OBJECT_NAME(msg->src));
                }
                priv->DeviceRemoved();
            }
            if (priv->onError)
                priv->onError(
                    MediaPlayer::ErrorType::GENERAL, error->message, debug);

            g_error_free(error);
            g_free(debug);

            break;
        }

        case GST_MESSAGE_ASYNC_DONE: {
            if (priv->onAsyncDone) {
                if (priv->skip_async) {
                    priv->skip_async = false;
                } else {
                    priv->onAsyncDone();
                }
            }
            break;
        }

        case GST_MESSAGE_EOS: {
            if (priv->onEndOfStream)
                priv->onEndOfStream();

            /// Note: Releasing wake lock after EOS.
            /// Cases of playlist, loop and shuffle mode should be handled from
            /// within the App
            if (priv->is_wake_lock_) {
                priv->is_wake_lock_ = false;
                priv->SystemWakeLockToggle(priv->is_wake_lock_);
            }
            break;
        }

        case GST_MESSAGE_STATE_CHANGED: {
            if (GST_MESSAGE_SRC(msg) == GST_OBJECT(priv->pipeline)) {
                GstState new_state, pending_state, old_state;
                gst_message_parse_state_changed(
                    msg, &old_state, &new_state, &pending_state);

                /// Aquire the wake lock.
                if ((pending_state == GST_STATE_PLAYING
                        || new_state == GST_STATE_PLAYING)
                    && (!priv->is_wake_lock_)) {
                    priv->is_wake_lock_ = true;
                    priv->SystemWakeLockToggle(priv->is_wake_lock_);
                }

                /// Release the wake lock.
                if ((old_state == GST_STATE_PLAYING) && (priv->is_wake_lock_)) {
                    priv->is_wake_lock_ = false;
                    priv->SystemWakeLockToggle(priv->is_wake_lock_);
                }

                if (new_state == GST_STATE_PAUSED
                    && pending_state == GST_STATE_PLAYING) {
                    auto start = priv->seek_offset.count();
                    if (start) {
                        if (gst_element_seek(GST_ELEMENT(priv->pipeline), 1.0,
                                GST_FORMAT_TIME, GST_SEEK_FLAG_FLUSH,
                                GST_SEEK_TYPE_SET, start, GST_SEEK_TYPE_NONE,
                                GST_CLOCK_TIME_NONE)) {
                            priv->skip_async = true;
                        }

                        priv->seek_offset = std::chrono::nanoseconds(0);
                    }
                }

                if (pending_state == GST_STATE_VOID_PENDING) {
                    switch (new_state) {
                        case GST_STATE_PLAYING:
                            priv->StateChanged(MediaPlayer::State::Playing);
                            if (priv->is_live_src_)
                                priv->StreamTypeChanged(priv->cur_stream_type,
                                    priv->cur_codec_type, priv);
                            break;

                        case GST_STATE_PAUSED:
                            priv->StateChanged(MediaPlayer::State::Paused);
                            break;

                        case GST_STATE_NULL:

                        case GST_STATE_READY:

                        default:
                            // We don't handle these here
                            break;
                    }
                }
            }
            break;
        }

        case GST_MESSAGE_TAG: {
            GstTagList *tags = NULL;
            gst_message_parse_tag(msg, &tags);
            gst_tag_list_foreach(tags, MediaPlayerPrivate::FillMetadata, priv);
            priv->MetadataChanged();
            gst_tag_list_unref(tags);
            break;
        }

        case GST_MESSAGE_BUFFERING: {
            gint buffering_percent = 0;
            gst_message_parse_buffering(msg, &buffering_percent);
            if (priv->onBuffering)
                priv->onBuffering(buffering_percent);
            break;
        }

        case GST_MESSAGE_REQUEST_STATE: {
            GstState state;
            gst_message_parse_request_state(msg, &state);
            gst_element_set_state(GST_ELEMENT(priv->pipeline), state);
            break;
        }

        default:
            break;
    }

    return TRUE;
}

gboolean MediaPlayerPrivate::MainloopIsRunning(MediaPlayerPrivate *priv) {
    // Wake up Create() now that the mainloop is running
    priv->thread_running = true;
    priv->cond.notify_one();

    return FALSE;
}

void MediaPlayerPrivate::MainloopThread(MediaPlayerPrivate *priv) {
    g_main_context_push_thread_default(priv->thread_main_context);

    auto *idle_source = g_idle_source_new();
    g_source_set_callback(idle_source,
        (GSourceFunc)MediaPlayerPrivate::MainloopIsRunning, priv, NULL);
    g_source_attach(idle_source, priv->thread_main_context);

    if (priv->stream_type != MediaPlayer::StreamType::ControlOnly) {
        auto *bus = gst_pipeline_get_bus(priv->pipeline);
        gst_bus_add_watch(bus, MediaPlayerPrivate::BusCallback, priv);
        gst_object_unref(bus);
    }

    g_main_loop_run(priv->main_loop);

    g_source_destroy(idle_source);
    g_main_context_pop_thread_default(priv->thread_main_context);
    g_source_unref(idle_source);
}

const char *MediaPlayerPrivate::EnumToString(MediaPlayer::StreamType type) {
    switch (type) {
        case MediaPlayer::StreamType::Music:
            return "music";
        case MediaPlayer::StreamType::Cue:
            return "cue";
        case MediaPlayer::StreamType::Tone:
            return "tone";
        case MediaPlayer::StreamType::Alarm:
            return "alarm";
        case MediaPlayer::StreamType::VoiceUI:
            return "voiceui";
        case MediaPlayer::StreamType::Call:
            return "call";
        default:
            return "unknown";
    }
}

MediaPlayer::Result MediaPlayerPrivate::GstToMediaPlayerResult(
    GstStateChangeReturn ret) {
    auto res = MediaPlayer::Result::Failure;

    switch (ret) {
        case GST_STATE_CHANGE_SUCCESS:
            res = MediaPlayer::Result::Success;
            break;

        case GST_STATE_CHANGE_ASYNC:
            res = MediaPlayer::Result::Async;
            break;

        case GST_STATE_CHANGE_FAILURE:
            res = MediaPlayer::Result::Failure;
            break;

        case GST_STATE_CHANGE_NO_PREROLL:
            res = MediaPlayer::Result::Async;
            break;
    }

    return res;
}

bool MediaPlayerPrivate::GstFlowReturnToBool(GstFlowReturn ret) {
    auto res = false;

    switch (ret) {
        case GST_FLOW_CUSTOM_SUCCESS_2:
        case GST_FLOW_CUSTOM_SUCCESS_1:
        case GST_FLOW_CUSTOM_SUCCESS:
        case GST_FLOW_OK:
            res = true;
            break;

        case GST_FLOW_ERROR:
        case GST_FLOW_NOT_SUPPORTED:
        default:
            res = false;
            break;
    }

    return res;
}

void MediaPlayerPrivate::FillMetadata(
    const GstTagList *list, const gchar *tag, gpointer user_data) {
    auto priv = (MediaPlayerPrivate *)user_data;
    int num = gst_tag_list_get_tag_size(list, tag);

    for (int i = 0; i < num; i++) {
        const GValue *val;
        GstSample *sample;
        GstBuffer *buf;
        GstMapInfo map;

        val = gst_tag_list_get_value_index(list, tag, i);

        if ((strcmp(tag, "album") == 0) && G_VALUE_HOLDS_STRING(val))
            priv->metadata[MediaPlayer::Tag::Album] = g_value_get_string(val);

        if ((strcmp(tag, "artist") == 0) && G_VALUE_HOLDS_STRING(val))
            priv->metadata[MediaPlayer::Tag::Artist] = g_value_get_string(val);

        if ((strcmp(tag, "bitrate") == 0) && G_VALUE_HOLDS_UINT(val)) {
            guint bitrate = g_value_get_uint(val);
            priv->metadata[MediaPlayer::Tag::AudioBitrate] =
                std::to_string(bitrate);
        }

        if ((strcmp(tag, "genre") == 0) && G_VALUE_HOLDS_STRING(val))
            priv->metadata[MediaPlayer::Tag::Genre] = g_value_get_string(val);

        if ((strcmp(tag, "title") == 0) && G_VALUE_HOLDS_STRING(val))
            priv->metadata[MediaPlayer::Tag::Title] = g_value_get_string(val);

        if ((strcmp(tag, "location") == 0) && G_VALUE_HOLDS_STRING(val))
            priv->metadata[MediaPlayer::Tag::Url] = g_value_get_string(val);

        if ((strcmp(tag, "image") == 0) && GST_VALUE_HOLDS_SAMPLE(val)) {
            sample = gst_value_get_sample(val);
            buf = gst_sample_get_buffer(sample);
            gst_buffer_map(buf, &map, GST_MAP_READ);

            GstCaps *caps = gst_sample_get_caps(sample);

            std::string str =
                gst_structure_get_name(gst_caps_get_structure(caps, 0));
            // Here expectation is that data in GstSample is always of type
            // "image/xx"
            std::string format = str.substr(str.find("/") + 1);

            if (!format.empty()) {
                std::ofstream file(
                    TMP_PATH "/cover_art_" + priv->app_name + "." + format,
                    std::ofstream::out);
                file.write((char *)map.data, map.size);

                priv->local_cover_art = true;
            }

            gst_buffer_unmap(buf, &map);

            priv->metadata[MediaPlayer::Tag::CoverArt] =
                TMP_PATH "/cover_art_" + priv->app_name + "." + format;
        }
    }
}

void MediaPlayerPrivate::ClearMetadata() {
    if (local_cover_art == true) {
        // Delete Cover Art only if it's generated by MediaPlayer Library.
        std::remove(metadata[MediaPlayer::Tag::CoverArt].c_str());
        local_cover_art = false;
    }

    if (!metadata.empty())
        metadata.clear();
}

void MediaPlayerPrivate::InitGlobalSetting() {
    /// Gets the initial value of Mute status.
    InitialiseMuteStatus();

    /// Gets the initial value of Volume.
    InitialiseVolumeStatus();

    return;
}

void MediaPlayerPrivate::NeedDataCb(
    GstAppSrc *src G_GNUC_UNUSED, guint size, gpointer user_data) {
    auto priv = (MediaPlayerPrivate *)user_data;

    if (priv->needData)
        priv->needData(size);
}

void MediaPlayerPrivate::EnoughDataCb(
    GstAppSrc *src G_GNUC_UNUSED, gpointer user_data) {
    auto priv = (MediaPlayerPrivate *)user_data;

    if (priv->enoughData)
        priv->enoughData();
}

gboolean MediaPlayerPrivate::SeekDataCb(
    GstAppSrc *src G_GNUC_UNUSED, guint64 offset, gpointer user_data) {
    auto priv = (MediaPlayerPrivate *)user_data;

    if (priv->seekData) {
        priv->seekData(offset);
        return TRUE;
    }

    return FALSE;
}

void MediaPlayerPrivate::SourceSetup(GstElement *pipeline G_GNUC_UNUSED,
    GstElement *source, gpointer user_data) {
    auto priv = (MediaPlayerPrivate *)user_data;
    GstAppSrcCallbacks appSrcCbs;

    if (!priv->set_source_buffer_)
        return;

    /* Configure appsrc with caps */
    if (priv->appsrc)
        gst_object_unref(priv->appsrc);
    priv->appsrc = GST_APP_SRC(gst_object_ref(source));

    if (priv->caps_) {
        gst_app_src_set_caps(priv->appsrc, priv->caps_);
        gst_caps_unref(priv->caps_);
    }

    /* Set up Callbacks */
    appSrcCbs.need_data = NeedDataCb;
    if (priv->seekData) {
        appSrcCbs.seek_data = SeekDataCb;
        g_object_set(G_OBJECT(priv->appsrc), "stream-type",
            GST_APP_STREAM_TYPE_SEEKABLE, NULL);
    }
    if (priv->enoughData) {
        appSrcCbs.enough_data = EnoughDataCb;
    }

    if (priv->is_raw_format_)
        g_object_set(G_OBJECT(priv->appsrc), "format", GST_FORMAT_TIME, NULL);
    else
        g_object_set(G_OBJECT(priv->appsrc), "format", GST_FORMAT_BYTES, NULL);

    gst_app_src_set_callbacks(priv->appsrc, &appSrcCbs, priv, NULL);
}

bool MediaPlayerPrivate::CanLoop() {
    if (onLoopModeChange)
        return true;
    else
        return false;
}

bool MediaPlayerPrivate::CanShuffle() {
    if (onShuffleStatusChange)
        return true;
    else
        return false;
}

bool MediaPlayerPrivate::CanGoNext() {
    if (onNext)
        return true;
    else
        return false;
}

bool MediaPlayerPrivate::CanGoPrevious() {
    if (onPrevious)
        return true;
    else
        return false;
}

MediaPlayer::StreamType MediaPlayerPrivate::GetStreamType() {
    return stream_type;
}

bool MediaPlayerPrivate::PlayCb() {
    if (onPlay) {
        onPlay();
        return true;
    }

    return false;
}

bool MediaPlayerPrivate::PauseCb() {
    if (onPause) {
        onPause();
        return true;
    }

    return false;
}

bool MediaPlayerPrivate::StopCb() {
    if (onStop) {
        onStop();
        return true;
    }

    return false;
}

void MediaPlayerPrivate::PadAdded(GstElement *dec, GstPad *pad, gpointer data) {
    auto priv = (MediaPlayerPrivate *)data;
    GstCaps *caps = NULL;

    priv->SetHigherSampleRate(false);

    caps = gst_pad_get_current_caps(pad);
    if (caps) {
        const GstStructure *s = NULL;
        const GValue *val = NULL;
        guint64 channel_mask = 0;
        int channels = 0;

        s = gst_caps_get_structure(caps, 0);

        val = gst_structure_get_value(s, "channels");
        if (val) {
            channels = g_value_get_int(val);
            priv->channel_count_ = channels;
        }
        val = NULL;
        val = gst_structure_get_value(s, "rate");
        if (val) {
            priv->sample_rate_ = g_value_get_int(val);
            if (priv->sample_rate_ > MAXRATE)
                priv->SetHigherSampleRate(true);
        }

        val = gst_structure_get_value(s, "channel-mask");
        if (val) {
            channel_mask = gst_value_get_bitmask(val);
            priv->channel_mask_ = channel_mask;
        }
        gst_caps_unref(caps);
    }
    GstPad *sinkpad = NULL;

#ifdef BASEMACHINE_qcs40x
    if (priv->melod_postproc_bin) {
        priv->MelodLink();
        sinkpad = gst_element_get_static_pad(priv->melod_postproc_bin, "sink");
    }
#endif

    if(!sinkpad) {
        sinkpad = gst_element_get_static_pad(priv->pulsesink, "sink");
    }

    if (gst_pad_link(pad, sinkpad) != GST_PAD_LINK_OK) {
        g_printerr("%s :: Line %d, gst_pad_link failed", __func__, __LINE__);
    }

}

bool MediaPlayerPrivate::Init(std::string name, MediaPlayer::StreamType type,
    MediaPlayer::Flags flags, MediaPlayer::Properties props) {
    // Read value of compressed offload flag from database.
    GstPad *pad;
    if (config_) {
        config_->Read(
            &compressed_offload_flag, "mediaplayer.compressed_offload");
#ifdef BASEMACHINE_qcs40x
        config_->Read(&melod_flag, "mediaplayer.melod");
#endif
    }
    if (type != MediaPlayer::StreamType::ControlOnly) {
        // Technically this needs to be called just once, but repeated calls
        // will have no effect
        if (!gst_init_check(0, NULL, NULL))
            return false;
        GST_DEBUG_CATEGORY_INIT(
            mediaplayerlib_debug, "loop-back", 0, "loop-back");
        pipeline = GST_PIPELINE(gst_pipeline_new(name.c_str()));
        if (flags & MediaPlayer::Flags::Timestamp) {
            // Use pulsedirectsink, which supports timestamping
            pulsesink =
                gst_element_factory_make("pulsedirectsink", "audiosink");
            g_object_set(pulsesink, "provide-clock", FALSE, NULL);
            // With timestamps rendering, we do not need to delay the playback
            // to ensure glitch free playback since the timestamps will do it
            // already. And we do not even want to delay sending the packets
            // either or the audio could arrive too late.
            // Similarly, there is no need for prebuffering (but we do want it
            // to be non-zero to allow auto-uncorking of the stream).
            // And minreq doesn't matter since we work with whole packets.
            g_object_set(pulsesink, "timestamp", TRUE, "tlength", 1, "minreq",
                1, "prebuf", 1, NULL);
        } else if (compressed_offload_flag
            && (type == MediaPlayer::StreamType::Music)) {
            pulsesink =
                gst_element_factory_make("pulsedirectsink", "audiosink");
            g_object_set(pulsesink, "provide-clock", FALSE, NULL);
            g_object_set(pulsesink, "device", "offload0", NULL);
        } else {
            // Use standard pulsesink for other cases
            pulsesink = gst_element_factory_make("pulsesink", "audiosink");
            if (compressed_offload_flag
                || type != MediaPlayer::StreamType::Music)
                g_object_set(pulsesink, "device", "offload1", NULL);
        }

        auto *st = gst_structure_new("property", "media.role", G_TYPE_STRING,
            EnumToString(type), "qti.adk.mediaplayerlib.name", G_TYPE_STRING,
            name.c_str(), NULL);

        for (auto prop : props) {
            gst_structure_set(st, prop.first.c_str(), G_TYPE_STRING,
                prop.second.c_str(), NULL);
        }

        g_object_set(pulsesink, "stream-properties", st, NULL);
        gst_structure_free(st);

        /* Create Post Processing bins */
        /* Create dtsx post proc bin (dap) */
        CreateDolbyPostprocBin();
        /* Create dtsx post proc bin (soundx->mcdyn) */
        CreateVirtualxPostprocBin();
#ifdef BASEMACHINE_qcs40x
        if (type == MediaPlayer::StreamType::Music && melod_flag) {
            /* Create melod post proc bin
             (audioconvert->drc->audioconvert->matrix->around->audioconvert->bassmanagement) */
            CreateMelodPostprocBin();
        }
#endif
        if (flags & MediaPlayer::Flags::EnableLiveSource) {
            is_live_src_ = true;
            GST_CAT_DEBUG(GST_CAT_DEFAULT,
                "pcmdetector is not found! Adding identity element instead.");
            pulsesrc = gst_element_factory_make("pulsesrc", "pulsesrc");
            pulsedirectsrc =
                gst_element_factory_make("pulsedirectsrc", "pulsedirectsrc");
            g_object_set(G_OBJECT(pulsedirectsrc), "encoding-format", "dsd", NULL);
            capsfilter = gst_element_factory_make("capsfilter", "capsfilter");

            stream_detect =
                gst_element_factory_make("dolbystreamdetect", "iec61937parser");
            if (!stream_detect)
                return false;
            pcm_stream_detect =
                gst_element_factory_make("pcmstreamdetect", "pcmdetector");
            if (!pcm_stream_detect) {
                return false;
            }
            pulsedirectsink =
                gst_element_factory_make("pulsedirectsink", "pulsedirectsink");
            g_object_set(G_OBJECT(pulsedirectsink), "tlength", 32768, "device",
                "offload0", NULL);

            avdec_dsd =
                gst_element_factory_make("avdec_dsd_msbf_planar", "avdec_dsd");
            g_assert(avdec_dsd);
            g_object_ref_sink(avdec_dsd);

            if (latency == LATENCY_LOW)
                g_object_set(G_OBJECT(pulsesrc), "stream-flags", 0x0000220f,
                    "buffer-time", (gint64)40000, "latency-time", (gint64)10000,
                    NULL);
            else
                g_object_set(G_OBJECT(pulsesrc), "stream-flags", 0x0000220f,
                    "buffer-time", (gint64)600000, NULL);

            CreateDecoderBin();
            gst_bin_add(GST_BIN(pipeline), pulsesrc);
            gst_bin_add(GST_BIN(pipeline), capsfilter);
            gst_bin_add(GST_BIN(pipeline), stream_detect);
            gst_bin_add(GST_BIN(pipeline), pulsesink);

            /* link elements */
            gst_element_link_pads(pulsesrc, "src", capsfilter, "sink");
            gst_element_link_pads(capsfilter, "src", stream_detect, "sink");
            cur_stream_type = GST_STREAM_TYPE_ENCODED;

            /* Attach probe to monitor caps change of iec61937 parser*/
            pad = gst_element_get_static_pad(stream_detect, "src");
            gst_pad_add_probe(pad,
                (GstPadProbeType)(
                    GST_PAD_PROBE_TYPE_BLOCK | GST_PAD_PROBE_TYPE_EVENT_DOWNSTREAM),
                sd_pad_probe_event_cb_caps, this, NULL);
            gst_object_unref(pad);

            /* Attach probe to monitor caps change of pcm stream detector*/
            pad = gst_element_get_static_pad(pcm_stream_detect, "src");
            gst_pad_add_probe(pad,
                (GstPadProbeType)(
                    GST_PAD_PROBE_TYPE_BLOCK | GST_PAD_PROBE_TYPE_EVENT_DOWNSTREAM),
                sd_pad_probe_event_cb_pcm_detect, this, NULL);
            gst_object_unref(pad);

        } else {
            if (compressed_offload_flag) {
                gint playbin_flags;
                // Creating pipeline with playbin as compressed offload is enabled
                pipeline = GST_PIPELINE(gst_element_factory_make("playbin", "player"));

                if (flags & MediaPlayer::Flags::EnableBuffering) {
                    g_object_get(pipeline, "flags", &playbin_flags, NULL);
                    // Set GST_PLAY_FLAG_BUFFERING
                    g_object_set(pipeline, "flags", playbin_flags | (1 << 8), NULL);
                }

                g_signal_connect(pipeline, "source-setup", G_CALLBACK(MediaPlayerPrivate::SourceSetup), this);
                g_signal_connect(pipeline, "element-setup", G_CALLBACK(MediaPlayerPrivate::ElementSetupCb), this);
                g_object_set(pipeline, "audio-sink", pulsesink, NULL);

            } else {
                // Creating pipeline with uridecodebin
                uridecodebin = gst_element_factory_make("uridecodebin", "player");

                if (flags & MediaPlayer::Flags::EnableBuffering) {
                    // Set GST_PLAY_FLAG_BUFFERING
                    g_object_set(uridecodebin, "use-buffering", 1 << 8, NULL);
                }

                g_signal_connect(uridecodebin, "source-setup",
                    G_CALLBACK(MediaPlayerPrivate::SourceSetup), this);
                gst_bin_add_many(GST_BIN(pipeline), uridecodebin, pulsesink, NULL);
#ifdef BASEMACHINE_qcs40x
                if(melod_postproc_bin) {
                    gst_bin_add(GST_BIN(pipeline), melod_postproc_bin);
                    if(!gst_element_link(melod_postproc_bin, pulsesink)) {
                        g_printerr("linking melod_postproc_bin to pulsesink failed\n");
                        gst_bin_remove(GST_BIN(pipeline), melod_postproc_bin);
                        g_object_unref(melod_postproc_bin);
                        melod_postproc_bin = NULL;
                    }
                }
#endif
                g_signal_connect(uridecodebin, "pad-added", G_CALLBACK(PadAdded), this);
            }
        }
    }
    stream_type = type;
    app_name = name;

    thread_main_context = g_main_context_new();
    main_loop = g_main_loop_new(thread_main_context, FALSE);

    lock = std::unique_lock<std::mutex>(mutex);

    // Start up a thread for receiving callbacks from the pipeline's bus
    thread_running = false;
    thread = std::thread(MediaPlayerPrivate::MainloopThread, this);

    // Wait until the thread is actually created
    cond.wait(lock, [this] { return thread_running; });

    if (DBusInit() < 0)
        return false;

    InitGlobalSetting();

    if (stream_type == MediaPlayer::StreamType::ControlOnly) {
        return true;
    } else {
        if (pipeline)
            return true;
        else
            return false;
    }
}

void MediaPlayerPrivate::SetPreferredSinkLatencyInMilliSeconds(std::chrono::milliseconds latency) {
    /* Convert milliseconds to microseconds as buffer-time in pulsesink expects it to
     * be in microseconds */
    gint64 latency_time = (gint64)(latency.count() * GST_USECOND);
    if (pulsesink && !compressed_offload_flag && stream_type != MediaPlayer::StreamType::Music) {
        g_object_set(G_OBJECT(pulsesink), "buffer-time", latency_time, "latency-time", (gint64)10000, NULL);
    }
}
gint MediaPlayerPrivate::GetChannelPositionFromString(gchar *str) {
    int i, position = GST_AUDIO_CHANNEL_POSITION_INVALID;

    for (i = 0; i < PA_MAX_CHANNEL_POSITION; i++) {
        if (!g_strcmp0(str, pa_gst_ch_pos_table[i].pa_pos_name)) {
            position = pa_gst_ch_pos_table[i].gst_pos;
            break;
        }
    }
    return position;
}

guint64 MediaPlayerPrivate::GetChannelMaskFromChannelMap(guint8 channels, std::vector<std::string> channel_map) {
    GstAudioChannelPosition gst_channel_positions[channels];
    guint8 channel_index = 0;
    guint64 channel_mask = 0;
    gchar *word;

    for (auto i = 0; i < channel_map.size(); i++) {
        gst_channel_positions[channel_index++] = (GstAudioChannelPosition)GetChannelPositionFromString((gchar *)channel_map.at(i).c_str());
    }

    if (channel_index >= channels && !gst_audio_channel_positions_to_mask(gst_channel_positions, channels, false, &channel_mask))
        GST_CAT_DEBUG(GST_CAT_DEFAULT, "Failed to get channel mask from positions");
    std::cout << "channel mask before returning= " << channel_mask << std::endl;
    return channel_mask;
}

GstCaps *MediaPlayerPrivate::GetGstCaps(PASourceDeviceInfo_t info) {
    GstCaps *caps;
    int blocksize;
    gchar *encoding_format = (gchar *)info.encoding_formats_.at(0).c_str();
    params_.sampling_rate = info.sample_spec_.rate_;
    params_.channels = info.sample_spec_.channels_;
    guint64 channel_mask = GetChannelMaskFromChannelMap(params_.channels, info.channel_map_);

    if (!g_strcmp0(encoding_format, "unknown-iec61937")) {
        GST_CAT_DEBUG(GST_CAT_DEFAULT, " Its unknown-iec61937 !!!");
        encoding_format = "audio/x-iec61937";
        blocksize = (params_.sampling_rate * 4) / 100;
        caps = gst_caps_new_simple(encoding_format, "payload-type",
            G_TYPE_STRING, "normal", "rate", G_TYPE_INT, params_.sampling_rate, NULL);
    } else if (!g_strcmp0(encoding_format, "unknown-4x-iec61937")) {
        GST_CAT_DEBUG(GST_CAT_DEFAULT, " Its unknown-4x-iec61937 !!!");
        encoding_format = "audio/x-iec61937";
        params_.sampling_rate = params_.sampling_rate / 4;
        blocksize = (params_.sampling_rate * 16) / 100;
        caps = gst_caps_new_simple(encoding_format, "payload-type",
            G_TYPE_STRING, "4x", "rate", G_TYPE_INT, params_.sampling_rate, NULL);

    } else if (!g_strcmp0(encoding_format, "unknown-hbr-iec61937")) {
        GST_CAT_DEBUG(GST_CAT_DEFAULT, " Its unknown-hbr-iec61937 !!!");
        encoding_format = "audio/x-iec61937";
        blocksize = (params_.sampling_rate * 16) / 100;
        caps = gst_caps_new_simple(encoding_format, "payload-type",
            G_TYPE_STRING, "hbr", "rate", G_TYPE_INT, params_.sampling_rate, NULL);
    } else if (!g_strcmp0(encoding_format, "pcm")) {
        GST_CAT_DEBUG(GST_CAT_DEFAULT, " Its PCM !!!");
        encoding_format = "audio/x-raw";
        caps = gst_caps_new_simple(encoding_format, "layout", G_TYPE_STRING,
            "interleaved", "rate", G_TYPE_INT, params_.sampling_rate, "channels",
            G_TYPE_INT, params_.channels, "channel-mask", GST_TYPE_BITMASK, channel_mask, "format", G_TYPE_STRING, "S16LE", NULL);
    } else {
        GST_CAT_DEBUG(GST_CAT_DEFAULT, " Bad Source Format !!!");
    }
    return caps;
}

bool MediaPlayerPrivate::SetSource(setup_info_t info) {
    if (!is_live_src_)
        return false;

    if (device_status == GST_DEVICE_ACTIVE)
        return true;
    switch (pcm_postproc_path) {
        case PCM_DOLBY_DAP_POSTPROC:
            GST_CAT_DEBUG(
                GST_CAT_DEFAULT, "PCM PATH : DAP post processing applied ");
            break;
        case PCM_DTS_VIRTUALX_POSTPROC:
            GST_CAT_DEBUG(GST_CAT_DEFAULT,
                "PCM PATH : Virtualx post processing applied ");
            break;
        case PCM_NO_POSTPROC:
            GST_CAT_DEBUG(
                GST_CAT_DEFAULT, "PCM PATH : No post processing applied ");
            break;
    }

    GstCaps *caps;
    guint i, size;
    gst_stream_type_t st = GST_STREAM_TYPE_ERROR;
    gint blocksize = 4800;
    GstPad *pad = NULL;
    GstElement *dbin;

    cur_device_name = (gchar *)info.device_info.name_.c_str();
    caps = GetGstCaps(info.device_info);
    std::cout << cur_device_name << " " << gst_caps_to_string(caps) << std::endl;

    if (caps != NULL)
        size = gst_caps_get_size(caps);
    else
        return false;

    for (i = 0; i < size; ++i) {
        GstStructure *s = gst_caps_get_structure(caps, i);
        if (gst_structure_has_name(s, "audio/x-raw"))
            st = GST_STREAM_TYPE_PCM;
        else if (gst_structure_has_name(s, "audio/x-dsd"))
            st = GST_STREAM_TYPE_DSD;
    }
    if (st == GST_STREAM_TYPE_PCM) {
        switch (cur_stream_type) {
            case GST_STREAM_TYPE_PCM:
                /* If previous stream is PCM and incomming stream is PCM
                   don't make any changes. */
                GST_CAT_DEBUG(GST_CAT_DEFAULT, "PCM --> PCM");
                /* Set device and caps  */
                g_object_set(
                    G_OBJECT(pulsesrc), "device", cur_device_name, NULL);
                g_object_set(G_OBJECT(capsfilter), "caps", caps, NULL);
                if (dap == NULL && pcm_postproc_path == PCM_DOLBY_DAP_POSTPROC)
                    GST_CAT_WARNING(GST_CAT_DEFAULT,
                        "Dolby DAP not found! DAP post processing is disabled");
                if (soundx == NULL
                    && pcm_postproc_path == PCM_DTS_VIRTUALX_POSTPROC)
                    GST_CAT_WARNING(GST_CAT_DEFAULT,
                        "Dts soundx not found! DAP post processing is "
                        "disabled");
                if (mcdyn == NULL
                    && pcm_postproc_path == PCM_DTS_VIRTUALX_POSTPROC)
                    GST_CAT_WARNING(GST_CAT_DEFAULT,
                        "Dts mcdyn not found! DAP post processing is disabled");

                gst_element_set_state(GST_ELEMENT(pipeline), GST_STATE_PLAYING);
                device_status = GST_DEVICE_ACTIVE;
                break;
            case GST_STREAM_TYPE_ENCODED:
                /* If previous stream is Encoded and incomming stream is PCM
                     1. Unlink the pipeline :
                            stream-detect->decoder_bin->pulsesink
                     2. Check if needed to remove dap_post_proc or
                   virtualx_post_proc from dolbydtsbin
                     3. create path:
                               capsfilter->pulsesink (no post processing
                   required) capsfilter->dap_post_proc_bin->pulsesink (dap post
                   processing required)
                               capsfilter->virtualx_post_proc_bin->pulsesink(soundx
                   post processing required)
                 */
                GST_CAT_DEBUG(GST_CAT_DEFAULT, "ENCODED --> PCM");
                /* Reset PCM stream detector stream type to UNKNOWN */
                pcm_decoded_stream_type = GST_STREAM_TYPE_ERROR;
                GST_DEBUG_OBJECT(
                    pipeline, "removing %" GST_PTR_FORMAT, stream_detect);
                gst_bin_remove(GST_BIN(pipeline), stream_detect);
                dbin = gst_bin_get_by_name(GST_BIN(pipeline), "Decoder-Bin");
                if (dbin != NULL) {
                    GST_DEBUG_OBJECT(
                        pipeline, "removing %" GST_PTR_FORMAT, decoder_bin);
                    gst_bin_remove(GST_BIN(pipeline), decoder_bin);
                }
                /* Set device and caps  */
                g_object_set(
                    G_OBJECT(pulsesrc), "device", cur_device_name, NULL);

                /*TBD based on the latency setting */
                if (strcmp(sink_name, "offload-low-latency") == 0
                    && cur_codec_type == GST_CODEC_TYPE_MAT) {
                    g_object_set(G_OBJECT(pulsesrc), "stream-flags", 0x0000220f,
                        "buffer-time", (gint64)40000, "latency-time",
                        (gint64)10000, NULL);
                }
                g_object_set(G_OBJECT(capsfilter), "caps", caps, NULL);

                if (!IsNonDolbyCodec(cur_codec_type)
                    && dolby_graph != GST_GRAPH_TYPE_DOLBY_DEC_ONLY
                    && pcm_postproc_path == PCM_DOLBY_DAP_POSTPROC) {
                    GST_DEBUG_OBJECT(decoder_bin, "removing %" GST_PTR_FORMAT,
                        dolby_postproc_bin);
                    gst_bin_remove(GST_BIN(decoder_bin), dolby_postproc_bin);
                }

                if (cur_codec_type == GST_CODEC_TYPE_DTSX
                    && dts_graph != GST_GRAPH_TYPE_DTSX_ONLY
                    && pcm_postproc_path == PCM_DTS_VIRTUALX_POSTPROC) {
                    GST_DEBUG_OBJECT(decoder_bin, "removing %" GST_PTR_FORMAT,
                        virtualx_postproc_bin);
                    gst_bin_remove(GST_BIN(decoder_bin), virtualx_postproc_bin);
                }
                GST_DEBUG_OBJECT(
                    pipeline, "adding %" GST_PTR_FORMAT, pcm_stream_detect);
                pcm_stream_detect =
                    gst_element_factory_make("pcmstreamdetect", "pcmdetector");
                pad = gst_element_get_static_pad(pcm_stream_detect, "src");
                gst_pad_add_probe(pad, (GstPadProbeType)(GST_PAD_PROBE_TYPE_BLOCK | GST_PAD_PROBE_TYPE_EVENT_DOWNSTREAM),
                    sd_pad_probe_event_cb_pcm_detect, this, NULL);
                gst_object_unref(pad);
                gst_bin_add(GST_BIN(pipeline), pcm_stream_detect);
                if (pcm_postproc_path == PCM_NO_POSTPROC) {
                    GST_DEBUG_OBJECT(
                        pipeline, "linking..capsfilter->pcm_stream_detect");
                    if (!gst_element_link(capsfilter, pcm_stream_detect)) {
                        g_printerr(
                            "Could not link capsfilter to pcm_stream_detect\n");
                        return false;
                    }
                } else {
                    if (pcm_postproc_path == PCM_DOLBY_DAP_POSTPROC) {
                        GST_DEBUG_OBJECT(pipeline, "adding %" GST_PTR_FORMAT,
                            dolby_postproc_bin);
                        gst_bin_add(GST_BIN(pipeline), dolby_postproc_bin);

                        GST_DEBUG_OBJECT(
                            pipeline, "linking..capsfilter->pcm_stream_detect");
                        if (!gst_element_link(capsfilter, pcm_stream_detect)) {
                            g_printerr(
                                "Could not link capsfilter to "
                                "pcm_stream_detect\n");
                            return false;
                        }
                    }
                    if (pcm_postproc_path == PCM_DTS_VIRTUALX_POSTPROC) {
                        GST_DEBUG_OBJECT(pipeline, "adding %" GST_PTR_FORMAT,
                            virtualx_postproc_bin);
                        gst_bin_add(GST_BIN(pipeline), virtualx_postproc_bin);

                        GST_DEBUG_OBJECT(
                            pipeline, "linking..capsfilter->pcm_stream_detect");
                        if (!gst_element_link(capsfilter, pcm_stream_detect)) {
                            g_printerr(
                                "Could not link capsfilter to "
                                "pcm_stream_detect\n");
                            return false;
                        }
                    }
                }

                if (dap && pcm_postproc_path == PCM_DOLBY_DAP_POSTPROC)
                    GST_CAT_WARNING(GST_CAT_DEFAULT,
                        "Dolby DAP not found! DAP post processing is disabled");
                if (soundx && pcm_postproc_path == PCM_DTS_VIRTUALX_POSTPROC)
                    GST_CAT_WARNING(GST_CAT_DEFAULT,
                        "Dts soundx not found! Virtualx post processing is "
                        "disabled");
                if (mcdyn == NULL
                    && pcm_postproc_path == PCM_DTS_VIRTUALX_POSTPROC)
                    GST_CAT_WARNING(GST_CAT_DEFAULT,
                        "Dts mcdyn not found! Virtualx post processing is "
                        "disabled");
                gst_element_set_state(GST_ELEMENT(pipeline), GST_STATE_PLAYING);
                cur_stream_type = GST_STREAM_TYPE_PCM;
                device_status = GST_DEVICE_ACTIVE;
                break;
            case GST_STREAM_TYPE_DSD:
                GST_CAT_DEBUG(GST_CAT_DEFAULT, "DSD --> PCM");
                /* Reset PCM stream detector stream type to UNKNOWN */
                pcm_decoded_stream_type = GST_STREAM_TYPE_ERROR;

                /* Remove pulsedirectsrc */

                GST_DEBUG_OBJECT(
                    pipeline, "removing %" GST_PTR_FORMAT, pulsedirectsrc);
                gst_element_set_state(pulsedirectsrc, GST_STATE_NULL);
                gst_bin_remove(GST_BIN(pipeline), pulsedirectsrc);

                /* Remove and add capsfilter */
                GST_DEBUG_OBJECT(
                    pipeline, "removing %" GST_PTR_FORMAT, capsfilter);
                gst_element_set_state(capsfilter, GST_STATE_NULL);
                gst_bin_remove(GST_BIN(pipeline), capsfilter);

                /* Add pulsesrc */
                GST_DEBUG_OBJECT(pipeline, "adding %" GST_PTR_FORMAT, pulsesrc);
                gst_bin_add(GST_BIN(pipeline), pulsesrc);

                GST_DEBUG_OBJECT(
                    pipeline, "adding %" GST_PTR_FORMAT, capsfilter);
                gst_bin_add(GST_BIN(pipeline), capsfilter);

                /* Set device and caps  */
                g_object_set(
                    G_OBJECT(pulsesrc), "device", cur_device_name, NULL);
                g_object_set(G_OBJECT(capsfilter), "caps", caps, NULL);

                GST_DEBUG_OBJECT(pipeline, "linking..pulsesrc->capsfilter");
                if (!gst_element_link_pads(
                        pulsesrc, "src", capsfilter, "sink")) {
                    g_printerr("Could not link pulsesrc->capsfilter\n");
                    return false;
                }

                if (dsd_path == DSD_PATH_NON_NATIVE) {
                    GST_DEBUG_OBJECT(
                        pipeline, "removing %" GST_PTR_FORMAT, avdec_dsd);
                    gst_bin_remove(GST_BIN(pipeline), avdec_dsd);
                } else if (dsd_path == DSD_PATH_NATIVE) {
                    /* Unlink pulsedirectsink */
                    GST_DEBUG_OBJECT(
                        pipeline, "removing %" GST_PTR_FORMAT, pulsedirectsink);
                    gst_bin_remove(GST_BIN(pipeline), pulsedirectsink);

                    /* Add pulsesink */
                    GST_DEBUG_OBJECT(
                        pipeline, "adding %" GST_PTR_FORMAT, pulsesink);
                    gst_bin_add(GST_BIN(pipeline), pulsesink);
                }

                GST_DEBUG_OBJECT(
                    pipeline, "adding %" GST_PTR_FORMAT, pcm_stream_detect);
                gst_bin_add(GST_BIN(pipeline), pcm_stream_detect);

                if (pcm_postproc_path == PCM_NO_POSTPROC) {
                    GST_DEBUG_OBJECT(
                        pipeline, "linking..capsfilter->pcm_stream_detect");
                    if (!gst_element_link(capsfilter, pcm_stream_detect)) {
                        g_printerr(
                            "Could not link capsfilter to pcm_stream_detect\n");
                        return false;
                    }
                } else {
                    if (pcm_postproc_path == PCM_DOLBY_DAP_POSTPROC) {
                        GST_DEBUG_OBJECT(pipeline, "adding %" GST_PTR_FORMAT,
                            dolby_postproc_bin);
                        gst_bin_add(GST_BIN(pipeline), dolby_postproc_bin);

                        GST_DEBUG_OBJECT(
                            pipeline, "linking..capsfilter->pcm_stream_detect");
                        if (!gst_element_link(capsfilter, pcm_stream_detect)) {
                            g_printerr(
                                "Could not link capsfilter to "
                                "pcm_stream_detect\n");
                            return false;
                        }
                    }
                }

                if (dap && pcm_postproc_path == PCM_DOLBY_DAP_POSTPROC)
                    GST_CAT_WARNING(GST_CAT_DEFAULT,
                        "Dolby DAP not found! DAP post processing is disabled");
                if (soundx && pcm_postproc_path == PCM_DTS_VIRTUALX_POSTPROC)
                    GST_CAT_WARNING(GST_CAT_DEFAULT,
                        "Dts soundx not found! Virtualx post processing is "
                        "disabled");
                if (mcdyn && pcm_postproc_path == PCM_DTS_VIRTUALX_POSTPROC)
                    GST_CAT_WARNING(GST_CAT_DEFAULT,
                        "Dts mcdyn not found! Virtualx post processing is "
                        "disabled");

                gst_element_set_state(GST_ELEMENT(pipeline), GST_STATE_PLAYING);
                cur_stream_type = GST_STREAM_TYPE_PCM;
                device_status = GST_DEVICE_ACTIVE;
                break;
        }
    } else if (st == GST_STREAM_TYPE_DSD) {
        blocksize = 4800;
        switch (cur_stream_type) {
            case GST_STREAM_TYPE_PCM:
                GST_CAT_DEBUG(GST_CAT_DEFAULT, "PCM --> DSD");
                if (chosen_dsd_path != dsd_path)
                    dsd_path = chosen_dsd_path;

                GST_DEBUG_OBJECT(
                    pipeline, "removing %" GST_PTR_FORMAT, pulsesrc);
                gst_bin_remove(GST_BIN(pipeline), pulsesrc);

                GST_DEBUG_OBJECT(
                    pipeline, "removing %" GST_PTR_FORMAT, capsfilter);
                gst_bin_remove(GST_BIN(pipeline), capsfilter);

                GST_DEBUG_OBJECT(
                    pipeline, "adding %" GST_PTR_FORMAT, pulsedirectsrc);
                gst_bin_add(GST_BIN(pipeline), pulsedirectsrc);

                GST_DEBUG_OBJECT(
                    pipeline, "adding %" GST_PTR_FORMAT, capsfilter);
                gst_bin_add(GST_BIN(pipeline), capsfilter);

                if (dsd_path == DSD_PATH_NON_NATIVE) {
                    caps = set_dsd_non_native_caps(caps);
                }
                /* Set device and caps  */
                g_object_set(G_OBJECT(pulsedirectsrc), "device",
                    cur_device_name, "blocksize", blocksize, NULL);
                g_object_set(G_OBJECT(capsfilter), "caps", caps, NULL);

                GST_DEBUG_OBJECT(
                    pipeline, "linking..pulsedirectsrc->capsfilter");
                if (!gst_element_link_pads(
                        pulsedirectsrc, "src", capsfilter, "sink")) {
                    g_printerr("Could not link pulsedirectsrc->capsfilter\n");
                    return false;
                }

                if (dsd_path == DSD_PATH_NATIVE) {
                    GST_DEBUG_OBJECT(
                        pipeline, "removing %" GST_PTR_FORMAT, pulsesink);
                    gst_bin_remove(GST_BIN(pipeline), pulsesink);

                    GST_DEBUG_OBJECT(
                        pipeline, "adding %" GST_PTR_FORMAT, pulsedirectsink);
                    gst_bin_add(GST_BIN(pipeline), pulsedirectsink);
                }

                GST_DEBUG_OBJECT(
                    pipeline, "removing %" GST_PTR_FORMAT, pcm_stream_detect);
                gst_bin_remove(GST_BIN(pipeline), pcm_stream_detect);

                if (pcm_postproc_path == PCM_DOLBY_DAP_POSTPROC) {
                    GST_DEBUG_OBJECT(pipeline, "removing %" GST_PTR_FORMAT,
                        dolby_postproc_bin);
                    gst_bin_remove(GST_BIN(pipeline), dolby_postproc_bin);
                } else if (pcm_postproc_path == PCM_DTS_VIRTUALX_POSTPROC) {
                    GST_DEBUG_OBJECT(pipeline, "removing %" GST_PTR_FORMAT,
                        virtualx_postproc_bin);
                    gst_bin_remove(GST_BIN(pipeline), virtualx_postproc_bin);
                }
                dbin = gst_bin_get_by_name(GST_BIN(pipeline), "Decoder-Bin");
                if (dbin != NULL) {
                    GST_DEBUG_OBJECT(
                        pipeline, "removing %" GST_PTR_FORMAT, decoder_bin);
                    gst_bin_remove(GST_BIN(pipeline), decoder_bin);
                    g_object_unref(dbin);
                }

                if (dsd_path == DSD_PATH_NON_NATIVE) {
                    GST_DEBUG_OBJECT(
                        pipeline, "adding %" GST_PTR_FORMAT, avdec_dsd);
                    gst_bin_add(GST_BIN(pipeline), avdec_dsd);

                    GST_DEBUG_OBJECT(
                        pipeline, "linking..capsfilter->avdec_dsd");
                    if (!gst_element_link_pads(
                            capsfilter, "src", avdec_dsd, "sink")) {
                        g_printerr("Could not link capsfilter->avdec_dsd\n");
                        return false;
                    }

                    GST_DEBUG_OBJECT(pipeline, "linking..avdec_dsd->pulsesink");
                    if (!gst_element_link_pads(
                            avdec_dsd, "src", pulsesink, "sink")) {
                        g_printerr("Could not link avdec_dsd->pulsesink\n");
                        return false;
                    }
                } else if (dsd_path == DSD_PATH_NATIVE) {
                    GST_DEBUG_OBJECT(
                        pipeline, "linking..capsfilter->pulsedirectsink");
                    if (!gst_element_link_pads(
                            capsfilter, "src", pulsedirectsink, "sink")) {
                        g_printerr(
                            "Could not link capsfilter->pulsedirectsink\n");
                        return false;
                    }
                }

                gst_element_set_state(GST_ELEMENT(pipeline), GST_STATE_PLAYING);
                cur_stream_type = GST_STREAM_TYPE_DSD;
                device_status = GST_DEVICE_ACTIVE;
                break;
            case GST_STREAM_TYPE_ENCODED:
                GST_CAT_DEBUG(GST_CAT_DEFAULT, "ENCODED --> DSD");

                if (chosen_dsd_path != dsd_path)
                    dsd_path = chosen_dsd_path;
                GST_DEBUG_OBJECT(
                    pipeline, "removing %" GST_PTR_FORMAT, pulsesrc);
                gst_bin_remove(GST_BIN(pipeline), pulsesrc);

                GST_DEBUG_OBJECT(
                    pipeline, "removing %" GST_PTR_FORMAT, capsfilter);
                gst_bin_remove(GST_BIN(pipeline), capsfilter);

                GST_DEBUG_OBJECT(
                    pipeline, "adding %" GST_PTR_FORMAT, pulsedirectsrc);
                gst_bin_add(GST_BIN(pipeline), pulsedirectsrc);

                GST_DEBUG_OBJECT(
                    pipeline, "adding %" GST_PTR_FORMAT, capsfilter);
                gst_bin_add(GST_BIN(pipeline), capsfilter);

                if (dsd_path == DSD_PATH_NON_NATIVE) {
                    caps = set_dsd_non_native_caps(caps);
                }

                /* Set device and caps  */
                g_object_set(G_OBJECT(pulsedirectsrc), "device",
                    cur_device_name, "blocksize", blocksize, NULL);
                g_object_set(G_OBJECT(capsfilter), "caps", caps, NULL);

                GST_DEBUG_OBJECT(
                    pipeline, "linking..pulsedirectsrc->capsfilter");
                if (!gst_element_link_pads(
                        pulsedirectsrc, "src", capsfilter, "sink")) {
                    g_printerr("Could not link pulsedirectsrc->capsfilter\n");
                    return false;
                }

                if (dsd_path == DSD_PATH_NATIVE) {
                    GST_DEBUG_OBJECT(
                        pipeline, "removing %" GST_PTR_FORMAT, pulsesink);
                    gst_bin_remove(GST_BIN(pipeline), pulsesink);

                    GST_DEBUG_OBJECT(
                        pipeline, "adding %" GST_PTR_FORMAT, pulsedirectsink);
                    gst_bin_add(GST_BIN(pipeline), pulsedirectsink);
                }

                /* remove unlinks automatically */
                GST_DEBUG_OBJECT(
                    pipeline, "removing %" GST_PTR_FORMAT, stream_detect);
                gst_bin_remove(GST_BIN(pipeline), stream_detect);

                dbin = gst_bin_get_by_name(GST_BIN(pipeline), "Decoder-Bin");
                if (dbin != NULL) {
                    GST_DEBUG_OBJECT(
                        pipeline, "removing %" GST_PTR_FORMAT, decoder_bin);
                    gst_bin_remove(GST_BIN(pipeline), decoder_bin);
                    g_object_unref(dbin);
                }

                if (!IsNonDolbyCodec(cur_codec_type)
                    && dolby_graph != GST_GRAPH_TYPE_DOLBY_DEC_ONLY
                    && pcm_postproc_path == PCM_DOLBY_DAP_POSTPROC) {
                    GST_DEBUG_OBJECT(decoder_bin, "removing %" GST_PTR_FORMAT,
                        dolby_postproc_bin);
                    gst_bin_remove(GST_BIN(decoder_bin), dolby_postproc_bin);
                }

                if (cur_codec_type == GST_CODEC_TYPE_DTSX
                    && dts_graph != GST_GRAPH_TYPE_DTSX_ONLY
                    && pcm_postproc_path == PCM_DTS_VIRTUALX_POSTPROC) {
                    GST_DEBUG_OBJECT(decoder_bin, "removing %" GST_PTR_FORMAT,
                        virtualx_postproc_bin);
                    gst_bin_remove(GST_BIN(decoder_bin), virtualx_postproc_bin);
                }

                if (dsd_path == DSD_PATH_NON_NATIVE) {
                    GST_DEBUG_OBJECT(
                        pipeline, "adding %" GST_PTR_FORMAT, avdec_dsd);
                    gst_bin_add(GST_BIN(pipeline), avdec_dsd);

                    GST_DEBUG_OBJECT(
                        pipeline, "linking..capsfilter->avdec_dsd");
                    if (!gst_element_link_pads(
                            capsfilter, "src", avdec_dsd, "sink")) {
                        g_printerr("Could not link capsfilter->avdec_dsd\n");
                        return false;
                    }

                    GST_DEBUG_OBJECT(pipeline, "linking..avdec_dsd->pulsesink");
                    if (!gst_element_link_pads(
                            avdec_dsd, "src", pulsesink, "sink")) {
                        g_printerr("Could not link avdec_dsd->pulsesink\n");
                        return false;
                    }
                } else if (dsd_path == DSD_PATH_NATIVE) {
                    GST_DEBUG_OBJECT(
                        pipeline, "linking..capsfilter->pulsedirectsink");
                    if (!gst_element_link_pads(
                            capsfilter, "src", pulsedirectsink, "sink")) {
                        g_printerr(
                            "Could not link capsfilter->pulsedirectsink\n");
                        return false;
                    }
                }

                gst_element_set_state(GST_ELEMENT(pipeline), GST_STATE_PLAYING);
                cur_stream_type = GST_STREAM_TYPE_DSD;
                device_status = GST_DEVICE_ACTIVE;
                break;
            case GST_STREAM_TYPE_DSD:
                GST_CAT_DEBUG(GST_CAT_DEFAULT, "DSD --> DSD");
                /* Set device and caps  */
                g_object_set(
                    G_OBJECT(pulsedirectsrc), "device", cur_device_name, NULL);
                g_object_set(G_OBJECT(capsfilter), "caps", caps, NULL);
                gst_element_set_state(GST_ELEMENT(pipeline), GST_STATE_PLAYING);
                device_status = GST_DEVICE_ACTIVE;
                break;
        }
    } else {
        /* Not PCM */
        switch (cur_stream_type) {
            case GST_STREAM_TYPE_PCM:
                /* If stream is changed from pcm to encoded
                       1. unlink the pcm pipeline
                       2. add dap_postproc_bin and virtualx_postproc_bin to
                   dolby/dts graph
                       3. switch to dolby/dts graph */

                GST_CAT_DEBUG(GST_CAT_DEFAULT, "PCM --> ENCODED");
                if (!stream_detect) {
                    /* stream detect plugin is not available
                     * Don't set pipeline to playing state */
                    return false;
                }
                GST_DEBUG_OBJECT(
                    pipeline, "removing %" GST_PTR_FORMAT, pcm_stream_detect);
                gst_element_set_state(pcm_stream_detect, GST_STATE_NULL);
                gst_bin_remove(GST_BIN(pipeline), pcm_stream_detect);

                /* Set device and caps  */
                g_object_set(
                    G_OBJECT(pulsesrc), "device", cur_device_name, NULL);
                if (strcmp(sink_name, "offload-low-latency") == 0
                    && cur_codec_type == GST_CODEC_TYPE_MAT) {
                    g_object_set(G_OBJECT(pulsesrc), "stream-flags", 0x0000220f,
                        "buffer-time", (gint64)150000, "latency-time",
                        (gint64)10000, NULL);
                }
                g_object_set(G_OBJECT(capsfilter), "caps", caps, NULL);
                if (pcm_postproc_path == PCM_DOLBY_DAP_POSTPROC) {
                    GST_DEBUG_OBJECT(pipeline, "removing %" GST_PTR_FORMAT,
                        dolby_postproc_bin);
                    gst_bin_remove(GST_BIN(pipeline), dolby_postproc_bin);
                } else if (pcm_postproc_path == PCM_DTS_VIRTUALX_POSTPROC) {
                    GST_DEBUG_OBJECT(pipeline, "removing %" GST_PTR_FORMAT,
                        virtualx_postproc_bin);
                    gst_bin_remove(GST_BIN(pipeline), virtualx_postproc_bin);
                }
                stream_detect = gst_element_factory_make(
                    "dolbystreamdetect", "iec61937parser");
                pad = gst_element_get_static_pad(stream_detect, "src");
                gst_pad_add_probe(pad,
                    (GstPadProbeType)(GST_PAD_PROBE_TYPE_BLOCK
                        | GST_PAD_PROBE_TYPE_EVENT_DOWNSTREAM),
                    sd_pad_probe_event_cb_caps, this, NULL);
                gst_object_unref(pad);

                GST_DEBUG_OBJECT(
                    pipeline, "adding   %" GST_PTR_FORMAT, stream_detect);
                gst_bin_add(GST_BIN(pipeline), stream_detect);
                GST_DEBUG_OBJECT(
                    pipeline, "linking..capsfilter->iec61937parser");
                if (!gst_element_link_many(capsfilter, stream_detect, NULL)) {
                    g_printerr("Could not link capsfilter -> iec61937parser\n");
                    return false;
                }

                if (pcm_postproc_path == PCM_DOLBY_DAP_POSTPROC
                    && !IsNonDolbyCodec(cur_codec_type)
                    && dolby_graph != GST_GRAPH_TYPE_DOLBY_DEC_ONLY) {
                    GST_DEBUG_OBJECT(decoder_bin, "adding %" GST_PTR_FORMAT,
                        dolby_postproc_bin);
                    gst_bin_add(GST_BIN(bin), dolby_postproc_bin);

                    switch (dolby_graph) {
                        case GST_GRAPH_TYPE_DOLBY_DEC_OAR_DAP:
                            GST_DEBUG_OBJECT(
                                pipeline, "linking..oar->dolby_postproc_bin");
                            if (!gst_element_link_pads(
                                    oar, "src", dolby_postproc_bin, "sink")) {
                                g_printerr(
                                    "Could not link oar -> "
                                    "dolby_postproc_bin\n");
                                return false;
                            }

                            GST_DEBUG_OBJECT(pipeline,
                                "linking..dolby_postproc_bin->identity_sink");
                            if (!gst_element_link_pads(dolby_postproc_bin,
                                    "src", identity_sink, "sink")) {
                                g_printerr(
                                    "Could not link dolby_postproc_bin -> "
                                    "identity_sink\n");
                                return false;
                            }
                            break;
                        case GST_GRAPH_TYPE_DOLBY_DEC_DAP:
                            if (cur_codec_type == GST_CODEC_TYPE_MAT) {
                                GST_DEBUG_OBJECT(pipeline,
                                    "linking..thdec->dolby_postproc_bin");
                                if (!gst_element_link_pads(thdec, "src",
                                        dolby_postproc_bin, "sink")) {
                                    g_printerr(
                                        "Could not link thdec -> "
                                        "dolby_postproc_bin\n");
                                    return false;
                                }
                            } else {
                                GST_DEBUG_OBJECT(pipeline,
                                    "linking..ddpdec->dolby_postproc_bin");
                                if (!gst_element_link_pads(ddpdec, "src",
                                        dolby_postproc_bin, "sink")) {
                                    g_printerr(
                                        "Could not link ddpdec -> "
                                        "dolby_postproc_bin\n");
                                    return false;
                                }
                            }

                            GST_DEBUG_OBJECT(pipeline,
                                "linking..dolby_postproc_bin->identity_sink");
                            if (!gst_element_link_pads(dolby_postproc_bin,
                                    "src", identity_sink, "sink")) {
                                g_printerr(
                                    "Could not link dolby_postproc_bin -> "
                                    "identity_sink\n");
                                return false;
                            }
                            break;
                        default:
                            break;
                    }
                }
                if (pcm_postproc_path == PCM_DTS_VIRTUALX_POSTPROC
                    && cur_codec_type == GST_CODEC_TYPE_DTSX
                    && dts_graph != GST_GRAPH_TYPE_DTSX_ONLY) {
                    GST_DEBUG_OBJECT(decoder_bin, "adding %" GST_PTR_FORMAT,
                        virtualx_postproc_bin);
                    gst_bin_add(GST_BIN(bin), virtualx_postproc_bin);

                    GST_DEBUG_OBJECT(
                        bin, "linking..dtsxdec->virtualx_postproc_bin");
                    if (!gst_element_link_pads(
                            dtsxdec, "src", virtualx_postproc_bin, "sink")) {
                        g_printerr(
                            "Could not link dtsxdec->virtualx_postproc_bin\n");
                        return false;
                    }

                    GST_DEBUG_OBJECT(
                        bin, "linking..virtualx_postproc_bin->identity_sink");
                    if (!gst_element_link_pads(virtualx_postproc_bin, "src",
                            identity_sink, "sink")) {
                        g_printerr(
                            "Could not link "
                            "virtualx_postproc_bin->identity_sink\n");
                        return false;
                    }
                }
                gst_element_set_state(GST_ELEMENT(pipeline), GST_STATE_PLAYING);
                cur_stream_type = GST_STREAM_TYPE_ENCODED;
                device_status = GST_DEVICE_ACTIVE;
                break;
            case GST_STREAM_TYPE_ENCODED:
                GST_CAT_DEBUG(GST_CAT_DEFAULT, "ENCODED --> ENCODED");
                if (!stream_detect) {
                    /* stream detect plugin is not available
                     * Don't set pipeline to playing state*/
                    return FALSE;
                }
                /* Set device and caps  */
                g_object_set(
                    G_OBJECT(pulsesrc), "device", cur_device_name, NULL);
                g_object_set(G_OBJECT(capsfilter), "caps", caps, NULL);
                gst_element_set_state(GST_ELEMENT(pipeline), GST_STATE_PLAYING);
                device_status = GST_DEVICE_ACTIVE;
                break;
            case GST_STREAM_TYPE_DSD:
                GST_CAT_DEBUG(GST_CAT_DEFAULT, "DSD --> ENCODED");

                /* Remove pulsedirectsrc */
                GST_DEBUG_OBJECT(
                    pipeline, "removing %" GST_PTR_FORMAT, pulsedirectsrc);
                gst_bin_remove(GST_BIN(pipeline), pulsedirectsrc);

                /* Remove and add capsfilter */
                GST_DEBUG_OBJECT(
                    pipeline, "removing %" GST_PTR_FORMAT, capsfilter);
                gst_bin_remove(GST_BIN(pipeline), capsfilter);

                GST_DEBUG_OBJECT(
                    pipeline, "adding %" GST_PTR_FORMAT, capsfilter);
                gst_bin_add(GST_BIN(pipeline), capsfilter);

                /* Add pulsesrc */
                GST_DEBUG_OBJECT(pipeline, "adding %" GST_PTR_FORMAT, pulsesrc);
                gst_bin_add(GST_BIN(pipeline), pulsesrc);

                /* Set device and caps  */
                g_object_set(
                    G_OBJECT(pulsesrc), "device", cur_device_name, NULL);
                g_object_set(G_OBJECT(capsfilter), "caps", caps, NULL);

                GST_DEBUG_OBJECT(pipeline, "linking..pulsesrc->capsfilter");
                if (!gst_element_link_pads(
                        pulsesrc, "src", capsfilter, "sink")) {
                    g_printerr("Could not link pulsesrc->capsfilter\n");
                    return false;
                }

                if (dsd_path == DSD_PATH_NON_NATIVE) {
                    GST_DEBUG_OBJECT(
                        pipeline, "removing %" GST_PTR_FORMAT, avdec_dsd);
                    gst_bin_remove(GST_BIN(pipeline), avdec_dsd);
                } else if (dsd_path == DSD_PATH_NATIVE) {
                    /* Unlink pulsedirectsink */
                    GST_DEBUG_OBJECT(
                        pipeline, "removing %" GST_PTR_FORMAT, pulsedirectsink);
                    gst_bin_remove(GST_BIN(pipeline), pulsedirectsink);

                    /* Add pulsesink */
                    GST_DEBUG_OBJECT(
                        pipeline, "adding %" GST_PTR_FORMAT, pulsesink);
                    gst_bin_add(GST_BIN(pipeline), pulsesink);
                }

                GST_DEBUG_OBJECT(
                    pipeline, "adding   %" GST_PTR_FORMAT, stream_detect);
                gst_bin_add(GST_BIN(pipeline), stream_detect);

                GST_DEBUG_OBJECT(
                    pipeline, "linking..capsfilter->iec61937parser");
                if (!gst_element_link_many(capsfilter, stream_detect, NULL)) {
                    g_printerr("Could not link capsfilter -> iec61937parser\n");
                    return false;
                }

                if (pcm_postproc_path == PCM_DOLBY_DAP_POSTPROC
                    && !IsNonDolbyCodec(cur_codec_type)
                    && dolby_graph != GST_GRAPH_TYPE_DOLBY_DEC_ONLY) {
                    GST_DEBUG_OBJECT(decoder_bin, "adding %" GST_PTR_FORMAT,
                        dolby_postproc_bin);
                    gst_bin_add(GST_BIN(bin), dolby_postproc_bin);

                    switch (dolby_graph) {
                        case GST_GRAPH_TYPE_DOLBY_DEC_OAR_DAP:
                            GST_DEBUG_OBJECT(
                                pipeline, "linking..oar->dolby_postproc_bin");
                            if (!gst_element_link_pads(
                                    oar, "src", dolby_postproc_bin, "sink")) {
                                g_printerr(
                                    "Could not link oar -> "
                                    "dolby_postproc_bin\n");
                                return false;
                            }

                            GST_DEBUG_OBJECT(pipeline,
                                "linking..dolby_postproc_bin->identity_sink");
                            if (!gst_element_link_pads(dolby_postproc_bin,
                                    "src", identity_sink, "sink")) {
                                g_printerr(
                                    "Could not link dolby_postproc_bin -> "
                                    "identity_sink\n");
                                return false;
                            }
                            break;
                        case GST_GRAPH_TYPE_DOLBY_DEC_DAP:
                            if (cur_codec_type == GST_CODEC_TYPE_MAT) {
                                GST_DEBUG_OBJECT(pipeline,
                                    "linking..thdec->dolby_postproc_bin");
                                if (!gst_element_link_pads(thdec, "src",
                                        dolby_postproc_bin, "sink")) {
                                    g_printerr(
                                        "Could not link thdec -> "
                                        "dolby_postproc_bin\n");
                                    return false;
                                }
                            } else {
                                GST_DEBUG_OBJECT(pipeline,
                                    "linking..ddpdec->dolby_postproc_bin");
                                if (!gst_element_link_pads(ddpdec, "src",
                                        dolby_postproc_bin, "sink")) {
                                    g_printerr(
                                        "Could not link ddpdec -> "
                                        "dolby_postproc_bin\n");
                                    return false;
                                }
                            }

                            GST_DEBUG_OBJECT(pipeline,
                                "linking..dolby_postproc_bin->identity_sink");
                            if (!gst_element_link_pads(dolby_postproc_bin,
                                    "src", identity_sink, "sink")) {
                                g_printerr(
                                    "Could not link dolby_postproc_bin -> "
                                    "identity_sink\n");
                                return false;
                            }
                            break;
                        default:
                            break;
                    }
                }
                if (pcm_postproc_path == PCM_DTS_VIRTUALX_POSTPROC
                    && cur_codec_type == GST_CODEC_TYPE_DTSX
                    && dts_graph != GST_GRAPH_TYPE_DTSX_ONLY) {
                    GST_DEBUG_OBJECT(decoder_bin, "adding %" GST_PTR_FORMAT,
                        virtualx_postproc_bin);
                    gst_bin_add(GST_BIN(bin), virtualx_postproc_bin);

                    GST_DEBUG_OBJECT(
                        bin, "linking..dtsxdec->virtualx_postproc_bin");
                    if (!gst_element_link_pads(
                            dtsxdec, "src", virtualx_postproc_bin, "sink")) {
                        g_printerr(
                            "Could not link dtsxdec->virtualx_postproc_bin\n");
                        return false;
                    }

                    GST_DEBUG_OBJECT(
                        bin, "linking..virtualx_postproc_bin->identity_sink");
                    if (!gst_element_link_pads(virtualx_postproc_bin, "src",
                            identity_sink, "sink")) {
                        g_printerr(
                            "Could not link "
                            "virtualx_postproc_bin->identity_sink\n");
                        return false;
                    }
                }

                gst_element_set_state(GST_ELEMENT(pipeline), GST_STATE_PLAYING);
                cur_stream_type = GST_STREAM_TYPE_ENCODED;
                device_status = GST_DEVICE_ACTIVE;
                break;
        }
    }
    if (caps != NULL)
        gst_caps_unref(caps);
    return true;
}

bool MediaPlayerPrivate::SetSource(std::string uri) {
    if (is_live_src_)
        return false;

    if (set_source_buffer_) {
        return false;
    }

    if (!gst_uri_is_valid(uri.c_str()))
        return false;

    auto ret = gst_element_set_state(GST_ELEMENT(pipeline), GST_STATE_READY);

    if (ret == GST_STATE_CHANGE_SUCCESS) {
        if (compressed_offload_flag)
            g_object_set(pipeline, "uri", uri.c_str(), NULL);
        else
            g_object_set(uridecodebin, "uri", uri.c_str(), NULL);
        ClearMetadata();
        return true;
    }

    return false;
}

bool MediaPlayerPrivate::SetSource(MediaPlayer::MediaInfo info,
    std::function<void(size_t size)> needDataCb,
    std::function<void(uint64_t offset)> seekDataCb,
    std::function<void(void)> enoughDataCb) {
    if (is_live_src_)
        return false;
    DecodeBinDelayParams params;
    params.sampling_rate = 0;
    params.channels = 0;

    if (!info.format.empty()) {
        caps_ = gst_caps_from_string(info.format.c_str());
        is_raw_format_ = true;
        if (!caps_)
            return false;
        std::vector<std::string> decoder_bin_elements;
        decoder_bin_elements.push_back("appsrc");
        decoder_bin_elements.push_back(info.format);

        const GValue *val = gst_structure_get_value(
            gst_caps_get_structure(caps_, 0), "channels");
        if (G_VALUE_HOLDS_INT(val)) {
            params.channels = g_value_get_int(val);
        }

        val = gst_structure_get_value(
            gst_caps_get_structure(caps_, 0), "rate");
        if (G_VALUE_HOLDS_INT(val)) {
            params.sampling_rate = g_value_get_int(val);
        }

        UpdateDelayOnDecoderBinChange(decoder_bin_elements, params);
    } else {
        std::vector<std::string> decoder_bin_elements;
        decoder_bin_elements.push_back("appsrc");
        decoder_bin_elements.push_back("UNKNOWN");
        UpdateDelayOnDecoderBinChange(decoder_bin_elements, params);
    }

    set_source_buffer_ = true;
    needData = needDataCb;
    enoughData = enoughDataCb;
    seekData = seekDataCb;

    auto ret = gst_element_set_state(GST_ELEMENT(pipeline), GST_STATE_READY);
    if (ret != GST_STATE_CHANGE_SUCCESS)
        return false;

    ClearMetadata();

    if (compressed_offload_flag) {
        /* set appsrc to playbin */
        g_object_set(pipeline, "uri", "appsrc://", NULL);
    } else {
        /* set appsrc to uridecodebin */
        g_object_set(uridecodebin, "uri", "appsrc://", NULL);
    }

    return true;
}

bool MediaPlayerPrivate::SetSource(std::function<void(size_t size)> needDataCb,
    std::function<void(uint64_t size)> seekDataCb,
    std::function<void(void)> enoughDataCb) {
    MediaPlayer::MediaInfo unknown_media_format;
    unknown_media_format.format.clear();

    is_raw_format_ = false;
    return SetSource(
        unknown_media_format, needDataCb, seekDataCb, enoughDataCb);
}
bool MediaPlayerPrivate::PushBuffer(void *data, size_t size) {
    auto gst_buffer = gst_buffer_new_and_alloc(size);
    gst_buffer_fill(gst_buffer, 0, data, size);

    auto ret = gst_app_src_push_buffer(GST_APP_SRC(appsrc), gst_buffer);

    return GstFlowReturnToBool(ret);
}
bool MediaPlayerPrivate::PushBuffer(void *data, size_t size,
    std::chrono::nanoseconds duration, std::chrono::nanoseconds timestamp) {
    auto gst_buffer = gst_buffer_new_and_alloc(size);
    gst_buffer_fill(gst_buffer, 0, data, size);

    auto dur = duration.count();
    auto ts = timestamp.count();

    GST_BUFFER_PTS(gst_buffer) = ts;
    GST_BUFFER_DURATION(gst_buffer) = dur;

    auto ret = gst_app_src_push_buffer(GST_APP_SRC(appsrc), gst_buffer);

    return GstFlowReturnToBool(ret);
}

bool MediaPlayerPrivate::PushEndOfStream() {
    auto ret = gst_app_src_end_of_stream(GST_APP_SRC(appsrc));

    return GstFlowReturnToBool(ret);
}

size_t MediaPlayerPrivate::GetBufferLevel() {
    if (!appsrc)
        return 0;
    else
        return gst_app_src_get_current_level_bytes(appsrc);
}

bool MediaPlayerPrivate::Next() {
    if (onNext) {
        onNext();
        return true;
    }

    return false;
}

bool MediaPlayerPrivate::Previous() {
    if (onPrevious) {
        onPrevious();
        return true;
    }

    return false;
}

MediaPlayer::LoopMode MediaPlayerPrivate::GetLoopMode() {
    return loop_mode_;
}

void MediaPlayerPrivate::SetLoopMode(MediaPlayer::LoopMode mode) {
    if (onLoopModeChange && (loop_mode_ != mode)) {
        loop_mode_ = mode;

        onLoopModeChange(mode);

        EmitPropertyChangeSignal("LoopStatus");
    }
}

bool MediaPlayerPrivate::GetShuffleStatus() {
    return shuffle_status_;
}

void MediaPlayerPrivate::SetShuffleStatus(bool status) {
    if (onShuffleStatusChange && (shuffle_status_ != status)) {
        shuffle_status_ = status;

        onShuffleStatusChange(status);

        EmitPropertyChangeSignal("Shuffle");
    }
}

MediaPlayer::Result MediaPlayerPrivate::Play() {
    if (stream_type == MediaPlayer::StreamType::ControlOnly) {
        PlaybackStatusChanged(MediaPlayer::State::Playing);
        return MediaPlayer::Result::Success;
    }

    auto ret = gst_element_set_state(GST_ELEMENT(pipeline), GST_STATE_PLAYING);
    auto res = GstToMediaPlayerResult(ret);

    return res;
}

MediaPlayer::Result MediaPlayerPrivate::Pause() {
    if (stream_type == MediaPlayer::StreamType::ControlOnly) {
        PlaybackStatusChanged(MediaPlayer::State::Paused);
        return MediaPlayer::Result::Success;
    }

    auto ret = gst_element_set_state(GST_ELEMENT(pipeline), GST_STATE_PAUSED);
    auto res = GstToMediaPlayerResult(ret);

    return res;
}

MediaPlayer::Result MediaPlayerPrivate::Stop() {
    if (stream_type == MediaPlayer::StreamType::ControlOnly) {
        PlaybackStatusChanged(MediaPlayer::State::Stopped);
        return MediaPlayer::Result::Success;
    }

    auto ret = gst_element_set_state(GST_ELEMENT(pipeline), GST_STATE_NULL);

    StateChanged(MediaPlayer::State::Stopped);
    ClearMetadata();

    auto res = GstToMediaPlayerResult(ret);

    return res;
}

void MediaPlayerPrivate::SetRateRequest(double rate) {
    if (onRateChangeRequest && (std::abs(rate_ - rate) > 0.001)) {
        onRateChangeRequest(rate);
    }
}

void MediaPlayerPrivate::SetSupportedRateRange(double min, double max) {
    min_rate_ = ((min == 0) || (min >= 1.0)) ? 1.0 : min;
    max_rate_ = (std::abs(max - 1.0) > 0.001) ? max : 1.0;

    EmitPropertyChangeSignal("MinimumRate");
    EmitPropertyChangeSignal("MaximumRate");
}

double MediaPlayerPrivate::GetPlaybackRate() {
    return rate_;
}

double MediaPlayerPrivate::GetMinPlaybackRate() {
    return min_rate_;
}

double MediaPlayerPrivate::GetMaxPlaybackRate() {
    return max_rate_;
}

bool MediaPlayerPrivate::SetRate(double rate) {
    if (stream_type == MediaPlayer::StreamType::ControlOnly
        || set_source_buffer_) {
        rate_ = rate;
    } else {
        // TODO: Set the rate on pipeline based on the rate provide if the type
        // is URI
        rate_ = rate;
    }

    EmitPropertyChangeSignal("Rate");
    return true;
}

bool MediaPlayerPrivate::SetRate(
    double rate G_GNUC_UNUSED, std::chrono::seconds position G_GNUC_UNUSED) {
    // TODO: Check if the type is application source buffer, valid position
    // value and set the rate on the pipeline
    return true;
}

bool MediaPlayerPrivate::SetRate(
    double rate G_GNUC_UNUSED, uint64_t offset G_GNUC_UNUSED) {
    // TODO: Check if the type is application source buffer, valid offset value
    // and set the rate on the pipeline
    return true;
}

std::chrono::nanoseconds MediaPlayerPrivate::GetDuration() {
    gint64 duration;

    if (gst_element_query_duration(
            GST_ELEMENT(pipeline), GST_FORMAT_TIME, &duration)) {
        return std::chrono::nanoseconds(duration);
    } else {
        return std::chrono::nanoseconds(0);
    }
}

std::chrono::nanoseconds MediaPlayerPrivate::GetPosition() {
    gint64 position;

    if ((std::get<0>(GetCurrentState()) == MediaPlayer::State::Paused)
        || (std::get<0>(GetCurrentState()) == MediaPlayer::State::Playing)) {
        if (gst_element_query_position(
                GST_ELEMENT(pipeline), GST_FORMAT_TIME, &position)) {
            return std::chrono::nanoseconds(position);
        }
    }

    return std::chrono::nanoseconds(0);
}

bool MediaPlayerPrivate::IsSeekable() {
    GstQuery *query;
    gint64 start, end;
    gboolean seekable = false;

    if (is_raw_format_)
        query = gst_query_new_seeking(GST_FORMAT_TIME);
    else
        query = gst_query_new_seeking(GST_FORMAT_BYTES);

    if (gst_element_query(GST_ELEMENT(pipeline), query)) {
        gst_query_parse_seeking(query, NULL, &seekable, &start, &end);
        gst_query_unref(query);
    }
    return seekable;
}

MediaPlayer::Result MediaPlayerPrivate::Seek(
    std::chrono::nanoseconds position) {
    auto start = position.count();
    GstState current_state;

    gst_element_get_state(
        GST_ELEMENT(pipeline), &current_state, NULL, GST_CLOCK_TIME_NONE);
    if (current_state == GST_STATE_READY || current_state == GST_STATE_NULL) {
        seek_offset = position;

        EmitSeekedSignal(position.count() / 1000);
        return MediaPlayer::Result::Success;
    }

    /* for unknown format seek the duration may be -1 check for is_seekable */
    if (position <= GetDuration() || IsSeekable()) {
        if (gst_element_seek(GST_ELEMENT(pipeline), 1.0, GST_FORMAT_TIME,
                GST_SEEK_FLAG_FLUSH, GST_SEEK_TYPE_SET, start,
                GST_SEEK_TYPE_NONE, GST_CLOCK_TIME_NONE)) {
            EmitSeekedSignal(position.count() / 1000);
            return MediaPlayer::Result::Success;
        }
    }

    return MediaPlayer::Result::Failure;
}

MediaPlayer::State MediaPlayerPrivate::GetState() {
    if (stream_type == MediaPlayer::StreamType::ControlOnly)
        return control_only_mediaplayer_state;

    GstState current_state;
    gst_element_get_state(
        GST_ELEMENT(pipeline), &current_state, NULL, GST_CLOCK_TIME_NONE);

    return StateToEnum(current_state);
}

std::tuple<MediaPlayer::State, MediaPlayer::Result>
MediaPlayerPrivate::GetCurrentState() {
    std::tuple<MediaPlayer::State, MediaPlayer::Result> res;

    if (stream_type == MediaPlayer::StreamType::ControlOnly) {
        res = std::make_tuple(
            control_only_mediaplayer_state, MediaPlayer::Result::Success);
        return res;
    }

    GstState current_state;
    auto ret =
        gst_element_get_state(GST_ELEMENT(pipeline), &current_state, NULL, 0);

    res = std::make_tuple(
        StateToEnum(current_state), GstToMediaPlayerResult(ret));

    return res;
}

std::map<MediaPlayer::Tag, std::string> MediaPlayerPrivate::GetMetadata() {
    if (stream_type == MediaPlayer::StreamType::ControlOnly)
        return metadata;

    metadata[MediaPlayer::Tag::Duration] =
        std::to_string(GetDuration().count());

    if (pulsesink) {
        GstPad *sink_pad = gst_element_get_static_pad(pulsesink, "sink");
        GstCaps *sink_caps = gst_pad_get_current_caps(sink_pad);

        const GValue *val = gst_structure_get_value(
            gst_caps_get_structure(sink_caps, 0), "format");
        if (G_VALUE_HOLDS_STRING(val)) {
            metadata[MediaPlayer::Tag::SampleFormat] = g_value_get_string(val);
        }

        val = gst_structure_get_value(
            gst_caps_get_structure(sink_caps, 0), "channels");
        if (G_VALUE_HOLDS_INT(val)) {
            metadata[MediaPlayer::Tag::Channels] =
                std::to_string(g_value_get_int(val));
        }

        val = gst_structure_get_value(
            gst_caps_get_structure(sink_caps, 0), "rate");
        if (G_VALUE_HOLDS_INT(val)) {
            metadata[MediaPlayer::Tag::SampleRate] =
                std::to_string(g_value_get_int(val));
        }

        gst_object_unref(GST_OBJECT(sink_pad));
        gst_caps_unref(sink_caps);
    }

    return metadata;
}

void MediaPlayerPrivate::SetMetadata(
    std::map<MediaPlayer::Tag, std::string> m) {
    ClearMetadata();

    metadata = m;

    MetadataChanged();

    return;
}

void MediaPlayerPrivate::SetOnErrorCb(
    std::function<void(MediaPlayer::ErrorType error_type, std::string error_msg,
        std::string debug_msg)>
        onErrorCb) {
    onError = onErrorCb;
}

void MediaPlayerPrivate::SetOnAsyncDoneCb(
    std::function<void(void)> onAsyncDoneCb) {
    onAsyncDone = onAsyncDoneCb;
}

void MediaPlayerPrivate::SetOnEndOfStreamCb(
    std::function<void(void)> onEndOfStreamCb) {
    onEndOfStream = onEndOfStreamCb;
}

void MediaPlayerPrivate::SetOnStateChangedCb(
    std::function<void(MediaPlayer::State new_state)> onStateChangedCb) {
    onStateChanged = onStateChangedCb;
}

void MediaPlayerPrivate::SetOnMetadataChangedCb(
    std::function<void(void)> onMetadataChangedCb) {
    onMetadataChanged = onMetadataChangedCb;
}

void MediaPlayerPrivate::SetNextCb(std::function<void(void)> onNextCb) {
    onNext = onNextCb;
}

void MediaPlayerPrivate::SetPreviousCb(std::function<void(void)> onPreviousCb) {
    onPrevious = onPreviousCb;
}

void MediaPlayerPrivate::SetLoopModeChangeCb(
    std::function<void(MediaPlayer::LoopMode status)> onLoopModeChangeCb) {
    onLoopModeChange = onLoopModeChangeCb;
}

void MediaPlayerPrivate::SetShuffleStatusChangeCb(
    std::function<void(bool)> onShuffleStatusChangeCb) {
    onShuffleStatusChange = onShuffleStatusChangeCb;
}

void MediaPlayerPrivate::SetBufferingCb(
    std::function<void(unsigned int buffering_percent)> onBufferingCb) {
    onBuffering = onBufferingCb;
}

void MediaPlayerPrivate::SetOnMuteUpdatedCb(
    std::function<void(bool mute_state)> onMuteUpdatedCb) {
    onMuteUpdated = onMuteUpdatedCb;
}

void MediaPlayerPrivate::SetOnVolumeUpdatedCb(
    std::function<void(double volume)> onVolumeUpdatedCb) {
    onVolumeUpdated = onVolumeUpdatedCb;
}

void MediaPlayerPrivate::SetPlayCb(std::function<void(void)> onPlayCb) {
    onPlay = onPlayCb;
}

void MediaPlayerPrivate::SetPauseCb(std::function<void(void)> onPauseCb) {
    onPause = onPauseCb;
}

void MediaPlayerPrivate::SetStopCb(std::function<void(void)> onStopCb) {
    onStop = onStopCb;
}

void MediaPlayerPrivate::SetOnRateChangeRequestCallback(
    std::function<void(double)> onRateChangeRequestCb) {
    onRateChangeRequest = onRateChangeRequestCb;
}

void MediaPlayerPrivate::SetHigherSampleRate(bool flag) {
    higher_sample_rate_flag = flag;
}

void MediaPlayerPrivate::SetOnTotalPipelineDelayChangeCb(std::function<void(uint64_t)> onTotalPipelineDelayChangeCb) {
    onTotalPipelineDelayChange = onTotalPipelineDelayChangeCb;
}

void MediaPlayerPrivate::DecodebinPadAddedCb(GstElement *element, GstPad *pad, gpointer user_data) {
    GstCaps *caps = NULL;
    auto priv = (MediaPlayerPrivate *)user_data;

    priv->SetHigherSampleRate(false);
    caps = gst_pad_get_current_caps(pad);

    if (caps) {
        const GstStructure *s = NULL;
        const GValue *val = NULL;
        int channels = 0;
        int rate = 0;

        s = gst_caps_get_structure(caps, 0);
        val = gst_structure_get_value(s, "channels");
        if (val) {
            channels = g_value_get_int(val);
            priv->channel_count_ = channels;
        }
        val = NULL;
        val = gst_structure_get_value(s, "rate");
        if (val) {
            rate = g_value_get_int(val);
            if (rate > MAXRATE)
                priv->SetHigherSampleRate(true);
        }
        gst_caps_unref(caps);
    }
}

void MediaPlayerPrivate::ElementSetupCb(GstElement *pipeline G_GNUC_UNUSED, GstElement *element, gpointer user_data) {
    auto priv = (MediaPlayerPrivate *)user_data;
    gchar *name = NULL;
    GstElementFactory *factory = gst_element_get_factory(element);

    if (factory)
        name = gst_plugin_feature_get_name(GST_PLUGIN_FEATURE(factory));

    if (name) {
        if (g_strcmp0(name, "decodebin") == 0) {
            g_signal_connect(element, "pad-added", G_CALLBACK(DecodebinPadAddedCb), priv);
        } else if (g_strcmp0(name, "audioresample") == 0) {
            if (priv->higher_sample_rate_flag)
                g_object_set(element, "resample-method", 4, "quality", 0, NULL);
        }
        if (g_strcmp0(name, "pulsesink") == 0) {
            if (priv->channel_count_ == 1) {
                // AVS Speech Reponses are MONO, Remap channels for better user experience
                if (priv->stream_type != MediaPlayer::StreamType::VoiceUI) {
                    // Set pulseaudio stream with PA_STREAM_NO_REMAP_CHANNELS
                    // Don't remap channels by their name, instead map them simply by their index
                    g_object_set(element, "stream-flags", 0x00000010, NULL);
                }
            }
        }
    }
}

void MediaPlayerPrivate::CreateVirtualxPostprocBin() {
    GstPad *pad;

    virtualx_postproc_bin = gst_bin_new("Virtualx-PostProc-Bin");
    g_assert(virtualx_postproc_bin);
    g_object_ref_sink(virtualx_postproc_bin);

    soundx = gst_element_factory_make("soundx", "soundx");
    if (soundx) {
        g_object_set(G_OBJECT(soundx), "soundx-out-mode", 4, NULL);
        InitPPElementWithSystemValue("soundx");
    } else {
        GST_CAT_WARNING(GST_CAT_DEFAULT,
            "DTS Soundx not found! Soundx post processing is disabled");
        soundx = gst_element_factory_make("identity", "soundx_passthrough");
    }

    mcdyn = gst_element_factory_make("mcdyn", "mcdynamics");
    if (mcdyn) {
        g_object_set(G_OBJECT(mcdyn), "mcd-mbhl-mvol-step", 100, NULL);
        InitPPElementWithSystemValue("mcdynamics");
    } else {
        GST_CAT_WARNING(GST_CAT_DEFAULT,
            "DTS mcdynamics not found! mcdynamics post processing is disabled");
        mcdyn = gst_element_factory_make("identity", "mcdynamics_passthrough");
    }

    GST_DEBUG_OBJECT(virtualx_postproc_bin, "adding %" GST_PTR_FORMAT, soundx);
    gst_bin_add(GST_BIN(virtualx_postproc_bin), soundx);

    GST_DEBUG_OBJECT(virtualx_postproc_bin, "adding %" GST_PTR_FORMAT, mcdyn);
    gst_bin_add(GST_BIN(virtualx_postproc_bin), mcdyn);

    /* ghost src and sink pads for dolby_postproc_bin */
    pad = gst_element_get_static_pad(soundx, "sink");
    gst_element_add_pad(virtualx_postproc_bin, gst_ghost_pad_new("sink", pad));
    gst_object_unref(pad);
    pad = gst_element_get_static_pad(mcdyn, "src");
    gst_element_add_pad(virtualx_postproc_bin, gst_ghost_pad_new("src", pad));
    gst_object_unref(pad);

    GST_DEBUG_OBJECT(virtualx_postproc_bin, "linking..soundx->mcdyn");
    if (!gst_element_link_pads(soundx, "src", mcdyn, "sink")) {
        g_printerr("Could not link soundx to mcdyn \n");
    }
}

void MediaPlayerPrivate::CreateDolbyPostprocBin() {
    GstPad *pad;
    dolby_postproc_bin = gst_bin_new("Dolby-PostProc-Bin");
    g_assert(dolby_postproc_bin);
    g_object_ref_sink(dolby_postproc_bin);
    dap = gst_element_factory_make("dap", "dap");
    if (!dap) {
        GST_CAT_WARNING(GST_CAT_DEFAULT,
            "Dolby DAP not found! DAP post processing is disabled");
        dap = gst_element_factory_make("identity", "dap_passthrough");
        g_object_set(G_OBJECT(dap), "signal-handoffs", FALSE, NULL);
    } else {
        InitPPElementWithSystemValue("dap");
    }
    datdownmix = gst_element_factory_make("datdownmix", "datdownmix");
    if (datdownmix == NULL) {
        datdownmix =
            gst_element_factory_make("identity", "datdownmix_passthrough");
        g_object_set(G_OBJECT(datdownmix), "signal-handoffs", FALSE, NULL);
    }
    audioresampler =
        gst_element_factory_make("audioresampler", "audioresampler");

    if (audioresampler)
        g_object_set(G_OBJECT(audioresampler), "mode", 1, NULL);
    else {
        audioresampler = gst_element_factory_make(
            "identity", "audioresampler_passthrough");
        g_object_set(
            G_OBJECT(audioresampler), "signal-handoffs", FALSE, NULL);
    }
    GST_DEBUG_OBJECT(dolby_postproc_bin, "adding %" GST_PTR_FORMAT, dap);
    gst_bin_add(GST_BIN(dolby_postproc_bin), dap);
    GST_DEBUG_OBJECT(dolby_postproc_bin, "adding %" GST_PTR_FORMAT, datdownmix);
    gst_bin_add(GST_BIN(dolby_postproc_bin), datdownmix);
    GST_DEBUG_OBJECT(
        dolby_postproc_bin, "adding %" GST_PTR_FORMAT, audioresampler);
    gst_bin_add(GST_BIN(dolby_postproc_bin), audioresampler);

    /* ghost src and sink pads for dolby_postproc_bin */
    pad = gst_element_get_static_pad(audioresampler, "sink");
    gst_element_add_pad(dolby_postproc_bin, gst_ghost_pad_new("sink", pad));
    gst_object_unref(pad);
    pad = gst_element_get_static_pad(dap, "src");
    gst_element_add_pad(dolby_postproc_bin, gst_ghost_pad_new("src", pad));
    gst_object_unref(pad);
    GST_DEBUG_OBJECT(dolby_postproc_bin, "linking..audioresampler->datdownmix");
    if (!gst_element_link_pads(audioresampler, "src", datdownmix, "sink")) {
        g_printerr("Could not link audioresampler to datdownmix\n");
        return;
    }
    GST_DEBUG_OBJECT(dolby_postproc_bin, "linking..datdownmix->dap");
    if (!gst_element_link_pads(datdownmix, "src", dap, "sink")) {
        g_printerr("Could not link datdownmix to dap\n");
    }
}
void MediaPlayerPrivate::CreateDecoderBin() {
    GstPad *pad;

    /* create result bin */
    decoder_bin = gst_bin_new("Decoder-Bin");
    g_assert(decoder_bin);
    g_object_ref_sink(decoder_bin);

    /* create elements  */
    ddpdec = gst_element_factory_make("ddpdec", "ddp_decoder");
    if (ddpdec) {
        g_object_ref_sink(ddpdec);
        if (!disable_ddp_force_timeslice_decoding)
            g_object_set(
                G_OBJECT(ddpdec), "force-timeslice-decoding", true, NULL);
    } else {
        GST_CAT_WARNING(GST_CAT_DEFAULT,
            "Dolby Digital Plus decoder not found! All the ddp buffers will be "
            "dropped");
        ddpdec = gst_element_factory_make("valve", "ddp_decoder_valve");
        g_object_set(G_OBJECT(ddpdec), "drop", TRUE, NULL);
    }

    matdec = gst_element_factory_make("matdec", "mat_decoder");
    if (matdec) {
        g_object_ref_sink(matdec);
    } else {
        GST_CAT_WARNING(GST_CAT_DEFAULT,
            "Dolby MAT decoder not found! All the MAT buffers will be dropped");
        matdec = gst_element_factory_make("valve", "mat_decoder_valve");
        g_object_set(G_OBJECT(matdec), "drop", TRUE, NULL);
    }

    thdec = gst_element_factory_make("thdec", "truehd_decoder");
    if (!thdec) {
        thdec =
            gst_element_factory_make("identity", "truehd_decoder_passthrough");
        GST_CAT_WARNING(GST_CAT_DEFAULT,
            "Dolby TrueHD decoder not found! All the TrueHD buffers will be "
            "dropped");
    }

    dtsxdec = gst_element_factory_make("dtsxdec", "dtsx_decoder");
    if (dtsxdec) {
        g_object_set(G_OBJECT(dtsxdec), "spkrout", dts_spkr_mask, NULL);
        g_object_set(G_OBJECT(dtsxdec), "sett1ccmode", sett1ccmode, NULL);
        g_object_set(
            G_OBJECT(dtsxdec), "extpp_basslevel", extpp_basslevel, NULL);
        g_object_set(G_OBJECT(dtsxdec), "extpp_dynamics", extpp_dynamics, NULL);
        g_object_set(
            G_OBJECT(dtsxdec), "extpp_speakersize", extpp_speakersize, NULL);
        g_object_set(G_OBJECT(dtsxdec), "small-speaker-mask",
            tbhdx_small_spkr_mask, NULL);
    } else {
        GST_CAT_WARNING(GST_CAT_DEFAULT,
            "DTS decoder not found! All the dts buffers will be dropped");
        dtsxdec = gst_element_factory_make("valve", "dtsx_decoder_valve");
        g_object_set(G_OBJECT(dtsxdec), "drop", TRUE, NULL);
        g_object_ref_sink(dtsxdec);
    }

    oar = gst_element_factory_make("oar", "oar");
    if (oar) {
        g_object_ref_sink(oar);
        g_object_set(G_OBJECT(oar), "speaker-bitfield", 1039, NULL);
    } else {
        GST_CAT_WARNING(GST_CAT_DEFAULT,
            "Dolby OAR not found! Atmos processing is disabled");
        oar = gst_element_factory_make("identity", "oar_passthrough");
        g_object_set(G_OBJECT(oar), "signal-handoffs", FALSE, NULL);
        g_object_ref_sink(oar);
    }

    decbin = gst_element_factory_make("decodebin", "decoderbin");
    if (decbin) {
        g_object_ref_sink(decbin);
        // g_signal_connect(decbin, "pad-added", G_CALLBACK(new_dec_pad_cb),
        // this);
    } else {
        GST_CAT_WARNING(GST_CAT_DEFAULT,
            "Decode bin not found! All the non dolby encoded buffers will be "
            "dropped");
        decbin = gst_element_factory_make("valve", "decbin_parser_valve");
        g_object_set(G_OBJECT(decbin), "drop", TRUE, NULL);
        g_object_ref_sink(decbin);
    }

    identity_src = gst_element_factory_make("identity", "identity_src");
    g_assert(identity_src);
    g_object_set(G_OBJECT(identity_src), "signal-handoffs", FALSE, NULL);

    identity_sink = gst_element_factory_make("identity", "identity_sink");
    g_assert(identity_sink);
    g_object_set(G_OBJECT(identity_sink), "signal-handoffs", FALSE, NULL);

    /* Create default graph */
    switch (dolby_graph) {
        case GST_GRAPH_TYPE_DOLBY_DEC_OAR_DAP:
            GST_CAT_DEBUG(GST_CAT_DEFAULT,
                "Creating graph : GST_GRAPH_TYPE_DOLBY_DEC_OAR_DAP");
            GST_DEBUG_OBJECT(
                decoder_bin, "adding %" GST_PTR_FORMAT, identity_src);
            gst_bin_add(GST_BIN(decoder_bin), identity_src);
            GST_DEBUG_OBJECT(decoder_bin, "adding %" GST_PTR_FORMAT, ddpdec);
            gst_bin_add(GST_BIN(decoder_bin), ddpdec);
            GST_DEBUG_OBJECT(decoder_bin, "adding %" GST_PTR_FORMAT, oar);
            gst_bin_add(GST_BIN(decoder_bin), oar);
            GST_DEBUG_OBJECT(
                decoder_bin, "adding %" GST_PTR_FORMAT, dolby_postproc_bin);
            gst_bin_add(GST_BIN(decoder_bin), dolby_postproc_bin);
            GST_DEBUG_OBJECT(
                decoder_bin, "adding %" GST_PTR_FORMAT, identity_sink);
            gst_bin_add(GST_BIN(decoder_bin), identity_sink);
            GST_DEBUG_OBJECT(decoder_bin, "linking..identity_src->ddpdec");
            if (!gst_element_link_pads(identity_src, "src", ddpdec, "sink")) {
                g_printerr("Could not link identity_src to ddpdec\n");
                return;
            }

            GST_DEBUG_OBJECT(decoder_bin, "linking..ddpdec->oar");
            if (!gst_element_link_pads(ddpdec, "src", oar, "sink")) {
                g_printerr("Could not link ddpdec to oar\n");
                return;
            }

            GST_DEBUG_OBJECT(decoder_bin, "linking..oar->dolby_postproc_bin");
            if (!gst_element_link_pads(
                    oar, "src", dolby_postproc_bin, "sink")) {
                g_printerr("Could not link oar to dolby_postproc_bin\n");
                return;
            }

            GST_DEBUG_OBJECT(
                decoder_bin, "linking..dolby_postproc_bin->identity_sink");
            if (!gst_element_link_pads(
                    dolby_postproc_bin, "src", identity_sink, "sink")) {
                g_printerr(
                    "Could not link dolby_postproc_bin to identity_sink\n");
                return;
            }
            break;
        case GST_GRAPH_TYPE_DOLBY_DEC_DAP:
            GST_CAT_DEBUG(GST_CAT_DEFAULT,
                "Creating graph : GST_GRAPH_TYPE_DOLBY_DEC_DAP");
            GST_DEBUG_OBJECT(
                decoder_bin, "adding %" GST_PTR_FORMAT, identity_src);
            gst_bin_add(GST_BIN(decoder_bin), identity_src);
            GST_DEBUG_OBJECT(decoder_bin, "adding %" GST_PTR_FORMAT, ddpdec);
            gst_bin_add(GST_BIN(decoder_bin), ddpdec);
            GST_DEBUG_OBJECT(
                decoder_bin, "adding %" GST_PTR_FORMAT, dolby_postproc_bin);
            gst_bin_add(GST_BIN(decoder_bin), dolby_postproc_bin);
            GST_DEBUG_OBJECT(
                decoder_bin, "adding %" GST_PTR_FORMAT, identity_sink);
            gst_bin_add(GST_BIN(decoder_bin), identity_sink);

            GST_DEBUG_OBJECT(decoder_bin, "linking..identity_src->ddpdec");
            if (!gst_element_link_pads(identity_src, "src", ddpdec, "sink")) {
                g_printerr("Could not link identity_src to ddpdec\n");
                return;
            }

            GST_DEBUG_OBJECT(
                decoder_bin, "linking..ddpdec->dolby_postproc_bin");
            if (!gst_element_link_pads(
                    ddpdec, "src", dolby_postproc_bin, "sink")) {
                g_printerr("Could not link ddpdec to dolby_postproc_bin\n");
                return;
            }

            GST_DEBUG_OBJECT(
                decoder_bin, "linking..dolby_postproc_bin->identity_sink");
            if (!gst_element_link_pads(
                    dolby_postproc_bin, "src", identity_sink, "sink")) {
                g_printerr(
                    "Could not link dolby_postproc_bin to identity_sink\n");
                return;
            }
            break;
        case GST_GRAPH_TYPE_DOLBY_DEC_ONLY:
            GST_CAT_DEBUG(GST_CAT_DEFAULT,
                "Creating graph : GST_GRAPH_TYPE_DOLBY_DEC_ONLY");
            GST_DEBUG_OBJECT(
                decoder_bin, "adding %" GST_PTR_FORMAT, identity_src);
            gst_bin_add(GST_BIN(decoder_bin), identity_src);
            GST_DEBUG_OBJECT(decoder_bin, "adding %" GST_PTR_FORMAT, ddpdec);
            gst_bin_add(GST_BIN(decoder_bin), ddpdec);
            GST_DEBUG_OBJECT(
                decoder_bin, "adding %" GST_PTR_FORMAT, identity_sink);
            gst_bin_add(GST_BIN(decoder_bin), identity_sink);
            GST_DEBUG_OBJECT(decoder_bin, "linking..identity_src->ddpdec");
            if (!gst_element_link_pads(identity_src, "src", ddpdec, "sink")) {
                g_printerr("Could not link identity_src to ddpdec\n");
                return;
            }

            GST_DEBUG_OBJECT(decoder_bin, "linking..ddpdec->identity_sink");
            if (!gst_element_link_pads(ddpdec, "src", identity_sink, "sink")) {
                g_printerr("Could not link ddpdec to identity_sink\n");
                return;
            }
            break;
        default:
            break;
    }

    cur_codec_type = GST_CODEC_TYPE_DDP;

    /* ghost src and sink pads */
    pad = gst_element_get_static_pad(identity_src, "sink");
    gst_element_add_pad(decoder_bin, gst_ghost_pad_new("sink", pad));
    gst_object_unref(pad);

    pad = gst_element_get_static_pad(identity_sink, "src");
    gst_element_add_pad(decoder_bin, gst_ghost_pad_new("src", pad));
    gst_object_unref(pad);
}

GstPadProbeReturn MediaPlayerPrivate::sd_pad_probe_event_cb_caps(
    GstPad *pad, GstPadProbeInfo *info, gpointer user_data) {
    auto priv = (MediaPlayerPrivate *)user_data;
    GstEvent *event;
    GstElement *dbin = NULL;
    GstCaps *caps;
    GstStructure *s;
    const gchar *name;
    const GstStructure *structure;
    GstCaps *pcmcaps = gst_caps_new_simple("audio/x-raw", "format",
        G_TYPE_STRING, "S16LE", "layout", G_TYPE_STRING, "interleaved", "rate",
        G_TYPE_INT, 44100, "channels", G_TYPE_INT, 2, NULL);

    if (GST_EVENT_TYPE(GST_PAD_PROBE_INFO_DATA(info))
        == GST_EVENT_CUSTOM_DOWNSTREAM) {
        event = gst_pad_probe_info_get_event(info);
        if (event)
            structure = gst_event_get_structure(event);

        if (!structure)
            return GST_PAD_PROBE_DROP;

        if (gst_structure_has_name(structure, "pauseframes")
            && priv->pause_frames == FALSE) {
            /* send signal to client on pause frames first time */
            RaisePauseBurstSignal();
            priv->pause_frames = TRUE;
        }

        if (gst_structure_has_name(structure, "unknown")
            && priv->stream_type_unknown == FALSE) {
            priv->stream_type_unknown = TRUE;

            /* Switch to PCM path */
            GST_CAT_DEBUG(GST_CAT_DEFAULT, "UNKNOWN --> PCM");
            /* Reset PCM stream detector stream type to UNKNOWN */
            priv->pcm_decoded_stream_type = GST_STREAM_TYPE_ERROR;

            /* remove unlinks automatically */
            GST_DEBUG_OBJECT(priv->pipeline, "removing %" GST_PTR_FORMAT,
                priv->stream_detect);
            gst_bin_remove(GST_BIN(priv->pipeline), priv->stream_detect);

            dbin = gst_bin_get_by_name(GST_BIN(priv->pipeline), "Decode-Bin");
            if (dbin != NULL) {
                GST_DEBUG_OBJECT(priv->pipeline, "removing %" GST_PTR_FORMAT,
                    priv->decoder_bin);
                gst_bin_remove(GST_BIN(priv->pipeline), priv->decoder_bin);
                g_object_unref(dbin);
            }

            /* Set device and caps  */
            g_object_set(G_OBJECT(priv->pulsesrc), "device",
                priv->cur_device_name, NULL);
            g_object_set(G_OBJECT(priv->capsfilter), "caps", pcmcaps, NULL);

            if (!IsNonDolbyCodec(priv->cur_codec_type)
                && priv->dolby_graph != GST_GRAPH_TYPE_DOLBY_DEC_ONLY
                && priv->pcm_postproc_path == PCM_DOLBY_DAP_POSTPROC) {
                GST_DEBUG_OBJECT(priv->decoder_bin, "removing %" GST_PTR_FORMAT,
                    priv->dolby_postproc_bin);
                gst_element_set_state(priv->dolby_postproc_bin, GST_STATE_NULL);
                gst_bin_remove(
                    GST_BIN(priv->decoder_bin), priv->dolby_postproc_bin);
            }

            if (priv->cur_codec_type == GST_CODEC_TYPE_DTSX
                && priv->dts_graph != GST_GRAPH_TYPE_DTSX_ONLY
                && priv->pcm_postproc_path == PCM_DTS_VIRTUALX_POSTPROC) {
                GST_DEBUG_OBJECT(priv->decoder_bin, "removing %" GST_PTR_FORMAT,
                    priv->virtualx_postproc_bin);
                gst_element_set_state(
                    priv->virtualx_postproc_bin, GST_STATE_NULL);
                gst_bin_remove(
                    GST_BIN(priv->decoder_bin), priv->virtualx_postproc_bin);
            }

            GST_DEBUG_OBJECT(priv->pipeline, "adding %" GST_PTR_FORMAT,
                priv->pcm_stream_detect);
            gst_bin_add(GST_BIN(priv->pipeline), priv->pcm_stream_detect);

            if (priv->pcm_postproc_path == PCM_NO_POSTPROC) {
                GST_DEBUG_OBJECT(
                    priv->pipeline, "linking..capsfilter->pcm_stream_detect");
                if (!gst_element_link(
                        priv->capsfilter, priv->pcm_stream_detect)) {
                    g_printerr(
                        "Could not link capsfilter to pcm_stream_detect\n");
                    return GST_PAD_PROBE_DROP;
                }
            } else {
                if (priv->pcm_postproc_path == PCM_DOLBY_DAP_POSTPROC) {
                    GST_DEBUG_OBJECT(priv->pipeline, "adding %" GST_PTR_FORMAT,
                        priv->dolby_postproc_bin);
                    gst_bin_add(
                        GST_BIN(priv->pipeline), priv->dolby_postproc_bin);

                    GST_DEBUG_OBJECT(priv->pipeline,
                        "linking..capsfilter->pcm_stream_detect");
                    if (!gst_element_link(
                            priv->capsfilter, priv->pcm_stream_detect)) {
                        g_printerr(
                            "Could not link capsfilter to pcm_stream_detect\n");
                        return GST_PAD_PROBE_DROP;
                    }
                }

                if (priv->pcm_postproc_path == PCM_DTS_VIRTUALX_POSTPROC) {
                    GST_DEBUG_OBJECT(priv->pipeline, "adding %" GST_PTR_FORMAT,
                        priv->virtualx_postproc_bin);
                    gst_bin_add(
                        GST_BIN(priv->pipeline), priv->virtualx_postproc_bin);

                    GST_DEBUG_OBJECT(priv->pipeline,
                        "linking..capsfilter->pcm_stream_detect");
                    if (!gst_element_link(
                            priv->capsfilter, priv->pcm_stream_detect)) {
                        g_printerr(
                            "Could not link capsfilter to pcm_stream_detect\n");
                        return GST_PAD_PROBE_DROP;
                    }
                }
            }

            if (priv->dap == NULL
                && priv->pcm_postproc_path == PCM_DOLBY_DAP_POSTPROC)
                GST_CAT_WARNING(GST_CAT_DEFAULT,
                    "Dolby DAP not found! DAP post processing is disabled");
            if (priv->soundx == NULL
                && priv->pcm_postproc_path == PCM_DTS_VIRTUALX_POSTPROC)
                GST_CAT_WARNING(GST_CAT_DEFAULT,
                    "Dts soundx not found! Virtualx post processing is "
                    "disabled");
            if (priv->mcdyn == NULL
                && priv->pcm_postproc_path == PCM_DTS_VIRTUALX_POSTPROC)
                GST_CAT_WARNING(GST_CAT_DEFAULT,
                    "Dts mcdyn not found! Virtualx post processing is "
                    "disabled");
            gst_element_set_state(
                GST_ELEMENT(priv->pipeline), GST_STATE_PLAYING);
            priv->cur_stream_type = GST_STREAM_TYPE_PCM;
            priv->device_status = GST_DEVICE_ACTIVE;
        }

        return GST_PAD_PROBE_DROP;
    }

    /* Check for caps event */
    if (GST_EVENT_TYPE(GST_PAD_PROBE_INFO_DATA(info)) != GST_EVENT_CAPS) {
        return GST_PAD_PROBE_PASS;
    }

    GST_CAT_DEBUG(GST_CAT_DEFAULT, "iec61937parser pad is blocked now");
    priv->pause_frames = FALSE;
    priv->stream_type_unknown = FALSE;

    /* Get caps */
    event = gst_pad_probe_info_get_event(info);
    gst_event_parse_caps(event, &caps);
    s = gst_caps_get_structure(caps, 0);
    name = gst_structure_get_name(s);
    g_assert(name);

    GST_CAT_DEBUG(GST_CAT_DEFAULT, "iec61937parser got new caps - %s\n", name);

    if (strcmp(name, "audio/x-ac3") == 0) {
        priv->new_codec_type = GST_CODEC_TYPE_DD;
        if (!priv->ddpdec) {
            GST_CAT_DEBUG(GST_CAT_DEFAULT,
                "Cannot decode DD content. Decoder not available");
            return GST_PAD_PROBE_DROP;
        }
    } else if (strcmp(name, "audio/x-eac3") == 0) {
        priv->new_codec_type = GST_CODEC_TYPE_DDP;
        if (!priv->ddpdec) {
            GST_CAT_DEBUG(GST_CAT_DEFAULT,
                "Cannot decode DDP content. Decoder not available");
            return GST_PAD_PROBE_DROP;
        }
    } else if (strcmp(name, "audio/x-mat") == 0) {
        priv->new_codec_type = GST_CODEC_TYPE_MAT;
        if (!priv->matdec) {
            GST_CAT_DEBUG(GST_CAT_DEFAULT,
                "Cannot decode MAT content. Decoder not available");
            return GST_PAD_PROBE_DROP;
        }
    } else if (strcmp(name, "audio/x-dts") == 0) {
        priv->new_codec_type = GST_CODEC_TYPE_DTSX;
        if (!priv->dtsxdec) {
            GST_CAT_DEBUG(GST_CAT_DEFAULT,
                "Cannot decode DTS content. Decoder not available");
            return GST_PAD_PROBE_DROP;
        }
    } else if (strcmp(name, "audio/mpeg") == 0) {
        int version = 0;
        gst_structure_get_int(s, "mpegversion", &version);
        if (version == 1)
            priv->new_codec_type = GST_CODEC_TYPE_MP3;
        else
            priv->new_codec_type = GST_CODEC_TYPE_AAC;

        if (!priv->decbin) {
            GST_CAT_DEBUG(GST_CAT_DEFAULT,
                "Cannot decode MP3/AAC content. Decoder bin not available");
            return GST_PAD_PROBE_DROP;
        }
    } else if (strcmp(name, "audio/x-wma") == 0) {
        priv->new_codec_type = GST_CODEC_TYPE_WMA;
        if (!priv->decbin) {
            GST_CAT_DEBUG(GST_CAT_DEFAULT,
                "Cannot decode WMA content. Decoder bin not available");
            return GST_PAD_PROBE_DROP;
        }
    } else if (strcmp(name, "audio/x-raw") == 0) {
        priv->new_codec_type = GST_CODEC_TYPE_ALS;
        if (!priv->decbin) {
            GST_CAT_DEBUG(GST_CAT_DEFAULT,
                "Cannot decode ALS content. Decoder bin not available");
            return GST_PAD_PROBE_DROP;
        }
    }

    if (priv->new_codec_type != GST_CODEC_TYPE_MAT
        && priv->cur_codec_type == GST_CODEC_TYPE_MAT
        && strcmp(priv->sink_name, "offload-low-latency") == 0) {
        g_idle_add(SetPsrcOriginalBuffer, priv);
    }

    if (priv->new_codec_type == GST_CODEC_TYPE_MAT
        && priv->cur_codec_type != GST_CODEC_TYPE_MAT
        && strcmp(priv->sink_name, "offload-low-latency") == 0) {
        g_idle_add(SetPsrcBufferMat, priv);
    }

    if (!IsNonDolbyCodec(priv->new_codec_type) && priv->dap == NULL
        && priv->dolby_graph != GST_GRAPH_TYPE_DOLBY_DEC_ONLY)
        GST_CAT_WARNING(GST_CAT_DEFAULT,
            "Dolby DAP not found! DAP post processing is disabled");

    if (!IsNonDolbyCodec(priv->new_codec_type) && priv->oar == NULL
        && priv->dolby_graph != GST_GRAPH_TYPE_DOLBY_DEC_ONLY)
        GST_CAT_WARNING(GST_CAT_DEFAULT,
            "Dolby OAR not found! Atmos processing is disabled");

    if (!IsNonDolbyCodec(priv->new_codec_type) && priv->soundx == NULL
        && priv->dts_graph != GST_GRAPH_TYPE_DTSX_ONLY)
        GST_CAT_WARNING(
            GST_CAT_DEFAULT, "Soundx not found! Soundx processing is disabled");

    if (!IsNonDolbyCodec(priv->new_codec_type) && priv->mcdyn == NULL
        && priv->dts_graph != GST_GRAPH_TYPE_DTSX_ONLY)
        GST_CAT_WARNING(GST_CAT_DEFAULT,
            "mcdynamics not found! mcdynamics processing is disabled");

    /* Based on the new caps from iec61937parser, change the decoder in the bin
     */
    if (priv->new_codec_type != priv->cur_codec_type) {
        priv->SwapDecodersInDecoderBin();
        gst_element_set_state(priv->pulsesink, GST_STATE_NULL);
        gst_element_set_state(priv->pulsesink, GST_STATE_PLAYING);
    } else {
        GST_CAT_DEBUG(GST_CAT_DEFAULT,
            "previous stream and new stream (codec) type are same");
        GST_CAT_DEBUG(GST_CAT_DEFAULT, "no need to swap the decoders");
    }

    dbin = gst_bin_get_by_name(GST_BIN(priv->pipeline), "Decoder-Bin");
    if (dbin == NULL) {
        GST_DEBUG_OBJECT(
            priv->pipeline, "adding %" GST_PTR_FORMAT, priv->decoder_bin);
        gst_bin_add(GST_BIN(priv->pipeline), priv->decoder_bin);

        GST_DEBUG_OBJECT(
            priv->pipeline, "linking..iec61937parser->decoder_bin");
        if (!gst_element_link_pads(
                priv->stream_detect, "src", priv->decoder_bin, "sink")) {
            g_printerr("Could not link iec61937parser->decoder_bin\n");
            return GST_PAD_PROBE_DROP;
        }

        GST_DEBUG_OBJECT(priv->pipeline, "linking..decoder_bin->pulsesink");
        if (priv->decoder_bin) {
            if (priv->pulsesink) {
                //	 if (!gst_element_link_pads(priv->decoder_bin, "src",
                //priv->pulsesink, "audio_sink")) { if
                // (!gst_element_link_pads_full(priv->decoder_bin, "src",
                // priv->pulsesink, "sink",GST_PAD_LINK_CHECK_CAPS)) {
                if (!gst_element_link_pads(
                        priv->decoder_bin, "src", priv->pulsesink, "sink")) {
                    g_printerr("Could not link decoder_bin->pulsesink\n");
                    return GST_PAD_PROBE_DROP;
                }
            }
        }
    } else {
        g_object_unref(dbin);
    }

    gst_element_set_state(priv->decoder_bin, GST_STATE_PLAYING);
    GST_CAT_DEBUG(GST_CAT_DEFAULT, "unblocking iec61937parser blocked pad");
    return GST_PAD_PROBE_PASS;
}

void MediaPlayerPrivate::SwapDecodersInDecoderBin() {
    switch (cur_codec_type) {
        case GST_CODEC_TYPE_DD:
        case GST_CODEC_TYPE_DDP:
            /* remove unlinks automatically */
            gst_element_set_state(ddpdec, GST_STATE_NULL);
            GST_DEBUG_OBJECT(decoder_bin, "removing %" GST_PTR_FORMAT, ddpdec);
            gst_bin_remove(GST_BIN(decoder_bin), ddpdec);

            if (IsNonDolbyCodec(new_codec_type)
                && dolby_graph != GST_GRAPH_TYPE_DOLBY_DEC_ONLY) {
                switch (dolby_graph) {
                    case GST_GRAPH_TYPE_DOLBY_DEC_OAR_DAP:
                        GST_DEBUG_OBJECT(
                            decoder_bin, "removing %" GST_PTR_FORMAT, oar);
                        gst_bin_remove(GST_BIN(decoder_bin), oar);

                        GST_DEBUG_OBJECT(decoder_bin,
                            "removing %" GST_PTR_FORMAT, dolby_postproc_bin);
                        gst_bin_remove(
                            GST_BIN(decoder_bin), dolby_postproc_bin);
                        break;
                    case GST_GRAPH_TYPE_DOLBY_DEC_DAP:
                        GST_DEBUG_OBJECT(decoder_bin,
                            "removing %" GST_PTR_FORMAT, dolby_postproc_bin);
                        gst_bin_remove(
                            GST_BIN(decoder_bin), dolby_postproc_bin);
                        break;
                    default:
                        break;
                }
            }
            break;
        case GST_CODEC_TYPE_MAT:
            /* remove unlinks automatically */
            gst_element_set_state(matdec, GST_STATE_NULL);
            GST_DEBUG_OBJECT(decoder_bin, "removing %" GST_PTR_FORMAT, matdec);
            gst_bin_remove(GST_BIN(decoder_bin), matdec);
            GST_DEBUG_OBJECT(decoder_bin, "removing %" GST_PTR_FORMAT, thdec);
            gst_bin_remove(GST_BIN(decoder_bin), thdec);

            if (IsNonDolbyCodec(new_codec_type)
                && dolby_graph != GST_GRAPH_TYPE_DOLBY_DEC_ONLY) {
                switch (dolby_graph) {
                    case GST_GRAPH_TYPE_DOLBY_DEC_OAR_DAP:
                        GST_DEBUG_OBJECT(
                            decoder_bin, "removing %" GST_PTR_FORMAT, oar);
                        gst_bin_remove(GST_BIN(decoder_bin), oar);

                        GST_DEBUG_OBJECT(decoder_bin,
                            "removing %" GST_PTR_FORMAT, dolby_postproc_bin);
                        gst_bin_remove(
                            GST_BIN(decoder_bin), dolby_postproc_bin);
                        break;
                    case GST_GRAPH_TYPE_DOLBY_DEC_DAP:
                        GST_DEBUG_OBJECT(decoder_bin,
                            "removing %" GST_PTR_FORMAT, dolby_postproc_bin);
                        gst_bin_remove(
                            GST_BIN(decoder_bin), dolby_postproc_bin);
                        break;
                    default:
                        break;
                }
            }
            break;
        case GST_CODEC_TYPE_DTSX:
            gst_element_set_state(dtsxdec, GST_STATE_NULL);
            /* remove unlinks automatically */
            GST_DEBUG_OBJECT(decoder_bin, "removing %" GST_PTR_FORMAT, dtsxdec);
            gst_bin_remove(GST_BIN(decoder_bin), dtsxdec);

            if (dts_graph == GST_GRAPH_TYPE_DTSX_VIRTUALX) {
                GST_DEBUG_OBJECT(decoder_bin, "removing %" GST_PTR_FORMAT,
                    virtualx_postproc_bin);
                gst_bin_remove(GST_BIN(decoder_bin), virtualx_postproc_bin);
            }
            break;
        case GST_CODEC_TYPE_MP3:
        case GST_CODEC_TYPE_AAC:
        case GST_CODEC_TYPE_WMA:
        case GST_CODEC_TYPE_ALS:
            /* remove unlinks automatically */
            gst_element_set_state(decbin, GST_STATE_NULL);
            GST_DEBUG_OBJECT(decoder_bin, "removing %" GST_PTR_FORMAT, decbin);
            gst_bin_remove(GST_BIN(decoder_bin), decbin);

            if (pcm_postproc_path == PCM_DOLBY_DAP_POSTPROC) {
                GST_DEBUG_OBJECT(decoder_bin, "removing %" GST_PTR_FORMAT,
                    dolby_postproc_bin);
                gst_bin_remove(GST_BIN(decoder_bin), dolby_postproc_bin);
            }
            break;
        default:
            g_printerr("unknown stream type");
            break;
    }

    switch (new_codec_type) {
        case GST_CODEC_TYPE_DD:
        case GST_CODEC_TYPE_DDP:

            switch (dolby_graph) {
                case GST_GRAPH_TYPE_DOLBY_DEC_OAR_DAP:
                    GST_DEBUG_OBJECT(
                        decoder_bin, "adding   %" GST_PTR_FORMAT, ddpdec);
                    gst_bin_add(GST_BIN(decoder_bin), ddpdec);

                    if (IsNonDolbyCodec(cur_codec_type)) {
                        GST_DEBUG_OBJECT(
                            decoder_bin, "adding   %" GST_PTR_FORMAT, oar);
                        gst_bin_add(GST_BIN(decoder_bin), oar);
                        GST_DEBUG_OBJECT(decoder_bin, "adding %" GST_PTR_FORMAT,
                            dolby_postproc_bin);
                        gst_bin_add(GST_BIN(decoder_bin), dolby_postproc_bin);
                    }

                    GST_DEBUG_OBJECT(
                        decoder_bin, "linking..identity_src->ddpdec");
                    if (!gst_element_link_pads(
                            identity_src, "src", ddpdec, "sink")) {
                        g_printerr("Could not link identity_src->ddpdec\n");
                        return;
                    }

                    GST_DEBUG_OBJECT(decoder_bin, "linking..ddpdec->oar");
                    if (!gst_element_link_pads(ddpdec, "src", oar, "sink")) {
                        g_printerr("Could not link ddpdec->oar\n");
                        return;
                    }

                    if (IsNonDolbyCodec(cur_codec_type)) {
                        GST_DEBUG_OBJECT(
                            decoder_bin, "linking..oar->dolby_postproc_bin");
                        if (!gst_element_link_pads(
                                oar, "src", dolby_postproc_bin, "sink")) {
                            g_printerr(
                                "Could not link oar -> dolby_postproc_bin\n");
                            return;
                        }

                        GST_DEBUG_OBJECT(decoder_bin,
                            "linking..dolby_postproc_bin->identity_sink");
                        if (!gst_element_link_pads(dolby_postproc_bin, "src",
                                identity_sink, "sink")) {
                            g_printerr(
                                "Could not link dolby_postproc_bin -> "
                                "identity_sink\n");
                            return;
                        }
                    }
                    break;
                case GST_GRAPH_TYPE_DOLBY_DEC_DAP:
                    GST_DEBUG_OBJECT(
                        decoder_bin, "adding   %" GST_PTR_FORMAT, ddpdec);
                    gst_bin_add(GST_BIN(decoder_bin), ddpdec);

                    if (IsNonDolbyCodec(cur_codec_type)) {
                        GST_DEBUG_OBJECT(decoder_bin, "adding %" GST_PTR_FORMAT,
                            dolby_postproc_bin);
                        gst_bin_add(GST_BIN(decoder_bin), dolby_postproc_bin);
                    }

                    GST_DEBUG_OBJECT(
                        decoder_bin, "linking..identity_src->ddpdec");
                    if (!gst_element_link_pads(
                            identity_src, "src", ddpdec, "sink")) {
                        g_printerr("Could not link identity_src->ddpdec\n");
                        return;
                    }

                    GST_DEBUG_OBJECT(
                        decoder_bin, "linking..ddpdec->dolby_postproc_bin");
                    if (!gst_element_link_pads(
                            ddpdec, "src", dolby_postproc_bin, "sink")) {
                        g_printerr(
                            "Could not link ddpdec->dolby_postproc_bin\n");
                        return;
                    }

                    if (IsNonDolbyCodec(cur_codec_type)) {
                        GST_DEBUG_OBJECT(decoder_bin,
                            "linking..dolby_postproc_bin->identity_sink");
                        if (!gst_element_link_pads(dolby_postproc_bin, "src",
                                identity_sink, "sink")) {
                            g_printerr(
                                "Could not link dolby_postproc_bin -> "
                                "identity_sink\n");
                            return;
                        }
                    }
                    break;
                case GST_GRAPH_TYPE_DOLBY_DEC_ONLY:
                    GST_DEBUG_OBJECT(
                        decoder_bin, "adding   %" GST_PTR_FORMAT, ddpdec);
                    gst_bin_add(GST_BIN(decoder_bin), ddpdec);

                    GST_DEBUG_OBJECT(
                        decoder_bin, "linking..identity_src->ddpdec");
                    if (!gst_element_link_pads(
                            identity_src, "src", ddpdec, "sink")) {
                        g_printerr("Could not link identity_src->ddpdec\n");
                        return;
                    }

                    GST_DEBUG_OBJECT(
                        decoder_bin, "linking..ddpdec->identity_sink");
                    if (!gst_element_link_pads(
                            ddpdec, "src", identity_sink, "sink")) {
                        g_printerr("Could not link ddpdec->identity_sink\n");
                        return;
                    }
                    break;
                default:
                    break;
            }
            break;
        case GST_CODEC_TYPE_MAT:
            switch (dolby_graph) {
                case GST_GRAPH_TYPE_DOLBY_DEC_OAR_DAP:
                    GST_DEBUG_OBJECT(
                        decoder_bin, "adding   %" GST_PTR_FORMAT, matdec);
                    gst_bin_add(GST_BIN(decoder_bin), matdec);
                    GST_DEBUG_OBJECT(
                        decoder_bin, "adding   %" GST_PTR_FORMAT, thdec);
                    gst_bin_add(GST_BIN(decoder_bin), thdec);
                    if (IsNonDolbyCodec(cur_codec_type)) {
                        GST_DEBUG_OBJECT(
                            decoder_bin, "adding   %" GST_PTR_FORMAT, oar);
                        gst_bin_add(GST_BIN(decoder_bin), oar);
                        GST_DEBUG_OBJECT(decoder_bin, "adding %" GST_PTR_FORMAT,
                            dolby_postproc_bin);
                        gst_bin_add(GST_BIN(decoder_bin), dolby_postproc_bin);
                    }

                    GST_DEBUG_OBJECT(
                        decoder_bin, "linking..identity_src->matdec");
                    if (!gst_element_link_pads(
                            identity_src, "src", matdec, "sink")) {
                        g_printerr("Could not link identity_src->matdec\n");
                        return;
                    }

                    GST_DEBUG_OBJECT(decoder_bin, "linking..matdec->thdec");
                    if (!gst_element_link_pads(matdec, "src", thdec, "sink")) {
                        g_printerr("Could not link matdec->thdec\n");
                        return;
                    }

                    GST_DEBUG_OBJECT(decoder_bin, "linking..thdec->oar");
                    if (!gst_element_link_pads(thdec, "src", oar, "sink")) {
                        g_printerr("Could not link thdec->oar\n");
                        return;
                    }

                    if (IsNonDolbyCodec(cur_codec_type)) {
                        GST_DEBUG_OBJECT(
                            decoder_bin, "linking..oar->dolby_postproc_bin");
                        if (!gst_element_link_pads(
                                oar, "src", dolby_postproc_bin, "sink")) {
                            g_printerr(
                                "Could not link oar->dolby_postproc_bin\n");
                            return;
                        }

                        GST_DEBUG_OBJECT(decoder_bin,
                            "linking..dolby_postproc_bin->identity_sink");
                        if (!gst_element_link_pads(dolby_postproc_bin, "src",
                                identity_sink, "sink")) {
                            g_printerr(
                                "Could not link dolby_postproc_bin -> "
                                "identity_sink\n");
                            return;
                        }
                    }
                    break;
                case GST_GRAPH_TYPE_DOLBY_DEC_DAP:
                    GST_DEBUG_OBJECT(
                        decoder_bin, "adding   %" GST_PTR_FORMAT, matdec);
                    gst_bin_add(GST_BIN(decoder_bin), matdec);
                    GST_DEBUG_OBJECT(
                        decoder_bin, "adding   %" GST_PTR_FORMAT, thdec);
                    gst_bin_add(GST_BIN(decoder_bin), thdec);
                    if (IsNonDolbyCodec(cur_codec_type)) {
                        GST_DEBUG_OBJECT(decoder_bin, "adding %" GST_PTR_FORMAT,
                            dolby_postproc_bin);
                        gst_bin_add(GST_BIN(decoder_bin), dolby_postproc_bin);
                    }

                    GST_DEBUG_OBJECT(
                        decoder_bin, "linking..identity_src->matdec");
                    if (!gst_element_link_pads(
                            identity_src, "src", matdec, "sink")) {
                        g_printerr("Could not link identity_src->matdec\n");
                        return;
                    }

                    GST_DEBUG_OBJECT(decoder_bin, "linking..matdec->thdec");
                    if (!gst_element_link_pads(matdec, "src", thdec, "sink")) {
                        g_printerr("Could not link matdec->thdec\n");
                        return;
                    }

                    GST_DEBUG_OBJECT(
                        decoder_bin, "linking..thdec->dolby_postproc_bin");
                    if (!gst_element_link_pads(
                            thdec, "src", dolby_postproc_bin, "sink")) {
                        g_printerr(
                            "Could not link thdec->dolby_postproc_bin\n");
                        exit(-1);
                    }

                    if (IsNonDolbyCodec(cur_codec_type)) {
                        GST_DEBUG_OBJECT(decoder_bin,
                            "linking..dolby_postproc_bin->identity_sink");
                        if (!gst_element_link_pads(dolby_postproc_bin, "src",
                                identity_sink, "sink")) {
                            g_printerr(
                                "Could not link dolby_postproc_bin -> "
                                "identity_sink\n");
                            return;
                        }
                    }
                    break;
                case GST_GRAPH_TYPE_DOLBY_DEC_ONLY:
                    GST_DEBUG_OBJECT(
                        decoder_bin, "adding   %" GST_PTR_FORMAT, matdec);
                    gst_bin_add(GST_BIN(decoder_bin), matdec);
                    GST_DEBUG_OBJECT(
                        decoder_bin, "adding   %" GST_PTR_FORMAT, thdec);
                    gst_bin_add(GST_BIN(decoder_bin), thdec);

                    GST_DEBUG_OBJECT(
                        decoder_bin, "linking..identity_src->matdec");
                    if (!gst_element_link_pads(
                            identity_src, "src", matdec, "sink")) {
                        g_printerr("Could not link identity_src->matdec\n");
                        return;
                    }

                    GST_DEBUG_OBJECT(decoder_bin, "linking..matdec->thdec");
                    if (!gst_element_link_pads(matdec, "src", thdec, "sink")) {
                        g_printerr("Could not link matdec->thdec\n");
                        return;
                    }

                    GST_DEBUG_OBJECT(
                        decoder_bin, "linking..thdec->identity_sink");
                    if (!gst_element_link_pads(
                            thdec, "src", identity_sink, "sink")) {
                        g_printerr("Could not link thdec->identity_sink\n");
                        return;
                    }
                    break;
            }
            break;
        case GST_CODEC_TYPE_DTSX:

            if (dts_graph == GST_GRAPH_TYPE_DTSX_ONLY) {
                GST_DEBUG_OBJECT(
                    decoder_bin, "adding   %" GST_PTR_FORMAT, dtsxdec);
                gst_bin_add(GST_BIN(decoder_bin), dtsxdec);

                GST_DEBUG_OBJECT(decoder_bin, "linking..identity_src->dtsxdec");
                if (!gst_element_link_pads(
                        identity_src, "src", dtsxdec, "sink")) {
                    g_printerr("Could not link identity_src->dtsxdec\n");
                    return;
                }

                GST_DEBUG_OBJECT(
                    decoder_bin, "linking..dtsxdec->identity_sink");
                if (!gst_element_link_pads(
                        dtsxdec, "src", identity_sink, "sink")) {
                    g_printerr("Could not link dtsxdec->identity_sink\n");
                    return;
                }
            } else {
                GST_DEBUG_OBJECT(
                    decoder_bin, "adding   %" GST_PTR_FORMAT, dtsxdec);
                gst_bin_add(GST_BIN(decoder_bin), dtsxdec);
                GST_DEBUG_OBJECT(decoder_bin, "adding   %" GST_PTR_FORMAT,
                    virtualx_postproc_bin);
                gst_bin_add(GST_BIN(decoder_bin), virtualx_postproc_bin);

                GST_DEBUG_OBJECT(decoder_bin, "linking..identity_src->dtsxdec");
                if (!gst_element_link_pads(
                        identity_src, "src", dtsxdec, "sink")) {
                    g_printerr("Could not link identity_src->dtsxdec\n");
                    return;
                }

                GST_DEBUG_OBJECT(
                    decoder_bin, "linking..dtsxdec->virtualx_postproc_bin");
                if (!gst_element_link_pads(
                        dtsxdec, "src", virtualx_postproc_bin, "sink")) {
                    g_printerr(
                        "Could not link dtsxdec->virtualx_postproc_bin\n");
                    return;
                }

                GST_DEBUG_OBJECT(decoder_bin,
                    "linking..virtualx_postproc_bin->identity_sink");
                if (!gst_element_link_pads(
                        virtualx_postproc_bin, "src", identity_sink, "sink")) {
                    g_printerr(
                        "Could not link "
                        "virtualx_postproc_bin->identity_sink\n");
                    return;
                }
            }
            break;
        case GST_CODEC_TYPE_MP3:
        case GST_CODEC_TYPE_AAC:
        case GST_CODEC_TYPE_WMA:
        case GST_CODEC_TYPE_ALS:
            GST_DEBUG_OBJECT(decoder_bin, "adding   %" GST_PTR_FORMAT, decbin);
            gst_bin_add(GST_BIN(decoder_bin), decbin);

            if (pcm_postproc_path == PCM_DOLBY_DAP_POSTPROC) {
                GST_DEBUG_OBJECT(
                    decoder_bin, "adding %" GST_PTR_FORMAT, dolby_postproc_bin);
                gst_bin_add(GST_BIN(decoder_bin), dolby_postproc_bin);
            }

            GST_DEBUG_OBJECT(decoder_bin, "linking..identity_src->decbin");
            if (!gst_element_link_pads(identity_src, "src", decbin, "sink")) {
                g_printerr("Could not link identity_src->decbin\n");
                exit(-1);
            }
            /* remaining pipeline is linked once decbin src pad is available in
             * new_dec_pad_cb() */
            break;
        default:
            g_printerr("unknown stream type");
            break;
    }

    cur_codec_type = new_codec_type;
    new_codec_type = (gst_codec_type_t)-1;
    GST_DEBUG_OBJECT(decoder_bin, "swap done");
}

GstPadProbeReturn MediaPlayerPrivate::sd_pad_probe_event_cb_pcm_detect(
    GstPad *pad, GstPadProbeInfo *info, gpointer user_data) {
    auto priv = (MediaPlayerPrivate *)user_data;
    GstEvent *event = NULL;
    const GstStructure *event_structure = NULL;
    GstStructure *s = NULL;
    const gchar *name = NULL;
    GstCaps *caps = NULL;
    GstEventType event_type;
    GstElement *dbin = NULL;

    event = gst_pad_probe_info_get_event(info);
    event_type = event->type;

    switch (event_type) {
        case GST_EVENT_CAPS:
            GST_CAT_DEBUG(GST_CAT_DEFAULT, "pcmdetector pad is blocked now");

            /* Parse caps */
            gst_event_parse_caps(event, &caps);
            priv->caps_ = caps;
            s = gst_caps_get_structure(caps, 0);
            name = gst_structure_get_name(s);
            g_assert(name);

            GST_CAT_DEBUG(
                GST_CAT_DEFAULT, "pcmdetector got new caps - %s\n", name);

            if (!g_strcmp0(name, "audio/x-dts")) {
                priv->new_codec_type = GST_CODEC_TYPE_DTSX;
                priv->new_stream_type = GST_STREAM_TYPE_DTS_CD;

                if (!priv->dtsxdec) {
                    GST_CAT_DEBUG(GST_CAT_DEFAULT,
                        "Cannot decode DTS content. Decoder not available");
                    return GST_PAD_PROBE_DROP;
                }
            } else if (!g_strcmp0(name, "audio/x-raw")) {
                priv->new_stream_type = GST_STREAM_TYPE_PCM;
            }

            if (priv->new_stream_type == priv->pcm_decoded_stream_type) {
                GST_CAT_DEBUG(GST_CAT_DEFAULT,
                    "previous stream and new stream (codec) type are same");
                GST_CAT_DEBUG(GST_CAT_DEFAULT, "no need to swap the decoders");
                goto exit;
            } else {
                if (priv->new_stream_type == GST_STREAM_TYPE_PCM) {
                    GST_CAT_DEBUG(GST_CAT_DEFAULT, "PCM type detected");
                    priv->pcm_decoded_stream_type = GST_STREAM_TYPE_PCM;

                    dbin = gst_bin_get_by_name(
                        GST_BIN(priv->pipeline), "Decoder-Bin");
                    if (dbin != NULL) {
                        GST_DEBUG_OBJECT(priv->pipeline,
                            "removing %" GST_PTR_FORMAT, dbin);
                        gst_element_set_state(
                            dbin, GST_STATE_NULL);
                        gst_bin_remove(
                            GST_BIN(priv->pipeline), dbin);
                        g_object_unref(dbin);
                    }

                    if (priv->pcm_postproc_path == PCM_NO_POSTPROC) {
                        GST_DEBUG_OBJECT(priv->pipeline,
                            "linking..pcm_stream_detect->pulsesink");
                        if (!gst_element_link(
                                priv->pcm_stream_detect, priv->pulsesink)) {
                            g_printerr(
                                "Could not link pcm_stream_detect to "
                                "pulsesink\n");
                            return GST_PAD_PROBE_DROP;
                        }
                    } else {
                        if (priv->pcm_postproc_path == PCM_DOLBY_DAP_POSTPROC) {
                            GST_DEBUG_OBJECT(priv->pipeline,
                                "linking..pcm_stream_detect->dolby_postproc_"
                                "bin");
                            if (!gst_element_link(priv->pcm_stream_detect,
                                    priv->dolby_postproc_bin)) {
                                g_printerr(
                                    "Could not link pcm_stream_detect to "
                                    "dolby_postproc_bin\n");
                                return GST_PAD_PROBE_DROP;
                            }

                            GST_DEBUG_OBJECT(priv->pipeline,
                                "linking..dolby_postproc_bin->pulsesink");
                            if (!gst_element_link(priv->dolby_postproc_bin,
                                    priv->pulsesink)) {
                                g_printerr(
                                    "Could not link dolby_postproc_bin to "
                                    "pulsesink\n");
                                return GST_PAD_PROBE_DROP;
                            }
                        }
                        if (priv->pcm_postproc_path
                            == PCM_DTS_VIRTUALX_POSTPROC) {
                            GST_DEBUG_OBJECT(priv->pipeline,
                                "linking..pcm_stream_detect->virtualx_postproc_"
                                "bin");
                            if (!gst_element_link(priv->pcm_stream_detect,
                                    priv->virtualx_postproc_bin)) {
                                g_printerr(
                                    "Could not link pcm_stream_detect to "
                                    "virtualx_postproc_bin\n");
                                return GST_PAD_PROBE_DROP;
                            }

                            GST_DEBUG_OBJECT(priv->pipeline,
                                "linking..virtualx_postproc_bin->pulsesink");
                            if (!gst_element_link(priv->virtualx_postproc_bin,
                                    priv->pulsesink)) {
                                g_printerr(
                                    "Could not link virtualx_postproc_bin to "
                                    "pulsesink\n");
                                return GST_PAD_PROBE_DROP;
                            }
                        }
                    }

                    GST_CAT_DEBUG(
                        GST_CAT_DEFAULT, "unblocking pcmdetector blocked pad");
                    gst_element_set_state(
                        priv->pcm_stream_detect, GST_STATE_PLAYING);
                    goto exit;
                } else if (priv->new_stream_type == GST_STREAM_TYPE_DTS_CD) {
                    GST_CAT_DEBUG(GST_CAT_DEFAULT, "DTS CD type detected");

                    /* If prev PCM detected type not equal to DTS CD */
                    if (priv->pcm_decoded_stream_type
                        != priv->new_stream_type) {
                        /* Unlink PCM stream detector and pulsesink */
                        GST_DEBUG_OBJECT(priv->pipeline,
                            "unlinking..pcm_stream_detect->pulsesink");
                        gst_element_unlink(
                            priv->pcm_stream_detect, priv->pulsesink);
                    }

                    priv->pcm_decoded_stream_type = priv->new_stream_type;
                }
            }

            if (!IsNonDolbyCodec(priv->new_codec_type) && priv->dap == NULL
                && priv->dolby_graph != GST_GRAPH_TYPE_DOLBY_DEC_ONLY)
                GST_CAT_WARNING(GST_CAT_DEFAULT,
                    "Dolby DAP not found! DAP post processing is disabled");

            if (!IsNonDolbyCodec(priv->new_codec_type) && priv->oar == NULL
                && priv->dolby_graph != GST_GRAPH_TYPE_DOLBY_DEC_ONLY)
                GST_CAT_WARNING(GST_CAT_DEFAULT,
                    "Dolby OAR not found! Atmos processing is disabled");

            if (priv->new_codec_type == GST_CODEC_TYPE_DTSX
                && priv->soundx == NULL
                && priv->dts_graph != GST_GRAPH_TYPE_DTSX_ONLY)
                GST_CAT_WARNING(GST_CAT_DEFAULT,
                    "Soundx not found! Soundx processing is disabled");

            if (priv->new_codec_type == GST_CODEC_TYPE_DTSX
                && priv->mcdyn == NULL
                && priv->dts_graph != GST_GRAPH_TYPE_DTSX_ONLY)
                GST_CAT_WARNING(GST_CAT_DEFAULT,
                    "mcdynamics not found! mcdynamics processing is disabled");
            if (priv->new_codec_type != priv->cur_codec_type) {
                priv->SwapDecodersInDecoderBin();

                gst_element_set_state(priv->pulsesink, GST_STATE_NULL);
                gst_element_set_state(priv->pulsesink, GST_STATE_PLAYING);
            } else {
                GST_CAT_DEBUG(GST_CAT_DEFAULT, "no change in codec type\n");
                GST_CAT_DEBUG(GST_CAT_DEFAULT, "no need to swap the decoders");
            }

            if (priv->pcm_decoded_stream_type == GST_STREAM_TYPE_DTS_CD) {
                dbin =
                    gst_bin_get_by_name(GST_BIN(priv->pipeline), "Decoder-Bin");
                if (dbin == NULL) {
                    /* Unlink PCM stream detector and pulsesink */
                    GST_DEBUG_OBJECT(priv->pipeline,
                        "unlinking..pcm_stream_detect->pulsesink");
                    gst_element_unlink(
                        priv->pcm_stream_detect, priv->pulsesink);

                    GST_DEBUG_OBJECT(priv->pipeline, "adding %" GST_PTR_FORMAT,
                        priv->decoder_bin);
                    gst_bin_add(GST_BIN(priv->pipeline), priv->decoder_bin);

                    GST_DEBUG_OBJECT(priv->pipeline,
                        "linking..pcm_stream_detect->decoder_bin");
                    if (!gst_element_link_pads(priv->pcm_stream_detect, "src",
                            priv->decoder_bin, "sink")) {
                        g_printerr(
                            "Could not link pcm_stream_detect->decoder_bin\n");
                        return GST_PAD_PROBE_DROP;
                    }

                    GST_DEBUG_OBJECT(
                        priv->pipeline, "linking..decoder_bin->pulsesink");
                    if (!gst_element_link_pads(priv->decoder_bin, "src",
                            priv->pulsesink, "sink")) {
                        g_printerr("Could not link decoder_bin->pulsesink\n");
                        return GST_PAD_PROBE_DROP;
                    }
                } else {
                    g_object_unref(dbin);
                }
            }

            gst_element_set_state(priv->pcm_stream_detect, GST_STATE_PLAYING);
            gst_element_set_state(priv->decoder_bin, GST_STATE_PLAYING);

            GST_CAT_DEBUG(
                GST_CAT_DEFAULT, "unblocking pcmdetector blocked pad");

            break;
        case GST_EVENT_CUSTOM_DOWNSTREAM:
            event_structure = gst_event_get_structure(event);

            if (event_structure) {
                if (gst_structure_has_name(event_structure, "PCM-Zero")) {
                    GST_CAT_DEBUG(GST_CAT_DEFAULT, "PCM-Zero");
                    /* TBD call format change */
                    // stream_type = GST_STREAM_TYPE_PCM_ZERO;
                } else if (gst_structure_has_name(event_structure, "PCM")) {
                    GST_CAT_DEBUG(GST_CAT_DEFAULT, "PCM");
                    /* TBD call format change */
                    // stream_type = GST_STREAM_TYPE_PCM;
                }

                priv->pcm_decoded_stream_type = GST_STREAM_TYPE_PCM;
            }
        default:
            break;
    }

exit:
    return GST_PAD_PROBE_PASS;
}

void MediaPlayerPrivate::RaisePauseBurstSignal() {
    // TBD
}

gboolean MediaPlayerPrivate::IsNonDolbyCodec(gst_codec_type_t codec_type) {
    gboolean ret = FALSE;

    if ((codec_type == GST_CODEC_TYPE_DTSX)
        || (codec_type == GST_CODEC_TYPE_MP3)
        || (codec_type == GST_CODEC_TYPE_AAC)
        || (codec_type == GST_CODEC_TYPE_WMA)
        || (codec_type == GST_CODEC_TYPE_ALS))
        ret = TRUE;

    return ret;
}

gint MediaPlayerPrivate::SetPsrcOriginalBuffer(gpointer user_data) {
    auto priv = (MediaPlayerPrivate *)user_data;
    gst_element_set_state(priv->pulsesrc, GST_STATE_NULL);
    g_object_set(G_OBJECT(priv->pulsesrc), "stream-flags", 0x0000220f,
        "buffer-time", (gint64)40000, "latency-time", (gint64)10000, NULL);
    gst_element_set_state(priv->pulsesrc, GST_STATE_PLAYING);
    return false;
}

gint MediaPlayerPrivate::SetPsrcBufferMat(gpointer user_data) {
    auto priv = (MediaPlayerPrivate *)user_data;
    gst_element_set_state(priv->pulsesrc, GST_STATE_NULL);
    g_object_set(G_OBJECT(priv->pulsesrc), "stream-flags", 0x0000220f,
        "buffer-time", (gint64)150000, "latency-time", (gint64)10000, NULL);
    gst_element_set_state(priv->pulsesrc, GST_STATE_PLAYING);
    return false;
}

void MediaPlayerPrivate::SetDolbyGraph(std::string graph) {
    if (graph.compare("DOLBYDEC_OAR_DAP") == 0)
        dolby_graph = GST_GRAPH_TYPE_DOLBY_DEC_OAR_DAP;
    else if (graph.compare("DOLBYDEC_DAP") == 0)
        dolby_graph = GST_GRAPH_TYPE_DOLBY_DEC_DAP;
    else if (graph.compare("DOLBYDEC") == 0)
        dolby_graph = GST_GRAPH_TYPE_DOLBY_DEC_ONLY;
}

void MediaPlayerPrivate::SetVirtualXGraph(std::string graph) {
    if (graph.compare("DTSX") == 0)
        dts_graph = GST_GRAPH_TYPE_DTSX_ONLY;
    else if (graph.compare("DTSX_VIRTUALX") == 0)
        dts_graph = GST_GRAPH_TYPE_DTSX_VIRTUALX;
}

void MediaPlayerPrivate::DeviceRemoved() {
    if (device_status == GST_DEVICE_INACTIVE)
        return;
    gst_element_set_state(GST_ELEMENT(pipeline), GST_STATE_NULL);
    device_status = GST_DEVICE_INACTIVE;
}

GstCaps *MediaPlayerPrivate::set_dsd_non_native_caps(GstCaps *caps) {
    GstStructure *structure = NULL;
    int32_t rate, dsd_type, channels;
    GstCaps *new_caps;

    structure = gst_caps_get_structure(caps, 0);
    gst_structure_get_int(structure, "rate", &rate);
    gst_structure_get_int(structure, "dsd-type", &dsd_type);
    gst_structure_get_int(structure, "channels", &channels);

    new_caps = gst_caps_new_simple("audio/x-dsd", "rate", G_TYPE_INT,
        (rate * dsd_type) / 8, "channels", G_TYPE_INT, channels, "lsbf",
        G_TYPE_BOOLEAN, FALSE, "planar", G_TYPE_BOOLEAN, TRUE, "dsd-type",
        G_TYPE_INT, dsd_type, "non-native", G_TYPE_BOOLEAN, TRUE,
        "capture-frequency", G_TYPE_INT, rate, NULL);

    return new_caps;
}

void MediaPlayerPrivate::StreamTypeChanged(gst_stream_type_t new_stream_type, gst_codec_type_t codec_type, MediaPlayerPrivate *priv) {
    std::vector<std::string> decoder_bin_elements;
    decoder_bin_elements.emplace_back(adk::mediaplayerlib::kLiveSourceBin);
    gst_stream_type_t stream_type;
    int32_t mat_val = 0;
    int32_t thd_val = 0;
    int32_t obj_val = 0;
    int32_t media_type = 0;
    DecodeBinDelayParams params;
    std::string stream_type_string = StreamTypeToString(new_stream_type, codec_type);

    if (new_stream_type == GST_STREAM_TYPE_ENCODED) {
        if ((codec_type == GST_CODEC_TYPE_MAT) || (codec_type == GST_CODEC_TYPE_THD)) {
            g_object_get(G_OBJECT(priv->thdec), "stream-type", &thd_val, NULL);
            g_object_get(G_OBJECT(priv->matdec), "stream-type", &mat_val, NULL);

            if (mat_val == 2) {
                stream_type_string = "MAT ATMOS";
            } else if (mat_val == 3) {
                stream_type_string = "MAT CH";
            } else if (mat_val == 1) {
                if (thd_val == 0)
                    stream_type_string = "THD CH";
                else if (thd_val == 1)
                    stream_type_string = "THD ATMOS";
            }
        } else if ((codec_type == GST_CODEC_TYPE_DD) || (codec_type == GST_CODEC_TYPE_DDP)) {
            g_object_get(G_OBJECT(priv->ddpdec), "object-audio", &obj_val, NULL);
            g_object_get(G_OBJECT(priv->ddpdec), "media-type", &media_type, NULL);

            if (media_type == 3) {
                if (obj_val == 1)
                    stream_type_string = "DD ATMOS";

                stream_type_string = "DD CH";
            } else if (media_type == 4) {
                if (obj_val == 1)
                    stream_type_string = "DDP ATMOS";
                else if (obj_val == 0)
                    stream_type_string = "DDP CH";
            }
        }
    } else if (new_stream_type == GST_STREAM_TYPE_PCM) {
        if (priv->pcm_decoded_stream_type == GST_STREAM_TYPE_DTS_CD)
            stream_type_string = "DTS CD";
    }
    decoder_bin_elements.push_back(stream_type_string);
    UpdateDelayOnDecoderBinChange(decoder_bin_elements, params_);
}

char *MediaPlayerPrivate::StreamTypeToString(gst_stream_type_t stream_type, gst_codec_type_t codec_type) {
    switch (stream_type) {
        case GST_STREAM_TYPE_ENCODED:
            switch (codec_type) {
                case GST_CODEC_TYPE_DD:
                    return "DD";
                case GST_CODEC_TYPE_DDP:
                    return "DDP";
                case GST_CODEC_TYPE_MAT:
                    return "MAT";
                case GST_CODEC_TYPE_THD:
                    return "TrueHD";
                case GST_CODEC_TYPE_DTSX:
                    return "DTS";
                case GST_CODEC_TYPE_MP3:
                    return "MP3";
                case GST_CODEC_TYPE_AAC:
                    return "AAC";
                case GST_CODEC_TYPE_WMA:
                    return "WMA";
                case GST_CODEC_TYPE_ALS:
                    return "ALS";
                default:
                    return "ENCODED";
            }
        case GST_STREAM_TYPE_PCM:
            return "PCM";
        case GST_STREAM_TYPE_PCM_ZERO:
            return "PCM Zero";
        case GST_STREAM_TYPE_DTS_CD:
            return "DTS CD";
        case GST_STREAM_TYPE_DSD:
            return "DSD";
        case GST_STREAM_TYPE_ERROR:
            return "Unknown";
        default:
            return "INVALID";
    }
}

MediaPlayerPrivate::PPTable MediaPlayerPrivate::GetPPPropMatchTable(std::string elem_name) {
    if (pp_prop_match.find(elem_name) != pp_prop_match.end()) {
        return pp_prop_match[elem_name];
    }
    return {};
}
void MediaPlayerPrivate::UpdateGstElemPropertyChar(std::string elem_name, std::string dbus_prop_name, char *value) {
    GstElement *elem = gst_bin_get_by_name(GST_BIN(pipeline), elem_name.c_str());
    PPTable pp_table = GetPPPropMatchTable(elem_name);

    // TODO: check if the element is active in the pipeline
    if (!elem) {
        g_printerr("elem is null, returning\n");
        return;
    }
    if (pp_table.find(dbus_prop_name) != pp_table.end()) {
        auto val = pp_table[dbus_prop_name];
        switch (std::get<0>(val)) {
            case GstElemPropType::kDirect:
                if (std::get<2>(val) == Types::kStr) {
                    std::string prop_name = std::get<1>(val);
                    g_object_set(G_OBJECT(elem), prop_name.c_str(), value, NULL);
                }
                break;
            case GstElemPropType::kPointer:
                std::get<3>(val)(value);
                break;
        }
    }
}

void MediaPlayerPrivate::UpdateGstElemPropertyStr(std::string elem_name, std::string dbus_prop_name, std::string value) {
    GstElement *elem = gst_bin_get_by_name(GST_BIN(pipeline), elem_name.c_str());
    PPTable pp_table = GetPPPropMatchTable(elem_name);

    // TODO: check if the element is active in the pipeline
    if (!elem) {
        g_printerr("elem is null, returning\n");
        return;
    }
    if (pp_table.find(dbus_prop_name) != pp_table.end()) {
        auto val = pp_table[dbus_prop_name];
        switch (std::get<0>(val)) {
            case GstElemPropType::kDirect:
                if (std::get<2>(val) == Types::kStr) {
                    std::string prop_name = std::get<1>(val);
                    g_object_set(G_OBJECT(elem), prop_name.c_str(), value.c_str(), NULL);
                }
                break;
            case GstElemPropType::kPointer:
                std::get<3>(val)(&value);
                break;
        }
    }
}

void MediaPlayerPrivate::VirtualizerFrontSpeakerAngle(void *value) {
    virtualizer_t *virtualizer = NULL;
    g_object_get(G_OBJECT(dap), "virtualizer", &virtualizer, NULL);
    auto val = *(uint8_t *)value;
    virtualizer->virtualizer_front_speaker_angle = static_cast<uint32_t>(val);
    g_object_set(G_OBJECT(dap), "virtualizer", virtualizer, NULL);
}

void MediaPlayerPrivate::VirtualizerSurroundSpeakerAngle(void *value) {
    virtualizer_t *virtualizer = NULL;
    g_object_get(G_OBJECT(dap), "virtualizer", &virtualizer, NULL);
    auto val = *(uint8_t *)value;
    virtualizer->virtualizer_surround_speaker_angle = static_cast<uint32_t>(val);
    g_object_set(G_OBJECT(dap), "virtualizer", virtualizer, NULL);
}

void MediaPlayerPrivate::VirtualizerRearSurroundSpeakerAngle(void *value) {
    virtualizer_t *virtualizer = NULL;
    g_object_get(G_OBJECT(dap), "virtualizer", &virtualizer, NULL);
    auto val = *(uint8_t *)value;
    virtualizer->virtualizer_rear_surround_speaker_angle = static_cast<uint32_t>(val);
    g_object_set(G_OBJECT(dap), "virtualizer", virtualizer, NULL);
}

void MediaPlayerPrivate::VirtualizerHeightSpeakerAngle(void *value) {
    virtualizer_t *virtualizer = NULL;
    g_object_get(G_OBJECT(dap), "virtualizer", &virtualizer, NULL);
    auto val = *(uint8_t *)value;
    virtualizer->virtualizer_height_speaker_angle = static_cast<uint32_t>(val);
    g_object_set(G_OBJECT(dap), "virtualizer", virtualizer, NULL);
}

void MediaPlayerPrivate::VirtualizerRearHeightSpeakerAngle(void *value) {
    virtualizer_t *virtualizer = NULL;
    g_object_get(G_OBJECT(dap), "virtualizer", &virtualizer, NULL);
    auto val = *(uint8_t *)value;
    virtualizer->virtualizer_rear_height_speaker_angle = static_cast<uint32_t>(val);
    g_object_set(G_OBJECT(dap), "virtualizer", virtualizer, NULL);
}

void MediaPlayerPrivate::IntelligentEqEnable(void *value) {
    intelligent_equalizer_t *ieq = NULL;
    g_object_get(G_OBJECT(dap), "intelligent-equalizer", &ieq, NULL);
    auto val = *(uint32_t *)value;
    ieq->ieq_en = static_cast<int32_t>(val);
    g_object_set(G_OBJECT(dap), "intelligent-equalizer", ieq, NULL);
}

void MediaPlayerPrivate::IntelligentEqAmount(void *value) {
    intelligent_equalizer_t *ieq = NULL;
    g_object_get(G_OBJECT(dap), "intelligent-equalizer", &ieq, NULL);
    auto val = *(uint8_t *)value;
    ieq->ieq_set = static_cast<int32_t>(val);
    g_object_set(G_OBJECT(dap), "intelligent-equalizer", ieq, NULL);
}

void MediaPlayerPrivate::VolumeLevelerAmount(void *value) {
    volume_leveler_t *vol_leveler = NULL;
    g_object_get(G_OBJECT(dap), "volume-leveler", &vol_leveler, NULL);
    auto val = *(uint8_t *)value;
    vol_leveler->volume_leveler_amount = static_cast<int32_t>(val);
    g_object_set(G_OBJECT(dap), "volume-leveler", vol_leveler, NULL);
}

void MediaPlayerPrivate::VolumeLevelerInTarget(void *value) {
    volume_leveler_t *vol_leveler = NULL;
    g_object_get(G_OBJECT(dap), "volume-leveler", &vol_leveler, NULL);
    auto val = *(int32_t *)value;
    vol_leveler->volume_leveler_in_target = val;
    g_object_set(G_OBJECT(dap), "volume-leveler", vol_leveler, NULL);
}

void MediaPlayerPrivate::VolumeLevelerOutTarget(void *value) {
    volume_leveler_t *vol_leveler = NULL;
    g_object_get(G_OBJECT(dap), "volume-leveler", &vol_leveler, NULL);
    auto val = *(int32_t *)value;
    vol_leveler->volume_leveler_out_target = val;
    g_object_set(G_OBJECT(dap), "volume-leveler", vol_leveler, NULL);
}

void MediaPlayerPrivate::DialogEnhancerEnable(void *value) {
    dialog_enhancer_t *dialog_enhancer = NULL;
    g_object_get(G_OBJECT(dap), "dialog-enhancer", &dialog_enhancer, NULL);
    auto val = *(uint32_t *)value;
    dialog_enhancer->dialog_enhancer_enable = static_cast<bool>(val);
    g_object_set(G_OBJECT(dap), "dialog-enhancer", dialog_enhancer, NULL);
}

void MediaPlayerPrivate::DialogEnhancerAmount(void *value) {
    dialog_enhancer_t *dialog_enhancer = NULL;
    g_object_get(G_OBJECT(dap), "dialog-enhancer", &dialog_enhancer, NULL);
    auto val = *(uint8_t *)value;
    dialog_enhancer->dialog_enhance_amt = static_cast<int32_t>(val);
    g_object_set(G_OBJECT(dap), "dialog-enhancer", dialog_enhancer, NULL);
}

void MediaPlayerPrivate::DialogEnhancerDuckingAmt(void *value) {
    dialog_enhancer_t *dialog_enhancer = NULL;
    g_object_get(G_OBJECT(dap), "dialog-enhancer", &dialog_enhancer, NULL);
    auto val = *(uint8_t *)value;
    dialog_enhancer->dialog_enhance_ducking_amt = static_cast<int32_t>(val);
    g_object_set(G_OBJECT(dap), "dialog-enhancer", dialog_enhancer, NULL);
}

void MediaPlayerPrivate::MiDialogEnhancerEn(void *value) {
    media_intelligence_t *mi = NULL;
    g_object_get(G_OBJECT(dap), "media-intelligence", &mi, NULL);
    auto val = *(uint32_t *)value;
    mi->mi_dialog_enhancer_en = static_cast<bool>(val);
    g_object_set(G_OBJECT(dap), "media-intelligence", mi, NULL);
}

void MediaPlayerPrivate::MiVolLevelerEn(void *value) {
    media_intelligence_t *mi = NULL;
    g_object_get(G_OBJECT(dap), "media-intelligence", &mi, NULL);
    auto val = *(uint32_t *)value;
    mi->mi_vol_leveler_en = static_cast<bool>(val);
    g_object_set(G_OBJECT(dap), "media-intelligence", mi, NULL);
}

void MediaPlayerPrivate::MiIeqEn(void *value) {
    media_intelligence_t *mi = NULL;
    g_object_get(G_OBJECT(dap), "media-intelligence", &mi, NULL);
    auto val = *(uint32_t *)value;
    mi->mi_ieq_en = static_cast<bool>(val);
    g_object_set(G_OBJECT(dap), "media-intelligence", mi, NULL);
}

void MediaPlayerPrivate::MiSurroundBoostEn(void *value) {
    media_intelligence_t *mi = NULL;
    g_object_get(G_OBJECT(dap), "media-intelligence", &mi, NULL);
    auto val = *(uint32_t *)value;
    mi->mi_surround_boost_en = static_cast<bool>(val);
    g_object_set(G_OBJECT(dap), "media-intelligence", mi, NULL);
}

void MediaPlayerPrivate::BassEnhancerEnable(void *value) {
    bass_enhancer_t *bass_enhancer = NULL;
    g_object_get(G_OBJECT(dap), "bass-enhancer", &bass_enhancer, NULL);
    auto val = *(uint32_t *)value;
    bass_enhancer->bass_enhancer_enable = static_cast<bool>(val);
    g_object_set(G_OBJECT(dap), "bass-enhancer", bass_enhancer, NULL);
}

void MediaPlayerPrivate::BassEnhancerWidthSet(void *value) {
    bass_enhancer_t *bass_enhancer = NULL;
    g_object_get(G_OBJECT(dap), "bass-enhancer", &bass_enhancer, NULL);
    auto val = *(int32_t *)value;
    bass_enhancer->bass_enhancer_width_set = val;
    g_object_set(G_OBJECT(dap), "bass-enhancer", bass_enhancer, NULL);
}

void MediaPlayerPrivate::BassEnhancerCutoffFreq(void *value) {
    bass_enhancer_t *bass_enhancer = NULL;
    g_object_get(G_OBJECT(dap), "bass-enhancer", &bass_enhancer, NULL);
    auto val = *(int32_t *)value;
    bass_enhancer->bass_enhancer_cutoff_freq = static_cast<uint32_t>(val);
    g_object_set(G_OBJECT(dap), "bass-enhancer", bass_enhancer, NULL);
}

void MediaPlayerPrivate::BassEnhancerBoostSet(void *value) {
    bass_enhancer_t *bass_enhancer = NULL;
    g_object_get(G_OBJECT(dap), "bass-enhancer", &bass_enhancer, NULL);
    auto val = *(int32_t *)value;
    bass_enhancer->bass_enhancer_boost_set = val;
    g_object_set(G_OBJECT(dap), "bass-enhancer", bass_enhancer, NULL);
}

void MediaPlayerPrivate::RegulatorEnable(void *value) {
    audio_regulator_t *audio_reg = NULL;
    g_object_get(G_OBJECT(dap), "audio-regulator", &audio_reg, NULL);
    auto val = *(uint32_t *)value;
    audio_reg->regulator_enable = static_cast<int32_t>(val);
    g_object_set(G_OBJECT(dap), "audio-regulator", audio_reg, NULL);
}

void MediaPlayerPrivate::RegulatorSpeakerDistEnable(void *value) {
    audio_regulator_t *audio_reg = NULL;
    g_object_get(G_OBJECT(dap), "audio-regulator", &audio_reg, NULL);
    auto val = *(uint32_t *)value;
    audio_reg->regulator_speaker_dist_enable = static_cast<int32_t>(val);
    g_object_set(G_OBJECT(dap), "audio-regulator", audio_reg, NULL);
}

void MediaPlayerPrivate::RegulatorRelaxationAmount(void *value) {
    audio_regulator_t *audio_reg = NULL;
    g_object_get(G_OBJECT(dap), "audio-regulator", &audio_reg, NULL);
    auto val = *(uint8_t *)value;
    audio_reg->regulator_relaxation_amount = static_cast<int32_t>(val);
    g_object_set(G_OBJECT(dap), "audio-regulator", audio_reg, NULL);
}

void MediaPlayerPrivate::RegulatorTimbrePreservation(void *value) {
    audio_regulator_t *audio_reg = NULL;
    g_object_get(G_OBJECT(dap), "audio-regulator", &audio_reg, NULL);
    auto val = *(uint8_t *)value;
    audio_reg->regulator_timbre_preservation = static_cast<int32_t>(val);
    g_object_set(G_OBJECT(dap), "audio-regulator", audio_reg, NULL);
}

uint64_t MediaPlayerPrivate::GetGSTPipelineDelay() {
    return delay_fields.gst_pipeline_delay;
}

uint64_t MediaPlayerPrivate::GetTotalDelay() {
    return delay_fields.gst_pipeline_delay + delay_fields.pa_delay;
}

void MediaPlayerPrivate::UpdateDelayOnDecoderBinChange(std::vector<std::string> decode_bin_formats, DecodeBinDelayParams params) {
    uint64_t decode_bin_delay = 0;
    if (decode_bin_formats.size() < 2) {
        return;
    }
    if (decode_bin_formats[0] == std::string(adk::mediaplayerlib::kAppSrc) ||
        decode_bin_formats[0] == std::string(adk::mediaplayerlib::kUridecodeBin)) {
        if (decode_bin_formats[1] == adk::mediaplayerlib::kPcm) {
            decode_bin_delay = params.channels;
        } else {
            decode_bin_delay = delay_fields.kDecDelayDefault;
        }
    } else if (decode_bin_formats[0] == std::string(adk::mediaplayerlib::kLiveSourceBin)) {
        for (std::size_t i = 1; i < decode_bin_formats.size(); i++) {
            std::string elem_name = decode_bin_formats[i];
            if (elem_name == std::string(adk::mediaplayerlib::kPcm)) {
                decode_bin_delay += params.channels;
            } else if (decode_bin_delay_map.find(elem_name) != decode_bin_delay_map.end()) {
                DecoderDelayTable delay_table = decode_bin_delay_map[elem_name];
                if (delay_table.find(params) != delay_table.end()) {
                    decode_bin_delay += delay_table[params];
                } else {
                    decode_bin_delay += delay_fields.kPluginDelayDefault;
                }
            } else {
                decode_bin_delay += delay_fields.kPluginDelayDefault;
            }
        }
    }
    if (decode_bin_delay != delay_fields.decode_bin_delay) {
        delay_fields.gst_pipeline_delay -= delay_fields.decode_bin_delay;
        delay_fields.decode_bin_delay = decode_bin_delay;
        delay_fields.gst_pipeline_delay += delay_fields.decode_bin_delay;
        EmitPropertyChangeSignal("GSTPipelineDelay");
        if (onTotalPipelineDelayChange) {
            onTotalPipelineDelayChange(GetTotalDelay());
        }
    }
}

void MediaPlayerPrivate::UpdateDelayOnPPBinChange(std::vector<std::string> pp_bin_elements, PPBinDelayParams params) {
    uint64_t pp_bin_delay = 0;
    for (auto &elem_name : pp_bin_elements) {
        if (pp_bin_delay_map.find(elem_name) != pp_bin_delay_map.end()) {
            PPDelayTable delay_table = pp_bin_delay_map[elem_name];
            if (delay_table.find(params) != delay_table.end()) {
                pp_bin_delay += delay_table[params];
            } else {
                pp_bin_delay += delay_fields.kPluginDelayDefault;
            }
        } else {
            pp_bin_delay += delay_fields.kPluginDelayDefault;
        }
    }
    if (pp_bin_delay != delay_fields.pp_bin_delay) {
        delay_fields.gst_pipeline_delay -= delay_fields.pp_bin_delay;
        delay_fields.pp_bin_delay = pp_bin_delay;
        delay_fields.gst_pipeline_delay += delay_fields.pp_bin_delay;
        EmitPropertyChangeSignal("GSTPipelineDelay");
        if (onTotalPipelineDelayChange) {
            onTotalPipelineDelayChange(GetTotalDelay());
        }
    }
}

}  // namespace adk
