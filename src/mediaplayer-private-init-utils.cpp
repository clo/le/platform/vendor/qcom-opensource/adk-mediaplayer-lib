/*
 * Copyright (c) 2020, The Linux Foundation. All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above
 *       copyright notice, this list of conditions and the following
 *       disclaimer in the documentation and/or other materials provided
 *       with the distribution.
 *     * Neither the name of The Linux Foundation nor the names of its
 *       contributors may be used to endorse or promote products derived
 *       from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS
 * BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
 * BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
 * IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include "mediaplayer-private.h"

namespace adk {

void MediaPlayerPrivate::InitPPPropMatchTables() {
    using std::placeholders::_1;
    PPTable dap_prop_match = {
        {"Mode", std::make_tuple(GstElemPropType::kDirect, "dap-profile", Types::kInt32, nullptr)},
        {"SpeakerPresenceMask", std::make_tuple(GstElemPropType::kDirect, "output-speaker-mask", Types::kUint64, nullptr)},
        {"VirtualizationMode", std::make_tuple(GstElemPropType::kDirect, "virtualizer-enable", Types::kInt32, nullptr)},
        {"FrontSpeakerAngle", std::make_tuple(GstElemPropType::kPointer, "virtualizer", Types::kVoidptr, std::bind(&MediaPlayerPrivate::VirtualizerFrontSpeakerAngle, this, _1))},
        {"SurroundSpeakerAngle", std::make_tuple(GstElemPropType::kPointer, "virtualizer", Types::kVoidptr, std::bind(&MediaPlayerPrivate::VirtualizerSurroundSpeakerAngle, this, _1))},
        {"RearSurroundSpeaker", std::make_tuple(GstElemPropType::kPointer, "virtualizer", Types::kVoidptr, std::bind(&MediaPlayerPrivate::VirtualizerRearSurroundSpeakerAngle, this, _1))},
        {"HeightSpeakerAngle", std::make_tuple(GstElemPropType::kPointer, "virtualizer", Types::kVoidptr, std::bind(&MediaPlayerPrivate::VirtualizerHeightSpeakerAngle, this, _1))},
        {"RearHeightSpeakerAngle", std::make_tuple(GstElemPropType::kPointer, "virtualizer", Types::kVoidptr, std::bind(&MediaPlayerPrivate::VirtualizerRearHeightSpeakerAngle, this, _1))},
        {"HeightFilterEn", std::make_tuple(GstElemPropType::kDirect, "height-filter", Types::kInt32, nullptr)},
        {"VlampEn", std::make_tuple(GstElemPropType::kDirect, "volume-leveler-enable", Types::kBool, nullptr)},
        {"UpmixEnable", std::make_tuple(GstElemPropType::kDirect, "surround-decoder-enable", Types::kBool, nullptr)},
        {"VmCal", std::make_tuple(GstElemPropType::kDirect, "volume-modeler-calibration", Types::kInt32, nullptr)},
        {"XmlFile", std::make_tuple(GstElemPropType::kDirect, "xml-file-location", Types::kStr, nullptr)},
        {"IntelligentEqEnable", std::make_tuple(GstElemPropType::kPointer, "intelligent-equalizer", Types::kVoidptr, std::bind(&MediaPlayerPrivate::IntelligentEqEnable, this, _1))},
        {"IntelligentEqAmount", std::make_tuple(GstElemPropType::kPointer, "intelligent-equalizer", Types::kVoidptr, std::bind(&MediaPlayerPrivate::IntelligentEqAmount, this, _1))},
        {"VolLevelerAmount", std::make_tuple(GstElemPropType::kPointer, "volume-leveler", Types::kVoidptr, std::bind(&MediaPlayerPrivate::VolumeLevelerAmount, this, _1))},
        {"VolLevelerInTarget", std::make_tuple(GstElemPropType::kPointer, "volume-leveler", Types::kVoidptr, std::bind(&MediaPlayerPrivate::VolumeLevelerInTarget, this, _1))},
        {"VolLevelerOutTarget", std::make_tuple(GstElemPropType::kPointer, "volume-leveler", Types::kVoidptr, std::bind(&MediaPlayerPrivate::VolumeLevelerOutTarget, this, _1))},
        {"DialogueEnhancerEnable", std::make_tuple(GstElemPropType::kPointer, "dialog-enhancer", Types::kVoidptr, std::bind(&MediaPlayerPrivate::DialogEnhancerEnable, this, _1))},
        {"DialogueEnhancerAmount", std::make_tuple(GstElemPropType::kPointer, "dialog-enhancer", Types::kVoidptr, std::bind(&MediaPlayerPrivate::DialogEnhancerAmount, this, _1))},
        {"DialogueEnhancerDucking", std::make_tuple(GstElemPropType::kPointer, "dialog-enhancer", Types::kVoidptr, std::bind(&MediaPlayerPrivate::DialogEnhancerDuckingAmt, this, _1))},
        {"SurroundBoost", std::make_tuple(GstElemPropType::kDirect, "surround-boost", Types::kInt32, nullptr)},
        {"CenterSpread", std::make_tuple(GstElemPropType::kDirect, "surround-decoder-center-spreading-enable", Types::kBool, nullptr)},
        {"DialogEnhancerEnable", std::make_tuple(GstElemPropType::kPointer, "media-intelligence", Types::kVoidptr, std::bind(&MediaPlayerPrivate::MiDialogEnhancerEn, this, _1))},
        {"VolLevelerEnable", std::make_tuple(GstElemPropType::kPointer, "media-intelligence", Types::kVoidptr, std::bind(&MediaPlayerPrivate::MiVolLevelerEn, this, _1))},
        {"IntEqEnable", std::make_tuple(GstElemPropType::kPointer, "media-intelligence", Types::kVoidptr, std::bind(&MediaPlayerPrivate::MiIeqEn, this, _1))},
        {"SurroundCompressorEnable", std::make_tuple(GstElemPropType::kPointer, "media-intelligence", Types::kVoidptr, std::bind(&MediaPlayerPrivate::MiSurroundBoostEn, this, _1))},
        {"VolumeModelerEnable", std::make_tuple(GstElemPropType::kDirect, "volume-modeler-enable", Types::kBool, nullptr)},
        {"VolumeMaxBoost", std::make_tuple(GstElemPropType::kDirect, "volume-boost", Types::kInt32, nullptr)},
        {"BassExtractionEnable", std::make_tuple(GstElemPropType::kDirect, "bass-extraction-enable", Types::kBool, nullptr)},
        {"BassLfeGain", std::make_tuple(GstElemPropType::kDirect, "bass-extraction-lfe-gain", Types::kInt32, nullptr)},
        {"BassCutoffFreq", std::make_tuple(GstElemPropType::kDirect, "bass-extraction-cutoff-freq", Types::kInt32, nullptr)},
        {"BassEnhancementEnable", std::make_tuple(GstElemPropType::kPointer, "bass-enhancer", Types::kVoidptr, std::bind(&MediaPlayerPrivate::BassEnhancerEnable, this, _1))},
        {"BassEnhancementBoost", std::make_tuple(GstElemPropType::kPointer, "bass-enhancer", Types::kVoidptr, std::bind(&MediaPlayerPrivate::BassEnhancerBoostSet, this, _1))},
        {"BassEnhancementCutoffFreq", std::make_tuple(GstElemPropType::kPointer, "bass-enhancer", Types::kVoidptr, std::bind(&MediaPlayerPrivate::BassEnhancerCutoffFreq, this, _1))},
        {"BassEnhancementWidth", std::make_tuple(GstElemPropType::kPointer, "bass-enhancer", Types::kVoidptr, std::bind(&MediaPlayerPrivate::BassEnhancerWidthSet, this, _1))},
        {"PreGain", std::make_tuple(GstElemPropType::kDirect, "pregain", Types::kInt32, nullptr)},
        {"PostGain", std::make_tuple(GstElemPropType::kDirect, "post-gain", Types::kInt32, nullptr)},
        {"SystemGain", std::make_tuple(GstElemPropType::kDirect, "system-gain", Types::kInt32, nullptr)},
        {"CalibrationBoost", std::make_tuple(GstElemPropType::kDirect, "calibration-boost", Types::kInt32, nullptr)},
        {"AudioRegulatorEnable", std::make_tuple(GstElemPropType::kPointer, "audio-regulator", Types::kVoidptr, std::bind(&MediaPlayerPrivate::RegulatorEnable, this, _1))},
        {"RegulatorSpeakerDistortionEnable", std::make_tuple(GstElemPropType::kPointer, "audio-regulator", Types::kVoidptr, std::bind(&MediaPlayerPrivate::RegulatorSpeakerDistEnable, this, _1))},
        {"RegulatorRelaxationAmount", std::make_tuple(GstElemPropType::kPointer, "audio-regulator", Types::kVoidptr, std::bind(&MediaPlayerPrivate::RegulatorRelaxationAmount, this, _1))},
        {"RegulatorTimbrePreservation", std::make_tuple(GstElemPropType::kPointer, "audio-regulator", Types::kVoidptr, std::bind(&MediaPlayerPrivate::RegulatorTimbrePreservation, this, _1))}};

    PPTable soundx_prop_match = {
        {"Discard", std::make_tuple(GstElemPropType::kDirect, "soundx-discard", Types::kBool, nullptr)},
        {"HeightDiscard", std::make_tuple(GstElemPropType::kDirect, "soundx-height-discard", Types::kBool, nullptr)},
        {"InMode", std::make_tuple(GstElemPropType::kDirect, "soundx-in-mode", Types::kInt32, nullptr)},
        {"OutMode", std::make_tuple(GstElemPropType::kDirect, "soundx-out-mode", Types::kInt32, nullptr)},
        {"InGain", std::make_tuple(GstElemPropType::kDirect, "soundx-in-gain", Types::kDouble, nullptr)},
        {"OutGain", std::make_tuple(GstElemPropType::kDirect, "soundx-out-gain", Types::kDouble, nullptr)},
        {"DownMixCoEff", std::make_tuple(GstElemPropType::kDirect, "soundx-downmix-coeff", Types::kDouble, nullptr)},
        {"VirtualHeightEnable", std::make_tuple(GstElemPropType::kDirect, "soundx-virtual-height-en", Types::kBool, nullptr)},
        {"VirtualHeightDecorrEnable", std::make_tuple(GstElemPropType::kDirect, "soundx-virt-height-decorr-en", Types::kBool, nullptr)},
        {"VirtualHeightElevfltEnable", std::make_tuple(GstElemPropType::kDirect, "soundx-virt-height-elevflt-en", Types::kBool, nullptr)},
        {"VirtualHeightSurroundEnable", std::make_tuple(GstElemPropType::kDirect, "soundx-virt-height-sur-en", Types::kBool, nullptr)},
        {"VirtualHeightToneAdjust", std::make_tuple(GstElemPropType::kDirect, "soundx-virt-height-tone-adjust", Types::kInt32, nullptr)},
        {"VirtualHeightUpmixEnable", std::make_tuple(GstElemPropType::kDirect, "soundx-height-upmix-en", Types::kBool, nullptr)},
        {"HeightUpmixDelay", std::make_tuple(GstElemPropType::kDirect, "soundx-height-upmix-delay", Types::kInt32, nullptr)},
        {"HeightUpmixHpbEnable", std::make_tuple(GstElemPropType::kDirect, "soundx-height-upmix-hpb-en", Types::kBool, nullptr)},
        {"HeightUpmixHpbPan", std::make_tuple(GstElemPropType::kDirect, "soundx-height-upmix-hpb-pan", Types::kDouble, nullptr)},
        {"HeightUpmixQclsCutGain", std::make_tuple(GstElemPropType::kDirect, "soundx-height-upmix-qcls-cut-gain", Types::kDouble, nullptr)},
        {"HeightUpmixQclsBoostGain", std::make_tuple(GstElemPropType::kDirect, "soundx-height-upmix-qcls-boost-gain", Types::kDouble, nullptr)},
        {"Tshd2HorizEnable", std::make_tuple(GstElemPropType::kDirect, "soundx-tshd2-horiz-en", Types::kBool, nullptr)},
        {"Tshd2HorizCentFrntMix", std::make_tuple(GstElemPropType::kDirect, "soundx-tshd2-horiz-centfrnt-mix", Types::kDouble, nullptr)},
        {"Tshd2HorizSurEnable", std::make_tuple(GstElemPropType::kDirect, "soundx-tshd2-horiz-sur-en", Types::kBool, nullptr)},
        {"Tshd2HorizSurLevel", std::make_tuple(GstElemPropType::kDirect, "soundx-tshd2-horiz-sur-level", Types::kDouble, nullptr)},
        {"Tshd2HorizPercurSelEnable", std::make_tuple(GstElemPropType::kDirect, "soundx-tshd2-horiz-percursel-en", Types::kBool, nullptr)},
        {"Tshd2HorizFtrTbhdxEnable", std::make_tuple(GstElemPropType::kDirect, "soundx-tshd2-horiz-frt-tbhdx-en", Types::kBool, nullptr)},
        {"Tshd2HorizFtrTbhdxProcMode", std::make_tuple(GstElemPropType::kDirect, "soundx-tshd2-horiz-frt-tbhdx-proc-mode", Types::kInt32, nullptr)},
        {"Tshd2HorizFrtTbhdxSpkSz", std::make_tuple(GstElemPropType::kDirect, "soundx-tshd2-horiz-frt-tbhdx-spk-sz", Types::kInt32, nullptr)},
        {"Tshd2HorizFrtTbhdxDynms", std::make_tuple(GstElemPropType::kDirect, "soundx-tshd2-horiz-frt-tbhdx-dynms", Types::kDouble, nullptr)},
        {"Tshd2HorizFrtTbhdxHpEnable", std::make_tuple(GstElemPropType::kDirect, "soundx-tshd2-horiz-frt-tbhdx-hp-en", Types::kBool, nullptr)},
        {"Tshd2HorizFrtTbhdxHpOrder", std::make_tuple(GstElemPropType::kDirect, "soundx-tshd2-horiz-frt-tbhdx-hp-order", Types::kInt32, nullptr)},
        {"Tshd2HorizFrtTbhdxBass", std::make_tuple(GstElemPropType::kDirect, "soundx-tshd2-horiz-frt-tbhdx-bass-lvl", Types::kDouble, nullptr)},
        {"Tshd2HorizFrtTbhdxExtraBass", std::make_tuple(GstElemPropType::kDirect, "soundx-tshd2-horiz-frt-tbhdx-extbass", Types::kDouble, nullptr)},
        {"Tshd2HorizCtrTbhdxEnable", std::make_tuple(GstElemPropType::kDirect, "soundx-tshd2-horiz-ctr-tbhdx-en", Types::kBool, nullptr)},
        {"Tshd2HorizCtrTbhdxSpkSz", std::make_tuple(GstElemPropType::kDirect, "soundx-tshd2-horiz-ctr-tbhdx-spk-sz", Types::kInt32, nullptr)},
        {"Tshd2HorizCtrTbhdxDynms", std::make_tuple(GstElemPropType::kDirect, "soundx-tshd2-horiz-ctr-tbhdx-dynms", Types::kDouble, nullptr)},
        {"Tshd2HorizCtrTbhdxHpEnable", std::make_tuple(GstElemPropType::kDirect, "soundx-tshd2-horiz-ctr-tbhdx-hp-en", Types::kBool, nullptr)},
        {"Tshd2HorizCtrTbhdxHpOrder", std::make_tuple(GstElemPropType::kDirect, "soundx-tshd2-horiz-ctr-tbhdx-hp-order", Types::kInt32, nullptr)},
        {"Tshd2HorizCtrTbhdxBass", std::make_tuple(GstElemPropType::kDirect, "soundx-tshd2-horiz-ctr-tbhdx-bass-lvl", Types::kDouble, nullptr)},
        {"Tshd2HorizCtrTbhdxExtraBass", std::make_tuple(GstElemPropType::kDirect, "soundx-tshd2-horiz-ctr-tbhdx-extbass", Types::kDouble, nullptr)},
        {"Tshd2HorizSurTbhdxEnable", std::make_tuple(GstElemPropType::kDirect, "soundx-tshd2-horiz-sur-tbhdx-en", Types::kBool, nullptr)},
        {"Tshd2HorizSurTbhdxProcMode", std::make_tuple(GstElemPropType::kDirect, "soundx-tshd2-horiz-sur-tbhdx-proc-mode", Types::kInt32, nullptr)},
        {"Tshd2HorizSurTbhdxSpkSz", std::make_tuple(GstElemPropType::kDirect, "soundx-tshd2-horiz-sur-tbhdx-spk-sz", Types::kInt32, nullptr)},
        {"Tshd2HorizSurTbhdxDynms", std::make_tuple(GstElemPropType::kDirect, "soundx-tshd2-horiz-sur-tbhdx-dynms", Types::kDouble, nullptr)},
        {"Tshd2HorizSurTbhdxHpEnable", std::make_tuple(GstElemPropType::kDirect, "soundx-tshd2-horiz-sur-tbhdx-hp-en", Types::kBool, nullptr)},
        {"Tshd2HorizSurTbhdxHpOrder", std::make_tuple(GstElemPropType::kDirect, "soundx-tshd2-horiz-sur-tbhdx-hp-order", Types::kInt32, nullptr)},
        {"Tshd2HorizSurTbhdxBass", std::make_tuple(GstElemPropType::kDirect, "soundx-tshd2-horiz-sur-tbhdx-bass-lvl", Types::kDouble, nullptr)},
        {"Tshd2HorizSurTbhdxExtraBass", std::make_tuple(GstElemPropType::kDirect, "soundx-tshd2-horiz-sur-tbhdx-extbass", Types::kDouble, nullptr)},
        {"Tshd2HorixLfeTbhdxEnable", std::make_tuple(GstElemPropType::kDirect, "soundx-tshd2-horiz-lfe-tbhdx-en", Types::kBool, nullptr)},
        {"Tshd2HorizLfeTbhdxSpkSz", std::make_tuple(GstElemPropType::kDirect, "soundx-tshd2-horiz-lfe-tbhdx-spk-sz", Types::kInt32, nullptr)},
        {"Tshd2HorizLfeTbhdxBass", std::make_tuple(GstElemPropType::kDirect, "soundx-tshd2-horiz-lfe-tbhdx-bass-lvl", Types::kDouble, nullptr)},
        {"Tshd2HorizLfeTbhdxGain", std::make_tuple(GstElemPropType::kDirect, "soundx-tshd2-horiz-lfegain-level", Types::kDouble, nullptr)},
        {"Tshd2HorizFrtDefEnable", std::make_tuple(GstElemPropType::kDirect, "soundx-tshd2-horiz-frt-def-en", Types::kBool, nullptr)},
        {"Tshd2HorizFrtDef", std::make_tuple(GstElemPropType::kDirect, "soundx-tshd2-horiz-frt-def-level", Types::kDouble, nullptr)},
        {"Tshd2HorizSurDefEnable", std::make_tuple(GstElemPropType::kDirect, "soundx-tshd2-horiz-sur-def-en", Types::kBool, nullptr)},
        {"Tshd2HorizSurDef", std::make_tuple(GstElemPropType::kDirect, "soundx-tshd2-horiz-sur-def-level", Types::kDouble, nullptr)},
        {"Tshd2HorizCtrDefEnable", std::make_tuple(GstElemPropType::kDirect, "soundx-tshd2-horiz-ctr-def-en", Types::kBool, nullptr)},
        {"Tshd2HorizCtrDef", std::make_tuple(GstElemPropType::kDirect, "soundx-tshd2-horiz-ctr-def-level", Types::kDouble, nullptr)},
        {"Tshd2HorizDialogCEnable", std::make_tuple(GstElemPropType::kDirect, "soundx-tshd2-horiz-diagc-en", Types::kBool, nullptr)},
        {"Tshd2HorizDialogC", std::make_tuple(GstElemPropType::kDirect, "soundx-tshd2-horiz-diagc-level", Types::kDouble, nullptr)},
        {"Tshd2HorizFrtShpfEnable", std::make_tuple(GstElemPropType::kDirect, "soundx-tshd2-horiz-frt-shpf-en", Types::kBool, nullptr)},
        {"Tshd2HorizFrtShpfFreq", std::make_tuple(GstElemPropType::kDirect, "soundx-tshd2-horiz-frt-shpf-freq", Types::kInt32, nullptr)},
        {"Tshd2HorizSurShpfEnable", std::make_tuple(GstElemPropType::kDirect, "soundx-tshd2-horiz-sur-shpf-en", Types::kBool, nullptr)},
        {"Tshd2HorizSurShpfFreq", std::make_tuple(GstElemPropType::kDirect, "soundx-tshd2-horiz-sur-shpf-freq", Types::kInt32, nullptr)},
        {"AppFrtSpacehpfFreq", std::make_tuple(GstElemPropType::kDirect, "soundx-app-frt-spacehpf-freq", Types::kDouble, nullptr)},
        {"AppSurSpacehpfFreq", std::make_tuple(GstElemPropType::kDirect, "soundx-app-sur-spacehpf-freq", Types::kDouble, nullptr)},
        {"AppFrtTbhdxSpkSize", std::make_tuple(GstElemPropType::kDirect, "soundx-app-frt-tbhdx-spksize", Types::kInt32, nullptr)},
        {"AppFrtTbhdxGain", std::make_tuple(GstElemPropType::kDirect, "soundx-app-frt-tbhdx-tgain", Types::kDouble, nullptr)},
        {"AppFrtTbhdxHpr", std::make_tuple(GstElemPropType::kDirect, "soundx-app-frt-tbhdx-hpr", Types::kDouble, nullptr)},
        {"AppFrtTbhdxExtBass", std::make_tuple(GstElemPropType::kDirect, "soundx-app-frt-tbhdx-extbass", Types::kDouble, nullptr)},
        {"AppCtrTbhdxSpkSize", std::make_tuple(GstElemPropType::kDirect, "soundx-app-ctr-tbhdx-spksize", Types::kInt32, nullptr)},
        {"AppCtrTbhdxGain", std::make_tuple(GstElemPropType::kDirect, "soundx-app-ctr-tbhdx-tgain", Types::kDouble, nullptr)},
        {"AppCtrTbhdxHpr", std::make_tuple(GstElemPropType::kDirect, "soundx-app-ctr-tbhdx-hpr", Types::kDouble, nullptr)},
        {"AppCtrTbhdxExtBass", std::make_tuple(GstElemPropType::kDirect, "soundx-app-ctr-tbhdx-extbass", Types::kDouble, nullptr)},
        {"AppSurTbhdxSpkSize", std::make_tuple(GstElemPropType::kDirect, "soundx-app-sur-tbhdx-spksize", Types::kInt32, nullptr)},
        {"AppSurTbhdxGain", std::make_tuple(GstElemPropType::kDirect, "soundx-app-sur-tbhdx-tgain", Types::kDouble, nullptr)},
        {"AppSurTbhdxHpr", std::make_tuple(GstElemPropType::kDirect, "soundx-app-sur-tbhdx-hpr", Types::kDouble, nullptr)},
        {"AppSurTbhdxExtBass", std::make_tuple(GstElemPropType::kDirect, "soundx-app-sur-tbhdx-extbass", Types::kDouble, nullptr)},
        {"AppSubTbhdxSpkSize", std::make_tuple(GstElemPropType::kDirect, "soundx-app-sub-tbhdx-spksize", Types::kInt32, nullptr)},
        {"AppSubTbhdxGain", std::make_tuple(GstElemPropType::kDirect, "soundx-app-sub-tbhdx-tgain", Types::kDouble, nullptr)},
        {"AppSubTbhdxHpr", std::make_tuple(GstElemPropType::kDirect, "soundx-app-sub-tbhdx-hpr", Types::kDouble, nullptr)},
        {"AppSubTbhdxExtBass", std::make_tuple(GstElemPropType::kDirect, "soundx-app-sub-tbhdx-extbass", Types::kDouble, nullptr)}};

    PPTable mcdyn_prop_match = {
        {"MbhlIoMode", std::make_tuple(GstElemPropType::kDirect, "mcd-mbhl-io-mode", Types::kInt32, nullptr)},
        {"MbhlMVolEnable", std::make_tuple(GstElemPropType::kDirect, "mcd-mbhl-mvol-en", Types::kInt32, nullptr)},
        {"MbhlMVol", std::make_tuple(GstElemPropType::kDirect, "mcd-mbhl-mvol-step", Types::kInt32, nullptr)},
        {"MbhlMVolMute", std::make_tuple(GstElemPropType::kDirect, "mcd-mbhl-mvol-mute", Types::kInt32, nullptr)},
        {"MbhlEnable", std::make_tuple(GstElemPropType::kDirect, "mcd-mbhl-en", Types::kInt32, nullptr)},
        {"MbhlOutputGain", std::make_tuple(GstElemPropType::kDirect, "mcd-mbhl-output-gain", Types::kDouble, nullptr)},
        {"MbhlAntiClip", std::make_tuple(GstElemPropType::kDirect, "mcd-mbhl-anticlip-lvl", Types::kDouble, nullptr)},
        {"MbhlCompAttacks", std::make_tuple(GstElemPropType::kDirect, "mcd-mbhl-comp-attacks", Types::kInt32, nullptr)},
        {"MbhlLowCompDecay", std::make_tuple(GstElemPropType::kDirect, "mcd-mbhl-lowcomp-decay", Types::kInt32, nullptr)},
        {"MbhlMidCompDecay", std::make_tuple(GstElemPropType::kDirect, "mcd-mbhl-midcomp-decay", Types::kInt32, nullptr)},
        {"MbhlHighCompDecay", std::make_tuple(GstElemPropType::kDirect, "mcd-mbhl-highcomp-decay", Types::kInt32, nullptr)},
        {"MbhlCompRatio", std::make_tuple(GstElemPropType::kDirect, "mcd-mbhl-comp-ratio", Types::kDouble, nullptr)},
        {"MbhlMode", std::make_tuple(GstElemPropType::kDirect, "mcd-mbhl-mode", Types::kInt32, nullptr)},
        {"MbhlBoost", std::make_tuple(GstElemPropType::kDirect, "mcd-mbhl-boost", Types::kDouble, nullptr)},
        {"MbhlLowCross", std::make_tuple(GstElemPropType::kDirect, "mcd-mbhl-lowcross", Types::kInt32, nullptr)},
        {"MbhlMidCross", std::make_tuple(GstElemPropType::kDirect, "mcd-mbhl-midcross", Types::kInt32, nullptr)},
        {"MbhlLowCompThresh", std::make_tuple(GstElemPropType::kDirect, "mcd-mbhl-lowcompthresh", Types::kDouble, nullptr)},
        {"MbhlMidCompThresh", std::make_tuple(GstElemPropType::kDirect, "mcd-mbhl-midcompthresh", Types::kDouble, nullptr)},
        {"MbhlHighCompThresh", std::make_tuple(GstElemPropType::kDirect, "mcd-mbhl-highcompthresh", Types::kDouble, nullptr)},
        {"MbhlCompMakeUpGain", std::make_tuple(GstElemPropType::kDirect, "mcd-mbhl-compmakeupgain", Types::kDouble, nullptr)},
        {"SoundXAppFrtMcdynLowCross", std::make_tuple(GstElemPropType::kDirect, "soundx-app-frt-mcdyn-lowcross", Types::kDouble, nullptr)},
        {"SoundXAppFrtMcdynMidCross", std::make_tuple(GstElemPropType::kDirect, "soundx-app-frt-mcdyn-midcross", Types::kDouble, nullptr)}
    };
#ifdef BASEMACHINE_qcs40x
    PPTable melod_prop_match = {
        {"BassManagementEnable", std::make_tuple(GstElemPropType::kPointer, "enable", Types::kVoidptr, std::bind(&MediaPlayerPrivate::BMHelper, this, "enable", _1))},
        {"BassManagementSubWooferOut", std::make_tuple(GstElemPropType::kPointer, "sub_woofer_out", Types::kVoidptr, std::bind(&MediaPlayerPrivate::BMHelper, this, "sub_woofer_out", _1))},
        {"BassManagementSurroundSpeakerLeft", std::make_tuple(GstElemPropType::kPointer, "surr_spk_left", Types::kVoidptr, std::bind(&MediaPlayerPrivate::BMHelper, this, "surr_spk_left", _1))},
        {"BassManagementSurroundSpeakerRight ", std::make_tuple(GstElemPropType::kPointer, "surr_spk_right", Types::kVoidptr, std::bind(&MediaPlayerPrivate::BMHelper, this, "surr_spk_right", _1))},
        {"BassManagementSpkSurrHeightLeft", std::make_tuple(GstElemPropType::kPointer, "surr_h_left", Types::kVoidptr, std::bind(&MediaPlayerPrivate::BMHelper, this, "surr_h_left", _1))},
        {"BassManagementSpkSurrHeightRight", std::make_tuple(GstElemPropType::kPointer, "surr_h_right", Types::kVoidptr, std::bind(&MediaPlayerPrivate::BMHelper, this, "surr_h_right", _1))},
        {"BassManagementCornerFreqBass", std::make_tuple(GstElemPropType::kPointer, "freq", Types::kVoidptr, std::bind(&MediaPlayerPrivate::BMHelper, this, "freq", _1))},

        {"DRCEnable", std::make_tuple(GstElemPropType::kPointer, "enable", Types::kVoidptr, std::bind(&MediaPlayerPrivate::DRCHelper, this, "enable", _1))},
        {"DRCMode", std::make_tuple(GstElemPropType::kPointer, "mode", Types::kVoidptr, std::bind(&MediaPlayerPrivate::DRCHelper, this, "mode", _1))},
        {"DRCRefLevel", std::make_tuple(GstElemPropType::kPointer, "ref_level", Types::kVoidptr, std::bind(&MediaPlayerPrivate::DRCHelper, this, "ref_level", _1))},

        {"MatrixEnable", std::make_tuple(GstElemPropType::kPointer, "enable", Types::kVoidptr, std::bind(&MediaPlayerPrivate::MatrixHelper, this, "enable", _1))},
        {"MatrixMode", std::make_tuple(GstElemPropType::kPointer, "mode", Types::kVoidptr, std::bind(&MediaPlayerPrivate::MatrixHelper, this, "mode", _1))},

        {"AroundProEnable", std::make_tuple(GstElemPropType::kPointer, "enable", Types::kVoidptr, std::bind(&MediaPlayerPrivate::AroundHelper, this, "enable", _1))},
        {"AroundProEffect", std::make_tuple(GstElemPropType::kPointer, "effect", Types::kVoidptr, std::bind(&MediaPlayerPrivate::AroundHelper, this, "effect", _1))},
        {"AroundProMode", std::make_tuple(GstElemPropType::kPointer, "mode", Types::kVoidptr, std::bind(&MediaPlayerPrivate::AroundHelper, this, "mode", _1))}

    };
#endif
    pp_prop_match = {
#ifdef BASEMACHINE_qcs40x
        {std::string(adk::mediaplayerlib::kMelodBin), melod_prop_match},
#endif
        {std::string(adk::mediaplayerlib::kDap), dap_prop_match},
        {std::string(adk::mediaplayerlib::kSoundx), soundx_prop_match},
        {std::string(adk::mediaplayerlib::kMcdynamics), mcdyn_prop_match}
    };
}

void MediaPlayerPrivate::InitDelayTables() {
    DecoderDelayTable ddp_delay_table = {
        {{48000, 8, ""}, 20}
    };
    DecoderDelayTable dsd_delay_table = {
        {{48000, 8, ""}, 60}
    };
    DecoderDelayTable mat_delay_table = {
        {{48000, 8, ""}, 10}
    };
    DecoderDelayTable thd_delay_table = {
        {{126000, 12, ""}, 20}
    };
    DecoderDelayTable dtsx_delay_table = {
        {{48000, 8, ""}, 40}
    };
    DecoderDelayTable aac_delay_table = {
        {{48000, 2, ""}, 5},
        {{44100, 2, ""}, 5},
        {{24000, 2, ""}, 5}
    };
    DecoderDelayTable mp3_delay_table = {
        {{48000, 2, ""}, 5},
        {{44100, 2, ""}, 5}
    };
    decode_bin_delay_map = {
        {std::string(adk::mediaplayerlib::kDsd), dsd_delay_table},
        {std::string(adk::mediaplayerlib::kDdp), ddp_delay_table},
        {std::string(adk::mediaplayerlib::kMat), mat_delay_table},
        {std::string(adk::mediaplayerlib::kTrueHd), thd_delay_table},
        {std::string(adk::mediaplayerlib::kDts), dtsx_delay_table},
        {std::string(adk::mediaplayerlib::kAac), aac_delay_table},
        {std::string(adk::mediaplayerlib::kMp3), mp3_delay_table}
    };
#ifdef BASEMACHINE_qcs40x
    PPDelayTable melod_delay_table = {
        {{48000, 8}, 6}
    };
#endif
    PPDelayTable dap_delay_table = {
        {{48000, 8}, 6}
    };
    PPDelayTable soundx_delay_table = {
        {{48000, 8}, 6}
    };
    PPDelayTable mcdynamics_delay_table = {
        {{48000, 8}, 6}
    };
    PPDelayTable oar_delay_table = {
        {{48000, 8}, 6}
    };
    pp_bin_delay_map = {
#ifdef BASEMACHINE_qcs40x
        {std::string(adk::mediaplayerlib::kMelodBin), melod_delay_table},
#endif
        {std::string(adk::mediaplayerlib::kDap), dap_delay_table},
        {std::string(adk::mediaplayerlib::kOar), oar_delay_table},
        {std::string(adk::mediaplayerlib::kSoundx), soundx_delay_table},
        {std::string(adk::mediaplayerlib::kMcdynamics), mcdynamics_delay_table}
    };
}
}  // namespace adk
